<div class="dn-single-post dn-basic-post-news col-xs-12">

    <?php dn_enqueue_style('loop-1-column') ?>

    <?php if ( has_post_thumbnail() ) { ?>
        <div class="special-link image-container"><a href="<?php the_permalink() ?>">
            <?php echo dn_get_background_image( get_post_thumbnail_id() ); ?>
        </a></div>
    <?php } ?>
    <div class="post-content">
        <h3 class="loop-title"><a class="special-link" href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
        <div class="deets">
            <span class="author">By <?php the_author(); ?></span> | <span class="date"><?php echo get_the_date("d M Y"); ?></span>
        </div>
        <?php the_excerpt(); ?>
        <a href="<?php the_permalink(); ?>" class="dn-button">Read More</a>
    </div>
</div>