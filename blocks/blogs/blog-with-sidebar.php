<?php
/* 
    Blog with Sidebar on the left
*/
get_header();

// Enqueue the required style & script
dn_enqueue_style('blog-with-sidebar');
wp_enqueue_script('js-infinite-scroll');

// Check whether the page has next page
ob_start();
next_posts_link();
$next_posts_link = ob_get_clean();

// Only print if has next link
$data_infinite = $infinite_status;
if($next_posts_link != ''){
	$data_infinite = 'data-infinite-scroll=\'{
		"path": ".next_posts_link a",
		"append": ".dn-single-post",
		"history": false,
		"status": ".page-load-status"
	}\'';

	$infinite_status = '
				<div class="page-load-status" style="display:none">
					<p class="infinite-scroll-request">Loading...</p>
					<p class="infinite-scroll-last">End of content</p>
					<p class="infinite-scroll-error">No more pages to load</p>
				</div>
	';
}

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php get_template_part('blocks/blogs/part-header') ?>

			<section class="results">
				<div class="container">
					<div class="row">
						<div class="the-content col-md-9 pull-right">
                            <?php if ( have_posts() ) : ?>

                                <div class="row infinite-container" <?php echo $data_infinite ?>>
                                    <?php while ( have_posts() ) : the_post(); ?>

                                        <?php 
                                        
                                            //get_template_part('blocks/blogs/loop-1-column');
                                            get_template_part('blocks/blogs/loop-3-column');
                                        
                                        ?>

                                    <?php endwhile; ?>
                                </div>

                            <?php else : ?>
                                <div class="col-xs-12">
                                    <h2>No post found!</h2>
                                </div>
                            <?php endif; ?>
                            
                            <?php // infinite loading bar ?>
                            <div class="infinite-status-container">
                                <?php echo $infinite_status ?>
                            </div>
                            
                        </div>
                        
                        <div class="the-sidebar hidden-sm hidden-xs col-md-3">
                            <?php get_template_part('blocks/blogs/part-sidebar') ?>
                        </div>
					</div>
				</div>

				<?php // this is for infinite scroll ?>
                <div class="next_posts_link" style="display:none"><?php next_posts_link(); ?></div>
                
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
