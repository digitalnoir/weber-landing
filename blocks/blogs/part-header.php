<?php if( is_archive() ){
    ?>
    <header class="archive-header entry-header">
        <div class="container">
            <?php
                the_archive_title( '<h1 class="archive-title">', '</h1>' );
                the_archive_description( '<div class="archive-description">', '</div>' );
            ?>
        </div>
    </header><!-- .archive-header entry-header -->
    <?php
}else{
        // Get page ID that set as blog page
        // view /wp-admin/options-reading.php
        $page_for_posts = get_option( 'page_for_posts' );
    ?>
    <header class="archive-header entry-header">
        <div class="container">
            <h1 class="archive-title"><?php echo get_the_title( $page_for_posts ) ?></h1>
        </div>
    </header><!-- .archive-header entry-header -->
    <?php
}