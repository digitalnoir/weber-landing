<div class="dn-hover-cards has-padding">
    <?php dn_enqueue_style('feature-tiles'); ?>
    <div class="container intro-content">
        <div class="row">
            <div class="col-xs-12">
                <h2><?php the_sub_field("title"); ?></h2>
            </div>
        </div>
    </div>

    <div class="container card-container">
        <div class="row">
        <?php
            if( have_rows('cards') ):
                while ( have_rows('cards') ) : the_row();
                ?>
                    <div class="col-lg-4 col-sm-6 col-xs-12 single-card-container">
                        <div class="dn-hover-card">

                            <?php the_sub_field("image") ?>

                            <div class="content">
                                <div class="content-inner">
                                    <div class="dn-card-small">

                                        <h3><?php the_sub_field("title"); ?></h3>
                                        <?php echo wpautop( get_sub_field("text") ); ?>
                                        <?php render_link_helper('link', 'dn-button feature-button'); ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                endwhile;
            endif;
            ?>
        </div>
    </div>
</div>