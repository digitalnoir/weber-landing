<div class="dn-rectangle-hover-blocks">

    <?php
        dn_enqueue_style('rectangle-hover-blocks');
    ?>

    <div class="container-flow">
        <div class="row" style="overflow:hidden">
            <?php 
                $col_width = 12 / count( get_sub_field('item'));
                if( have_rows('item')):
                    $ctr = 1;
                    while(have_rows('item')) : the_row();
                ?>
                    <?php $background_image = get_sub_field('image'); ?>
                    <div class="col-sm-<?php echo  $col_width ?> dn-steps-item  " style="background-image: url(<?php echo  $background_image['url'] ?>)">
                            
                        <?php 
                            $link = get_sub_field('link');
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>

                        <?php if($thumbnail = get_sub_field('thumbnail')): ?>
                            <a href="<?php echo  esc_url($link_url) ?> " target="<?php echo esc_attr($link_target) ?>">
                                <img src="<?php echo  $thumbnail['url'] ?>" alt="thumb" />
                            </a>
                        <?php endif; ?>

                        <a href="<?php echo  esc_url($link_url) ?> " target="<?php echo esc_attr($link_target) ?>"><?php echo  esc_html($link_title) ?></a>

                    </div>
                <?php
                    endwhile;
                endif;
                ?>
        </div>
    </div>
</div>