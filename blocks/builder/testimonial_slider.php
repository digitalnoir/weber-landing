<div class="dn-basic-slider">
    
    <?php
        dn_enqueue_style('slick');
        dn_enqueue_style('testimonial-slider');
        wp_enqueue_script('js-testimonial-slider');
    ?>

    <div class="container intro-content">
        <div class="row">
            <h2><?php the_sub_field("title"); ?></h2>
        </div>
    </div>

    <div class="container slider-container">
        <div class="row">
            <div class="slick-slider-container testimonial-slider">
                <?php
                    if( have_rows('slider_contents') ):
                        while ( have_rows('slider_contents') ) : the_row();
                            


?><div class="dn-testimonial">
    <div class="inner-testimonial">
        <?php 
                        
            $is_image_exist = get_sub_field("image");
        
            if( $is_image_exist != '' ) : 
            
                echo '<div class="img-container">' . $is_image_exist. '</div>';
            
            endif; ?>

        <blockquote>
            <?php echo wpautop( get_sub_field("testimonial") ); ?>
        </blockquote>
        <div class="info">
            <div class="author"><?php the_sub_field("author"); ?></div>
            <div class="author-desc"><?php the_sub_field("subtitle"); ?></div>
        </div>
    </div>
</div><?php

                            
                        endwhile;
                    endif;
                ?>
            </div>
        </div>
        <?php /*
        <div class="slider-overlay slider-overlay-left"></div>
        <div class="slider-overlay slider-overlay-right"></div>
        */ ?>
    </div>

</div>