<?php
    
    // Used to keep track of the arrangement of the dividers
    global $HorizDivierState;
    $HorizDivierState = 0;
    
?>

<div class="dn-dividers has-padding">
    <?php dn_enqueue_style('image-text-alternating-rows'); ?>
    <div class="container">
        <div class="row">
            <?php
                if( have_rows('divider_blocks') ):
                    while ( have_rows('divider_blocks') ) : the_row();
                        get_template_part('blocks/builder/reusable/content', 'horiz-divider');
                        $HorizDivierState++;
                    endwhile;
                endif;
            ?>
        </div>
    </div>
</div>