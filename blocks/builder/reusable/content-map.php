<?php

    $google_map = get_sub_field("maps");

?>

<div class="gmap" data-lat="<?php echo $google_map['lat']; ?>" data-lng="<?php echo $google_map['lng']; ?>" data-icon="<?php echo THEME_URL; ?>/img/marker.svg"></div>
    <?php /* <div id="bubble-info" class="bubble-info hidden">
        <?php the_field("address", "options"); ?>
        <p><a target="_blank" rel="nofollow" href="<?php the_field("google_maps_link", "options"); ?>">View on Google Maps</a></p>
    </div> */ ?>