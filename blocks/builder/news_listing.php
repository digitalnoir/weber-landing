<div class="dn-news-basic dn-block-flex">
    
    <?php dn_enqueue_style('news-listing') ?>

    <?php if ( get_sub_field("title") ) { ?>
        <div class="container intro-content">
            <div class="row">
                <div class="col-xs-12"><h2><?php the_sub_field("title"); ?></h2></div>
            </div>
        </div>
    <?php } ?>

    <div class="container post-container">
        <div class="row">
        <?php

        $posts_per_page = get_sub_field('number_of_post') != '' ? get_sub_field('number_of_post') : 6;
        $category       = get_sub_field('post_category');

        $the_query = new WP_Query( array(
            'post_type' => 'post',
            'posts_per_page' => $posts_per_page,
            'ignore_sticky_posts' => true
        ));

        if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                ?>
                <div class="col-lg-4 col-sm-6"><?php get_template_part('blocks/builder/reusable/content', 'post-block'); ?></div>
                <?php
            }
            wp_reset_postdata();
        }
        ?>
        </div>
    </div>
</div>