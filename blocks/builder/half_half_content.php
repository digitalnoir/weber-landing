<div class="dn-repeat-card">
    <?php dn_enqueue_style('half-half-content'); ?>
    <div class="container-fluid">
        <div class="row">
            <?php
                if( have_rows('repeat_cards_blocks') ):
                    while ( have_rows('repeat_cards_blocks') ) : the_row();
                        ?>
                            <div class="col-md-6 content">
                                <div class="dn-card">
                                    <?php

                                        $bg_image = get_sub_field("bg_image");

                                        if($bg_image != ''){
                                            echo $bg_image;
                                            echo '<div class="bg-shader"></div>';
                                        }
                                    ?>
                                    <h2><?php the_sub_field("title"); ?></h2>
                                    <?php echo wpautop( get_sub_field("editor") ); ?>
                                    <?php render_link_helper('link', 'dn-button feature-button'); ?>
                                </div>
                            </div>
                        <?php
                    endwhile;
                endif;
            ?>
        </div>
    </div>
</div>