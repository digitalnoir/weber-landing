<?php
    $form = get_sub_field("form");
?>

<div class="dn-grav-form">
    <?php dn_enqueue_style('gravity-form') ?>
    <?php if($form['title']): ?>
        <div class="container intro-content">
            <div class="row">
                <div class="col-xs-12">
                    <h2><?php echo $form['title'] ?></h2>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php gravity_form( $form['id'], false, false, false, null, false, 0, true ); ?>
            </div>
        </div>
    </div>
</div>