<div class="dn-team-cards dn-block-flex">
    
    <?php dn_enqueue_style('news-listing') ?>
    <?php dn_enqueue_style('team-cards') ?>

    <div class="container intro-content">
        <div class="row">
            <h2 class="cards-title col-xs-12"><?php the_sub_field("title"); ?></h2>
        </div>
    </div>

    <div class="container team-container post-container">
        <div class="row mobile-sliding-blocks">
        <?php
            if( have_rows('team_members') ):
                while ( have_rows('team_members') ) : the_row();
                    get_template_part('blocks/builder/reusable/content', 'team');
                endwhile;
            endif;
        ?>
        </div>
    </div>
</div>