
<div class="dn-full-image">
    <?php dn_enqueue_style('text-with-background-image'); ?>
    <?php the_sub_field("background_image"); ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="dn-card">
                    <h2><?php the_sub_field( "title" ); ?></h2>
                    <?php echo wpautop( get_sub_field( "editor" ) ); ?>
                    <?php render_link_helper('link', 'dn-button feature-button'); ?>
                </div>
            </div>
        </div>
    </div>
</div>