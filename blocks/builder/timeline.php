<div class="dn-content-timeline">
    
    <?php dn_enqueue_style('timeline') ?>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="top-content">
                    <h2><?php the_sub_field("title"); ?></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="timeline-inner">

                    <div class="timeline-head top"></div>

                    <?php
                        $timeline_contents = get_sub_field('timeline_contents');

                        if( have_rows('timeline_contents') ):
                            $timeline_counter = 0;
                            while ( have_rows('timeline_contents') ) : the_row(); $timeline_counter++;

                                $class = array();
                                $class[] = $timeline_counter % 2 == 0 ? 'reverse' : '';
                                $class[] = $timeline_counter == sizeof( $timeline_contents ) ? 'last' : '';

                            ?>
                                <div class="timeline-row row <?php echo implode(' ', $class) ?>">

                                    <div class="content col-sm-6">
                                        <h3><?php the_sub_field("date"); ?></h3>
                                        <h4><?php the_sub_field("title"); ?></h4>
                                        <div class="content-inner">
                                            <?php echo wpautop( get_sub_field("content") ); ?>
                                        </div>
                                    </div>
                                    <div class="image col-sm-6">
                                        <div class="image-holder">
                                            <?php echo get_sub_field("image"); ?>
                                        </div>
                                    </div>
                                    <div class="divider"></div>
                                </div>
                            <?php
                            
                            endwhile;
                        endif;
                    ?>

                    <div class="vertical-line"></div>

                    <div class="timeline-head bottom"></div>

                </div>
            </div>
        </div>
    </div>
</div>