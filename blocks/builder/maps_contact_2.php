<div class="map-contact long-desc">
    <?php dn_enqueue_style('maps-contact') ?>
    <div class="contact">
        <div class="inner">
            <div class="address"><?php the_sub_field('contact_details') ?></div>
        </div>
    </div>
    <div class="map">
        <?php get_template_part('blocks/builder/reusable/content', 'map'); ?>
    </div>
</div>