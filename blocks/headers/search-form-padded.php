<div class="shader-search"></div>
<div class="header-search-form padded">
    <?php dn_enqueue_style('search-form-padded') ?>
    <div class="form-container container">
        <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ) ?>">
            <input type="text" class="search-field" placeholder="Enter keyword..." value="<?php echo get_search_query() ?>" name="s" />
            <button type="submit" class="search-submit">
                <span class="hidden-xs">Search</span>
                <span class="visible-xs"><i class="icon-search"></i></span>
            </button>
        </form>
    </div>
</div>