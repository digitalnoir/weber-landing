<div class="filter-item-icon">
	<span class="icon-copy">
		<svg
			xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128">
			<title>Weber Q</title>
			<defs>
				<path d="M64,112c-26.5,0-48-21.5-48-48c0-26.5,21.5-48,48-48s48,21.5,48,48S90.5,112,64,112" stroke-width="3"></path>
			</defs>
			<text text-anchor="middle" fill="currentColor">
				<textPath startOffset="50%"
					xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#c-q-non-electric-243157">Weber Q
				</textPath>
			</text>
        </svg>
        
	</span>
	<span class="border-icon">
        <svg viewBox="0 0 129 129" data-id="circle">
            <circle cx="64.5" cy="64.5" r="64" style="fill: none; stroke: currentColor; stroke-miterlimit: 10" vector-effect="non-scaling-stroke" stroke-dasharray="3,2"></circle>
        </svg>
	</span>
	<span class="icon-type type-q-non-electric-ll" style="background-image: url(<?php echo THEME_URL ?>/img/type-q-non-electric-filter.png);"></span>
</div>
<span class="sub-heading">Weber Q</span>