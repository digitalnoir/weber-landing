

<div class="section-available-products">
    <?php
        $product_heading = get_field('include_product_heading', $sess['partner_id']);
        if(trim($product_heading) != '') :
    ?>
    <div class="container product-range-title">
        <h2><?php echo $product_heading ?></h2>
    </div>
    <?php endif; ?>

    <?php

        // fetch available product to purchase
        $available_products = get_field('include_product', $sess['partner_id']);

        $args = array(
            'post_type' => array('product', 'product_variation'),
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'post__in' => (array) $available_products,
        );
        $the_query = new WP_Query( $args );
 
        // The Loop
        if ( $the_query->have_posts() ) {
            echo '<div class="container"><div class="woocommerce">';
            echo '<ul class="products">';
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                
                do_action( 'woocommerce_shop_loop' );
                wc_get_template_part( 'content', 'product' );
                
            }
            echo '</ul>';
            echo '</div></div>';
        } 
        /* Restore original Post Data */
        wp_reset_postdata();
    ?>
</div>


<div class="section-how-it-works">
    <?php

        $how_it_works = get_field('landing_how_it_works', $sess['partner_id']);
        $_term_id = $how_it_works;
        if($_term_id == 35){
            include locate_template( 'woocommerce/how-weber-q.php' );
        }
        if($_term_id == 45){
            include locate_template( 'woocommerce/how-gas.php' );
        }
        if($_term_id == 64){
            include locate_template( 'woocommerce/how-charcoal.php' );
        }
        if($_term_id == 71){
            include locate_template( 'woocommerce/how-electric.php' );
        }

    ?>
</div>