<?php
	global $dn_option;
	$icon_url = wp_get_attachment_image_src( $section_icon_image,'full' ); 
	$is_image = get_field('video_background_image'); 
	
	$image_url = ($is_image != '' && !empty($is_image)) ? $is_image : get_bloginfo('template_url').'/img/video-background.jpg'  ;
	$series_tax_id = get_field('series_name',get_the_ID());
	
	$args = array(
		'post_type' => 'video',
		'post_status' => 'publish',
		'meta_key' => 'specific_video_for',
		'meta_query' => array(
			'key' => 'specific_video_for',
			'value' => get_the_ID(),
			'compare' => 'LIKE'
		),
			
	);
	$the_query = new WP_Query( $args );
	$total_post = $the_query->found_posts;
	
	// ONLY SHOW IF THERE A POST TYPE HERE
	if($total_post > 0) :

	
?>

<div class="slider-post-type-holder type-video">
	<div class="slider-post-type-bg" style="background-image:url(<?php echo $image_url ?>)"></div>
	<div class="slider-icon-image"><img src="<?php bloginfo('template_url')?>/img/videos-icon.png" alt="" /></div>
	<h2>Videos</h2>
	<h3 class="black">for
		<?php the_title() ?>
	</h3>
	<?php
	// The Loop
	if ( $the_query->have_posts() ) {
		echo '<div class="post-type-slider" data-posts_per_page="3"><ul class="slides">';
		while ( $the_query->have_posts() ) {$the_query->the_post();
			$feat_image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'clipping-mask' );
			?>
			
	<li>
		<a href="<?php echo get_permalink() ?>">
			<div class="inner-slide">
				<div class="play-button"><img src="<?php echo get_bloginfo('template_url')?>/img/play-video.png" alt="" /></div>
				<div class="slider-image"> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 367.25 264.719" >
					<image xlink:href="<?php echo $feat_image[0] ?>" width="<?php echo $feat_image[1] ?>" height="<?php echo $feat_image[2] ?>" clip-path="url(#image-clipping)"/>
					<image class="shadow-black shadow" xlink:href="<?php echo get_bloginfo('template_url')?>/img/masking-shadow.png" width="370" height="265" clip-path="url(#image-clipping)"/>
					<rect class="shadow-red shadow" x="0" y="0" width="370" height="264" fill="rgba(224,11,25,0.5)" clip-path="url(#image-clipping)" />
					</svg>
					 </div>
				<h4><?php echo get_the_title() ?></h4>
			</div>
		</a>
	</li>
	
	<?php
		}
		echo '</ul></div>';
	} else {
		echo 'No videos found';
	}
	wp_reset_postdata();
	?>
	<div class="see-all-link"><a class="button-red" href="<?php echo get_permalink($dn_option['videos_page']); ?>/?filter-for=1&product_id=<?php echo get_the_ID() ?>">See All</a></div>
</div>

<?php endif; ?>