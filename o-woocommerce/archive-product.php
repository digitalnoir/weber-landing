<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); 

$_image_banner = get_field('banner_image');
if(!empty($_image_banner) && is_array($_image_banner)){
	$banner_image = 'background-image:url('.$_image_banner['url'].');';
}
?>
<div class="header-banner small-title" style="<?php echo $banner_image ?>">
	<div class="container">
		<div class="header-title">
			<h1>Browse All Barbecues</h1>
		</div>
	</div>
</div>
<div class="container tick-head-trigger series-content">
	<div class="row">
		<div id="content-shop" class="content-area col-md-12">
			<div class="breadcrumbs-container clearfix">
				<div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#"> 
	<!-- Breadcrumb NavXT 5.2.2 --> 
	<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to Weber." href="<?php echo esc_url( home_url( '/' ) ); ?>" class="home">Home</a></span> &gt; <span><span>Browse All Barbecues</span></span>
	</div>
				<?php
					$is_active = $_SESSION['compare_item'];
					$active_classes = (sizeof($is_active) > 0 ? 'active' : '');
					$active_number	= (sizeof($is_active) > 0 ? sizeof($is_active) : '0');

				?>
				<div class="compare-item <?php echo $active_classes?>"> <a href="<?php echo $dn_option['compare_page_id'] ?>">
					<div class="inner-compare"> <span class="compare-number"><?php echo $active_number ?></span> <span class="compare-text">Compare</span> <span class="compare-icon"></span> </div>
					</a> </div>
			</div>
			
			<div class="archive-title row clearfix">
			
				<div class="col-md-5 col-sm-12 col-xs-12"><h2>Browse All Barbecues</h2></div>
				<div class="col-md-7 col-sm-12 col-xs-12 sorting">
				
				<span class="title">Sort By:</span>
				<?php
					$order_by			= $_GET['orderby'] != '' ? $_GET['orderby'] : 'date';
					$newest				= ($order_by == 'date' ? 'class="active"' : 'class="active"');
					$alpha				= ($order_by == 'name_asc' ? 'class="active"' : '');
					$price				= ($order_by == 'price' ? 'class="active"' : '');
					$price_desc		= ($order_by == 'price-desc' ? 'class="active"' : '');
				?>
				<ul>
					<li><a data-order="date" <?php echo $newest ?>>Newest</a></li>
					<li><a data-order="name_asc" <?php echo $alpha ?>>Alphabetical</a></li>
					<li><a data-order="price" <?php echo $price ?>>$$ Low to High</a></li>
					<li><a data-order="price-desc" <?php echo $price_desc ?>>$$ High to Low</a></li>
				</ul>
					
				</div>
				
				<div class="select-sort">
				<?php
					/**
					 * woocommerce_before_shop_loop hook
					 *
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */
					do_action( 'woocommerce_before_shop_loop' );
					?>
					</div>
			</div>
			
			<div class="woocommerce-content clearfix">
				<div id="shop-sidebar" class="shop-sidebar product-sidebar">
					<div class="inner-sidebar">
					<div class="shop-sidebar-title">Filter Products</div>
					
					<?php
					// IF COME FROM HOME
					$product_search_range = (isset($_GET['search-product']) && $_GET['search-product'] != 'accessories' ) ? $_GET['search-product'] : false ;
					$product_search_use	 = (isset($_GET['search-use']) && $_GET['search-use'] != '' ) ? $_GET['search-use'] : false ;
					
					if($product_search_range){
						$search_range_term = get_term_by('slug',$product_search_range,'range');
						$search_range_term_id = $search_range_term->term_id;
					}
					
					if($product_search_use){
						$search_use_term = get_term_by('slug',$product_search_use,'application');
						$search_use_term_id = $search_use_term->term_id;
					}
					if($product_search_range && $product_search_use){
						$trigger_submit = ' trigger-submit ';
					}
					
					?>
					
					<form id="filter-products" method="GET" class="forms-filter <?php echo $trigger_submit ?>">
							<?php
								$filter_range = get_query_var( 'filter_range',array());
								$filter_size = get_query_var( 'filter_size',array());
								$filter_application = get_query_var( 'filter_application',array());
								$is_reset_active = ((sizeof($filter_range[0]) < 1) && (sizeof($filter_size[0]) < 1) && (sizeof($filter_application[0]) < 1 && !$product_search_range && !$product_search_use ) ? 'checked="checked" ' : '');
							?>
						<div class="filter-fieldset filter-all">
						<ul>
						<li>
							<input type="checkbox" name="reset" id="all-filter" value="reset" <?php echo $is_reset_active ?>>
							<label for="all-filter"><span class="name">All</span> <span class="checked"></span> <span></span></label>
							</li>
							</ul>
						</div>
						
						<div class="filter-fieldset filter-range">
							<h3 class="filter-title"><span>Range</span></h3>
							<?php
							
							$term_array = $checklist_array = array();
							
							$term = get_terms('range',array('parent'=>0,'hide_empty'=>false));
							if(!empty($term)):
								echo '<ul>';
								foreach($term as $range_parent){
									$name = str_replace('Range','',$range_parent->name);
									$is_checked = (in_array_r($range_parent->term_id,$filter_range) || $search_range_term_id == $range_parent->term_id ? 'checked="checked"' : '');
									$cheked_array = (in_array_r($range_parent->term_id,$filter_range) ? $range_parent->term_id : '');
									echo '<li>';
									echo '<input type="checkbox" name="filter-range[]" id="'.$range_parent->slug.'" value="'.$range_parent->term_id.'" '.$is_checked.' />';
									echo '<label for="'.$range_parent->slug.'"><span class="name">'. $name.'</span> <span class="checked"></span></label>';
									echo '</li>';
									array_push($term_array,$range_parent->term_id);
									
									if(!empty($cheked_array)){
										array_push($checklist_array,$cheked_array);
									}
								}
								echo '</ul>';
							endif;
							?>
						</div>
						
						
						<div class="filter-fieldset filter-series">
							<h3 class="filter-title"><span>Series</span></h3>
							<?php
							
							
							if(sizeof($checklist_array) < 1):
								
									foreach($term_array as $parent_id){
											$term = get_terms('range',array('parent'=>$parent_id,'hide_empty'=>false));
											echo '<ul class="term-group-'.$parent_id.'">';
											foreach($term as $range_parent){
												$name = str_replace('Series','',$range_parent->name);
												$is_checked = (in_array_r($range_parent->term_id,$filter_range) ? 'checked="checked"' : '');
												echo '<li>';
												echo '<input type="checkbox" name="filter-series[]" id="'.$range_parent->slug.'" value="'.$range_parent->term_id.'" '.$is_checked.' />';
												echo '<label for="'.$range_parent->slug.'"><span class="name">'. $name.'</span> <span class="checked"></span></label>';
												echo '</li>';
											}
											echo '</ul>';
									}
								
								
								else:
								
								
									foreach($checklist_array as $parent_id){
											$term = get_terms('range',array('parent'=>$parent_id,'hide_empty'=>false));
											echo '<ul class="term-group-'.$parent_id.'">';
											foreach($term as $range_parent){
												$name = str_replace('Series','',$range_parent->name);
												$is_checked = (in_array_r($range_parent->term_id,$filter_range) ? 'checked="checked"' : '');
												echo '<li>';
												echo '<input type="checkbox" name="filter-range[]" id="'.$range_parent->slug.'" value="'.$range_parent->term_id.'" '.$is_checked.' />';
												echo '<label for="'.$range_parent->slug.'"><span class="name">'. $name.'</span> <span class="checked"></span></label>';
												echo '</li>';
											}
											echo '</ul>';
									}
								
								
							endif;
							?>
						</div>
						
						
						<div class="filter-fieldset filter-size">
							<h3 class="filter-title"><span>Size</span></h3>
							<?php
							$term_array = array();
							$term = get_terms('size',array('parent'=>0,'hide_empty'=>false));
							if(!empty($term)):
								echo '<ul>';
								foreach($term as $range_parent){
									$is_checked = (in_array_r($range_parent->term_id,$filter_size) ? 'checked="checked"' : '');
									echo '<li>';
									echo '<input type="checkbox" name="filter-size[]" id="'.$range_parent->slug.'" value="'.$range_parent->term_id.'" '.$is_checked.' />';
									echo '<label for="'.$range_parent->slug.'"><span class="name">'. $range_parent->name.'</span> <span class="checked"></span></label>';
									echo '</li>';
								}
								echo '</ul>';
							endif;
							?>
						</div>
						
						
						<div class="filter-fieldset last-filter-fieldset filter-application">
							<h3 class="filter-title"><span>Application</span></h3>
							<?php
							$term_array = array();
							$term = get_terms('application',array('parent'=>0,'hide_empty'=>false));
							if(!empty($term)):
								echo '<ul>';
								foreach($term as $range_parent){
									$is_checked = (in_array_r($range_parent->term_id,$filter_application) || $search_use_term_id == $range_parent->term_id ? 'checked="checked"' : '');
									echo '<li>';
									echo '<input type="checkbox" name="filter-application[]" id="'.$range_parent->slug.'" value="'.$range_parent->term_id.'" '.$is_checked.' />';
									echo '<label for="'.$range_parent->slug.'"><span class="name">'. $range_parent->name.'</span> <span class="checked"></span></label>';
									echo '</li>';
								}
								echo '</ul>';
							endif;
							?>
						</div>
						<?php
							// Keep query string vars intact
							foreach ( $_GET as $key => $val ) {
								if ( 'orderby' === $key || 'submit' === $key ) {
									continue;
								}
								if ( is_array( $val ) ) {
									foreach( $val as $innerVal ) {
										echo '<input type="hidden" name="' . esc_attr( $key ) . '[]" value="' . esc_attr( $innerVal ) . '" />';
									}
								} else {
									echo '<input type="hidden" name="' . esc_attr( $key ) . '" value="' . esc_attr( $val ) . '" />';
								}
							}
						?>
						<input type="hidden" name="filter-shop" value="true" />		
						<input type="hidden" id="pagination" name="pagination" value="1" />
						<input type="hidden" id="product_type" name="product_type" value="product" />						

						
					</form>
					<div class="bottom-form-brush"></div>
				
				</div>
				</div>
				
				<div id="ajax-holder" class="clearfix">
				<?php
					/**
					 * woocommerce_before_main_content hook
					 *
					 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
					 * @hooked woocommerce_breadcrumb - 20
					 */
					do_action( 'woocommerce_before_main_content' );
				?>
<div class="loading-infinite"><div class="circle"><div class="inner"></div></div><div class="circle">	<div class="inner"></div></div><div class="circle">	<div class="inner"></div></div><div class="circle">	<div class="inner"></div></div><div class="circle">	<div class="inner"></div></div>
</div>
				<?php do_action( 'woocommerce_archive_description' ); ?>
				<?php if ( have_posts() ) : ?>
				
				<?php woocommerce_product_loop_start(); // return <ul> ?>
				<?php // woocommerce_product_subcategories(); ?>
				<?php while ( have_posts() ) : the_post(); ?>
				<?php wc_get_template_part( 'content', 'product' ); ?>
				<?php endwhile; // end of the loop. ?>
				<?php woocommerce_product_loop_end(); // return </ul> ?>
				<?php
				/**
				 * woocommerce_after_shop_loop hook
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				//do_action( 'woocommerce_after_shop_loop' );
			?>
				<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
				<?php wc_get_template( 'loop/no-products-found.php' ); ?>
				<?php endif; ?>
				<?php
			/**
			 * woocommerce_after_main_content hook
			 *
			 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
			 */
			do_action( 'woocommerce_after_main_content' );
			?>
			<div id="loadmore-trigger" data-paged="2"><div class="loading-infinite"><div class="circle"><div class="inner"></div></div><div class="circle">	<div class="inner"></div></div><div class="circle">	<div class="inner"></div></div><div class="circle">	<div class="inner"></div></div><div class="circle">	<div class="inner"></div></div>
</div></div>
			</div>
			<?php
			/**
			 * woocommerce_sidebar hook
			 *
			 * @hooked woocommerce_get_sidebar - 10
			
			do_action( 'woocommerce_sidebar' ); */
			?>
			</div>
		</div>
	</div>
</div>
<?php get_footer( 'shop' ); ?>