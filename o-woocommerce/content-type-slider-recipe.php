<?php

	$icon_url = wp_get_attachment_image_src( $section_icon_image,'full' ); 
	$is_image = get_field('recipe_background_image'); 
	
	$image_url = ($is_image != '' && !empty($is_image)) ? $is_image : ''  ;
	$series_tax_id = get_field('series_name',get_the_ID());
	
	$args = array(
		'post_type' => 'recipe',
		'post_status' => 'publish',
		'meta_key' => 'specific_recipe_for',
		'meta_query' => array(
			'key' => 'specific_recipe_for',
			'value' => get_the_ID(),
			'compare' => 'LIKE'
		),
			
	);
	$the_query = new WP_Query( $args );
	$total_post = $the_query->found_posts;
	
	// ONLY SHOW IF THERE A POST TYPE HERE
	if($total_post > 0) :
	
	
?>

<div class="slider-post-type-holder type-video">
	<div class="slider-post-type-bg" style="background-image:url(<?php echo $image_url ?>)"></div>
	<div class="slider-icon-image"><img src="<?php bloginfo('template_url')?>/img/videos-icon.png" alt="" /></div>
	<h2>Recipes</h2>
	<h3 class="black">for <?php the_title() ?></h3>
	<?php
	// The Loop
	if ( $the_query->have_posts() ) {
		echo '<div class="post-type-slider" data-posts_per_page="4"><ul class="slides">';
		while ( $the_query->have_posts() ) {$the_query->the_post();
			$featured_image = get_the_post_thumbnail(get_the_ID(),'slider_thumb');
			
			echo '<li><a href="'.get_permalink().'"><div class="inner-slide">';
			echo '<div class="slider-image">'.$featured_image.'</div> <h4>' . get_the_title() . '</h4></div></a></li>';
			
		}
		echo '</ul></div>';
	} else {
		echo 'No videos found';
	}
	wp_reset_postdata();
	?>
	<div class="see-all-link"><a class="button-red" href="#">See All</a></div>
</div>

<?php endif; ?>
