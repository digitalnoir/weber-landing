<?php global $product, $dn_option; ?>

<div class="single-product-content">
	<div id="product-<?php the_ID(); ?>" class="row">
		<div class="product-main-detail container">
			<div class="row">
				
				<div class="single-product-content col-md-6 col-sm-12  col-xs-12 pull-right">
					<div class="summary entry-summary">
						<div class="badge-guarantee">
						<?php
						$product_image_type = get_field('product_image_type');
				 
				$color_options = get_field('color_choice');
				$product_badge = get_field('product_badge');
						
							if($product_badge != '' && $product_badge != 'no' && $product_badge != 'sold_out'):
							
							$is_new_size = $product_badge == 'award_choice' ? '1' : '';
							$is_print_size = '';
							if($product_badge == 'award_choice'){
								$is_print_size = 'width="85" height="86"';
							}
							if($product_badge == 'guarantee_new'){
								$is_print_size = 'width="98" height="85"';
							}
							?>
							<div class="the-badges"><img src="<?php bloginfo('template_url')?>/img/badge-<?php echo $product_badge.$is_new_size?>.png?v=2018" alt="" <?php echo $is_print_size?>  /></div>
							<?php endif; ?>

<?php
$show_specialist_guarantee = get_field('show_specialist_dealer_badge');
if($show_specialist_guarantee){
?>
<div class="the-badges"><a href="https://www.theweberspecialist.com.au/" target="_blank"><img src="<?php bloginfo('template_url')?>/img/badge-specialist-new.png" class="specialist-image" alt="" width="85" height="86" /></a></div>
<?php } ?>

<?php
$show_badge_guarantee = get_field('show_guarantee_badge');
if($show_badge_guarantee){
?>
<div class="the-badges"><img src="<?php bloginfo('template_url')?>/img/badge-guarantee.png" class="guarantee-image" alt="" width="85" height="85" /></div>
<?php } ?>
							
						</div>
						<div class="product-title">
							<h1 class="product_title entry-title">
							<?php if(get_field('hide_weber_title',get_the_ID()) != true) : ?> 
		<span class="weber-brand">Weber&reg;</span>
		<?php endif; ?>
								<?php the_title() ?>
							</h1>
						</div>
						<div class="clearfix"></div>
						<div class="price-color clearfix" <?php if($product->get_price_html() == '') { echo 'style="margin-top:15px; margin-bottom:0"'; } ?>>
							<div class="the-price">
<p class="price"><?php echo $product->get_price_html(); if( $product->get_price_html() != '' ) { ?><span class="rrp">rrp</span> <?php } ?>
								
								
								<?php if(get_field('regular_price_note') != '') : ?>
								<span class="second-note"><?php echo get_field('regular_price_note') ?></span>
								<?php endif; ?>
								
								<?php if(get_field('_sku') != '') : ?>
								<div class="sku-note">SKU: <?php echo get_field('_sku') ?></div>
								<?php endif; ?>
								</p>
								<?php if(get_field('secondary_price') == true) :
								
								 ?>
													<p class="price price-2"><?php echo woocommerce_price(get_field('secondary_rrp')) ?><span class="rrp">rrp</span> <span class="second-note"><?php echo get_field('secondary_price_note') ?></span>
													
													
													<?php if(get_field('secondary_sku') != '') : ?>
													<div class="sku-note">SKU: <?php echo get_field('secondary_sku') ?></div>
													<?php endif; ?>
													</p>
								<?php
							
								$additional_price = get_field('additional_price');
								if(is_array($additional_price) && sizeof($additional_price) > 0){

									foreach($additional_price as $price){
								
										if( $price['additional_price'] ){
											echo '<p class="price price-2">';
											echo woocommerce_price($price['additional_price']).'<span class="rrp">rrp</span> ';
										
													
											if( $price['additional_price_note'] ){
												echo '<span class="second-note">'.$price['additional_price_note'].'</span>';
											}
									
											if( $price['additional_sku'] ){
												echo '<div class="sku-note">SKU: '. $price['additional_sku'] .'</div>';
											}
											echo '</p>';
											
										}// end if pice is set
										
									}// end foreach
								}// end if is array
							
								?>					
													
													
								<?php endif; //secondary price ?>
							</div>
							<?php
							
							$gallery_colorss = get_field('gallery_color');
							// only show if gallery is more than 1
							if($product_image_type == 'color' && sizeof($gallery_colorss) > 1) : ?>
							<div class="color-choice">
								<?php
								
								$x = 0;
								$total_color = sizeof($gallery_colorss);
								$color_colors = $total_color > 1 ? 'colours' : 'colour';
								echo '<span class="availability-color">Available in '.$total_color.' '.$color_colors.'</span>';
								echo '<form id="change-color" method="post" data-security="'. wp_create_nonce( 'change-color' ) .'" data-product_id="'.get_the_ID().'" >';
								foreach($gallery_colorss as $color_option){
										$x++;
										$selected = ($x == 1) ? 'checked="checked"' : '';
										
										$color_number = str_replace('#','',$color_option['product_color']);
										
										echo '<input type="radio" name="color-option" id="color-'.$color_number.'" value="'.$x.'" '.$selected.' >';
										echo '<label for="color-'.$color_number.'"><span class="color-label" style="background-color:'.$color_option['product_color'].'">&nbsp;</span></label>';
								}
								echo '</form>';
							
							?>
							</div>
							<?php endif; ?>
						</div>

					</div>
				</div>
				<div class="single-product-images col-md-6 col-sm-12  col-xs-12">
					<div id="product-slider-container">
						<?php
				
				

				if($product_badge != '' && $product_badge == 'sold_out'){
					echo '<div class="sold-out-badge"><img src="'.get_bloginfo('template_url').'/img/badge-sold-out.png" alt="" /></div>';
				}
				
				//print_r($color_options_gallery);
				
				
				
				
				// if single image
				if($product_image_type == 'single'){
					$gallery_single_image = get_field('gallery_single');		
					echo '<img src="'.$gallery_single_image['url'].'" width="'.$gallery_single_image['width'].'" height="'.$gallery_single_image['height'].'" alt="'.$gallery_single_image['alt'].'" />';
				}//end if its single image
				
				
				
				
				// if slider
				if($product_image_type == 'slider'){
					$gallery_slider = get_field('gallery_slider');
					if(sizeof($gallery_slider) > 1){
						?>
						<div id="product-slider">
							<ul class="slides">
								<?php foreach( $gallery_slider as $image ): ?>
								<li> <a href="<?php echo $image['url']; ?>" class="zoom" data-rel="prettyPhoto[product-gallery]"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a> </li>
								<?php endforeach; ?>
							</ul>
						</div>
						<div id="product-carousel" class="flexslider">
							<ul class="slides">
								<?php foreach( $gallery_slider as $image ): ?>
								<li>
									<div class="inner-thumb"> <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" /></div>
								</li>
								<?php endforeach; ?>
							</ul>
						</div>
					<?php
					}//end if there more than 1 in slider
					else{
						echo '<img src="'.$gallery_slider[0]['url'].'" width="'.$gallery_slider[0]['width'].'" height="'.$gallery_slider[0]['height'].'" alt="'.$gallery_slider[0]['alt'].'" />';
					}// endi if else
				}//end if its for slider
				
				
				
				// if single image
				if($product_image_type == 'color'){
					$gallery_color = get_field('gallery_color');		
					/* $first_image = $gallery_color[0]['gallery_product_image'];
					echo '<img src="'.$first_image['url'].'" width="'.$first_image['width'].'" height="'.$first_image['height'].'" alt="'.$first_image['alt'].'" />'; */
					if(is_array($gallery_color)):
						$index_color = 0;
						foreach($gallery_color as $image){ $index_color++;
							$show_first = $index_color == 1 ? 'show' : '';
							$first_image = $image['gallery_product_image'];
							echo '<img src="'.$first_image['url'].'" width="'.$first_image['width'].'" height="'.$first_image['height'].'" alt="'.$first_image['alt'].'" class="product-image-color-option '. $show_first .'" data-color="color-'. str_replace('#','',$image['product_color']) .'" />';
						}
					endif;
				}//end if its single image
				
				
			
				?>
				<div class="loading-infinite"><div class="circle"><div class="inner"></div></div><div class="circle">	<div class="inner"></div></div><div class="circle">	<div class="inner"></div></div><div class="circle">	<div class="inner"></div></div><div class="circle">	<div class="inner"></div></div>
</div>

					</div>
					<div class="sharing-single-product"><div class="share-label col-md-5 col-sm-5"><img src="<?php bloginfo('template_url')?>/img/share-icon.png" alt="Share Recipe Icon" /></div>
					<div class="share-thing col-md-7 col-sm-7">
						<?php // weber_share_button(get_the_ID()) ?>
					</div></div>
				</div>
				
				<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="product-description">
							<?php the_content() ?>
						</div>
						<?php
							$video_text = get_field('video_text');
							$video_url = get_field('video_url');
							$is_table = $video_text != '' && $video_url != '' ? 'table-view' : '';
						?>
						<div class="product-summary <?php echo $is_table ?>">
							
								
									<?php //the_field('summary_product') ?>
									<?php
										// if video button
										if($video_text != '' && $video_url != ''){
											?>
											<div class="vid-button">
												<a href="<?php echo $video_url ?>" class="popup-youtube product-video-popup">
													<span class="bg"><?php weber_icon('button-red') ?></span>
													<span class="text"><?php echo $video_text ?></span>
													<span class="play"><?php weber_icon('play-button') ?></span>
												</a>
											</div>
											<?php
										} 
									?>
								
									
									<div class="compare-check single-product-compare text-center">
										<?php
									$check_compare = $_SESSION['compare_item'];
									if(is_array($check_compare)){
										if(in_array($product->id,$check_compare)){
											$extra_class = 'active';
										}
									}
								?>
										<a data-product_id="<?php echo $product->id ?>" data-security="<?php echo wp_create_nonce( 'compare-check' ); ?>" class="<?php echo $extra_class ?>"><span></span> Compare</a>
								</div>
							
						</div>
						<!-- product-summary -->
						
						<div class="devider"></div>
						<div class="product-find-retailer">
							<h3>Find a retailer for this product</h3>
							
							<?php
							$is_specialist_only = get_field('show_specialist_dealer_badge') != '' ? 'yes' : 'no';
							$get_terms = wp_get_post_terms(get_the_ID(),'range');
							
							$actual_term = array();
							if(sizeof($get_terms ) > 0 && is_array($get_terms )){
								$i = 0;
								foreach($get_terms as $term){
									$i++;
									
									// only for spirit, add condition
									if($term->term_id != 14){ 
									
										if($term->parent != 0){
											$termparent[$i] = get_term_by('id',$term->parent,'range');
											$actual_term = $termparent[$i]->term_id;
										}
									}else{
										$actual_term = 14;
									}
									
									
								}
							}
							?>
							
							<form id="find-weber-postcodes-2" action="<?php echo $dn_option['where_to_buy_page'] ?>">
								<div class="input-holder">
									<span class="input-bg"><?php weber_icon('input-bg') ?></span>
									<input type="text" name="autocomplete-location" id="autocomplete-location-2" placeholder="enter your postcode" value="" />
									<input type="hidden" name="autocomplete-coordinate" id="autocomplete-coordinate-2" />
									<input type="hidden" name="product_id" id="product_id" value="<?php echo get_the_ID() ?>" />
									<input type="hidden" name="specialist" id="specialist" value="<?php echo $is_specialist_only ?>" />
									<input type="hidden" name="range" id="range" value="<?php echo $actual_term ?>" />
									<input type="hidden" name="autocomplete-type" id="autocomplete-type-2" />
									<input type="hidden" name="autocomplete-type-name" id="autocomplete-type-name-2" />
									<input type="hidden" name="search_type" value="search_by_postcode" />
								</div>
								<div class="button-holder">
									<button class='svg-button' type='submit'>
										<span class='btn-red'><?php weber_icon('button-outline') ?></span>
										<span class='anchor'>Find</span>
									</button>
								</div>
							</form>
						</div>
				</div>
				
			
			</div>
		</div>
		<!-- product section1  -->




		<div class="product-features clearfix">
			<?php
				$featured_detail = get_field('featured_detail');
				if($featured_detail)	{
					$z = 0;
					$total_featured = sizeof($featured_detail);
					$col_width = 100 / $total_featured;
					
					$style = 'width:'.$col_width.'%;';
					
					foreach($featured_detail as $detail){
						$z++;
						$feature_bg = $detail['featured_bg_image'];
						if($feature_bg != ''){
							$style .= 'background-image:url('.$feature_bg.');';
						}
						?>
			<div class="product-feature-col" style="<?php echo $style ?>">
				<div class="feature"><span><?php echo $detail['featured_text'] ?></span></div>
			</div>
			<?php			
					}
				}
			?>
		</div>
		<!-- product-features -->
		
				
		
		
		
		
		
		
		
		
		
		
		
		<?php
		$show_hide_sccessories = get_field('show_hide_sccessories');
		if(!empty($show_hide_sccessories) && $show_hide_sccessories[0] == 'Yes') :
		
		$charcoal_page_id = $dn_option['range_charcoal'];
		$accessories_review_title = get_field('accessories_review_title',$charcoal_page_id);
		$accessories_review_desc = get_field('accessories_review_desc',$charcoal_page_id);
		$accessories_review_link = get_field('accessories_review_link',$charcoal_page_id);
		?>
		
		
		<div class="section-accessories-review">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="title"> <img src="<?php bloginfo('template_url')?>/img/assets/range-charcoal/weber-gourmet.png" alt="" width="95" height="76">
							<h2 class="accessories-review-title"><?php echo $accessories_review_title ?></h2>
							<?php echo $accessories_review_desc ?>
							<div class="acc-button"><a href="<?php echo $accessories_review_link ?>" class="button-red">See more Accessories</a></div>
						</div>
					</div>
					<div class="col-md-6 plate-image"> <img src="<?php bloginfo('template_url')?>/img/assets/range-charcoal/plates.jpg" alt="" width="849" height="491"> </div>
				</div>
			</div>
		</div>
		<div class="section-tick-banner">
			<div class="recipes-images">&nbsp;</div>
		</div>
		
		<?php endif; ?>
		
		
		
		
		
		
		
		
		
		
		
		
		
		

		
		<div class="product-specification">
			<?php
			$specification_image = get_field('specification_image');
			$specification_handbook = get_field('specification_handbook');
			$specification_full = get_field('specification_full');
		?>
			<div class="container">
				<div class="row">
					<div class="titles col-md-12">
						<h2>
						<?php if(get_field('hide_weber_title',get_the_ID()) != true) : ?> 
						<span>Weber&reg;</span>
						<?php endif; ?>
						<?php the_title() ?></h2>
						<h3>Full Specs</h3>
					</div>
					
					
					<div class="spec-detail col-md-6 col-sm-6 col-xs-12 pull-right">
					<?php
					$consruction_content = get_field('compare_construction');
					$features_content = get_field('compare_features');
					$color_content = get_field('compare_color');
					$compare_dimensions = get_field('compare_dimensions');
					
					$print_features .= $consruction_content != '' ? "<div class='spec-item'><h4>Construction</h4>\r\n".$consruction_content.'</div>' : '' ;
					$print_features .= $features_content != '' ? "<div class='spec-item'><h4>Features</h4>\r\n".$features_content.'</div>' : '' ;
					$print_features .= $color_content != '' ? "<div class='spec-item'><h4>Colour</h4>\r\n".$color_content.'</div>' : '' ;
					$print_features .= $compare_dimensions != '' ? "<div class='spec-item'><h4>Dimensions</h4>\r\n".$compare_dimensions.'</div>' : '' ;
					
					echo $print_features;
					
					?>
					</div>
					
					<div class="spec-image col-md-6 col-sm-6">
						<div class="spec-image">
							<?php 
						if(!empty($specification_image) && is_array($specification_image)):
							echo '<img src="'.$specification_image['url'].'" width="'.$specification_image['width'].'" height="'.$specification_image['height'].'" alt="'.$specification_image['alt'].'" />';
						else:
						
							// if single image
if($product_image_type == 'single'){
					$gallery_single_image = get_field('gallery_single');		
					echo '<img src="'.$gallery_single_image['url'].'" width="'.$gallery_single_image['width'].'" height="'.$gallery_single_image['height'].'" alt="'.$gallery_single_image['alt'].'" />';
}//end if its single image
				
				
				
				
				// if slider
if($product_image_type == 'slider'){
					$gallery_slider = get_field('gallery_slider');
					echo '<img src="'.$gallery_slider[0]['url'].'" width="'.$gallery_slider[0]['width'].'" height="'.$gallery_slider[0]['height'].'" alt="'.$gallery_slider[0]['alt'].'" />';
}//end if its for slider
				
				
				
				// if single image
if($product_image_type == 'color'){
					$gallery_color = get_field('gallery_color');		
					$first_image = $gallery_color[0]['gallery_product_image'];
					echo '<img src="'.$first_image['url'].'" width="'.$first_image['width'].'" height="'.$first_image['height'].'" alt="'.$first_image['alt'].'" />';
}//end if its single image
						
						// end if specification image empty
						endif;
						 ?>
						</div>
						<?php if($specification_handbook != ''){ ?>
						<div class="download-button"> <a href="<?php echo $specification_handbook ?>" target="_blank"><span>Download The Handbook</span></a> </div>
						<?php } ?>
					</div>
					
				</div>
			</div>
		</div>
		<!-- product-specificaion --> 
		<?php /*
		<div class="recipe-section">
			<div class="container">
				<?php wc_get_template_part( 'content', 'type-slider-recipe'); ?>
			</div>
		</div>
		*/
		?>
		
		<div class="video-section">
			<div class="container">
				<?php wc_get_template_part( 'content', 'type-slider-video'); ?>
				<div class="clearfix"></div>
			</div>
		</div>
		
		<div class="accessories-section">
			<div class="container">
				<?php wc_get_template_part( 'content', 'type-slider-accessories'); ?>
				<div class="clearfix"></div>
			</div>
		</div>
		
	</div>
	<!-- #product-<?php the_ID(); ?> --> 
</div>
