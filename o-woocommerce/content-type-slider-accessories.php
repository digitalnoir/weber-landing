<?php
	global $dn_option;
	$icon_url = wp_get_attachment_image_src( $section_icon_image,'full' ); 
	$is_image = get_field('accessories_background_image'); 
	$is_accessories = get_field('product_type'); 
	
	$image_url = ($is_image != '' && !empty($is_image)) ? $is_image : get_bloginfo('template_url').'/img/assets/range-charcoal/cooking-charcoal.png'  ;
	$series_tax_id = get_field('series_name',get_the_ID());
	
	
	if($is_accessories != 'accessories') {
		$args = array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'meta_key' => 'accessories_for',
			'meta_query' => array(
				'key' => 'accessories_for',
				'value' => get_the_ID(),
				'compare' => 'LIKE'
			),
				
		);
	}else{
		$terms = wp_get_post_terms( get_the_ID(), 'range', array('hide_empty'=> false) );
		$args = array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'post__not_in' => array(get_the_ID()),
			'meta_key' => 'product_type',
			'meta_query' => array(
				'key' => 'product_type',
				'value' => 'accessories',
				'compare' => '='
			),
			'tax_query' => array(
					array(
						'taxonomy' => 'range',
						'field' => 'id',
						'terms' => $terms[0]->term_id,
					)
			)
		);
	
	}
	
	
	$the_query = new WP_Query( $args );
	$total_post = $the_query->found_posts;
	
	// ONLY SHOW IF THERE A POST TYPE HERE
	if($total_post > 0) :
?>

<div class="slider-post-type-holder type-accessories">
	<div class="slider-post-type-bg" style="background-image:url(<?php echo $image_url ?>)"></div>
	<div class="slider-icon-image"><img src="<?php bloginfo('template_url')?>/img/accessories-icon.png" alt="" /></div>
	<?php if($is_accessories != 'accessories') { ?>
		<h2>Accessories</h2>
		<h3 class="cream">for <?php the_title() ?></h3>
	<?php }else{ ?>
		<h2 class="related-accessories">Related Accessories</h2>	
		<h3 class="cream"></h3>
	<?php }?>
	<?php
	// The Loop
	if ( $the_query->have_posts() ) {
		echo '<div class="post-type-slider" data-posts_per_page="4"><ul class="slides">';
		while ( $the_query->have_posts() ) {$the_query->the_post();
			
			$feat_image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'clipping-mask' );
			?>
			
			<li>
				<a href="<?php echo get_permalink()?>">
					<div class="inner-slide">
						<div class="slider-image"> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 367.25 264.719" >
							<image xlink:href="<?php bloginfo('template_url') ?>/img/mask-fallback.png" width="370" height="264" clip-path="url(#image-clipping)"/>
							<image xlink:href="<?php echo $feat_image[0] ?>" width="370" height="264" clip-path="url(#image-clipping)"/>
							</svg> </div>
						<h4><?php echo get_the_title() ?></h4>
					</div>
				</a>
			</li>
			
			<?php
		}
		echo '</ul></div>';
	} else {
		echo 'No accessories found';
	}
	wp_reset_postdata();
	?>
	<div class="see-all-link"><a class="button-red" href="<?php echo $dn_option['browse_all_accessories']; ?>/?filter-for=1&product_id=<?php echo get_the_ID() ?>">See All</a></div>
</div>

<?php endif; ?>