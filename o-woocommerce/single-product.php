<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' );

global $dn_option;

$_image_banner = get_field('banner_image');
if(!empty($_image_banner) && is_array($_image_banner)){
	$banner_image = 'background-image:url('.$_image_banner['url'].');';
}

$_post_term = wp_get_post_terms(get_the_ID(),'range',array("fields" => "all"));
$parent_term = get_term( $_post_term[0]->parent, 'range' );

$parent_term_name = $parent_term->name;
$product_term_name = $_post_term[0]->name;

// get link for parent range breadcrumb
if($parent_term->slug == 'weber-q-range'){
	$range_page = $dn_option['range_weber_q'];
}
if($parent_term->slug == 'charcoal-range'){
	$range_page = $dn_option['range_charcoal'];
}
if($parent_term->slug == 'premium-gas-range'){
	$range_page = $dn_option['range_premium_gas'];
}
if($parent_term->slug == 'electric-range'){
	$range_page = $dn_option['range_pulse'];
}

// get link for parent series breadcrumb
$get_series = get_posts(array(
	'post_type'=>'page',
	'posts_per_page' => 1,
	'meta_query' => array(
		array(
			'key' => 'series_name',
			'value' => $_post_term[0]->term_id,
		)
	)
));
$series_page = $get_series[0]->ID;



$is_accessories = get_field('product_type');

?>

<div class="header-banner" style="<?php echo $banner_image ?>">
	<div class="container">
		<div class="header-title">
			<?php if($is_accessories != 'accessories') : ?>
			<div class="range-title"><?php echo $product_term_name ?> <span><?php echo $parent_term_name ?></span></div>
			<?php else: ?>
			<div class="range-title">Accessories</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<div class="tick-head-trigger clearfix">
	<div id="content" class="content-area col-md-12">
		<div class="breadcrumbs-container container">
			<div class="row">
				<div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#"> 
				<?php if($is_accessories != 'accessories') : ?>
					<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to Weber." href="<?php echo esc_url( home_url( '/' ) ); ?>" class="home">Home</a></span> &gt;
					<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to All Barbecues" href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ) ?>">Barbecues</a></span> &gt;
					<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to <?php echo $parent_term_name ?>." href="<?php echo get_permalink($range_page) ?>"><?php echo str_replace('Range','',$parent_term_name) ?></a></span> &gt;
					<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to <?php echo $product_term_name ?>." href="<?php echo get_permalink($series_page) ?>"><?php echo str_replace('Series','',$product_term_name) ?></a></span> &gt;
					<span typeof="v:Breadcrumb"><span property="v:title"><a href="<?php the_permalink()?>"><?php the_title()?></a></span></span>
				<?php else: ?>
					<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to Weber." href="<?php echo esc_url( home_url( '/' ) ); ?>" class="home">Home</a></span> &gt;
					<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to All Accessories." href="<?php echo $dn_option['browse_all_accessories']; ?>">Accessories</a></span> &gt;
					<span typeof="v:Breadcrumb"><span property="v:title"><a href="<?php the_permalink()?>"><?php the_title()?></a></span></span>
				<?php endif; ?>
				</div>
				<?php
					$is_active = $_SESSION['compare_item'];
					$active_classes = (sizeof($is_active) > 0 ? 'active' : '');
					$active_number	= (sizeof($is_active) > 0 ? sizeof($is_active) : '0');

				?>
				<?php if($is_accessories != 'accessories') : ?>
				<div class="compare-item <?php echo $active_classes?>"> <a href="<?php echo $dn_option['compare_page_id']?>">
					<div class="inner-compare"> <span class="compare-number"><?php echo $active_number ?></span> <span class="compare-text">Compare</span> <span class="compare-icon"></span> </div>
					</a> </div>
				<?php endif; ?>
			</div>
		</div>
		<?php
				/**
				 * woocommerce_before_main_content hook
				 *
				 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
				 * @hooked woocommerce_breadcrumb - 20
				 */
				//do_action( 'woocommerce_before_main_content' );
			?>
		<?php while ( have_posts() ) : the_post(); ?>
		<?php wc_get_template_part( 'content', 'single-product' ); ?>
		<?php endwhile; // end of the loop. ?>
		<?php
				/**
				 * woocommerce_after_main_content hook
				 *
				 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
				 */
				//do_action( 'woocommerce_after_main_content' );
			?>
		<?php
				/**
				 * woocommerce_sidebar hook
				 *
				 * @hooked woocommerce_get_sidebar - 10
				 */
				do_action( 'woocommerce_sidebar' );
			?>
	</div>
</div>
<?php get_footer( 'shop' ); ?>