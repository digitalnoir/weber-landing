<?php global $product, $dn_option; ?>

<div class="single-product-content">
	<div id="product-<?php the_ID(); ?>" class="row">
		<div class="product-main-detail container">
			<div class="row">
				
				<div class="single-product-content col-md-6 col-sm-6 col-xs-12 pull-right">
					<div class="summary entry-summary">
						<div class="badge-guarantee">
						<?php
						$product_image_type = get_field('product_image_type');
				 
				$color_options = get_field('color_choice');
				$product_badge = get_field('product_badge');
						
							if($product_badge != '' && $product_badge != 'no' && $product_badge != 'sold_out'):
							?>
							<div class="the-badges"><img src="<?php bloginfo('template_url')?>/img/badge-<?php echo $product_badge?>.png?v=2018" alt="" /></div>
							<?php endif; ?>

<?php
$show_specialist_guarantee = get_field('show_specialist_dealer_badge');
if($show_specialist_guarantee){
?>
<div class="the-badges"><a href="https://www.theweberspecialist.com.au/" target="_blank"><img src="<?php bloginfo('template_url')?>/img/badge-specialist-new.png" class="specialist-image" alt="" width="85" height="86" /></a></div>
<?php } ?>

<?php
$show_badge_guarantee = get_field('show_guarantee_badge');
if($show_badge_guarantee){
?>
<div class="the-badges"><img src="<?php bloginfo('template_url')?>/img/badge-guarantee-new.png" class="guarantee-image" alt="" width="85" height="85" /></div>
<?php } ?>
							
						</div>
						<div class="product-title">
							
							<h1 class="product_title entry-title">
							<?php if(get_field('hide_weber_title',get_the_ID()) != true) : ?>
							<span class="weber-brand">Weber&reg;</span>
							<?php endif; ?>
								<?php the_title() ?>
							</h1>
						</div>
												<div class="clearfix"></div>
						<div class="price-color clearfix" <?php if($product->get_price_html() == '') { echo 'style="margin-top:15px; margin-bottom:0"'; } ?>>
							<div class="the-price">
								

<p class="price"><?php echo $product->get_price_html(); if($product->get_price_html() != '') { ?><span class="rrp">rrp</span> <?php } ?>
								
<?php if(get_field('regular_price_note') != '') : ?>
<span class="second-note"><?php echo get_field('regular_price_note') ?></span>
<?php endif; ?>

<?php if(get_field('_sku') != '') : ?>
<div class="sku-note">SKU: <?php echo get_field('_sku') ?></div>
<?php endif; ?>
</p>
								<?php if(get_field('secondary_price') == true) : ?>
								<p class="price price-2"><?php echo woocommerce_price(get_field('secondary_rrp')) ?><span class="rrp">rrp</span> <span class="second-note"><?php echo get_field('secondary_price_note') ?></span>
								
								
								<?php if(get_field('secondary_sku') != '') : ?>
								<div class="sku-note">SKU: <?php echo get_field('secondary_sku') ?></div>
								<?php endif; ?>
								</p>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="single-product-images col-md-6 col-sm-6 col-xs-12">
					<div id="product-slider-container">
						<?php
				
				
				
				$option_qty = sizeof($color_options);

				if($product_badge != '' && $product_badge == 'sold_out'){
					echo '<div class="sold-out-badge"><img src="'.get_bloginfo('template_url').'/img/badge-sold-out.png" alt="" /></div>';
				}
				
				//print_r($color_options_gallery);
				
				
				
				
				// if single image
				if($product_image_type == 'single'){
					$gallery_single_image = get_field('gallery_single');		
					echo '<img src="'.$gallery_single_image['url'].'" width="'.$gallery_single_image['width'].'" height="'.$gallery_single_image['height'].'" alt="'.$gallery_single_image['alt'].'" />';
				}//end if its single image
				
				
				
				
				// if slider
				if($product_image_type == 'slider'){
					$gallery_slider = get_field('gallery_slider');
					if(sizeof($gallery_slider) > 1){
						?>
						<div id="product-slider">
							<ul class="slides">
								<?php foreach( $gallery_slider as $image ): ?>
								<li> <a href="<?php echo $image['url']; ?>" class="zoom" data-rel="prettyPhoto[product-gallery]"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a> </li>
								<?php endforeach; ?>
							</ul>
						</div>
						<div id="product-carousel" class="flexslider">
							<ul class="slides">
								<?php foreach( $gallery_slider as $image ): ?>
								<li>
									<div class="inner-thumb"> <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" /></div>
								</li>
								<?php endforeach; ?>
							</ul>
						</div>
						<?php
					}//end if there more than 1 in slider
					else{
						echo '<img src="'.$gallery_slider[0]['url'].'" width="'.$gallery_slider[0]['width'].'" height="'.$gallery_slider[0]['height'].'" alt="'.$gallery_slider[0]['alt'].'" />';
					}// endi if else
				}//end if its for slider
				
				
				
				// if single image
				if($product_image_type == 'color'){
					$gallery_color = get_field('gallery_color');		
					/* $first_image = $gallery_color[0]['gallery_product_image'];
					echo '<img src="'.$first_image['url'].'" width="'.$first_image['width'].'" height="'.$first_image['height'].'" alt="'.$first_image['alt'].'" />'; */
					if(is_array($gallery_color)):
						$index_color = 0;
						foreach($gallery_color as $image){ $index_color++;
							$show_first = $index_color == 1 ? 'show' : '';
							$first_image = $image['gallery_product_image'];
							echo '<img src="'.$first_image['url'].'" width="'.$first_image['width'].'" height="'.$first_image['height'].'" alt="'.$first_image['alt'].'" class="product-image-color-option '. $show_first .'" data-color="color-'. str_replace('#','',$image['product_color']) .'" />';
						}
					endif;
				}//end if its single image
				
				
			
				?>
						<div class="loading-infinite">
							<div class="circle">
								<div class="inner"></div>
							</div>
							<div class="circle">
								<div class="inner"></div>
							</div>
							<div class="circle">
								<div class="inner"></div>
							</div>
							<div class="circle">
								<div class="inner"></div>
							</div>
							<div class="circle">
								<div class="inner"></div>
							</div>
						</div>
						
					</div><div class="sharing-single-product"><div class="share-label col-md-5 col-sm-5"><img src="<?php bloginfo('template_url')?>/img/share-icon.png" alt="Share Recipe Icon" /></div>
					<div class="share-thing col-md-7 col-sm-7">
						<?php weber_share_button(get_the_ID()) ?>
					</div></div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">


						<div class="product-description">
							<?php the_content() ?>
						</div>
						<div class="devider"></div>
						<div class="product-find-retailer">
							<h3>Find a retailer for this product</h3>
              <?php
							$is_specialist_only = get_field('show_specialist_dealer_badge') != '' ? 'yes' : 'no';
							?>
							<form id="find-weber-postcodes-2" action="<?php echo $dn_option['where_to_buy_page'] ?>">
								<div class="input-holder">
									<span class="input-bg"><?php weber_icon('input-bg') ?></span>
									<input type="text" name="autocomplete-location" id="autocomplete-location-2" placeholder="enter your postcode" value="" />
									<input type="hidden" name="autocomplete-coordinate" id="autocomplete-coordinate-2" />
									<input type="hidden" name="product_id" id="product_id" value="<?php echo get_the_ID() ?>" />
									<input type="hidden" name="specialist" id="specialist" value="<?php echo $is_specialist_only ?>" />
									<input type="hidden" name="autocomplete-type" id="autocomplete-type-2" />
									<input type="hidden" name="autocomplete-type-name" id="autocomplete-type-name-2" />
									<input type="hidden" name="search_type" value="search_by_postcode" />
								</div>
								<div class="button-holder">
									<button class='svg-button' type='submit'>
										<span class='btn-red'><?php weber_icon('button-outline') ?></span>
										<span class='anchor'>Find</span>
									</button>
								</div>
							</form>
						</div>
				</div>
				
				
			</div>
		</div>
		<!-- product section1  -->
		
		<div class="accessories-section">
			<div class="container">
				<?php wc_get_template_part( 'content', 'type-slider-accessories-not-product'); ?>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- #product-<?php the_ID(); ?> --> 
</div>
