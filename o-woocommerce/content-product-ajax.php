<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}




	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 3 );
	$classes[] = 'col-md-4';


// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] ) {
	$classes[] = 'first';
}
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ) {
	$classes[] = 'last';
}
$classes[] = 'loop-product-item product';
$product_badge = get_field('product_badge');
foreach($classes as $class){
	$classs .= $class.' ';
}
?>

<li class="loop-product-item product col-md-4 col-sm-4">
	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
	<a href="<?php the_permalink(); ?>">
	<div class="product-loop-title">
		<div class="weber-inc">
		<h3><span>Weber&reg;</span> <?php the_title(); ?></h3>
		<?php
				if(get_field('hide_weber_title',get_the_ID()) != true) :
							$badge_class = '';
					else: 
							$badge_class = ' center-badge ';
				endif; ?>
			<?php		if($product_badge != '' && $product_badge != 'no' && $product_badge != 'sold_out'):?>
			<span class="badge <?php echo $badge_class ?>"> <img src="<?php bloginfo('template_url')?>/img/badge-<?php echo $product_badge?>.png?v=2018" alt="" /></span>
			<?php endif; ?></div>
	</div>
	<div class="product-loop-images">
		<?php
		
		if($product_badge != '' && $product_badge != 'no' && $product_badge == 'sold_out'):
		?>
		<div class="sold-out"> <img src="<?php bloginfo('template_url')?>/img/badge-sold-out.png" alt="" /></div>
		<?php
		endif;
		
			/**
			 * woocommerce_before_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			//do_action( 'woocommerce_before_shop_loop_item_title' );
			$imgattr = wp_get_attachment_image_src(get_post_thumbnail_id(),'full');
			echo '<div class="load-img" data-src="'.$imgattr[0].'">';
			echo '<svg width="'.$imgattr[1].'" height="'.$imgattr[2].'" version="1.1"
     xmlns="http://www.w3.org/2000/svg" xmlns:xlink= "http://www.w3.org/1999/xlink">
	<image xlink:href="'.$imgattr[0].'" x="0" y="0" width="'.$imgattr[1].'" height="'.$imgattr[2].'" />
</svg>';
			echo '</div>';
		?>
	</div>
	<?php
			/**
			 * woocommerce_after_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_template_loop_rating - 5
			 * @hooked woocommerce_template_loop_price - 10
			 */
			//do_action( 'woocommerce_after_shop_loop_item_title' );
		?>
	</a>
	<?php

		/**
		 * woocommerce_after_shop_loop_item hook
		 *
		 * @hooked woocommerce_template_loop_add_to_cart - 10
		 */
		do_action( 'woocommerce_after_shop_loop_item' );

	?>
	<div class="compare-check">
		<?php
		$check_compare = $_SESSION['compare_item'];
		if(is_array($check_compare)){
			if(in_array($product->id,$check_compare)){
				$extra_class = 'active';
			}
		}
	?>
		<a data-product_id="<?php echo $product->id ?>" data-security="<?php echo wp_create_nonce( 'compare-check' ); ?>" class="<?php echo $extra_class ?>"><span></span> Compare</a> </div>
	<div class="border-loop"></div>
</li>
