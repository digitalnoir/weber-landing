<?php

if(!isset($_SESSION)) session_start();

/* Digital Noir Constant */
define('THEME_VERSION', '1.0.1'.time());
define('THEME_URL', get_template_directory_uri());
define('THEME_DIR', get_template_directory());
define('HOME_URL', home_url() );
define('GOOGLE_MAP_KEY', 'AIzaSyAgrVVX2jVeSFXkgWubu9sgguu1cGYomwk');

// SFTP class
require 'sftp/vendor/autoload.php';
use phpseclib\Net\SFTP;

// Pronto Details
define('SFTP_HOST', 'sftp-demandware.weberbbq.com.au' );
define('SFTP_USER', 'webpartnersftp' );
define('SFTP_PASS', 'Thoaqu5kauNe8xai' );
//define('SFTP_DESTINATION', '/staging/orders/' ); // STAGING Directory
define('SFTP_DESTINATION', '/production/orders/' ); // LIVE Directory

/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'digital_noir_starter_pack_scripts' );
function digital_noir_starter_pack_scripts() {

	// Load our style
	wp_enqueue_style( 'style-main', THEME_URL . '/assets/dist/css/style.css','', THEME_VERSION );	

	// Localize the script with new data
	wp_register_script( 'js-main', THEME_URL . '/assets/dist/js/main.js', array('jquery'), THEME_VERSION, true );
	
	$dn_variable = array(
		'ajax_url' => admin_url( 'admin-ajax.php' )
	);
	wp_localize_script( 'js-main', 'dn_variable', $dn_variable );

	// disable guttenberg css
	wp_dequeue_style( 'wp-block-library' );

	/* if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	} */

	// PREPARE THE JS AND CSS FILE TO USE LATER
	wp_register_script('js-lazysize', THEME_URL. '/assets/dist/js/lazysizes.js', array('jquery'), THEME_VERSION, true); // put
	// wp_register_script('js-isotope', THEME_URL. '/assets/dist/js/isotope.js', array('jquery'), THEME_VERSION, true);
	wp_register_script('js-magnific', THEME_URL. '/assets/dist/js/magnific.popup.js', array('jquery'), THEME_VERSION, true);
	wp_register_script('js-slick', THEME_URL. '/assets/dist/js/slick.js', array('jquery'), THEME_VERSION, true);
	wp_register_script('js-tinysort', THEME_URL. '/assets/dist/js/tinysort.js', array('jquery'), THEME_VERSION, true);
	// wp_register_script('js-infinite-scroll', THEME_URL. '/assets/dist/js/infinite-scroll.js', array('jquery'), THEME_VERSION, true);
	// wp_register_script('js-google-maps', 'https://maps.googleapis.com/maps/api/js?v=3.exp&key=' . GOOGLE_MAP_KEY, '', '', true );

	// Blocks JS
	// wp_register_script('js-sliding-gallery', THEME_URL. '/assets/dist/js/sliding-gallery.js', array('jquery', 'js-magnific', 'js-slick'), THEME_VERSION, true);
	// wp_register_script('js-testimonial-slider', THEME_URL. '/assets/dist/js/testimonial-slider.js', array('jquery', 'js-slick'), THEME_VERSION, true);
	// wp_register_script('js-text-gallery', THEME_URL. '/assets/dist/js/text-gallery.js', array('jquery', 'js-slick'), THEME_VERSION, true);
	// wp_register_script('js-maps', THEME_URL. '/assets/dist/js/maps.js', array('jquery', 'js-google-maps'), THEME_VERSION, true);

	// all page should has lazypage
	wp_enqueue_script('js-lazysize');

	// Load our js
	wp_enqueue_script( 'js-slick' );
	wp_enqueue_script( 'js-main' );

}

/////////////////////
///// ACF Global ////
/////////////////////

// Include field type for ACF5
// $version = 5 and can be ignored until ACF6 exists
add_action('acf/include_field_types', 'dn_include_field_types_focal_point');
function dn_include_field_types_focal_point( $version ) {
	include_once( THEME_DIR . '/inc/acf-focal_point.php' );
}

// Add ACF Gravity form integration
require THEME_DIR . '/inc/acf-gravity_forms/acf-gravity_forms.php';



// INCLUDE NECESSARY FUNCTION 
require THEME_DIR . '/inc/function-white-label.php';
require THEME_DIR . '/emails/function-email.php';

// ONLY INCLUDE BASED ON WHERE THE CODE TRIGGERED 
if( !is_admin() ){
	require THEME_DIR . '/inc/function-front-end.php';
}else{
	require THEME_DIR . '/inc/function-back-end.php';
}

require THEME_DIR . '/inc/function-icon.php';
require THEME_DIR . '/inc/function-custom.php';
require THEME_DIR . '/inc/function-woocommerce.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

// 1. Add custom field input @ Product Data > Variations > Single Variation
 
add_action( 'woocommerce_variation_options_pricing', 'bbloomer_add_custom_field_to_variations', 10, 3 );
 
function bbloomer_add_custom_field_to_variations( $loop, $variation_data, $variation ) {
	echo '<div class="product_custom_field">';
    echo '<h2><strong>Product Stock</strong></h2>';
		woocommerce_wp_text_input( array(
		'id' => 'inventory-au-sw',
		'class' => 'short',
		'label' => __( 'BLACKTOWN (SW)', 'woocommerce' ),
		'value' => get_post_meta( $variation->ID, 'inventory-au-sw', true ),
		'custom_attributes' => array('readonly' => 'readonly'),
		)
		);
		woocommerce_wp_text_input( array(
		'id' => 'inventory-au-bw',
		'class' => 'short',
		'label' => __( 'LOGANLEA (BW)', 'woocommerce' ),
		'value' => get_post_meta( $variation->ID, 'inventory-au-bw', true ),
		'custom_attributes' => array('readonly' => 'readonly'),
		)
		);

		woocommerce_wp_text_input( array(
			'id' => 'inventory-au-aw',
			'class' => 'short',
			'label' => __( 'ATHOL PARK (AW)', 'woocommerce' ),
			'value' => get_post_meta( $variation->ID, 'inventory-au-aw', true ),
			'custom_attributes' => array('readonly' => 'readonly'),
			)
		);
		
		woocommerce_wp_text_input( array(
			'id' => 'inventory-au-mw',
			'class' => 'short',
			'label' => __( 'TRUGANINA (MW)', 'woocommerce' ),
			'value' => get_post_meta( $variation->ID, 'inventory-au-mw', true ),
			'custom_attributes' => array('readonly' => 'readonly'),
			)
		);
		woocommerce_wp_text_input( array(
			'id' => 'inventory-au-pw',
			'class' => 'short',
			'label' => __( 'FORRESTFIELD (PW)', 'woocommerce' ),
			'value' => get_post_meta( $variation->ID, 'inventory-au-pw', true ),
			'custom_attributes' => array('readonly' => 'readonly'),
			)
		);
		woocommerce_wp_text_input( array(
			'id' => 'inventory-au-hw',
			'class' => 'short',
			'label' => __( 'DERWENT PARK (HW)', 'woocommerce' ),
			'value' => get_post_meta( $variation->ID, 'inventory-au-hw', true ),
			'custom_attributes' => array('readonly' => 'readonly'),
			)
			);
		woocommerce_wp_text_input( array(
		'id' => 'inventory-au',
		'class' => 'short',
		'label' => __( 'WINGFIELD (AU)', 'woocommerce' ),
		'value' => get_post_meta( $variation->ID, 'inventory-au', true ),
		'custom_attributes' => array('readonly' => 'readonly'),
		)
		);
		echo '</div>';
}
 
// Display Fields
add_action('woocommerce_product_options_general_product_data', 'woocommerce_product_custom_fields');


function woocommerce_product_custom_fields()
{
    global $woocommerce, $post;
    echo '<div class="product_custom_field single_product_custom_field">';
    echo '<h2><strong>Product Stock</strong></h2>';
    // Custom Product Text Field
    woocommerce_wp_text_input(
        array(
            'id' => 'inventory-au-sw',
            'placeholder' => '',
            'label' => __('BLACKTOWN (SW)', 'woocommerce'),
			'desc_tip' => 'true',
			'custom_attributes' => array('readonly' => 'readonly'),
        )
    );
    woocommerce_wp_text_input(
        array(
            'id' => 'inventory-au-bw',
            'placeholder' => '',
            'label' => __('LOGANLEA (BW)', 'woocommerce'),
			'desc_tip' => 'true',
			'custom_attributes' => array('readonly' => 'readonly'),
        )
	);
	woocommerce_wp_text_input(
        array(
            'id' => 'inventory-au-aw',
            'placeholder' => '',
            'label' => __('ATHOL PARK (AW)', 'woocommerce'),
			'desc_tip' => 'true',
			'custom_attributes' => array('readonly' => 'readonly'),
        )
    );
    woocommerce_wp_text_input(
        array(
            'id' => 'inventory-au-mw',
            'placeholder' => '',
            'label' => __('TRUGANINA (MW)', 'woocommerce'),
			'desc_tip' => 'true',
			'custom_attributes' => array('readonly' => 'readonly'),
        )
    );
    woocommerce_wp_text_input(
        array(
            'id' => 'inventory-au-pw',
            'placeholder' => '',
            'label' => __('FORRESTFIELD (PW)', 'woocommerce'),
			'desc_tip' => 'true',
			'custom_attributes' => array('readonly' => 'readonly'),
        )
    );
    woocommerce_wp_text_input(
        array(
            'id' => 'inventory-au-hw',
            'placeholder' => '',
            'label' => __('DERWENT PARK (HW)', 'woocommerce'),
			'desc_tip' => 'true',
			'custom_attributes' => array('readonly' => 'readonly'),
        )
    );
    woocommerce_wp_text_input(
        array(
            'id' => 'inventory-au',
            'placeholder' => '',
            'label' => __('WINGFIELD (AU)', 'woocommerce'),
			'desc_tip' => 'true',
			'custom_attributes' => array('readonly' => 'readonly'),
        )
    );
   
    echo '</div>';

}

/**
 * Display delivery and assembly field on the single product page
 */
function cfwc_display_custom_field() {
	global $post , $wpdb;
	// Check for the custom field value
	$ismandatory = get_field("da_mandatory");
	$dacost = get_field( "da_cost" );
	$product = wc_get_product( $post->ID );
    $currenpostcode = $_SESSION['weber_partner']['customer_postcode'];
	$datable=$wpdb->prefix.'weber_postcode';
	$result = $wpdb->get_results( "SELECT postcode FROM $datable where postcode=".$currenpostcode); 
	if(!empty($result && $dacost !='')){
	 ?>
	<div class="cus-require-sec">
			<ul>
				<?php if($ismandatory){
					echo '<li class="disable-checked"><label>Delivery and assembly is required for this item <input type="checkbox" checked name="delivery-and-assembly"><span class="checkmark"></span></label></li>';
				} else {
                    // only offer DA if enabled
                    if( ! weber_is_da_disabled() ){
                        echo '<li><label>I require delivery and assembly ('.wc_price( $dacost ).')<input type="checkbox" name="delivery-and-assembly"><span class="checkmark"></span></label></li>';
                    }
				}
				?>
			</ul>
		</div>
	<?php 
	}
   }
   add_action( 'woocommerce_after_add_to_cart_button', 'cfwc_display_custom_field' );

   /**
 * Add the delivery and assembly as item data to the cart object
 * 
 */
function cfwc_add_custom_field_item_data( $cart_item_data, $product_id, $variation_id, $quantity ) {
    
    // from shop page - check whether the product DA is mandatory
    $is_mandatory_da = get_post_meta( $product_id, 'da_mandatory', true );

    if( $is_mandatory_da ){

        $cart_item_data['da'] = 1;

    }
    // from single post
    elseif( ! empty( $_POST['delivery-and-assembly'] ) ){

        // Add the item data
        $cart_item_data['da'] = $_POST['delivery-and-assembly'];

    }

    
    return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', 'cfwc_add_custom_field_item_data', 10, 4 );


/**
 * WooCommerce Extra Feature
 * --------------------------
 *
 * Add custom fee to cart automatically
 *
 */
function woo_add_cart_fee() {
	global $woocommerce;
	$items = $woocommerce->cart->get_cart();

    $found_da_cost = array();
	foreach($items as $item => $values) { 
        
        $ismandatory = get_post_meta( $values['product_id'], 'da_mandatory', true );
        $cost = get_post_meta( $values['product_id'], 'da_cost', true );

        // if any DA cost in cart item
		if( $values['da'] ){
			$found_da_cost[] = $cost;
		}
	}
	if( !empty( $found_da_cost ) ){
        $da_fees = max( $found_da_cost );
		$woocommerce->cart->add_fee( __('Delivery and Assembly', 'woocommerce'), $da_fees );
	}  
	
	  
  }
  add_action( 'woocommerce_cart_calculate_fees', 'woo_add_cart_fee' );

  /**
 * Add a DA field to each cart item
 */
function prefix_after_cart_item_name( $cart_item, $cart_item_key ) {
	global $wpdb;
	$currenpostcode = $_SESSION['weber_partner']['customer_postcode'];
	$datable = $wpdb->prefix.'weber_postcode';
	$result = $wpdb->get_results( "SELECT postcode FROM $datable where postcode=".$currenpostcode); 
	$dacost = get_post_meta($cart_item['product_id'], 'da_cost', true );
	if(!empty($result && $dacost !='')){
		$ismandatory = get_post_meta($cart_item['product_id'], 'da_mandatory', true );
		if($ismandatory){
			printf('<div class="cus-require-sec"><ul><li class="disable-checked"><label>Delivery and assembly is required for this item <input type="checkbox" class="delivery_assembly" checked="" readonly name="delivery-and-assembly" product-id="'.$product_id.'"><span class="checkmark"></span></label></li></ul> </div>',
			'prefix-cart-notes',
			$cart_item_key,
			$cart_item_key,
			$notes
			);
		}else{ 

            // only shop option is enabled
            if( ! weber_is_da_disabled() ){
                if($cart_item['da']){

                    printf('<div class="cus-require-sec"><ul><li><label>I required delivery and assembly for this item <input type="checkbox" id="cart_notes_%s" class="delivery_assembly" readonly checked="" name="delivery-and-assembly" data-cart-id="%s"><span class="checkmark"></span></label></li></ul> </div>',
                    'prefix-cart-notes',
                    $cart_item_key,
                    $cart_item_key,
                    $notes
                    );
                }else{
                    printf('<div class="cus-require-sec"><ul><li><label>Delivery and assembly is optional for this item <input type="checkbox"  id="cart_notes_%s" class="delivery_assembly" name="delivery-and-assembly" data-cart-id="%s"><span class="checkmark"></span></li></label></ul> </div>','prefix-cart-notes',
                    $cart_item_key,
                    $cart_item_key,
                    $notes
                    );
                }
            }
		}
	}
	$notes = isset( $cart_item['notes'] ) ? $cart_item['notes'] : '';

   }
   add_action( 'woocommerce_after_cart_item_name', 'prefix_after_cart_item_name', 10, 2 );
   
/**
 * Update cart item notes
 */
function prefix_update_cart_notes() {
	$cart = WC()->cart->cart_contents;
	$cart_id = $_POST['cart_id'];
	$notes = $_POST['security'];
	$cart_item = $cart[$cart_id];
	$cart_item['da'] = $notes;
	WC()->cart->cart_contents[$cart_id] = $cart_item;
	WC()->cart->set_session();
	wp_send_json( array( 'success' => 1 ) );
	exit;
}

add_action( 'wp_ajax_prefix_update_cart_notes',  'prefix_update_cart_notes',10);
add_action( 'wp_ajax_nopriv_prefix_update_cart_notes','prefix_update_cart_notes',10);

/**
 * Creating Order XML Files for the Pronto
 * Creating 
 */

add_action('weber_thankyou', 'order_xml_files_for_the_pronto', 10, 1);
function order_xml_files_for_the_pronto( $order_id ) {
   
    if ( ! $order_id )
        return;
    $xmlitem = array();
$order = wc_get_order($order_id);
$employeId= get_post_meta( $order_id, 'billing_employee_id', true );
$customerName = $order->get_billing_first_name().' '.$order->get_billing_last_name();
$partnerid = $_SESSION['weber_partner']['partner_id'];
$partnerprontocode = get_post_meta( $partnerid, 'partner_pronto_code', true );

$warehousename = $_SESSION['weber_partner']['warehouse'];
$discount = $_SESSION['weber_partner']['discount'];
$business_code = $_SESSION['weber_partner']['business_code'];
$campaign_name = $_SESSION['weber_partner']['campaign_name'];
$order_subtotal = $order->get_subtotal();
$order_total=$order->get_total(); 
$order_total_tax=$order->get_total_tax();
$order_shipping_total=$order->get_shipping_total();
$order_shipping_tax =$order->get_shipping_tax(); 
$order_shipping_subtotal = $order_shipping_total-$order_shipping_tax;
$order_shipping_method=$order->get_shipping_method();

// use once
$weber_order_items = $order->get_items();

// loop over highest DA if any
$is_da = $heighest = array();
foreach ( $weber_order_items as $item_id => $item ) { 

    $choose_da = wc_get_order_item_meta( $item_id, '_da', true ); 
    if($choose_da == 'on' || $choose_da == 1 ){

        // get product id as da id
        $_product_id = wc_get_order_item_meta( $item_id, '_product_id', true ); 
        $is_da[$_product_id] = get_post_meta( $_product_id, 'da_cost', true );

    }

}

// if has da, get the heighest
if( !empty( $is_da ) ){
    $heighest = array_keys( $is_da, max($is_da) );
    $heighest = $heighest[0];
}

$position = 0;

foreach ( $weber_order_items as $item_id => $item ) { $position++;

    // reset
    $_da_xml = $_price_adjustment = '';


   $product_id = $item->get_variation_id() != 0 ? $item->get_variation_id() : $item->get_product_id();
   $parent_product_id = $item->get_product_id();
   $da_mandatory = get_post_meta($parent_product_id, 'da_mandatory', true );
   $da_cost = get_post_meta($parent_product_id, 'da_cost', true );

   $product = wc_get_product( $product_id );
   $_regular_price = $product->get_price();

   // indicate whether the product has DA attached to it
   // accessories will not have this line
   if( $da_cost != '' ){

    // only one DA should available
    if( !is_array($heighest) && $parent_product_id == $heighest ){

        $da_gross = weber_price( get_post_meta( $parent_product_id, 'da_cost', true ) );
        $da_net_tax = weber_get_net_price( $da_gross );
        $da_net = $da_net_tax['net'];
        $da_tax = $da_net_tax['tax'];
        $da_name = 'Delivery and Assembly: Delivery and Assembly';
        $da_code = get_post_meta( $parent_product_id, 'da_code', true );
        $da_text = 'optionAssemblyService'.$da_code;


    }else{
        
        // any other DA optional or not available
        $da_gross = $da_net_tax = $da_tax = '0.00';
        $da_code = 'DEFAULT';
        $__da_code = get_post_meta( $parent_product_id, 'da_code', true );
        $da_name = 'Optional - '. $__da_code .': Assembly Unavailable';
        $da_text = 'optionAssemblyService'.$__da_code;


    }


        // output the xml
        $_da_xml = '<option-lineitems>
                <option-lineitem>
                    <net-price>'.$da_net.'</net-price>
                    <tax>'.$da_tax.'</tax>
                    <gross-price>'.$da_gross.'</gross-price>
                    <base-price>'.$da_gross.'</base-price>
                    <lineitem-text>'.$da_name.'</lineitem-text>
                    <tax-basis>'.$da_gross.'</tax-basis>
                    <option-id>'.$da_text.'</option-id>
                    <value-id>'.$da_code.'</value-id>
                    <product-id>'.$da_code.'</product-id>
                </option-lineitem>
            </option-lineitems>';

   }


    $partner = get_post_meta( $order_id, '_weber_partner_promo', true);
    $discount = $partner['discount'] / 100;
    $discount_amount = $_regular_price * $discount;
    $_partner_id = $partner['partner_id'];
    $_partner_pronto_code = get_post_meta( $_partner_id, 'partner_pronto_code', true );
    $_coupon_code = strtoupper( $partner['business_code'] );

   // show the discount here
   $_price_adjustment = '<price-adjustments>
                <price-adjustment>
                    <net-price>-'. weber_price( $discount_amount ) .'</net-price>
                    <tax>0.00</tax>
                    <gross-price>-'. weber_price( $discount_amount ) .'</gross-price>
                    <base-price>-'. weber_price( $discount_amount ) .'</base-price>
                    <lineitem-text>'.$_partner_pronto_code.'</lineitem-text>
                    <tax-basis>0.00</tax-basis>
                    <promotion-id>'.$_partner_pronto_code.'</promotion-id>
                    <campaign-id>'.$_partner_pronto_code.'</campaign-id>
                    <coupon-id>'.$_coupon_code.'</coupon-id>
                    <custom-attributes>
                        <custom-attribute attribute-id="conditionTypeCode">NO CODE</custom-attribute>
                    </custom-attributes>
                </price-adjustment>
                </price-adjustments>';


    $_source_warehouse = wc_get_order_item_meta( $item_id, '_source_warehouse', true );
    $choose_da = wc_get_order_item_meta( $item_id, '_da', true ); 

   
   $variation_id = $item->get_variation_id();
   $product = $item->get_product();
   $name = $item->get_name();
   $quantity = $item->get_quantity();
   $subtotal = $item->get_subtotal();
   $total = $item->get_total();
   $tax = $item->get_subtotal_tax();
   $taxclass = $item->get_tax_class();
   $taxstat = $item->get_tax_status();
   $allmeta = $item->get_meta_data();

   $type = $item->get_type();
   $price = $total - $tax;

   // product sku
   $sku = $product->get_sku();
   $regular_price = weber_price( $_regular_price );
   $tax_net_price = weber_get_net_price( $_regular_price );

   $xmlitem[] = ' <product-lineitem>
                <net-price>'. $tax_net_price['net'] .'</net-price>
                <tax>'. $tax_net_price['tax'] .'</tax>
                <gross-price>'. $regular_price .'</gross-price>
                <base-price>'. $regular_price .'</base-price>
                <lineitem-text>'. $name .'</lineitem-text>
                <tax-basis>'. $regular_price .'</tax-basis>
                <position>' . $position .'</position>
                <product-id>'. $sku .'</product-id>
                <product-name>'. $name .'</product-name>
                <quantity unit="">'. $quantity .'</quantity>
                <tax-rate>0.1</tax-rate>
                <shipment-id>SHIPMENT-'.$order_id.'</shipment-id>

                '. $_da_xml .'
                
                <gift>false</gift>
                <custom-attributes>
                    <custom-attribute attribute-id="sourceInventory">'. $_source_warehouse .'</custom-attribute>
                </custom-attributes>

                '. $_price_adjustment .'

            </product-lineitem>';
}
	$xml='';
    $xml .= '<?xml version="1.0" encoding="UTF-8"?>
    <orders xmlns="http://www.demandware.com/xml/impex/order/2006-10-31">
    <order order-no="B2B'.$order_id.'">
        <order-date>'.$order->get_date_created().'</order-date>
        <created-by>storefront</created-by>
        <original-order-no>B2B'.$order_id.'</original-order-no>
        <currency>AUD</currency>
        <customer-locale>en_AU</customer-locale>
        <taxation>gross</taxation>
        <invoice-no>B2B'.$order_id.'</invoice-no>
        <customer>
            <customer-no>'.$order->get_customer_id().'</customer-no>
            <customer-name>'.$customerName.'</customer-name>
            <customer-email>'.$order->get_billing_email().'</customer-email>
            <billing-address>
                <first-name>'.$order->get_billing_first_name().'</first-name>
                <last-name>'.$order->get_billing_last_name().'</last-name>
                <address1>'.$order->get_billing_address_1().'</address1>
                <city>'.$order->get_billing_city().'</city>
                <postal-code>'.$order->get_billing_postcode().'</postal-code>
                <state-code>'.$order->get_billing_state().'</state-code>
                <country-code>'. $order->get_billing_country().'</country-code>
                <phone>'.$order->get_billing_phone().'</phone>
            </billing-address>
        </customer>
        <status>
            <order-status>NEW</order-status>
            <shipping-status>NOT_SHIPPED</shipping-status>
            <confirmation-status>CONFIRMED</confirmation-status>
            <payment-status>PAID</payment-status>
        </status>
        <current-order-no>B2B'.$order_id.'</current-order-no>
        <product-lineitems>';
         foreach ($xmlitem as  $valuexml) {
        	 $xml .= $valuexml;
          } 
       $xml .= '</product-lineitems>
        <shipping-lineitems>
            <shipping-lineitem>
                <net-price>'.$order_shipping_subtotal.'</net-price>
                <tax>'.$order_shipping_tax.'</tax>
                <gross-price>'.$order_shipping_total.'</gross-price>
                <base-price>'.$order_shipping_total.'</base-price>
                <lineitem-text>Shipping</lineitem-text>
                <tax-basis>'.$order_shipping_total.'</tax-basis>
                <item-id>STANDARD_SHIPPING</item-id>
                <shipment-id>SHIPMENT-'.$order_id.'</shipment-id>
                <tax-rate>0.1</tax-rate>
            </shipping-lineitem>
        </shipping-lineitems>
        <shipments>
            <shipment shipment-id="SHIPMENT-'.$order_id.'">
                <status>
                    <shipping-status>NOT_SHIPPED</shipping-status>
                </status>
                <shipping-method>'.$order_shipping_method.'</shipping-method>
                <shipping-address>
                    <first-name>'.$order->get_billing_first_name().'</first-name>
                    <last-name>'.$order->get_billing_last_name().'</last-name>
                    <address1>'.$order->get_billing_address_1().'</address1>
                    <city>'.$order->get_billing_city().'</city>
                    <postal-code>'.$order->get_billing_postcode().'</postal-code>
                    <state-code>'.$order->get_billing_state().'</state-code>
                    <country-code>'. $order->get_billing_country().'</country-code>
                    <phone>'.$order->get_billing_phone().'</phone>
                </shipping-address>
                <gift>false</gift>
                <totals>
                    <merchandize-total>
                        <net-price>0.00</net-price>
                        <tax>0.00</tax>
                        <gross-price>0.00</gross-price>
                    </merchandize-total>
                    <adjusted-merchandize-total>
                        <net-price>0.00</net-price>
                        <tax>0.00</tax>
                        <gross-price>0.00</gross-price>
                    </adjusted-merchandize-total>
                    <shipping-total>
                        <net-price>0.00</net-price>
                        <tax>0.00</tax>
                        <gross-price>0.00</gross-price>
                    </shipping-total>
                    <adjusted-shipping-total>
                        <net-price>0.00</net-price>
                        <tax>0.00</tax>
                        <gross-price>0.00</gross-price>
                    </adjusted-shipping-total>
                    <shipment-total>
                        <net-price>0.00</net-price>
                        <tax>0.00</tax>
                        <gross-price>0.00</gross-price>
                    </shipment-total>
                </totals>
            </shipment>
        </shipments>
        <totals>
            <merchandize-total>
                <net-price>0.00</net-price>
                <tax>0.00</tax>
                <gross-price>0.00</gross-price>
            </merchandize-total>
            <adjusted-merchandize-total>
                <net-price>0.00</net-price>
                <tax>0.00</tax>
                <gross-price>0.00</gross-price>
            </adjusted-merchandize-total>
            <shipping-total>
                <net-price>0.00</net-price>
                <tax>0.00</tax>
                <gross-price>0.00</gross-price>
            </shipping-total>
            <adjusted-shipping-total>
                <net-price>0.00</net-price>
                <tax>0.00</tax>
                <gross-price>0.00</gross-price>
            </adjusted-shipping-total>
            <order-total>
                <net-price>0.00</net-price>
                <tax>0.00</tax>
                <gross-price>0.00</gross-price>
            </order-total>
        </totals>
        <payments>
            <payment>
                <custom-method>
                    <method-name>COMM_WEB</method-name>
                </custom-method>
                <amount>'.$order->get_total().'</amount>
            </payment>
        </payments>
        <custom-attributes>
            <custom-attribute attribute-id="employee_id">'.$employeId.'</custom-attribute>
        </custom-attributes>
    </order>
</orders>';

// debug only
// return $xml;


    $rootPath = str_replace(ABSPATH,"",getcwd());
    update_post_meta($order_id, 'unique_order_number','B2B'.$order_id);
    file_put_contents($rootPath .'/xml-record/B2B'.$order_id.'.xml', $xml);
    $filename= "B2B".$order_id.".xml";
    if($filename){

        # write file to a FTP server.
        $sftp = new SFTP( SFTP_HOST );
        $user = SFTP_USER;
        $pass = SFTP_PASS;
        if (!$sftp->login($user, $pass)) {

            // error when trying to logged in to SFTP, notify the admin to manually push to pronto via woo admin page
            do_action('weber_pronto_failed_send_xml', $order_id);

        }
        else {
            
            $finalFilename = $filename;
            $dataFile = $rootPath.'/xml-record/'.$finalFilename;
            $fileContent = file_get_contents($dataFile);
            $sftp->put( SFTP_DESTINATION . $finalFilename, $fileContent);

            // push to pronto works, save a custom field to indicate
            update_post_meta( $order_id, '_pushed_to_pronto', 'yes' );

        }
    }

}
/**
 * Create menu for the Delivery Range 
 */

/**
* Create menu for the Delivery Range
*/
add_action('admin_menu', 'delivery_range_pages');
function delivery_range_pages(){
    add_menu_page('Delivery Range', 'Delivery Range', 'manage_options', 'delivery-range', 'delivery_range_output','dashicons-media-spreadsheet');
    add_submenu_page('delivery-range', 'All Delivery Range', 'All DA Ranges', 'manage_options', 'delivery-range' );
    add_submenu_page('delivery-range', 'Delivery Range Postcode', 'Delivery Range Postcode', 'manage_options', 'delivery-range-menu1','delivery_postcode');
    add_submenu_page('', '', '', 'manage_options', 'delivery-range-menu2','delete_page');
    add_submenu_page('', '', '', 'manage_options', 'delivery-range-menu3','delivery_range_import_csv');
    
}

function delivery_range_output(){
    global $wpdb;
    $postcode = $wpdb->prefix.'weber_postcode';
    $delivery_postcode = $wpdb->prefix.'weber_delivery_postcode';
    $result = $wpdb->get_results("SELECT * FROM $postcode ORDER BY postcode ASC");

    if(isset($_POST['addnew'])){
        $wpdb->insert($postcode, array('postcode' => $_POST['postcode'], 'tech' => $_POST['tech'], 'area' => $_POST['area']));
        wp_redirect('admin.php?page=delivery-range');
    }
    if(isset($_GET['postcode']) && $_GET['postcode'] !=''){
        if(isset($_POST['update'])){
            $update = $wpdb->query(" UPDATE `$postcode` SET tech = '".$_POST['tech']."', area = '".$_POST['area']."' WHERE postcode =".$_POST['postcode']);
            if($update){
                echo '<div class="alert alert-success" role="alert">
                Data update successfully!
              </div>';
            }
        }
        $result = $wpdb->get_results("SELECT $postcode.postcode, $postcode.tech, $postcode.area FROM $postcode WHERE $postcode.postcode =".$_GET['postcode']);
        $result = json_decode(json_encode($result),true);
         
    }
    include_once(get_template_directory().'/delivery-range-menu/index.php');
}

function delivery_postcode(){
    global $wpdb;
    $delivery_postcode = $wpdb->prefix.'weber_delivery_postcode';
    $result = $wpdb->get_results("SELECT * FROM $delivery_postcode ORDER BY postcode ASC");
    $result = json_decode(json_encode($result),true);

    if(isset($_POST['addnew'])){
        $wpdb->insert($delivery_postcode, array('postcode' => $_POST['postcode'], 'warehouse' => $_POST['warehouse']));
        wp_redirect('admin.php?page=delivery-range-menu1');
    }
    if(isset($_GET['postcode']) && $_GET['postcode'] !=''){
    if(isset($_POST['update'])){
         $update = $wpdb->query(" UPDATE `$delivery_postcode` SET warehouse = '".$_POST['warehouse']."' WHERE postcode =".$_POST['postcode']);
         if($update){
            echo '<div class="alert alert-success" role="alert">
            Data update successfully!
          </div>';
        }
    }
        $result = $wpdb->get_results("SELECT * FROM $delivery_postcode WHERE postcode =".$_GET['postcode']);
         $result = json_decode(json_encode($result),true);
    }

    include_once(get_template_directory().'/delivery-range-menu/edit.php');
}
function delete_page(){
    global $wpdb;
    $postcode = $wpdb->prefix.'weber_postcode';
    $delivery_postcode = $wpdb->prefix.'weber_delivery_postcode';
    if(isset($_GET['postcode']) && isset($_GET['return']) && $_GET['return'] =='menu2') {
    $wpdb->query("DELETE FROM $delivery_postcode WHERE postcode =".$_GET['postcode']);
     wp_redirect('admin.php?page=delivery-range-menu1');
    }
    if(isset($_GET['postcode']) && isset($_GET['return']) && $_GET['return'] =='menu1' ) {
        $wpdb->query("DELETE FROM $postcode WHERE postcode =".$_GET['postcode']);
        wp_redirect('admin.php?page=delivery-range');
    }
}

function delivery_range_import_csv(){
    global $wpdb;
    $postcode = $wpdb->prefix.'weber_postcode';
    $delivery_postcode = $wpdb->prefix.'weber_delivery_postcode';
    if(isset($_POST["csvupdate_menu2"]) && isset($_POST["menu1"])){
        $filename=$_FILES["csv_file"]["tmp_name"];
        if($_FILES["csv_file"]["size"] > 0){
        $file = fopen($filename, "r");
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE){
             $wpdb->insert($postcode, array('postcode' =>$getData[0], 'warehouse' => $getData[1]));
        }
        fclose($file);
         wp_redirect('admin.php?page=delivery-range');
        }
    }

    if(isset($_POST["csvupdate"]) && isset($_POST["menu1"])){
        $filename=$_FILES["csv_file"]["tmp_name"];
        if($_FILES["csv_file"]["size"] > 0){
            $file = fopen($filename, "r");
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE){
            $wpdb->insert($delivery_postcode, array('postcode' =>$getData[0], 'tech' => $getData[1]));
        }
        fclose($file);
             wp_redirect('admin.php?page=delivery-range-menu1');
        }
    }
}

add_action( 'admin_enqueue_scripts','include_admin_script');
function include_admin_script(){

    wp_register_style('icon','https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', false, '0.0.1' );
    wp_register_style('bootstrap-min','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', false, '0.0.1' );
    wp_register_style('bootstrap-data-table','https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css', false, '0.0.1' );
    wp_register_style('bootstrap-responsive','https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap.min.css', false, '0.0.1' );
    wp_register_script('jquery-dataTables','https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js','jquery','0.0.1',true );
    wp_register_script('bootstrap-dataTables','https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js','jquery','0.0.1',true );
    wp_register_script('responsive-dataTables','https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js','jquery','0.0.1',true );
    wp_register_script('min-dataTables','https://cdn.datatables.net/responsive/2.2.5/js/responsive.bootstrap.min.js','jquery','0.0.1',true );

    if($_GET['page']=='delivery-range' || $_GET['page']=='delivery-range-menu1'){
       
        wp_enqueue_style('icon');       
        wp_enqueue_style('bootstrap-min');       
        wp_enqueue_style('bootstrap-data-table');       
        wp_enqueue_style('bootstrap-responsive');      
       
        wp_enqueue_script('jquery-dataTables');         
        wp_enqueue_script('bootstrap-dataTables');       
        wp_enqueue_script('responsive-dataTables');       
        wp_enqueue_script('min-dataTables');

    }
     
}


/**
* Disable Emoji Support
*/
function disable_emojis_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}

// disable emoji on email
add_action( 'init', 'disable_emojis' );
function disable_emojis() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
    
 // Edit Start by Sherica //
add_action('init', 'weber_insert_partnerform_log');
function weber_insert_partnerform_log(){

    // only need this to run once
    if( get_option('weber_partnerform_log') == 'true' ){
        return;
    }
    global $wpdb;

    $table_name = $wpdb->prefix.'weber_partnerform_log';
    $create_table_query = 'CREATE TABLE '.$table_name.'(
        ID bigint(20) NOT NULL AUTO_INCREMENT,
        email varchar(100) NOT NULL,
        fullname text NOT NULL,
        mobile text NOT NULL,
        address text NOT NULL,
        postcode varchar(6) NOT NULL,
        storename text NOT NULL,
        department text NOT NULL,
        partner_id bigint(20) NOT NULL,
        campaign_id bigint(20) NOT NULL,
        start_date datetime NOT NULL,
        end_date datetime NOT NULL,
        unique_code varchar(10) NOT NULL,
        has_purchase int(2) NOT NULL,
        PRIMARY KEY (ID))';

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );    
    dbDelta( $create_table_query );

    // update option to indicate its has been run
    update_option( 'weber_partnerform_log', 'true', false );

}
 add_action('admin_menu', 'partner_form_settings');
function partner_form_settings(){
    add_submenu_page('options-general.php', 'Partner Form Options', 'Partner Form Options', 'manage_options', 'partner-form-options',  'partner_form_options');
}
function partner_form_options(){
    $html = ''; $msg ='';
    if($_SERVER["REQUEST_METHOD"]=="POST" && $_POST["partnerform_options"]){
        update_option("partnerform_options", $_POST["partnerform_options"]);
        $msg = "<p>Options saved successfully.</p>";
    }
    $opt = get_option("partnerform_options");
    if(!isset($opt["toc"])){$opt["toc"]='';}
    //if(!isset($opt["department"])){$opt["department"]='';}
    $html .= '<div class ="wrap">';
    $html .= '<h1>Partner Form Options</h1>';
    $html .= '<hr>'.$msg;
    $pages = get_pages();
    $html .= '<form method="post" action="">';
    $html .= '<p><label style="display:block;padding:5px;font-weight:bold;">Select Terms & Conditions page:</label> <select name="partnerform_options[toc]"><option value="0">Select Page</option>';
    if($pages){foreach($pages as $k=>$v){$html .= '<option value="'.$v->ID.'" '.selected($v->ID, $opt["toc"], false).'>'.$v->post_title.'</option>';}}
    $html .= '</select></p>';
    
    //$html .= '<p><label style="display:block;padding:5px;font-weight:bold;">Enter all department option (each in a separate line):</label> <textarea name="partnerform_options[department]">'.$opt["department"].'</textarea></p>';
    
    $html .= '<p><input type="submit" value="Save Options"></p>';
    $html .= '</form>';
    
    $html .= '</div>';
    echo $html;
    
}

add_action('init', 'process_weber_partner_form');
function process_weber_partner_form(){
    if($_SERVER["REQUEST_METHOD"]=="POST" && $_POST["weber_partner"]){
        $data = $_POST["weber_partner"];
        if($_SESSION['partnerform_error'] == ''){$_SESSION['partnerform_error']=array();}
        if(!isset($_POST["weber_partner"]["toc"])){
            $_SESSION['partnerform_error'][] = "Please read accept the Terms & Conditions.";
            $error = true;
        }
        if(empty($data["fullname"])){
            $_SESSION['partnerform_error'][] = "Please enter your full name.";
            $error = true;
        }
        if(empty($data["email"])){
            $_SESSION['partnerform_error'][] = "Please enter your email address.";
            $error = true;
        }
        if(empty($data["postcode"])){
            $_SESSION['partnerform_error'][] = "Please enter your postcode.";
            $error = true;
        }
        if($error){return;}
        else{
            unset($_SESSION['partnerform_error']);
            global $wpdb;
            $table = $wpdb->prefix.'weber_partnerform_log';
            $table2 = $wpdb->prefix.'weber_business_code';
            $camp = get_partner_campaign($data["partner_id"]);
            if($camp){
                $ucode = weber_check_create_unique_code( 8 ); //weber_create_unique_code(8);
                $data["campaign_id"] = $camp->ID; // Put your code to get campaign id here.
                $data["start_date"] = $camp->start_date; // Put your code to get campaign start date here.
                $data["end_date"] = $camp->end_date; // Put your code to get campaign end date here. 
                $data["unique_code"] = $ucode; // Put your code to get unique code here.
                unset($data["toc"]);
                
                $data2 = array(
                "campaign_id" => $camp->ID,
                "business_code" => $ucode,
                "status" => "available",
                "discount" => $camp->discount,
                "partner_id" => $camp->partner_id,
                "partner_name" => $camp->partner_name,
                "campaign_name" => $camp->campaign_name,
                "start_date" => $camp->start_date,
                "end_date" => $camp->end_date
                );
                
                $wpdb->insert($table, $data);
                $wpdb->insert($table2, $data2);
            }
            
            
            

            
            $to = $data["email"];
            $from = get_option( 'admin_email' );
            $blogname = get_option( 'blogname' );
            $headers = array("From: ".$blogname." <".$from.">") ;
            $subject = $blogname." Shop Link";
            $custName = $data["fullname"];
            $body = "Dear " .$custName."\r\n
            Thanks for registering your details!  You’re all set to head to the Weber Partners Page to go shopping!\r\n
            Please click on this link below to grant access to the partner page. \r\n \r\n";
            $link = get_permalink($data["partner_id"]);
            $link .= "?email=".$data["email"]."&uniquecode=".$data["unique_code"];
            $body .= $link;
            $body .= "\r\n \r\n Happy Grilling from the team at Weber!";
            
            wp_mail( $to, $subject, $body, $headers );
            //@mail($to, $subject, $body, $headers);
        }
    }

}


function weber_check_partner_form_error(){

    if( !isset( $_SESSION['partnerform_error'] ) && $_SERVER["REQUEST_METHOD"]=="POST" && $_POST["weber_partner"]){
        echo '<ul class="partner-error" style="background-color: #55aa55;">';
            echo '<li>Your form has been submitted successfully. Please check your email for confirmation link. </li>';
        echo '</ul>';
        return;
    } elseif( !isset( $_SESSION['partnerform_error'] ) || $_SESSION['partnerform_error']==''){
        return;
    }

    echo '<ul class="partner-error">';
    foreach( (array) $_SESSION['partnerform_error'] as $message ){
        echo '<li>'. $message .'</li>';
    }
    echo '</ul>';

    // already displayed, remove the error
    $_SESSION['partnerform_error']='';
    unset($_SESSION['partnerform_error']);
}

function get_partner_campaign($partnerId=0){
    global $wpdb;
    $table = $wpdb->prefix.'weber_business_campaign';
    if($partnerId && is_numeric($partnerId)){
        $sql = "SELECT * FROM `$table` WHERE `partner_id`=$partnerId LIMIT 1";
        $row = $wpdb->get_row($sql);
    }
    if($row){return $row;} else {return false;}
}
/*function weber_create_unique_code( $length )  {

	$char = "abcdefghijklmnpqrstuvwxyz123456789";
	$char = str_shuffle($char);
	for($i = 0, $rand = '', $l = strlen($char) - 1; $i < $length; $i ++) {
		$rand .= $char{mt_rand(0, $l)};
	}

	// ensure its unique
	$is_exist = weber_is_code_exist( $rand );
	if( $is_exist ){
		return weber_create_unique_code( $length );
	}

	return $rand;
}

// check whether the code is exits
function weber_is_code_exist( $string ){

	$string = strtolower($string);

	global $wpdb;
	$sql = "
		SELECT * FROM wp_weber_business_code
		WHERE business_code = '{$string}'
	";

	$result = $wpdb->get_row($sql);
    if(!empty($result) > 0){return true;}
    
    $sql = "
		SELECT * FROM wp_weber_partnerform_log
		WHERE unique_code = '{$string}'
	";

	$result = $wpdb->get_row($sql);
    if(!empty($result) > 0){return true;}
    return false;
}*/
add_action('wp', 'process_weber_partner_visitor');
function process_weber_partner_visitor(){
    if($_SERVER["REQUEST_METHOD"]=="GET" && $_GET["email"] && $_GET["uniquecode"]){
        $pid = get_the_ID();
        global $wpdb;
        $table = $wpdb->prefix."weber_partnerform_log";
        $sql = "SELECT * FROM  `$table` WHERE `email`='".sanitize_email($_GET["email"])."' AND `unique_code`='".sanitize_text_field($_GET["uniquecode"])."' AND `partner_id`=$pid";
        $row = $wpdb->get_row($sql, ARRAY_A);
        
        if($row){
            $business_code  =  sanitize_text_field( $_GET["uniquecode"] );
            $postcode       =  sanitize_text_field( $row['postcode'] );
            $email          = sanitize_email($_GET["email"]);

            $validate = weber_validate_business_access( $business_code, $postcode, $email );

            // save posted data to use later
            $_SESSION['partner_posted_data'] = array(
            'business_code' => $business_code,
            'email_address' => $email,
            'postcode'      => $postcode,
            );
        }
    }
}
function weber_addnewCampaign_html($post_id){
    $html = '
    <div id="add-campain-wrap">
			<h3 style="margin-bottom:5px">Add Campaign</h3>
			<table id="weber-business-codes-add" class="acf-table">
				<thead>
					<tr>
						<th>Campaign Name <span class="acf-required">*</span></th>
						<th>Start Date <span class="acf-required">*</span></th>
						<th>End Date<span class="acf-required">*</span></th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<tr class="the-form">
						<td>
							<input type="text" name="_w_campaign_name" autocomplete="off" />            
						</td>
						<td>
							<input type="text" name="_w_start_date" autocomplete="off" />            
						</td>
						<td>
							<input type="text" name="_w_end_date" autocomplete="off" />            
						</td>
						<td>
                            <input type="hidden" name="_w_total_code" value="0" />
							<button id="generate-code" class="acf-button button button-primary" data-id="'.$post_id.'">Submit</button>            
						</td>
					</tr>
				</tbody>
			</table>
    </div>';
    return $html;
}
// Edit End by Sherica //
