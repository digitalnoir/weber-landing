<?php

// shipping variable
$warehouse_map = array(
    'sw' => array(
        'postcode' => 2148,
        'suburb' => 'BLACKTOWN',
    ),
    'bw' => array(
        'postcode' => 4131,
        'suburb' => 'LOGANLEA',
    ),
    'aw' => array(
        'postcode' => 5012,
        'suburb' => 'ATHOL PARK',
    ),
    'mw' => array(
        'postcode' => 3029,
        'suburb' => 'TRUGANINA',
    ),
    'pw' => array(
        'postcode' => 6058,
        'suburb' => 'FORRESTFIELD',
    ),
    'hw' => array(
        'postcode' => 7009,
        'suburb' => 'DERWENT PARK',
    ),
    'accessories' => array(
        'postcode' => 5013,
        'suburb' => 'WINGFIELD',
    ),
);

define('WAREHOUSE_LOCATION', $warehouse_map);

// oneflo endpoint
define('FREIGHT_LOGIN_URL', 'https://uat-thirdpartywcf.flipgroup.com.au/DataService.svc?singleWsdl');
define('FREIGHT_GETQUOTE_URL', 'https://bau-uat-ws.azurewebsites.net/Data/ChainIT/DataService.svc');


// helper
function oneflo_check_expire_session(){

    $expire = get_option('weber_oneflow_session_expire');
    $now = current_time('mysql');

    // if still active don't regenerate
    if( strtotime( $now ) < strtotime( $expire ) ){
        // to do check expire
        //return true;
    }

    // regenerate the session and store to db
    oneflo_login();

}

// login and store to option table
function oneflo_login(){

    // REQUEST BODY
    ob_start();
    include THEME_DIR . '/inc/freight-login.php';
    $login_body = ob_get_clean();


    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => FREIGHT_LOGIN_URL,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => $login_body,
    CURLOPT_HTTPHEADER => array(
            "SoapAction: http://www.opensys.com.au/ChainIT/4.0/ChainITDataServices/IChainITService/Login",
            "Content-Type: text/xml"
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $response);
    $xml = new SimpleXMLElement( $response );
    $login_result = $xml->sBody->LoginResponse->LoginResult;
    if( $login_result->AccountID != 0 ){
        
        // save to option table
        ob_start();
        include THEME_DIR . '/inc/freight-session.php';
        update_option('weber_oneflow_session', ob_get_clean(), false);
        update_option('weber_oneflow_session_expire', date('Y-m-d 00:00:00', strtotime( $login_result->Expires)), false);

        return true;

    }else{
        // cannot login
        return false;
    }

}


// Call Oneflo service to get shipping cost
function oneflo_get_quote( $cart_item ){

    // ensure we have active login session
    oneflo_check_expire_session();

    // REQUEST BODY
    ob_start();
    include THEME_DIR . '/inc/freight-calculator.php';
    $quote_body = ob_get_clean();


    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => FREIGHT_LOGIN_URL,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => $quote_body,
    CURLOPT_HTTPHEADER => array(
            "SoapAction: http://www.opensys.com.au/ChainIT/4.0/ChainITDataServices/IChainITService/GetChargeQuote2",
            "Content-Type: text/xml"
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $response);
    $xml = new SimpleXMLElement( $response );

    $shipping_result = json_encode( $xml );
    $response_array = json_decode( $shipping_result, true );

    $service_pricing = $response_array['sBody']['GetChargeQuote2Response']['GetChargeQuote2Result'];
    if( isset($service_pricing['ServicePricing']) && !empty($service_pricing['ServicePricing']) ){
        return $service_pricing['ServicePricing'];
    }
    return false;
    


}



// original tutorial
// https://code.tutsplus.com/tutorials/create-a-custom-shipping-method-for-woocommerce--cms-26098
add_action( 'woocommerce_shipping_init', 'weber_oneflo_shipping_method' );
function weber_oneflo_shipping_method() {
    if ( ! class_exists( 'Weber_Oneflo_Shipping_Method' ) ) {
        class Weber_Oneflo_Shipping_Method extends WC_Shipping_Method {
            /**
             * Constructor for your shipping class
             *
             * @access public
             * @return void
             */
            public function __construct() {
                $this->id                 = 'weber_oneflo'; 
                $this->method_title       = __( 'Oneflo Shipping', 'weber_oneflo' );  
                $this->method_description = __( 'Custom Shipping Method for Oneflo', 'weber_oneflo' );

                $this->enabled = isset( $this->settings['enabled'] ) ? $this->settings['enabled'] : 'yes';
                $this->title = isset( $this->settings['title'] ) ? $this->settings['title'] : __( 'Standard Shipping', 'weber_oneflo' );
            }

            /**
             * Init your settings
             *
             * @access public
             * @return void
             */
            function init() {
                // Load the settings API
                $this->init_form_fields(); 
                $this->init_settings(); 

                // Save settings in admin if you have any defined
                add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
            }

            /**
             * Define settings field for this shipping
             * @return void 
             */
            function init_form_fields() { 

                $this->form_fields = array(

                 'enabled' => array(
                      'title' => __( 'Enable', 'weber_oneflo' ),
                      'type' => 'checkbox',
                      'description' => __( 'Enable this shipping.', 'weber_oneflo' ),
                      'default' => 'yes'
                      ),

                 'title' => array(
                    'title' => __( 'Title', 'weber_oneflo' ),
                      'type' => 'text',
                      'description' => __( 'Title to be display on site', 'weber_oneflo' ),
                      'default' => __( 'Standard Shipping', 'weber_oneflo' )
                      ),

                 );

            }

            // helper to see whether customer in Delivery & Assembly area
            public function is_postcode_in_da_coverage( ){

                global $wpdb;
                $currenpostcode = $_SESSION['weber_partner']['customer_postcode'];
                $datable = $wpdb->prefix.'weber_postcode';
                $result = $wpdb->get_results( "SELECT postcode FROM $datable where postcode = ".$currenpostcode ); 

                if( !empty( $result ) ){
                    return true;
                }

                return false;
            }

            /**
             * This function is used to calculate the shipping cost. Within this function we can check for weights, dimensions and other parameters.
             *
             * @access public
             * @param mixed $package
             * @return void
             */
            public function calculate_shipping( $package = array() ) {

                // if free shipping enabled
                $partner_id = $_SESSION['weber_partner']['partner_id'];
                $is_free_shipping = get_field('enable_free_shipping', $partner_id);
                if( $is_free_shipping ){

                    $rate = array(
                        'id' => $this->id.'_free',
                        'label' => 'Free',
                        'cost' => 0,
                    );
    
                    $this->add_rate( $rate );
                    return;

                }

                

                // if DA exist on cart
                $cart_content = WC()->cart->get_cart();
                $custom_fee = false;
                foreach( $cart_content as $item => $values ) { 
                    
                    // check if DA already on RRP
                    $ismandatory = get_post_meta($values['product_id'], 'da_mandatory', true );
                    if($ismandatory){
                        $custom_fee = true;
                        break;
                    }

                    // check if manual DA choosen by customer
                    if( $values['da'] ){
                        
                        if($ismandatory){
                            
                        }else{
                            $cost = get_post_meta($values['product_id'], 'da_cost', true );
                            if( trim($cost) != ''){
                                $custom_fee = true;
                                break;
                            }
                            
                        }
                    }
            
                }

                // free shipping because already charge on DA
                // add extra validation whether the customer is in DA coverage, if not then use oneflo
                if( $custom_fee && $this->is_postcode_in_da_coverage() ){
                    $rate = array(
                        'id' => $this->id.'_free',
                        'label' => 'Free',
                        'cost' => 0,
                    );
    
                    $this->add_rate( $rate );
                    return;
                }  


                //////////////////////////////////////////
                // START FREIGHT CALCULATION FOR NON DA //
                //////////////////////////////////////////


                // Check whether the product has static cost
                $has_static_cost = false;
                $static_cost = $the_freight_cost = array();
                foreach( $cart_content as $item => $values ) { 
                    
                    // check if flat rate exists
                    $is_flat_rate = get_post_meta( $values['product_id'], 'flat_shipping_cost', true );
                    if( trim( $is_flat_rate ) != '' && $is_flat_rate != 0 ){
                        $static_cost[] = $is_flat_rate;
                    }

                }

                // if has static cost, use the heighest
                if( !empty($static_cost) ){
                    $the_static_cost = max( $static_cost );
                    $the_freight_cost[] = $the_static_cost;
                    $has_static_cost = true;
                }


                // if all product in cart has static flat rate, no need hook to oneflo
                if( sizeof($static_cost) == sizeof($cart_content) && !empty($static_cost)){

                    $rate = array(
                        'id' => $this->id.'_standard_shipping',
                        'label' => 'Shipping Standard',
                        'cost' => round($the_static_cost, 2),
                    );
    
                    $this->add_rate( $rate );

                    return;
                }

                
                $weight = $cost = $volume = $total_items = 0;

                foreach ( $package['contents'] as $item_id => $values ) { $total_items++;
                    $_product = $values['data']; 
                    $weight += $_product->get_weight() * $values['quantity']; 

                    // calculate volume - we set the volume on woo setting as meter not centimeter
                    $_m_volumne = $_product->get_width() * $_product->get_height() * $_product->get_length();
                    // $_m_volumne = round( ( $_cm_volume / 1000000), 3 );
                    $volume += $_m_volumne;
                }

                // development only
                //$volume = 1;
                //$weight = 2;

                // Grab warehouse to dispatch
                $warehouse = strtolower( $_SESSION['weber_partner']['warehouse'] );
                $warehouse_location = WAREHOUSE_LOCATION[$warehouse];

                // grab one suburb from big data
                $customer_postcode = $_SESSION['weber_partner']['customer_postcode'];
                $customer_suburb = weber_get_suburb_from_postcode( $customer_postcode );


                // grab from oneflo
                $cart_item = array(
                    'warehouse_suburb' => $warehouse_location['suburb'],
                    'warehouse_postcode' => $warehouse_location['postcode'],
                    'customer_suburb' => $customer_suburb,
                    'customer_postcode' => $customer_postcode,
                    'total_item' => $total_items,
                    'total_weight' => $weight,
                    'total_volume' => $volume
                );
                
                $resulte = oneflo_get_quote( $cart_item );

                // loop all available service
                $i = 0;
                $oneflo_cost = array();
                foreach( (array) $resulte as $service){ $i++;

                    if (strpos($service['ServiceName'], 'M3') !== false ) {

                        $service_price = $service['TotalCharge'];
                        $the_freight_cost[] = $service_price;
                        break;

                    }
    
                    
                }

                // compare the heighest
                if( !empty($the_freight_cost)){

                    $the_real_freight_cost = max( $the_freight_cost );

                    $rate = array(
                        'id' => $this->id.'_standard_shipping',
                        'label' => 'Shipping Standard',
                        'cost' => round($the_real_freight_cost, 2),
                    );

                    $this->add_rate( $rate );
                }

                
            }
        }
    }
}

// Helper get the correct suburb from postcode
// Check whether we need to change this with actual suburb
function weber_get_suburb_from_postcode( $postcode ){

    global $wpdb;
    $sql = "
        SELECT suburb FROM {$wpdb->prefix}weber_postcode_suburb
        WHERE postcode = '{$postcode}'
    ";

    $suburb = $wpdb->get_row($sql, ARRAY_A);
    return $suburb['suburb'];

}

// Naming Shipping Method
add_filter( 'woocommerce_shipping_methods', 'add_weber_oneflo_shipping_method' );
function add_weber_oneflo_shipping_method( $methods ) {
    $methods[] = 'Weber_Oneflo_Shipping_Method';
    return $methods;
}


// Hide shipping on cart
add_filter( 'woocommerce_cart_ready_to_calc_shipping', 'weber_disable_shipping_calc_on_cart', 99 );
function weber_disable_shipping_calc_on_cart( $show_shipping ) {
    if( is_cart() ) {
        return false;
    }
    return $show_shipping;
}


// hook into the fragments in AJAX and add our new table to the group
add_filter('woocommerce_update_order_review_fragments', 'weber_shipping_order_fragments_split_shipping', 10, 1);
function weber_shipping_order_fragments_split_shipping($order_fragments) {

	ob_start();
	weber_shipping_woocommerce_order_review_shipping_split();
	$weber_shipping_woocommerce_order_review_shipping_split = ob_get_clean();

	$order_fragments['.weber-checkout-review-shipping-table'] = $weber_shipping_woocommerce_order_review_shipping_split;

	return $order_fragments;

}

// We'll get the template that just has the shipping options that we need for the new table
function weber_shipping_woocommerce_order_review_shipping_split( $deprecated = false ) {
	wc_get_template( 'checkout/shipping-order-review.php', array( 'checkout' => WC()->checkout() ) );
}

// The html to move anywhere
// add_action('woocommerce_after_checkout_billing_form', 'weber_shipping_move_new_shipping_table');
function weber_shipping_move_new_shipping_table() {
    echo '<table class="shop_table weber-checkout-review-shipping-table">
    <h3 class="cart-head">Shipping options</h3>
    </table>';
}