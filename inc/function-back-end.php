<?php

/* Set Default Function */
add_action( 'after_setup_theme', 'digital_noir_starter_pack_setup' );
function digital_noir_starter_pack_setup() {

	// Enable Post thumbnail and document title
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'title-tag' );

	// Registering Navigation Menu
	register_nav_menus( array(
		'header' => __( 'Header Menu', 'digitalnoir' ),
		'footer' => __( 'Footer Menu', 'digitalnoir' ),
	) );

	// Add HTML 5 support
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'digital_noir_starter_pack_content_width', 1140 );

}



/////////////////////////////////////////
//////// Add Couple Image Size //////////
/////////////////////////////////////////
add_image_size( "image-2560", 2560, 99999, false ); // no crop
add_image_size( "image-1920", 1920, 99999, false ); // no crop
add_image_size( "image-1600", 1600, 99999, false ); // no crop
add_image_size( "image-1366", 1366, 99999, false ); // no crop
add_image_size( "image-1024", 1024, 99999, false ); // no crop
add_image_size( "image-768", 768, 99999, false ); // no crop
add_image_size( "image-640", 640, 99999, false ); // no crop
add_image_size( "image-520", 500, 99999, false ); // no crop
add_image_size( "image-420", 400, 99999, false ); // no crop


// Remove default image sizes here, we dont' use so often
add_filter( 'intermediate_image_sizes_advanced', 'prefix_remove_default_images', 999, 1 );
function prefix_remove_default_images( $sizes ) {

    //unset( $sizes['thumbnail']); // 150px
    //unset( $sizes['small']); // 150px
    //unset( $sizes['large']); // 1024px
    //unset( $sizes['medium_large']); // 768px
    
    return $sizes;
}

///////////////////////////////////////////////////
//////// Register admin.js and admin.css //////////
///////////////////////////////////////////////////
add_action( 'admin_enqueue_scripts', 'dn_load_custom_wp_admin_style' );
function dn_load_custom_wp_admin_style() {
    
    // style
    wp_enqueue_style( 'dn-wp-admin-css', THEME_URL . '/assets/dist/css/admin.css', false, THEME_VERSION  );
    
    // javascript
	wp_register_script( 'js-theme-admin', THEME_URL . '/assets/dist/js/admin.js', array('jquery', 'acf-input'), THEME_VERSION, true );	
	wp_localize_script( 'js-theme-admin', 'theme_var',
        array(
            'upload' => THEME_URL.'/img/acf-thumbnail/',
        )
	);
	wp_enqueue_script( 'js-theme-admin');
}


//////////////////////////////////////
// ACF - Create custom options page //
//////////////////////////////////////
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
        'page_title' => 'Core Options',
        'autoload' => false, // make less SQL query
    ));
	
}

//////////////////////////////////
// ACF field sync (through git) //
//////////////////////////////////
add_filter('acf/settings/load_json', 'dn_acf_json_load_point');
function dn_acf_json_load_point( $paths ) {
    
    // remove original path (optional)
    unset($paths[0]);
    
    // append path
    $paths[] = get_stylesheet_directory() . '/acf-json';
    
    // return
    return $paths;
    
}

//////////////////////////////////////////////////////////
// ACF - Change the default text for the Add Row button //
//////////////////////////////////////////////////////////
add_filter( 'gettext', 'dn_change_default_add_repeater', 20, 3 );
function dn_change_default_add_repeater( $translated_text, $text, $domain ) {

    if( $domain == 'acf') {

        switch ( $translated_text ) {
            case 'Add Row' :
            $translated_text = __( 'Add +', 'acf' );
            break;
        }
    }

    return $translated_text;
}

//////////////////////////////////////////////////////////
// ACF - Register Maps API //
//////////////////////////////////////////////////////////
add_filter('acf/init', 'dn_acf_google_map_api');
function dn_acf_google_map_api( $api ){

	acf_update_setting('google_api_key', GOOGLE_MAP_KEY);
	
}

//////////////////////////////////////////////////////////
// ACF - Add Video Link to Gallery Image //
//////////////////////////////////////////////////////////
add_filter("attachment_fields_to_edit", "dn_acf_attachment_field_to_edit", null, 2);
function dn_acf_attachment_field_to_edit($form_fields, $post) {
    // $form_fields is a special array of fields to include in the attachment form
    // $post is the attachment record in the database
    //     $post->post_type == 'attachment'
    // (attachments are treated as posts in Wordpress)
     
    // add our custom field to the $form_fields array
    // input type="text" name/id="attachments[$attachment->ID][gallery_video]"
    $form_fields["gallery_video"] = array(
        "label" => __("Youtube Video URL"),
        "input" => "text", // this is default if "input" is omitted
        "value" => get_post_meta($post->ID, "_gallery_video", true)
    );
    // if you will be adding error messages for your field, 
    // then in order to not overwrite them, as they are pre-attached 
    // to this array, you would need to set the field up like this:
    $form_fields["gallery_video"]["label"] = __("Youtube Video URL");
    $form_fields["gallery_video"]["input"] = "text";
	$form_fields["gallery_video"]["value"] = get_post_meta($post->ID, "_gallery_video", true);	
	
    return $form_fields;
}

//////////////////////////////////////////////////////////
// ACF - Add Video Link to Gallery Image - on save post //
//////////////////////////////////////////////////////////
add_filter('attachment_fields_to_save', 'dn_acf_attachment_field_to_save', 10, 2);
function dn_acf_attachment_field_to_save($post, $fields){

	$attachment_id = $post['ID'];
	if($fields['gallery_video'] != ''){
		update_post_meta($attachment_id, "_gallery_video", $fields['gallery_video']);	
	}

	return $post;	

}

/////////////////////////////////////////
////// WP Editor - TINYMCE Tweak ////////
/////////////////////////////////////////

// add class to link
add_action('admin_head','dn_tinymce_wp_link_classes', 1, 10);
function dn_tinymce_wp_link_classes( $args ){

    ?>
    <script>
    jQuery( document ).ready( function( $ ) {
        $('#link-options').append( 
            '<div style="padding-top:10px">'+ 
                '<label><span>Link Style</span>'+
                '<select name="wpse-link-class" id="wpse_link_class">'+
                    '<option value="none">None</option>'+
                    '<option value="dn-button blue">Button Blue</option>'+
                    '<option value="dn-button red">Button Red</option>'+
            '</select>'+
            '</label>'+
        '</div>' );
        
        if($('#link-options').length > 0){
                wpLink.getAttrs = function() {
                    wpLink.correctURL();        
                    return {
                        class:      $( '#wpse_link_class' ).val(),
                        href:       $.trim( $( '#wp-link-url' ).val() ),
                        target:     $( '#wp-link-target' ).prop( 'checked' ) ? '_blank' : ''
                    };
                }
            }
    });</script>
    <style>
    .has-text-field #wp-link .query-results {
        top: 250px!important;
    }
    </style>
    <?php

}

// Remove Heading 1 from editor
add_filter( 'tiny_mce_before_init', 'dn_mce_before_init_insert_formats' ); 
function dn_mce_before_init_insert_formats( $init_array ) {  
	
    $init_array['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Preformatted=pre;';
    
	return $init_array;  
	
} 



// Hook several function to admin init
add_action( 'admin_init', 'my_theme_add_editor_styles' );
function my_theme_add_editor_styles() { 

    // add custom style to wp-editor
    add_editor_style( 'custom-editor-style.css' );    

    // disable gravity form css
    update_option( 'rg_gforms_disable_css', 1 ); 
    
}

// Add table button to tinymce
add_filter( 'mce_buttons', 'dn_add_the_table_button' );
add_filter( 'mce_external_plugins', 'dn_add_the_table_plugin', 9999, 1 );

function dn_add_the_table_button( $buttons ) {
	array_push( $buttons, 'table' );
	return $buttons;
 }

function dn_add_the_table_plugin( $plugins ) {
    $plugins['table'] = get_stylesheet_directory_uri() . '/assets/dist/js/tinymce.js';
	return $plugins;
}

// add postcode database
add_action('init', 'weber_insert_initial_postcode');
function weber_insert_initial_postcode(){

    // only need this to run once
    if( get_option('weber_initial_postcode') == 'true' ){
        return;
    }
    global $wpdb;

    $table_name = $wpdb->prefix.'weber_postcode';
    $create_table_query = 'CREATE TABLE '.$table_name.'(
        postcode INTEGER NOT NULL,
        tech VARCHAR(100),
        area VARCHAR(100),
        warehouse VARCHAR(100),
        PRIMARY KEY (postcode))';

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );    
    dbDelta( $create_table_query );

    // import initial file
    ini_set('auto_detect_line_endings',TRUE);
    $file = locate_template( 'postcode-database.csv' );
    
    $handle = fopen("$file", "r");
    $iteration = 0;
    while ( ($data = fgetcsv($handle) ) !== FALSE ) {
    
        $iteration++;
        // remove heading
        if($iteration == 1){
            continue;
        }


        $wpdb->insert( 
            $table_name, 
            array( 
                'postcode'  => $data[0], 
                'tech'      => $data[1],
                'area'      => $data[2],
                'warehouse' => $data[3],
            )
        );
    }
    ini_set('auto_detect_line_endings',FALSE);

    // update option to indicate its has been run
    update_option( 'weber_initial_postcode', 'true', false );

}

// add postcode database
add_action('init', 'weber_insert_initial_delivery_postcode');
function weber_insert_initial_delivery_postcode(){

    // only need this to run once
    if( get_option('weber_initial_delivery_postcode') == 'true' ){
        return;
    }
    global $wpdb;

    $table_name = $wpdb->prefix.'weber_delivery_postcode';
    $create_table_query = 'CREATE TABLE '.$table_name.'(
        postcode VARCHAR(100) NOT NULL,
        warehouse VARCHAR(100),
        PRIMARY KEY (postcode))';

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );    
    dbDelta( $create_table_query );

    // import initial file
    ini_set('auto_detect_line_endings',TRUE);
    $file = locate_template( 'delivery-postcode-database.csv' );
    
    $handle = fopen("$file", "r");
    $iteration = 0;
    while ( ($data = fgetcsv($handle) ) !== FALSE ) {
    
        $iteration++;
        // remove heading
        if($iteration == 1){
            continue;
        }


        $wpdb->insert( 
            $table_name, 
            array( 
                'postcode'  => $data[0], 
                'warehouse' => $data[1],
            )
        );
    }
    ini_set('auto_detect_line_endings',FALSE);

    // update option to indicate its has been run
    update_option( 'weber_initial_delivery_postcode', 'true', false );

}

// add postcode database
add_action('init', 'weber_insert_campaign_log');
function weber_insert_campaign_log(){

    // only need this to run once
    if( get_option('weber_campaign_log') == 'true' ){
        return;
    }
    global $wpdb;

    $table_name = $wpdb->prefix.'weber_campaign_log';
    $create_table_query = 'CREATE TABLE '.$table_name.'(
        ID bigint(20) NOT NULL AUTO_INCREMENT,
        business_code varchar(10) NOT NULL,
        billing_email varchar(100) NOT NULL,
        billing_firstname longtext NOT NULL,
        billing_lastname longtext NOT NULL,
        billing_postcode varchar(100) NOT NULL,
        order_id bigint(20) NOT NULL,
        partner_id bigint(20) NOT NULL,
        campaign_name varchar(100) NOT NULL,
        campaign_start_date date NOT NULL,
        campaign_end_date date NOT NULL,
        order_total bigint(20) NOT NULL,
        PRIMARY KEY (ID))';

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );    
    dbDelta( $create_table_query );

    // update option to indicate its has been run
    update_option( 'weber_campaign_log', 'true', false );

}


// add postcode database
add_action('init', 'weber_insert_initial_postcode_suburb');
function weber_insert_initial_postcode_suburb(){

    // only need this to run once
    if( get_option('weber_initial_postcode_suburb') == 'yes' ){
        return;
    }
    global $wpdb;

    $table_name = $wpdb->prefix.'weber_postcode_suburb';
    $create_table_query = 'CREATE TABLE '.$table_name.'(
        postcode VARCHAR(100) NOT NULL,
        suburb VARCHAR(100) NOT NULL
        )';

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );    
    dbDelta( $create_table_query );


    $arrContextOptions = array(
        "ssl"=>array(
            "verify_peer"=>false,
            "verify_peer_name"=>false,
        ),
    ); 
    
    $data = file_get_contents( THEME_URL . '/postcode.json', false, stream_context_create($arrContextOptions) );
    $json = ( json_decode($data) );
    foreach($json as $obj){
        $label = $obj->label;
        $explode = explode(', ', $label);
        $poscode = $explode[0];
        $suburb = $explode[1];

        $wpdb->insert( 
            $table_name, 
            array( 
                'postcode'  => $poscode, 
                'suburb' => $suburb,
            )
        );
    }

    // update option to indicate its has been run
    update_option( 'weber_initial_postcode_suburb', 'yes', false );

}

// add postcode database
add_action('init', 'weber_change_campaign_log_add_date');
function weber_change_campaign_log_add_date(){

    // only need this to run once
    if( get_option('weber_alter_campaign_log_table') == 'run' ){
        return;
    }

    global $wpdb;
    $wpdb->query(" ALTER TABLE wp_weber_campaign_log ADD order_date DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER order_total ");
    $wpdb->query(" ALTER TABLE wp_weber_campaign_log ADD campaign_id bigint(20) NOT NULL AFTER partner_id ");


    // update option to indicate its has been run
    update_option( 'weber_alter_campaign_log_table', 'run', false );

}


// campaign report
function weber_manage_report_campaign(){

    $campaign_id = (int) $_GET['campaign_id'];
    if($campaign_id == 0){
        echo 'Invalid Campaign ID';
        return;
    }

    global $wpdb;
    $sql = "
            SELECT 
                bcode.business_code,
                bcode.status,
                bcode.start_date,
                bcode.end_date,
                clog.billing_email,
                clog.billing_firstname,
                clog.billing_lastname,
                clog.billing_postcode,
                clog.order_id,
                clog.order_total,
                clog.order_date

            FROM {$wpdb->prefix}weber_business_code bcode 
            LEFT JOIN {$wpdb->prefix}weber_campaign_log clog
            ON bcode.business_code = clog.business_code 
            WHERE bcode.campaign_id = {$campaign_id}   
            ";
    $results = $wpdb->get_results($sql, ARRAY_A);

?>
    <div class="wrap">
        <h2>Campaign Report</h2>
        <div class="campaign-summary">
            <?php include locate_template( 'campaign/admin-summary.php' ) ?>
        </div>
    </div>
<?php
}

// data table ajax
add_action('wp_ajax_weber_campaign_report_datatable', 'weber_campaign_report_datatable_callback');
function weber_campaign_report_datatable_callback(){

    // initilize all variable
	$params = $columns = $totalRecords = $data = array();

    $params = $_REQUEST;    	

	//define index of column
	$columns = array( 
        0 => 'business_code',
        1 => 'status',
        2 => 'order_id',
        3 => 'billing_email',
        4 => 'billing_firstname',
        5 => 'billing_lastname',
        6 => 'order_total',
        7 => 'order_date'
	);

	$where = $sqlTot = $sqlRec = "";

	// check search value exist
	if( !empty($params['search']['value']) ) {   
		$where .=" AND ";
		$where .=" ( order_id LIKE '%".$params['search']['value']."%' ";    
        $where .=" OR bcode.business_code LIKE '%". strtolower( $params['search']['value'] )."%' ";
        $where .=" OR status LIKE '%".$params['search']['value']."%' ";
        $where .=" OR billing_email LIKE '%".$params['search']['value']."%' ";
        $where .=" OR billing_firstname LIKE '%".$params['search']['value']."%' ";
        $where .=" OR billing_lastname LIKE '%".$params['search']['value']."%' ";
        //$where .=" OR user_id LIKE '%".$params['search']['value']."%' ";
        //$where .=" OR profession LIKE '%".$params['search']['value']."%' ";
        //$where .=" OR event_name LIKE '%".$params['search']['value']."%' ";
        $where .=" ) ";
		//$where .=" OR CreatedDateTime LIKE '".$params['search']['value']."%' )";
    }
    
    global $wpdb;

    // getting total number records without any search
    
    $select_join = " ";

    $campaign_id = $params['campaign_id'];

    $sqlRec = "
                SELECT 
                bcode.business_code,
                bcode.status,
                bcode.start_date,
                bcode.end_date,
                clog.billing_email,
                clog.billing_firstname,
                clog.billing_lastname,
                clog.billing_postcode,
                clog.order_id,
                clog.order_total,
                clog.order_date

            FROM {$wpdb->prefix}weber_business_code bcode 
            LEFT JOIN {$wpdb->prefix}weber_campaign_log clog
            ON bcode.business_code = clog.business_code 
            WHERE bcode.campaign_id = {$campaign_id}        
    ";

    // need to think whether we need this
    // AND completion_status = 1
    
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

        $sqlRec .= $where;
        
	}

    $sqlRec .=  "  ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']." ";
    
    // SQL total dont need limit, grab before limit
    $sqlTot = $sqlRec;
     
    $sqlRec .=  "LIMIT ".$params['start']." ,".$params['length'];

    $db_query = $wpdb->get_results($sqlRec, ARRAY_A);

    $foundrows = $wpdb->get_results($sqlTot);

    $totalRecords = sizeof($foundrows);		

    
	//iterate on results row and create new index array of data
	foreach($db_query as $row){

        $order_id = !empty($row['order_id']) ? '<a href="'. get_edit_post_link( $row['order_id'] ) .'" target="_blank">'.$row['order_id'].'</a>' : '';
        $order_total = !empty($row['order_total']) ? wc_price( $row['order_total'] ) : '';

        $data[] = array(
            0 => strtoupper($row['business_code']), 
            1 => $row['status'], 
            2 => $order_id, 
            3 => $row['billing_email'], 
            4 => $row['billing_firstname'], 
            5 => $row['billing_lastname'], 
            6 => $order_total, 
            7 => $row['order_date'], 
        );
        
	}	


	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

    echo json_encode($json_data);  // send data as json format
    exit;

}