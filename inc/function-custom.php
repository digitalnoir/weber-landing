<?php

/*
    Usage:
    This file contain project speficic function, you can copy any function
    from function-collection.php file and put here or any function
*/

// check whether the access code already generated
function weber_validate_business_access( $business_code, $postcode, $email_address ){

    $business_code = trim($business_code);
    $business_code = sanitize_text_field( $business_code );
    $business_code = strtolower($business_code);

    // change should be start here
    global $wpdb;
    $sql = "
        SELECT * FROM {$wpdb->prefix}weber_business_code
        WHERE business_code = '{$business_code}'
    ";

    $result = $wpdb->get_row($sql);
 
    // not found, return imidiately
    if( empty($result) ){

        // store error message in session
        $_SESSION['partner_error'] = array(
            'message' => 'Access code invalid. Please ensure it is correct.',
        );

        return false;
    }

    // if has been redeemed
    if( ! empty($result) && $result->status == 'redeemed' ){

        // store error message in session
        $_SESSION['partner_error'] = array(
            'message' => 'Access code has been redeemed.',
        );

        return false;

    }

    // post id for use later
    $post_id = $result->partner_id;

    // validate postcode range
    $postcode_coverage = "
        SELECT * FROM {$wpdb->prefix}weber_delivery_postcode
        WHERE postcode = '{$postcode}'
    ";

    $postcode_coverage_results = $wpdb->get_row($postcode_coverage);
 
    // not found, return imidiately
    if( empty($postcode_coverage_results) ){

        // store error message in session
        $_SESSION['partner_error'] = array(
            'message' => 'Sorry, we can not deliver to your area.',
        );

        return false;
    }

    // warehouse
    $warehouse = $postcode_coverage_results->warehouse;

    // has purchase
    // if customer use their friend unique code
    $has_purchase = 0;
    $has_purchase_sql = "
        SELECT * FROM {$wpdb->prefix}weber_campaign_log
        WHERE billing_email = '{$email_address}'
        AND campaign_id = {$result->campaign_id}
    ";

    $has_purchase_sql_results = $wpdb->get_row($has_purchase_sql);
 
    // not found, return imidiately
    if( ! empty($has_purchase_sql_results) ){
        $has_purchase = 1;
    }


    // repeater format
    // partner_campaigns_0_campaign_name
    // partner_campaigns_0_start_date
    // partner_campaigns_0_end_date
    // partner_campaigns_0_access_code

    //$repeater_index = $result->meta_key;
    //$repeater_index = str_replace('partner_campaigns_', '', $repeater_index);
    //$repeater_index = str_replace('_access_code', '', $repeater_index);

    // start date validation
    if( ! empty($result) ){
        
        $_business_partner  = $result->partner_name;//get_the_title( $post_id );
        $_campaign_name     = $result->campaign_name;//get_post_meta( $post_id, 'partner_campaigns_'. $repeater_index .'_campaign_name', true );
        $_start_date        = $result->start_date;//get_post_meta( $post_id, 'partner_campaigns_'. $repeater_index .'_start_date', true );
        $_end_date          = $result->end_date;//get_post_meta( $post_id, 'partner_campaigns_'. $repeater_index .'_end_date', true );
        $_partner_discount  = $result->discount;//get_post_meta( $post_id, 'partner_discount', true );
        $_campaign_id       = $result->campaign_id;

        // current Adelaide time
        $now = strtotime( current_time('Y-m-d H:i:s') );

        $unix_campaign_start    = strtotime( $_start_date );
        $unix_campaign_end      = strtotime( $_end_date );

        // if campaign not started yet
        if( $now < $unix_campaign_start ){

            // store error message in session
            $_SESSION['partner_error'] = array(
                'message' => 'Access code invalid. Please ensure it is correct.',
            );

            return false;
        }

        // if campaign has been end
        if( $now > $unix_campaign_end ){

            // store error message in session
            $_SESSION['partner_error'] = array(
                'message' => 'The campaign has ended and cannot be redeemed',
            );

            return false;
        }


        // date validated, store to session
        $_SESSION['weber_partner'] = array(
            'partner_id' => $post_id,
            'partner_name' => $_business_partner,
            'campaign_id' => $_campaign_id,
            'campaign_name' => $_campaign_name,
            'email_address' => $email_address,
            'start_date' => $_start_date,
            'end_date' => $_end_date,
            'discount' => $_partner_discount,
            'customer_postcode' => $postcode,
            'warehouse' => $warehouse,
            'business_code' => $business_code,
            'has_purchase' => $has_purchase,
            'campaign_rules' => get_post_meta( $post_id, 'campaign_rules', true ),
        );

        // remove all cart item (if any), because its a fresh login
        if( ! WC()->cart->is_empty() ){
            WC()->cart->empty_cart();
        }

        // start woocommerce session
        WC()->customer->set_billing_postcode( $postcode );
        WC()->customer->set_billing_email( $email_address );
    }
}



// all product must be sold individualy
add_filter( 'woocommerce_is_sold_individually', 'weber_remove_all_quantity_fields', 10, 2 );
function weber_remove_all_quantity_fields( $return, $product ) {
    return true;
}

// disable admin bar
add_filter('show_admin_bar', '__return_false');


// add datepicker on variable product
add_action('woocommerce_product_after_variable_attributes', 'weber_color_woocommerce_product_after_variable_attributes', 10, 3);
function weber_color_woocommerce_product_after_variable_attributes( $loop, $varition_data, $variation ){


}

// remove search function
add_action( 'parse_query', 'weber_disable_search_filter_query' );
function weber_disable_search_filter_query( $query, $error = true ) {
    if ( is_search() ) {
        $query->is_search = false;
        $query->query_vars[s] = false;
        $query->query[s] = false;
        if ( $error == true ) $query->is_404 = true;
    }
}
add_filter( 'get_search_form', 'weber_remove_search_form' );
function weber_remove_search_form(){
    return null;
}

// helper to indicate user has bought
function weber_is_user_has_purchased(){

    if( isset( $_SESSION['weber_partner'] ) && $_SESSION['weber_partner']['has_purchase'] == 1 ){
        return true;
    }

    return false;

}

add_action('wp_head', 'weber_hide_side_cart_css', 9999);
function weber_hide_side_cart_css(){
    // hide button if already purchase
    if( weber_is_user_has_purchased() ){
        echo '<style>.xoo-wsc-modal{display:none!important}</style>';
    }
    
    // hide cart button on order received
    echo '<style>body.woocommerce-order-received .xoo-wsc-modal{display:none!important}</style>';

}


// check wether we only sell some variations
function weber_add_parent_product_from_variation( $products ){

    // check
    if( empty($products) ){
        return;
    }

    $return = array();

    // allow parent product to be visible
    foreach($products as $id){
        $_product = wc_get_product($id);
        if( $_product->get_type() == 'variation'){
            $return[] = $_product->get_parent_id();
        }

        // keep original product id
        $return[] = $id;
        
    }

    return $return;

}

// re invent the wheel from weber_partner_variation_is_active;
add_filter( 'woocommerce_get_children', 'weber_partner_one_product_only_variation_filter', 20, 3 );
function weber_partner_one_product_only_variation_filter( $children, $var, $variation ) {

    // check included product
    $include = (array) get_field('include_product', $_SESSION['weber_partner']['partner_id']);
    $limit_product = array();

    foreach($include as $id){
        $_product = wc_get_product($id);
        if( $_product->get_type() == 'variation'){
            $parent = $_product->get_parent_id();
            $limit_product[] = $parent;
        }
    }

    // bail early if not meet
    if( empty($limit_product) || $_SESSION['weber_partner']['campaign_rules'] != 'one_product_only' ){
        return $children;
    }

    $new_children = array();
    foreach( $children as $variation_id){

        $parent_product = wc_get_product($variation_id);
        $parent_id = $parent_product->get_parent_id();
        if( in_array($parent_id, $limit_product) ){
            if( ! in_array( $variation_id, $include) ){
                continue;
            }
        }

        $new_children[] = $variation_id;

    }   

    return $new_children;
}

// helper to hide DA options
function weber_is_da_disabled(){

    $partner_id = $_SESSION['weber_partner']['partner_id'];
    $disabled_da = (int) get_field('disable_delivery_assembly', $partner_id);

    return $disabled_da;

}