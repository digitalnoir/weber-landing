<?php

// Based on the code fom this plugin
// https://wordpress.org/plugins/acf-focal-point/
class acf_field_focal_point extends acf_field {
	
	
	/*
	*  __construct
	*
	*  This function will setup the field type data
	*
	*  @type	function
	*  @date	5/03/2014
	*  @since	5.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/
	
	function __construct() {
		
		/*
		*  name (string) Single word, no spaces. Underscores allowed
		*/
		
		$this->name = 'focal_point';
		
		
		/*
		*  label (string) Multiple words, can include spaces, visible when selecting a field type
		*/
		
		$this->label = __('Focal Point', 'acf-focal_point');
		
		
		/*
		*  category (string) basic | content | choice | relational | jquery | layout | CUSTOM GROUP NAME
		*/
		
		$this->category = 'jquery';
		
		
		/*
		*  defaults (array) Array of default settings which are merged into the field object. These are used later in settings
		*/
		
		$this->defaults = array(
			'save_format'	=>	'tag',
			'preview_size'	=>	'medium',
		);
		
		
		/*
		*  l10n (array) Array of strings that are used in JavaScript. This allows JS strings to be translated in PHP and loaded via:
		*  var message = acf._e('focal_point', 'error');
		*/
		
		$this->l10n = array();
		
				
		// do not delete!
    	parent::__construct();
    	
	}
	
	
	/*
	*  render_field_settings()
	*
	*  Create extra settings for your field. These are visible when editing a field
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/
	
	function render_field_settings( $field ) {
		
		/*
		*  acf_render_field_setting
		*
		*  This function will create a setting for your field. Simply pass the $field parameter and an array of field settings.
		*  The array of settings does not require a `value` or `prefix`; These settings are found from the $field array.
		*
		*  More than one setting can be added by copy/paste the above code.
		*  Please note that you must also have a matching $defaults value for the field name (font_size)
		*/
		
		// Render return value radio
		acf_render_field_setting( $field, array(
			'label'			=> __('Return Value','acf-focal_point'),
			'instructions'	=> __('Specify the returned value on front end','acf-focal_point'),
			'type'			=> 'radio',
			'name'			=> 'save_format',
			'layout'		=>	'horizontal',
			'choices'		=> 	array(
				'object'		=>	__("Image Object",'acf'),
				'tag'			=>	__("Image Tag",'acf')
			)
		));
	}
	
	
	
	/*
	*  render_field()
	*
	*  Create the HTML interface for your field
	*
	*  @param	$field (array) the $field being rendered
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/
	
	function render_field( $field ) {

		// Merge defaults
		$field = array_merge($this->defaults, $field);
		
		// Get set image id
		$id = (isset($field['value']['id'])) ? $field['value']['id'] : '';


		// data vars
		$data = array(
			'top'		=>	isset($field['value']['top']) ? $field['value']['top'] : '',
			'left'		=>	isset($field['value']['left']) ? $field['value']['left'] : '',
			'right'		=>	isset($field['value']['right']) ? $field['value']['right'] : '',
			'bottom'	=>	isset($field['value']['bottom']) ? $field['value']['bottom'] : '',
		);
		

		
		// If we already have an image set...
		if ($id) {
			
			// Get image by ID, in size set via options
			$img = wp_get_attachment_image_src($id, $field['preview_size']);
						
		}
			
		// If image found...
		// Set to hide add image button / show canvas
		$is_active 	= ($id) ? 'active' : '';

		// And set src
		$url = ($id) ? $img[0] : '';
		
		
		// create Field HTML
		?>
		<div class="acf-focal_point <?php echo $is_active; ?>" data-preview_size="<?php echo $field['preview_size']; ?>">

			<input class="acf-focal_point-id" type="hidden" name="<?php echo $field['name']; ?>[id]" value="<?php echo $id; ?>" />

			<?php foreach ($data as $k => $d): ?>
				<input class="acf-focal_point-<?php echo $k ?>" type="hidden" name="<?php echo $field['name']; ?>[<?php echo $k ?>]" value="<?php echo $d ?>" />
			<?php endforeach ?>

			<div class="has-image" style="min-height:100px;min-width:150px;background:#eaeaea">
				<span class="acf-button-delete acf-icon -cancel acf-icon-cancel dark" data-name="remove"></span>
				<img class="acf-focal_point-image" src="<?php echo $url; ?>" />
				<canvas class="acf-focal_point-canvas"></canvas>
			</div>

			<div class="clear"></div>

			<div class="no-image">
				<p><?php _e('No image selected','acf'); ?> <input type="button" class="button add-image" value="<?php _e('Add Image','acf'); ?>" />
			</div>

		</div><?php
	}
	
	/*
	*  format_value()
	*
	*  This filter is appied to the $value after it is loaded from the db and before it is returned to the template
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value (mixed) the value which was loaded from the database
	*  @param	$post_id (mixed) the $post_id from which the value was loaded
	*  @param	$field (array) the field array holding all the field options
	*
	*  @return	$value (mixed) the modified value
	*/
	
	function format_value( $value, $post_id, $field ) {

		// Merge defaults
		$field = array_merge($this->defaults, $field);

		// validate
		if( !$value ) {
			return false;
		}

		// Get image ID
		$image_ID = $value['id'];		

		
		if ($field['save_format'] == 'tag'){

			// print the image including <div ....
			return dn_get_background_image( $image_ID, $value['left'], $value['top'] );;

		}else{
			// print the array
			$return_object = array(
				'id' => $image_ID,
				'left' => $value['left'],
				'top' => $value['top'],
			);
			return $return_object;
		}
	
	}
	
	
}


// create field
new acf_field_focal_point();

// print background image in full size
function dn_get_background_image ( $image_ID, $focal_x = '', $focal_y = '', $size = 'image-1920') {

    if( $image_ID == '' || !is_numeric($image_ID) ){
        return;
	}	


	// Basic setup
    // Start with the oiriginal, large size image as a fallback
    $final_output = '<div class="lazyload bg-image bg-image-' . $image_ID . '"';

	// If the focal point is being used, apply the style directly onto the tag
    if ( $focal_x || $focal_y ) {

        $final_output .= ' style="';
        
        if ( $focal_x ) {
            $final_output .= "background-position-x:" . round($focal_x * 100) . "%;";
        }

        if ( $focal_y ) {
            $final_output .= "background-position-y:" . round($focal_y * 100) . "%;";
        }
        $final_output .= '"';
	}

	// ensure the screen set available, if not, just use the full image
	$scrset = wp_get_attachment_image_srcset($image_ID, $size);
	$source = wp_get_attachment_image_src($image_ID, 'full');
	$is_scrset_exists = $scrset != '' ? $scrset :  $source[0];

	$final_output .= ' data-bgset="' . $is_scrset_exists .'"';
	
	// Call an end to the bg-image tag
	$final_output .= '></div>';
	
	return $final_output;

}


// register ACF unique ID
class ACF_Field_Unique_ID extends acf_field {

	/**
	 * Initialize the class.
	 */
	public static function init() {
		add_action(
			'acf/include_field_types',
			function() {
				if ( ! class_exists( 'acf_field' ) ) {
					return;
				}

				new static();
			}
		);
	}

	/**
	 * Initialize the field.
	 */
	public function __construct() {
		$this->name     = 'unique_id';
		$this->label    = 'Unique ID';
		$this->category = 'basic';

		parent::__construct();
	}

	/**
	 * Render the HTML field.
	 *
	 * @param array $field The field data.
	 */
	public function render_field( $field ) {
		printf(
			'<input type="text" name="%s" value="%s" style="text-transform:uppercase" placeholder="" readonly>',
			esc_attr( $field['name'] ),
			esc_attr( $field['value'] )
		);
	}

	// create unique ID
	public function weber_create_unique_code( $length )  {

		$char = "abcdefghijklmnopqrstuvwxyz0123456789";
		$char = str_shuffle($char);
		for($i = 0, $rand = '', $l = strlen($char) - 1; $i < $length; $i ++) {
			$rand .= $char{mt_rand(0, $l)};
		}

		// ensure its unique
		$is_exist = $this->weber_is_code_exist( $rand );
		if( $is_exist ){
			return $this->weber_create_unique_code( $length );
		}

		return $rand;
	}

	// check whether the access code already generated
	public function weber_is_code_exist( $string ){

		$string = strtolower($string);

		global $wpdb;
		$sql = "
			SELECT * FROM {$wpdb->prefix}postmeta
			WHERE meta_value = '{$string}'
			AND meta_key LIKE '%_access_code'
		";

		$result = $wpdb->get_row($sql);
		return sizeof($result) > 0 ? true : false;

	}

	/**
	 * Define the unique ID if one does not already exist.
	 *
	 * @param string $value   The field value.
	 * @param int    $post_id The post ID.
	 * @param array  $field   The field data.
	 *
	 * @return string The filtered value.
	 */
	public function update_value( $value, $post_id, $field ) {

		if ( ! empty( $value ) ) {
			return $value;
		}

		$access_code = $this->weber_create_unique_code( 8 ); // length 8 character

		return $access_code;
	}
}

new ACF_Field_Unique_ID();


// ACF WEBER
add_filter('acf/pre_render_fields', 'weber_acf_campaign_render', 10, 2);
function weber_acf_campaign_render($fields, $post_id){

	$return = array();
	foreach( (array) $fields as $vv){
		$orig = array();
		foreach($vv as $key => $val){
			$orig[$key] = $val;
		}
		$orig['_weber_post_id'] = $post_id;
		$return[] = $orig;
	}

	return $return;

}

// create database
add_action('admin_init', 'weber_create_business_code_table');
function weber_create_business_code_table(){

		// create custom database
        // only need this to run once
        if( get_option('weber_business_code_database_create') != 'true' ){
           
            global $wpdb;

            $table_name = $wpdb->prefix.'weber_business_code';
            $create_table_query = 'CREATE TABLE IF NOT EXISTS '.$table_name.'(
                ID bigint(20) NOT NULL AUTO_INCREMENT,
                campaign_id bigint(20) NOT NULL,
				business_code varchar(100) NOT NULL UNIQUE,
				status varchar(100) NOT NULL,
                discount varchar(100) NOT NULL,
                partner_id bigint(20) NOT NULL,
                partner_name varchar(100) NOT NULL,
                campaign_name varchar(100) NOT NULL,
                start_date datetime NOT NULL DEFAULT "0000-00-00 00:00:00",
                end_date datetime NOT NULL DEFAULT "0000-00-00 00:00:00",
                PRIMARY KEY (ID))';
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );    
			dbDelta( $create_table_query );
			
			$campaign_table_name = $wpdb->prefix.'weber_business_campaign';
            $campaign_run_query = 'CREATE TABLE IF NOT EXISTS '.$campaign_table_name.'(
                ID bigint(20) NOT NULL AUTO_INCREMENT,
				campaign_name varchar(100) NOT NULL,
				status varchar(100) NOT NULL,
                discount varchar(100) NOT NULL,
                partner_id bigint(20) NOT NULL,
				partner_name varchar(100) NOT NULL,
				total_code bigint(20) NOT NULL,
                start_date datetime NOT NULL DEFAULT "0000-00-00 00:00:00",
                end_date datetime NOT NULL DEFAULT "0000-00-00 00:00:00",
                PRIMARY KEY (ID))';
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );    
            dbDelta( $campaign_run_query );

            // indicate it has been running
            update_option('weber_business_code_database_create', 'true');

        }

}


// register ACF Weber Campaign Code
class ACF_Weber_Campaign extends acf_field {

	/**
	 * Initialize the class.
	 */
	public static function init() {
		add_action(
			'acf/include_field_types',
			function() {
				if ( ! class_exists( 'acf_field' ) ) {
					return;
				}

				new static();
			}
        );

	}

	/**
	 * Initialize the field.
	 */
	public function __construct() {
		$this->name     = 'campaign_generator';
		$this->label    = 'Campaign Generator';
		$this->category = 'basic';

		parent::__construct();
	}

	/**
	 * Render the HTML field.
	 *
	 * @param array $field The field data.
	 */
	public function render_field( $field ) {

		// check whether discount already stated
		$post_id = $field['_weber_post_id'];
		$discount = get_post_meta($post_id, 'partner_discount', true);

		wp_register_style( 'weber-jquery-ui', THEME_URL . '/jquery-ui.css',null, 1.1 );
    	wp_enqueue_style( 'weber-jquery-ui' );  

        ?>
        <div id="weber-business-codes">
            <table id="weber-business-codes-table" class="acf-table acf-repeater">
				<thead>
					<tr>
						<th class="acf-row-handle"></th>
						<th class="acf-th">Campaign Name</th>
						<th class="acf-th">Discount</th>
						<th class="acf-th">Start Date</th>
						<th class="acf-th">End Date</th>
						<th class="acf-th" style="width: 260px;">Action</th>
					</tr>
				</thead>
				<?php
					$all_campaign = weber_unique_code_tr( $post_id );
					if( trim($all_campaign) == ''){
						echo '<tbody id="replace--results"><tr><td colspan="7" align="center">No campaign found</td></tr></tbody>';
					}else{
						echo $all_campaign;
					}
				?>
            </table>

			
			<?php
				if( !is_numeric($discount)):
					echo '<div class="add--campaign-warning">';
					echo '<h3 style="margin-bottom:5px">Add Campaign</h3>';
					echo 'To add a campaign, please publish the post first.';
					echo '</div>';
                // Edit Start  //
				elseif(trim($all_campaign) == ''):
                    echo weber_addnewCampaign_html($post_id);
                endif;?>

        </div>
        <?php
        // Edit End  //
		/*
		printf(
			'<input type="text" name="%s" value="%s" style="text-transform:uppercase" placeholder="" readonly>',
			esc_attr( $field['name'] ),
			esc_attr( $field['value'] )
		);*/
	}

	// create unique ID
	public function weber_create_unique_code( $length )  {

		$char = "abcdefghijklmnopqrstuvwxyz0123456789";
		$char = str_shuffle($char);
		for($i = 0, $rand = '', $l = strlen($char) - 1; $i < $length; $i ++) {
			$rand .= $char{mt_rand(0, $l)};
		}

		// ensure its unique
		$is_exist = $this->weber_is_code_exist( $rand );
		if( $is_exist ){
			return $this->weber_create_unique_code( $length );
		}

		return $rand;
	}

	// check whether the access code already generated
	public function weber_is_code_exist( $string ){

		$string = strtolower($string);

		global $wpdb;
		$sql = "
			SELECT * FROM {$wpdb->prefix}postmeta
			WHERE meta_value = '{$string}'
			AND meta_key LIKE '%_business_code'
		";

		$result = $wpdb->get_row($sql);
		return sizeof($result) > 0 ? true : false;

	}

	/**
	 * Define the unique ID if one does not already exist.
	 *
	 * @param string $value   The field value.
	 * @param int    $post_id The post ID.
	 * @param array  $field   The field data.
	 *
	 * @return string The filtered value.
	 */
	public function update_value( $value, $post_id, $field ) {

		if ( ! empty( $value ) ) {
			return $value;
		}

		$business_code = $this->weber_create_unique_code( 8 ); // length 8 character

		return 'saved'; $business_code;
	}
}

new ACF_Weber_Campaign();