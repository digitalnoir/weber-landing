<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cha="http://www.opensys.com.au/ChainIT/4.0/ChainITDataServices">
   <soapenv:Header/>
   <soapenv:Body>
      <cha:GetChargeQuote2>
   
         <!-- Session Credentials Available By following the "Login Web Service Call"
         https://flipgroup.atlassian.net/wiki/spaces/FLIP/pages/703102977/2a+Example+Web+Service+Call+Login-->      
         <?php echo get_option('weber_oneflow_session'); ?>
 
         <?php
            // dispatch date calculate for next business day
            $now = current_time('mysql');
            $next_business_day = date('Y-m-d', strtotime($now . ' +1 Weekday'));
         ?>

         <cha:effectiveDate><?php echo $next_business_day ?></cha:effectiveDate>
         <!--Mandatory:-->
         <cha:senderLocation><?php echo $cart_item['warehouse_suburb'] ?></cha:senderLocation>
         <!--Mandatory:-->
         <cha:senderPostcode><?php echo $cart_item['warehouse_postcode'] ?></cha:senderPostcode>
         <!--Mandatory:-->


         <cha:receiverLocation><?php echo $cart_item['customer_suburb'] ?></cha:receiverLocation>
         <!--Mandatory:-->
         <cha:receiverPostcode><?php echo $cart_item['customer_postcode'] ?></cha:receiverPostcode>
         <!--Mandatory:-->
         <cha:receiverCountryCode>AU</cha:receiverCountryCode>
         <!--Mandatory:-->


         <cha:items><?php echo $cart_item['total_item'] ?></cha:items>
         <!--Mandatory:-->
         <cha:weight><?php echo $cart_item['total_weight'] ?></cha:weight>
         <!--Mandatory:-->
         <cha:volume><?php echo $cart_item['total_volume'] ?></cha:volume>
         <!--Mandatory:-->
 
      </cha:GetChargeQuote2>
   </soapenv:Body>
</soapenv:Envelope>