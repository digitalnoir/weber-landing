<?php

require THEME_DIR . '/inc/freight-function.php';

//remove element
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);


// woocommerce support
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
    
	// add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}



// CHANGE PLACEHOLDER IMAGE
add_action( 'init', 'weber_fix_thumbnail' );
function weber_fix_thumbnail() {
    add_filter('woocommerce_placeholder_img_src', 'custom_woocommerce_placeholder_img_src');
   
    function custom_woocommerce_placeholder_img_src( $src ) {
        
        $is_accessories = get_post_meta(get_the_ID(),'product_type',true);
        
        if($is_accessories != 'accessories'){
            $src = get_bloginfo('template_url') . '/img/placeholder.png';
        }else{
            $src = get_bloginfo('template_url') . '/img/placeholder-accessories.png';
        }
         
        return $src;
    }
}

// ADD AJAX URL
add_action('wp_head','pluginname_ajaxurl');
function pluginname_ajaxurl() {
    ?>
    <script type="text/javascript">
    var weber_ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
    <?php
}

function register_session(){
    if( !session_id() )
        session_start();
}
add_action('init','register_session');

// COMPARE SYSTEM
add_action( 'wp_ajax_weber_compare_function', 'weber_compare_function' );
add_action( 'wp_ajax_nopriv_weber_compare_function', 'weber_compare_function' );
function weber_compare_function() {
    
    $security = $_POST['security'];
    $product_id = $_POST['product_id'];
    $compare_action = $_POST['compare_action'];

    // security is not working with wp rocket
    /* if ( ! wp_verify_nonce( $security, 'compare-check' ) ) {
        exit;
    }  */
        
    if( !session_id())
     session_start();
                
    $curent_items = $_SESSION['compare_item'];
    if(sizeof($curent_items) < 4){  
    
        if($compare_action === 'add'){
            if(empty($curent_items) && !is_array($curent_items) ){
                    $compare_items = array();
                    array_push($compare_items,$product_id);
                    $update_array = array_unique($compare_items);
                    $_SESSION['compare_item'] = $update_array;
            }else{
                    array_push($curent_items,$product_id);
                    $update_array = array_unique($curent_items);
                    $_SESSION['compare_item'] = $update_array;
            }
        }
        
        if($compare_action === 'remove'){
            if(($key = array_search($product_id, $curent_items)) !== false) {
                
                    unset($curent_items[$key]);
                    $_SESSION['compare_item'] = $curent_items;
            }
        }
    
        $slot_available = true;
    }else{
        
        if($compare_action === 'remove'){
            if(($key = array_search($product_id, $curent_items)) !== false) {
                
                    unset($curent_items[$key]);
                    $_SESSION['compare_item'] = $curent_items;
            }
        }
        
        $slot_available = false;
    }
    
    $qty_compare = sizeof($_SESSION['compare_item']);
    $is_active = (($qty_compare > 0) ? true : false);
    $in_array = (in_array($product_id, $_SESSION['compare_item']) ? true : false);



    echo json_encode(   array(
                                    'is_added'      => $slot_available,
                                    'is_active'     => $is_active,
                                    'in_array'      => $in_array,
                                    'compare_qty'   => $qty_compare,
                                    ));
    
    die();
        
        
}


// ADD COMPARE
add_action( 'wp_ajax_weber_compare_add', 'weber_compare_add' );
add_action( 'wp_ajax_nopriv_weber_compare_add', 'weber_compare_add' );
function weber_compare_add() {
    $security = $_POST['security'];
    $product_id = $_POST['product_id'];
    
    /* if ( ! wp_verify_nonce( $security, 'compare-add-select' ) ) {
        exit;
    }  */
    if( !session_id())
     session_start();
                
    $curent_items = $_SESSION['compare_item'];
    if(sizeof($curent_items) < 4){  
        if(empty($curent_items) && !is_array($curent_items) ){
                    $compare_items = array();
                    array_push($compare_items,$product_id);
                    $update_array = array_unique($compare_items);
                    $_SESSION['compare_item'] = $update_array;
        }else{
                    array_push($curent_items,$product_id);
                    $update_array = array_unique($curent_items);
                    $_SESSION['compare_item'] = $update_array;
        }
    }
    $in_array = (in_array($product_id, $_SESSION['compare_item']) ? true : false);
    echo json_encode(   array(
                                    'in_array'      => 'aa',
                                    ));
    
    
    die();
    
}

// DELETE COMPARE
add_action( 'wp_ajax_weber_compare_delete', 'weber_compare_delete' );
add_action( 'wp_ajax_nopriv_weber_compare_delete', 'weber_compare_delete' );
function weber_compare_delete() {
    
    $security = $_POST['security'];
    $product_id = $_POST['product_id'];
    
    /* if ( ! wp_verify_nonce( $security, 'compare-delete' ) ) {
        exit;
    } */ 
        
    if( !session_id())
     session_start();
                
    $curent_items = $_SESSION['compare_item'];
    if(($key = array_search($product_id, $curent_items)) !== false) {
        unset($curent_items[$key]);
        $_SESSION['compare_item'] = $curent_items;
    }
    
    $in_array = (in_array($product_id, $_SESSION['compare_item']) ? true : false);
    echo json_encode(   array(
                                    'in_array'      => $in_array,
                                    ));
    
    
    die();
}

function in_array_r($needle, $haystack, $strict = false) {
    if(is_array($haystack)){
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }
    }

    return false;
}

// REFINE SEARCH PRODUCT BBQ
add_action( 'pre_get_posts', 'weber_refine_product_filter' );
function weber_refine_product_filter( $query ) {
    if ( ! is_admin() && $query->is_main_query() )
        if(isset($_GET['filter-shop']) && $_GET['filter-shop'] == true) :
        
        $filter_range_array = $filter_size_array = $filter_application_array = array();
        
        $filter_range = $_GET['filter-range'];
        $filter_size = $_GET['filter-size'];
        $filter_application = $_GET['filter-application'];
        
        array_push($filter_range_array,$filter_range);
        array_push($filter_size_array,$filter_size);
        array_push($filter_application_array,$filter_application);

        
        set_query_var( 'filter_range', $filter_range_array );
        set_query_var( 'filter_size', $filter_size_array );
        set_query_var( 'filter_application', $filter_application_array );


        if(sizeof($filter_range_array[0]) > 0){
            $tax_range = array(
            'taxonomy' => 'range',
            'field' => 'id',
            'terms' => $filter_range_array[0],
            'operator'=> 'IN'
        );
        }
        
        if(sizeof($filter_size_array[0]) > 0){
            $tax_size = array(
            'taxonomy' => 'size',
            'field' => 'id',
            'terms' => $filter_size_array[0],
            'operator'=> 'IN'
        );
        }
        
        if(sizeof($filter_application_array[0]) > 0){
            $tax_application = array(
            'taxonomy' => 'application',
            'field' => 'id',
            'terms' => $filter_application_array[0],
            'operator'=> 'IN'
        );
        }
        
        
    $taxquery = array(
                'relation' => 'AND',
        $tax_range,
                $tax_size,
                $tax_application
    );

    $query->set( 'tax_query', $taxquery );
        endif;
        
        return $query;
                
}

// ONLY SHOW PRODUCT BBQ ON ARCHIVE PRODUCT
add_action( 'pre_get_posts', 'weber_exclude_accessories_from_shop' );
function weber_exclude_accessories_from_shop( $query ) {

    if ( ! is_admin() && $query->is_main_query() && is_product_category() ) :
        
        if(is_product_category(15)){
            $meta_query = array(
                'key' => 'product_type',
                'value' => 'accessories',
                'compare' => '='
            );
        }else{
            $meta_query = array(
                'key' => 'product_type',
                'value' => 'accessories',
                'compare' => '!='
            );
        }

        $query->set( 'meta_key', 'product_type' );
        $query->set( 'meta_query', $meta_query );
        
    endif;
        
    return $query;
        
}
// ADD CUSTOM ORDERING
add_filter( 'woocommerce_get_catalog_ordering_args', 'custom_woocommerce_get_catalog_ordering_args' );
function custom_woocommerce_get_catalog_ordering_args( $args ) {
  $orderby_value = isset( $_GET['orderby'] ) ? woocommerce_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );

    if ( 'name_asc' == $orderby_value ) {
        $args['orderby'] = 'title';
        $args['order'] = 'ASC';
        $args['meta_key'] = '';
    }
    
    if ( 'name_desc' == $orderby_value ) {
        $args['orderby'] = 'title';
        $args['order'] = 'DESC';
        $args['meta_key'] = '';
    }

    return $args;
}

add_filter( 'woocommerce_default_catalog_orderby_options', 'custom_woocommerce_catalog_orderby' );
add_filter( 'woocommerce_catalog_orderby', 'custom_woocommerce_catalog_orderby' );

function custom_woocommerce_catalog_orderby( $sortby ) {
    $sortby['name_asc'] = 'A > Z';
    $sortby['name_desc'] = 'Z > A';
    return $sortby;
}

add_filter('woocommerce_default_catalog_orderby', 'weber_default_catalog_orderby');
function weber_default_catalog_orderby() {
     return 'date'; // Can also use title and price
}


function weber_accessories_relationship( $args, $field, $post ){
    // increase the posts per page
    $args['meta_key'] = 'product_type';
        $args['meta_value'] = 'product';


    return $args;
}
// filter for a specific field based on it's name
add_filter('acf/fields/relationship/query/name=accessories_for', 'weber_accessories_relationship', 10, 3);
add_filter('acf/fields/relationship/query/name=specific_video_for', 'weber_accessories_relationship', 10, 3);
add_filter('acf/fields/relationship/query/name=specific_recipe_for', 'weber_accessories_relationship', 10, 3);





// CHANGE PRODUCT COLOR
// COMPARE SYSTEM
add_action( 'wp_ajax_weber_change_color_product', 'weber_change_color_product' );
add_action( 'wp_ajax_nopriv_weber_change_color_product', 'weber_change_color_product' );
function weber_change_color_product() {
    
    $security = $_POST['security'];
    $color_seq = $_POST['color_seq'] - 1;
    $product_id = $_POST['product_id'];

    /* if ( ! wp_verify_nonce( $security, 'change-color' ) ) {
        exit;
    } */
    
    ob_start();
    
    
                $color_options = get_field('color_choice',$product_id);
                $product_badge = get_field('product_badge',$product_id);
                
                if($product_badge != '' && $product_badge == 'sold_out'){
                    echo '<div class="sold-out-badge"><img src="'.get_bloginfo('template_url').'/img/badge-sold-out.png" alt="" /></div>';
                }
                
                
                $gallery_single_image = get_field('gallery_color',$product_id);     
                $first_image = $gallery_single_image[$color_seq]['gallery_product_image'];
                echo '<img src="'.$first_image['url'].'" width="'.$first_image['width'].'" height="'.$first_image['height'].'" alt="'.$first_image['alt'].'" />';

    
    
    $output_html = ob_get_contents();
    ob_end_clean();
    
    echo json_encode(   array(
                                    'html'      => $output_html,
                                    ));
    
    die();
    
    
}

function weber_pagination($pages = '', $range = 4){  
     $showitems = ($range * 2)+1;  
 
     global $paged;
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
 
     if(1 != $pages)
     {
         echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
          if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
  
          for ($i=1; $i <= $pages; $i++)
          {
              if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
              {
                  echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
              }
          }
  
          if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";  
          if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
          echo "</div>\n";
      }
 }


// AJAX FILTERING SHOULB WORKING
add_action( 'wp_ajax_weber_filter_ajax', 'weber_filter_ajax' );
add_action( 'wp_ajax_nopriv_weber_filter_ajax', 'weber_filter_ajax' );
function weber_filter_ajax(){
    

    // data
    $range_array             = $_POST['filter-range'];
    $series_array            = $_POST['filter-series'];
    $size_array              = $_POST['filter-size'];
    $application_array = $_POST['filter-application'];
    $type_array              = $_POST['filter-type'];
    $paged                       = !empty($_POST['pagination']) ? $_POST['pagination'] : 1;
    $product_type            = $_POST['product_type'];
    $product_spec_for    = $_POST['product_id'];
    $search_term             = $_POST['search-accessories'];
    
    $compare_accessories = ($product_type == 'product') ? '!=' : '=' ;
    
    
    
    
    
    $children =   array();                  
    $i =0;
    if(is_array($range_array)):
        foreach($range_array as $parent){
        $i++;
                                    
        $childs[$i] = get_terms( 'range', 'child_of='.$parent );
        foreach($childs[$i] as $child[$i]){
            if(is_array($series_array)):
                if(in_array($child[$i]->term_id,$series_array)){
                    array_push($children,$child[$i]->term_id);
                    $has_child = true;
                }
                endif;
            }
            if(!$has_child){
                array_push($children,$parent);
            }
            $has_child = false; // reset
            
        }
        $sum_range_array = array_unique($children);
        
    endif;
    
    
    if(sizeof($sum_range_array) > 0){
            $tax_range = array(
            'taxonomy' => 'range',
            'field' => 'id',
            'terms' => $sum_range_array,
            'operator'=> 'IN'
        );
        }else{
            if(sizeof($series_array) > 0){
                $tax_range = array(
            'taxonomy' => 'range',
            'field' => 'id',
            'terms' => $series_array,
            'operator'=> 'IN'
        );
            }
        }
    
        
        if(sizeof($size_array) > 0){
            $tax_size = array(
            'taxonomy' => 'size',
            'field' => 'id',
            'terms' => $size_array,
            'operator'=> 'IN'
        );
        }
        
        if(sizeof($application_array) > 0){
            $tax_application = array(
            'taxonomy' => 'application',
            'field' => 'id',
            'terms' => $application_array,
            'operator'=> 'IN'
        );
        }
        
        if(sizeof($type_array) > 0){
            $tax_type = array(
            'taxonomy' => 'type',
            'field' => 'id',
            'terms' => $type_array,
            'operator'=> 'IN'
        );
        }
        
        
    $taxquery = array(
                'relation' => 'AND',
        $tax_range,
                $tax_size,
                $tax_application,
                $tax_type
    );
        
        
        
        if($product_spec_for != ''){
                        $args = array(
                            'post_type' => 'product',
                            'paged'         => $paged,
                            'post_status' => 'publish',
                            'meta_query' => array(
                                            'relation' => 'AND',
                                            array(
                                                'key' => 'product_type',
                                                'value' => 'accessories',
                                                'compare' => '='
                                            ),
                                            array(
                                                'key' => 'accessories_for',
                                                'value' => $product_spec_for,
                                                'compare' => 'LIKE'
                                            )
                            ),
                            'tax_query' => $taxquery,
                        );
        }else{
                        $args = array(
                            'post_type' => 'product',
                            'paged'         => $paged,
                            'meta_key' => 'product_type',
                            'post_status' => 'publish',
                            'meta_query' => array(
                                    'key' => 'product_type',
                                    'value' => 'accessories',
                                    'compare' => $compare_accessories
                            ),
                            'tax_query' => $taxquery,
                        );
        }
        
        // search
        $show_clear_search = false;
        if($search_term != ''){
            $args['s'] = $search_term;
            $show_clear_search = true;
        }
        
        
        
        // orderby
        $orderby    = $_POST['orderby'];
        if ( 'name_asc' == $orderby ) {
            $args['orderby'] = 'title';
            $args['order'] = 'ASC';

        }
        if ( 'price' == $orderby ) {
            $args['orderby']  = "meta_value_num";
            $args['order']    = 'ASC';
            $args['meta_key'] = '_price';
        }
        if ( 'price-desc' == $orderby ) {
            $args['orderby']  = "meta_value_num";
            $args['order']    = 'DESC';
            $args['meta_key'] = '_price';
        }
        if ( 'date' == $orderby ) {
            $args['orderby']  = "date";
            $args['order']    = 'DESC';         

        }

        // REFINE POSTS_PER_PAGE
        $args['posts_per_page'] = 15;
        
        ob_start();

        
        $the_query = new WP_Query( $args );
        
                do_action( 'woocommerce_before_main_content' ); ?>
                <div class="loading-infinite"><div class="circle"><div class="inner"></div></div><div class="circle">   <div class="inner"></div></div><div class="circle"> <div class="inner"></div></div><div class="circle"> <div class="inner"></div></div><div class="circle"> <div class="inner"></div></div>
</div>
                <?php do_action( 'woocommerce_archive_description' ); ?>
                <?php if ( $the_query->have_posts() ) : ?>
                    <?php woocommerce_product_loop_start(); ?>
                    <?php //woocommerce_product_subcategories(); ?>
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    
                            <?php 
                            if($product_type == 'product'){
                                wc_get_template_part( 'content', 'product' );
                            }
                            if($product_type == 'accessories'){
                                wc_get_template_part( 'content', 'product-accessories' );
                            }
                            ?>
                            
                    <?php endwhile; // end of the loop. ?>
                    <?php woocommerce_product_loop_end();
                    
                    if ( $the_query->max_num_pages > 1 ) {
                        
                    
                    ?>
                    
                    
                    <?php }
                    
                    echo '<div id="loadmore-trigger" data-paged="2"><div class="loading-infinite"><div class="circle"><div class="inner"></div></div><div class="circle">   <div class="inner"></div></div><div class="circle"> <div class="inner"></div></div><div class="circle"> <div class="inner"></div></div><div class="circle"> <div class="inner"></div></div>
</div></div>';
 ?>
                    
                    <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
                    <?php wc_get_template( 'loop/no-products-found.php' ); ?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
                <?php do_action( 'woocommerce_after_main_content' );
                
                
            
        /*
        
        echo '<pre>';
        print_r($args);
        echo '</pre>';
    */
        
        
        $output_html = ob_get_contents();
        ob_end_clean();
        

        $whici_child_active = !empty($range_array) ? $range_array : array();

    echo json_encode(   array(
                                    'html'      => $output_html,
                                    'only_show' => $whici_child_active,
                                    'show_clear' => $show_clear_search
                                    ));
    
    die();
    
    
}


// ADD FILTERING ON BACKEND
add_action( 'restrict_manage_posts', 'weber_add_fitering_products' );
function weber_add_fitering_products(){

    $type = 'product';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }

    //only add filter to post type you want
    if ('product' == $type){
        //change this to the list of values you want to show
        //in 'label' => 'value' format
        $values = array(
            'BBQ Product' => 'product', 
            'Accessories' => 'accessories',
        );
        ?>
        <select name="product_type_bbq">
        <option value=""><?php _e('Show All Product', 'weber'); ?></option>
        <?php
            $current_v = isset($_GET['product_type_bbq'])? $_GET['product_type_bbq']:'';
            foreach ($values as $label => $value) {
                printf
                    (
                        '<option value="%s"%s>%s</option>',
                        $value,
                        $value == $current_v? ' selected="selected"':'',
                        $label
                    );
                }
        ?>
        </select>
                
                
                
                
                <?php /* RANGE FILTER */ /*?>
                <select name="product_range_filter" id="product_range_filter">
        <option value=""><?php _e('Show All Range', 'weber'); ?></option>
        <?php
                
                $range_terms = get_terms('range',array(
                                                    'hide_empty'        => false, 
                                                    'parent'                        => 0,
                                                    'fields'            => 'all', 
                                                    'hierarchical'      => true,
                                                    'cache_domain'      => 'core'
                                            ));
                $parent_arr = array();
                foreach($range_terms as $term){
                    $parent_arr[$term->name] = $term->term_id;
                }
                
                $range_values = $parent_arr;
                
            $current_range = isset($_GET['product_range_filter'])? $_GET['product_range_filter']:'';
            foreach ($range_values as $label => $value) {
                printf(
                        '<option value="%s"%s>%s</option>',
                        $value,
                        $value == $current_range ? ' selected="selected"':'',
                        $label
                    );
                }
        ?>
        </select>
                */ ?>
                
                
                
                
                <?php /* SERIES FILTER */ ?>
                <span id="series-select-filter">
                <?php
                if( isset($_GET['product_range_filter']) && $_GET['product_range_filter'] != '' ) :
                ?>
                <select name="product_series_filter" id="product_series_filter">
        <option value=""><?php _e('Show All Series', 'weber'); ?></option>
        <?php
                
                $range_terms = get_terms('range',array(
                                                    'hide_empty'        => false, 
                                                    'parent'                        => $_GET['product_range_filter'],
                                                    'fields'            => 'all', 
                                                    'hierarchical'      => true,
                                                    'cache_domain'      => 'core'
                                            ));
                $parent_arr = array();
                foreach($range_terms as $term){
                    $parent_arr[$term->name] = $term->term_id;
                }
                
                $range_values = $parent_arr;
                
            $current_range = isset($_GET['product_series_filter'])? $_GET['product_series_filter']:'';
            foreach ($range_values as $label => $value) {
                printf(
                        '<option value="%s"%s>%s</option>',
                        $value,
                        $value == $current_range ? ' selected="selected"':'',
                        $label
                    );
                }
        ?>
        </select>
                
                <?php endif; ?>
                </span>
                
        <?php
    }
}

// get query variable for range series filtering in the backend
add_filter( 'parse_query', 'weber_add_fitering_products_filter' );
function weber_add_fitering_products_filter( $query ){
    global $pagenow;
    $type = 'product';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }
    if ( 'product' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['product_type_bbq']) && $_GET['product_type_bbq'] != '') {
        $query->query_vars['meta_key'] = 'product_type';
        $query->query_vars['meta_value'] = $_GET['product_type_bbq'];
    }
        
        if ( 'product' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['product_range_filter']) && $_GET['product_range_filter'] != '' && $_GET['product_series_filter'] == '') {
        $get_term_slud = get_term_by('id',$_GET['product_range_filter'],'range');
        $query->query_vars['range'] = $get_term_slud->slug;
    }
        
        if ( 'product' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['product_range_filter']) && $_GET['product_range_filter'] != '' && $_GET['product_series_filter'] != '') {
        $get_term_slug = get_term_by('id',$_GET['product_series_filter'],'range');
        $query->query_vars['range'] = $get_term_slug->slug;
    }
        
}

// filter ajax admin
add_action( 'wp_ajax_weber_filter_series_range_ajax', 'weber_filter_series_range_ajax' );
add_action( 'wp_ajax_nopriv_weber_filter_series_range_ajax', 'weber_filter_series_range_ajax' );
function weber_filter_series_range_ajax(){
    
    $parent_range = $_POST['parent_range'] ? $_POST['parent_range'] : '';
    
    ob_start();
    ?>
    <select name="product_series_filter" id="product_series_filter">
        <option value=""><?php _e('Show All Series', 'weber'); ?></option>
        <?php
                
                $range_terms = get_terms('range',array(
                                                    'hide_empty'        => false, 
                                                    'parent'                        => $parent_range,
                                                    'fields'            => 'all', 
                                                    'hierarchical'      => true,
                                                    'cache_domain'      => 'core'
                                            ));
                $parent_arr = array();
                foreach($range_terms as $term){
                    $parent_arr[$term->name] = $term->term_id;
                }
                
                $range_values = $parent_arr;
                
   
            foreach ($range_values as $label => $value) {
                printf(
                        '<option value="%s">%s</option>',
                        $value,
                        $label
                    );
                }
        ?>
        </select>
    <?php
    $output_html = ob_get_contents();
    ob_end_clean();


    echo json_encode(   array(
                                    'html'      => $output_html,
                                    ));
    
    die();
}

// CLEANING WOO COLUMN IN BACKEND
add_filter( 'manage_edit-product_columns', 'weber_woo_manage_columns', 10, 1 );
function weber_woo_manage_columns( $columns ) { 
    
    //unset( $columns['sku'] );
    unset( $columns['is_in_stock'] );
    //unset( $columns['product_cat'] );
    unset( $columns['product_tag'] );
    unset( $columns['featured'] );
    unset( $columns['product_type'] );

    return $columns;
}




// LOAD MORE AJAXSIS
add_action( 'wp_ajax_weber_load_more_product', 'weber_load_more_product' );
add_action( 'wp_ajax_nopriv_weber_load_more_product', 'weber_load_more_product' );
function weber_load_more_product(){
    
    $paged                       = $_POST['page_no'];

    // data
    $range_array             = $_POST['filter-range'];
    $series_array            = $_POST['filter-series'];
    $size_array              = $_POST['filter-size'];
    $application_array = $_POST['filter-application'];
    $type_array              = $_POST['filter-type'];
    
    $product_type            = $_POST['product_type'];
    $product_spec_for    = $_POST['product_id'];
    $compare_accessories = ($product_type == 'product') ? '!=' : '=' ;
    $search_term             = $_POST['search-accessories'];
    
    $is_mobile               = $_POST['is_mobile'];
    
    
    
    
    
    $children =   array();                  
    $i =0;
    if(is_array($range_array)):
        foreach($range_array as $parent){
        $i++;
                                    
        $childs[$i] = get_terms( 'range', 'child_of='.$parent );
        foreach($childs[$i] as $child[$i]){
            if(is_array($series_array)):
                if(in_array($child[$i]->term_id,$series_array)){
                    array_push($children,$child[$i]->term_id);
                    $has_child = true;
                }
                endif;
            }
            if(!$has_child){
                array_push($children,$parent);
            }
            $has_child = false; // reset
            
        }
        $sum_range_array = array_unique($children);
        
    endif;
    
    
    if(sizeof($sum_range_array) > 0){
            $tax_range = array(
            'taxonomy' => 'range',
            'field' => 'id',
            'terms' => $sum_range_array,
            'operator'=> 'IN'
        );
        }else{
            if(sizeof($series_array) > 0){
                $tax_range = array(
            'taxonomy' => 'range',
            'field' => 'id',
            'terms' => $series_array,
            'operator'=> 'IN'
        );
            }
        }
    
        
        if(sizeof($size_array) > 0){
            $tax_size = array(
            'taxonomy' => 'size',
            'field' => 'id',
            'terms' => $size_array,
            'operator'=> 'IN'
        );
        }
        
        if(sizeof($application_array) > 0){
            $tax_application = array(
            'taxonomy' => 'application',
            'field' => 'id',
            'terms' => $application_array,
            'operator'=> 'IN'
        );
        }
        
        if(sizeof($type_array) > 0){
            $tax_type = array(
            'taxonomy' => 'type',
            'field' => 'id',
            'terms' => $type_array,
            'operator'=> 'IN'
        );
        }
        
        
    $taxquery = array(
                'relation' => 'AND',
        $tax_range,
                $tax_size,
                $tax_application,
                $tax_type
    );
        
        
        if($is_mobile == 'false'){
            if($product_spec_for != ''){
                            $args = array(
                                'post_type' => 'product',
                                'paged'         => $paged,
                                'post_status' => 'publish',
                                'meta_query' => array(
                                                'relation' => 'AND',
                                                array(
                                                    'key' => 'product_type',
                                                    'value' => 'accessories',
                                                    'compare' => '='
                                                ),
                                                array(
                                                    'key' => 'accessories_for',
                                                    'value' => $product_spec_for,
                                                    'compare' => 'LIKE'
                                                )
                                ),
                                'tax_query' => $taxquery,
                            );
            }else{
                            $args = array(
                                'post_type' => 'product',
                                'paged'         => $paged,
                                'meta_key' => 'product_type',
                                'post_status' => 'publish',
                                'meta_query' => array(
                                        'key' => 'product_type',
                                        'value' => 'accessories',
                                        'compare' => $compare_accessories
                                ),
                                'tax_query' => $taxquery,
                            );
            }
        }else{
            if($product_spec_for != ''){
                            $args = array(
                                'post_type' => 'product',
                                'paged'         => $paged,
                                'posts_per_page'    => 4,
                                'post_status' => 'publish',
                                'meta_query' => array(
                                                'relation' => 'AND',
                                                array(
                                                    'key' => 'product_type',
                                                    'value' => 'accessories',
                                                    'compare' => '='
                                                ),
                                                array(
                                                    'key' => 'accessories_for',
                                                    'value' => $product_spec_for,
                                                    'compare' => 'LIKE'
                                                )
                                ),
                                'tax_query' => $taxquery,
                            );
            }else{
                            $args = array(
                                'post_type' => 'product',
                                'paged'         => $paged,
                                'posts_per_page'    => 4,
                                'post_status' => 'publish',
                                'meta_key' => 'product_type',
                                'meta_query' => array(
                                        'key' => 'product_type',
                                        'value' => 'accessories',
                                        'compare' => $compare_accessories
                                ),
                                'tax_query' => $taxquery,
                            );
            }
        }
        
        
        // search
        $show_clear_search = false;
        if($search_term != ''){
            $args['s'] = $search_term;
            $show_clear_search = true;
        }
        
        
        
        // orderby
        $orderby    = $_POST['orderby'];
        if ( 'name_asc' == $orderby ) {
            $args['orderby'] = 'title';
            $args['order'] = 'ASC';

        }
        if ( 'price' == $orderby ) {
            $args['orderby']  = "meta_value_num";
            $args['order']    = 'ASC';
            $args['meta_key'] = '_price';
        }
        if ( 'price-desc' == $orderby ) {
            $args['orderby']  = "meta_value_num";
            $args['order']    = 'DESC';
            $args['meta_key'] = '_price';
        }
        if ( 'date' == $orderby ) {
            $args['orderby']  = "date";
            $args['order']    = 'DESC';         

        }

        // REFINE POSTS_PER_PAGE
        $args['posts_per_page'] = 15;
        
        ob_start();

        
        $the_query = new WP_Query( $args );
        
                // do_action( 'woocommerce_before_main_content' ); ?>
                <?php // do_action( 'woocommerce_archive_description' ); ?>
                <?php if ( $the_query->have_posts() ) : ?>
                    <?php // woocommerce_product_loop_start(); ?>
                    <?php // woocommerce_product_subcategories(); ?>
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    
                            <?php 
                            
                            
                            if($product_type == 'product'){
                                if($is_mobile == 'true'){
                                    wc_get_template_part( 'content', 'product' );
                                }else{
                                    wc_get_template_part( 'content', 'product' );
                                }
                            }
                            if($product_type == 'accessories'){
                                if($is_mobile == 'true'){
                                    wc_get_template_part( 'content', 'product-accessories' );
                                }else{
                                    wc_get_template_part( 'content', 'product-accessories' );
                                }
                            }
                            ?>
                            
                    <?php endwhile; // end of the loop. ?>
                    <?php // woocommerce_product_loop_end();
                    
                    $more_post = true;
                    ?>
                    
                    
        

                    
                    <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
                    <?php 
                    $more_post = false; ?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
                <?php // do_action( 'woocommerce_after_main_content' );
            

        
        
        $output_html = ob_get_contents();
        ob_end_clean();
        

        $whici_child_active = !empty($range_array) ? $range_array : array();

    echo json_encode(   array(
                                    'html'      => $output_html,
                                    'only_show' => $whici_child_active,
                                    'more_post' => $more_post,
                                    'show_clear' => $show_clear_search
                                    ));
    
    die();
    
    
}








// RELEVANSSI NEW FILTER
add_filter('relevanssi_block_one_letter_searches','weber_dont_block_one_letter_search', 99);
function weber_dont_block_one_letter_search(){
    return false;
}






// WEBER SEARCH PRODUCT AJAX TOP
add_action( 'wp_ajax_weber_top_search_product', 'weber_top_search_product' );
add_action( 'wp_ajax_nopriv_weber_top_search_product', 'weber_top_search_product');
function weber_top_search_product() {
        global $wpdb,
               $query,
                $wp_query;
        $resultsPosts = array();
        $resultsTerms = array();
        $term         = sanitize_text_field( $_GET['term'] );
        
        

                $queries =  new WP_Query( array(
                    's'                => $term,
                    'posts_per_page'      => -1,
                    'post_type'        => 'product',
                    'post_status' => 'publish',
                    'meta_query' => array(
                                array(
                                    'key' => 'product_type',
                                    'value' => 'product',
                                )
                            )
                ) );

            $queries->query_vars['s'];
            relevanssi_do_query($queries);

            foreach ( $queries->posts as $post ) {
                $tempObject = array(
                    'id'       => $post->ID,
                    'type'     => 'post',
                    'taxonomy' => null,
                    'postType' => $post->post_type
                );
                $linkTitle  = apply_filters( 'the_title', $post->post_title, $post->ID );
                $linkTitle  = apply_filters( 'search_autocomplete_modify_title', $linkTitle, $tempObject );
                
                    $linkURL = get_permalink( $post->ID );
                    $linkURL = apply_filters( 'search_autocomplete_modify_url', $linkURL, $tempObject );
                
                $linkImg = get_the_post_thumbnail( $post->ID,'full' );
                $linkImg = apply_filters( 'search_autocomplete_modify_image', $linkImg, $tempObject );
                
                
                $resultsPosts[] = array(
                    'title' => $linkTitle,
                    'url'   => $linkURL,
                    'imgurl'   => $linkImg,
                );
            }
            
            
            
            $tempTerms = new WP_Query( array(
                    's'                => $term,
                    'numberposts'      => -1,
                    'post_type'        => 'product',
                    'post_status' => 'publish',
                    'meta_query' => array(
                                array(
                                    'key' => 'product_type',
                                    'value' => 'accessories',
                                )
                            )
                ) );
                
                $tempTerms->query_vars['s'];
                relevanssi_do_query($tempTerms);

            foreach ( $tempTerms->posts as $post ) {
                $tempObject = array(
                    'id'       => $post->ID,
                    'type'     => 'post',
                    'taxonomy' => null,
                    'postType' => $post->post_type
                );
                $linkTitle  = apply_filters( 'the_title', $post->post_title, $post->ID );
                $linkTitle  = apply_filters( 'search_autocomplete_modify_title', $linkTitle, $tempObject );
                
                    $linkURL = get_permalink( $post->ID );
                    $linkURL = apply_filters( 'search_autocomplete_modify_url', $linkURL, $tempObject );
                
                $linkImg = get_the_post_thumbnail( $post->ID,'full' );
                $linkImg = apply_filters( 'search_autocomplete_modify_image', $linkImg, $tempObject );
                
                
                $resultsTerms[] = array(
                    'title' => $linkTitle,
                    'url'   => $linkURL,
                    'imgurl'   => $linkImg,
                );
            }
        
        
        
        
        $results = array_merge( $resultsPosts,$resultsTerms );
        

        foreach( $results as $index => $result ) {
            $results[$index]['title'] = html_entity_decode( $result['title'] );
        }

        $results = apply_filters( 'search_autocomplete_modify_results', $results );
        echo json_encode( array( 'results' => array_slice( $results, 0, 99999 ), 'last_term' => $term ) );
        die();
    }


function get_the_slug($ID) {
    $post_data = get_post($ID, ARRAY_A);
    $slug = $post_data['post_name'];
    return $slug; 
}


// CHANGE REWRITE RULES

// shop title
function wc_custom_shop_archive_title( $title ) {
    if ( is_shop() ) {
        return str_replace( __( 'Products Archive - Weber', 'woocommerce' ), 'Browse All Barbecues', $title );
    }

    return $title;
}
add_filter( 'wp_title', 'wc_custom_shop_archive_title' );




// ADD RICH SNIPPET USING JSON RATHER THAN WOO DEFAULT
add_action('wp_head', 'weber_rich_snippet_data');
function weber_rich_snippet_data(){
    global $wp_query;
    
    $post_type = $wp_query->queried_object->post_type;
    $post_id = $wp_query->queried_object->ID;
    $is_single = $wp_query->is_single;
    
    
    if($is_single && $post_type == 'product'){
        
        $title          = 'Weber&reg; '.get_the_title($post_id);
        $content        = get_post_field('post_content', $post_id);
        $permalink  = get_permalink($post_id);
        $sku                = get_post_meta($post_id,'_sku',true) != '' ? get_post_meta($post_id,'_sku',true) : sanitize_title($title);
        $image_1        = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),'full');
        $image_2        = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),'medium');
        
        
        $_price_array = array();
        
        // REGULAR PRICE
        $reg_price  = round(get_post_meta($post_id,'_regular_price',true),0);
        $reg_sku    = $sku.'-'.$reg_price;
        if($reg_price){
            $_price_array[] = array(
                                                "@type" => "Offer",
                                                "@id" => $permalink."#SKU-".$reg_sku,
                                                "price" => intval($reg_price),
                                                "priceCurrency" => "AUD",
                                                "availability" => "http://schema.org/InStock",
                                                "sku" => $reg_sku,
                                                "itemCondition" => "NewCondition"
                                                );
        }
        
                
        // helper
        $sku_second_price = get_post_meta($post_id,'secondary_sku',true) != '' ? get_post_meta($post_id,'secondary_sku',true) : sanitize_title($title);
        
        // SECONDARY PRICE
        $add_price  = round(get_post_meta($post_id,'secondary_rrp',true),0);
        $add_sku    = $sku_second_price.'-'.$add_price;
        if($add_price){
            $_price_array[] = array(
                                                "@type" => "Offer",
                                                "@id" => $permalink."#SKU-".$add_sku,
                                                "price" => intval($add_price),
                                                "priceCurrency" => "AUD",
                                                "availability" => "http://schema.org/InStock",
                                                "sku" => $add_sku,
                                                "itemCondition" => "NewCondition"
                                                );
        }
        
        
        // REPEATER PRICE
        $_is_repeat = get_field('additional_price',$post_id);
        if(is_array($_is_repeat)){
            foreach($_is_repeat as $repeat){
                $_price_array[] = array(
                                                "@type" => "Offer",
                                                "@id" => $permalink."#SKU-additional-".$repeat['additional_price'],
                                                "price" => intval($repeat['additional_price']),
                                                "priceCurrency" => "AUD",
                                                "availability" => "http://schema.org/InStock",
                                                "sku" => "SKU-additional-".$repeat['additional_price'],
                                                "itemCondition" => "NewCondition"
                                                );
            }
        }
        
        
        $arr = array(
            "@context" => "http://schema.org",
            "@type" => "Product",
            "name" => $title,
            "description" => $content,
            "url" => $permalink,
            "sku" => $sku,
            "image" => array(
                                    array(
                                        "@type" => "ImageObject",
                                        "url"=> $image_1[0],
                                        "height" => $image_1[2],
                                        "width" => $image_1[1]
                                    ),
                                    array(
                                        "@type"=> "ImageObject",
                                        "url"=> $image_2[0],
                                        "height" => $image_2[2],
                                        "width" => $image_2[1]
                                    ),
                            ),
            "offers" => $_price_array
            
            
        );
        echo "\n\r<script type='application/ld+json'>";
        echo json_encode($arr);
        echo "</script>\n\r\n\r";
        
    }

}

// DYNAMIC COMPARED DATA
add_action('template_redirect','weber_dynamic_compare',99);
function weber_dynamic_compare(){
    if ($_SERVER['REQUEST_URI'] == '/product-in-compare/') {
        global $wp_query;
        $wp_query->is_404 = false;
        status_header(200);
        weber_print_product_in_compare();
        exit();
    }
}

function weber_print_product_in_compare(){

    header('Content-Type: application/json');
    
    $sess = $_SESSION['compare_item'];
    $check_compare = is_array( $sess ) ? $sess : array();

    echo json_encode($check_compare);
    

}

// CLEAR WP ROCKET CACHE EVERYTIME POST IS UPDATE
$actions = array(
    'save_post',            // Save a post
    'deleted_post',         // Delete a post
    'trashed_post',         // Empty Trashed post
    'edit_post',            // Edit a post - includes leaving comments
    'delete_attachment',    // Delete an attachment - includes re-uploading
);
    // Add the action for each event
foreach ( $actions as $event ) {
    add_action( $event, 'weber_wp_rocket_purge_all', 10 );
}

function weber_wp_rocket_purge_all() {
    if ( !defined('WP_ROCKET_VERSION') ) {
        return false;
    }
    rocket_clean_domain();
    return true;
}

// FIX VISUAL COMPOSER NOT SHOWING WITH NEWEST WOOCOMMERCE
add_filter('woocommerce_shop_manager_editable_roles', 'weber_fix_shop_manager_role', 50, 1);
function weber_fix_shop_manager_role($roles){

    $roles[] = 'shop_manager';  

    return $roles;
}


/////////////////////////////
//// Session management  ////
/////////////////////////////


// helper check session
function weber_check_valid_session(){

    if ( isset( $_SESSION['weber_partner'] ) && isset( $_SESSION['weber_partner']['end_date'] )) :

        // check campaign end
        $campaign_end = strtotime( $_SESSION['weber_partner']['end_date'] );
        $now = strtotime( current_time('Y-m-d H:i:s') );
    
        if( $now > $campaign_end ){
            return false;
        }else{
            return true;
        }

    else:        
        return false;
    endif;

    return true;

}

// helper to get all session related
function weber_get_partner_details(){

    if(!isset($_SESSION['weber_partner'])){

        return false;

    }

    return $_SESSION['weber_partner'];

}

// login
add_action('init', 'weber_partner_login_validation');
function weber_partner_login_validation(){
    
    $login_action = $_POST['login_business_nonce'];
    if( isset( $_POST['login_business_nonce'] ) && wp_verify_nonce( $_POST['login_business_nonce'], 'login_business' ) ){

        $business_code  =  sanitize_text_field( $_POST['business_code'] );
        $postcode       =  sanitize_text_field( $_POST['postcode'] );
        $email          = sanitize_email( $_POST['email_address'] );

        $validate = weber_validate_business_access( $business_code, $postcode, $email );

        // save posted data to use later
        $_SESSION['partner_posted_data'] = array(
            'business_code' => $business_code,
            'email_address' => $email,
            'postcode'      => $postcode,
        );

        // just in case its needed to add extra parameter
        if( ! $validate ){
            wp_safe_redirect( home_url('/') );
            exit;
        }else{
            wp_safe_redirect( home_url('/') );
            exit;
        }

    }

}

// error message
function weber_check_business_code_error(){

    if( !isset( $_SESSION['partner_error'] ) || $_SESSION['partner_error'] == '' ){
        return;
    }

    echo '<ul class="partner-error">';
    foreach( (array) $_SESSION['partner_error'] as $message ){
        echo '<li>'. $message .'</li>';
    }
    echo '</ul>';

    // already displayed, remove the error
    $_SESSION['partner_error'] = '';

}


/////////////////////////////
//// Price discount hook ////
/////////////////////////////

// Change price in the frontend
add_filter( 'woocommerce_get_price_html', 'weber_alter_price_display', 9999, 2 );
function weber_alter_price_display( $price_html, $product ) {
    
    // ONLY ON FRONTEND
    if ( is_admin() ) return $price_html;
    
    // ONLY IF PRICE NOT NULL
    if ( '' === $product->get_price() ) return $price_html;
    
    // IF HAS VALID SESSION
    if ( weber_check_valid_session() ) {
        
        $orig_price = wc_get_price_to_display( $product );

        $partner = weber_get_partner_details();
        $discount = $partner['discount'] / 100;
        $discount_amount = $orig_price * $discount;
        
        $price_html = wc_price( $orig_price - $discount_amount );
    }
    
    return $price_html;
 
}

// Cart calculations
add_action( 'woocommerce_before_calculate_totals', 'weber_alter_price_cart', 9999 );
function weber_alter_price_cart( $cart ) {
 
    if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;
 
    if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 ) return;
 
    // IF NOT HAS VALID SESSION, DONT APPLY DISCOUNT
    if ( ! weber_check_valid_session() ) return;
 
    // LOOP THROUGH CART ITEMS & APPLY 20% DISCOUNT
    foreach ( $cart->get_cart() as $cart_item_key => $cart_item ) {
        $product = $cart_item['data'];
        $price = $product->get_price();

        $partner = weber_get_partner_details();
        $discount = $partner['discount'] / 100;
        $discount_amount = $price * $discount;

        $cart_item['data']->set_price( $price - $discount_amount );
    }
 
}

// Add to cart validation
add_filter( 'woocommerce_add_to_cart_validation', 'add_the_date_validation', 9999, 3 );  
function add_the_date_validation( $passed, $product_id, $qty ) { 

    $in_cart = $is_same_category = false;
    $parent_ranges = array();
  
    foreach( WC()->cart->get_cart() as $cart_item ) {
        $product_in_cart = $cart_item['product_id'];

        // prevent more than 1 product in cart
        if ( $product_in_cart === $product_id ) {
            $in_cart = true;
        }

        // populate bbq product in same category
        if( get_post_meta( $product_in_cart, 'product_type', true) != 'accessories' ){

            // collect all range
            $_terms = wp_get_post_terms( $product_in_cart, 'product_cat' );
            foreach( $_terms as $term){
                if( $term->parent == 0 ){

                    // If product assigned to parent category
                    $parent_ranges[$term->term_id] = $term->term_id;

                }else{

                    // if product assigned to sub product category only
                    $parent_ranges[$term->parent] = $term->parent;

                }
            }

        } // end if its not accessories product

    }
    
    // If buying variation product don't let them buy multiple variation
    if ( $in_cart ) {
        //wc_add_notice( __( 'You cannot add another "'. get_the_title( $product_id ) .'" to your cart.', 'woocommerce' ), 'error' );
        //$passed = false;
    }

    // clean up product category ranges
    // 15 is the term id for uncategorized
    unset($parent_ranges[15]);


    // If buying product in same category - barbie only
    $product_type = get_post_meta( $product_id, 'product_type', true);
    if( $product_type != 'accessories' ){

        $_terms = wp_get_post_terms( $product_id, 'product_cat' );
        foreach( $_terms as $term){

            if( in_array( $term->term_id,  $parent_ranges) ){
                $is_same_category = true;
                break;
            }

            if( in_array( $term->term_id,  $parent_ranges) && $term->parent != 0 ){
                $is_same_category = true;
                break;
            }

        }
        // same category
        if ( $is_same_category ) {
            wc_add_notice( __( 'Please select only one barbecue from this range to add to your cart.', 'woocommerce' ), 'error' );
            $passed = false;
        }

    }

    // if campaign type to only sell 1 product
    if( $_SESSION['weber_partner']['campaign_rules'] == 'one_product_only' ){

        $include = get_field('include_product', $_SESSION['weber_partner']['partner_id']);

        // add parent product if only sell some variations
        $include = weber_add_parent_product_from_variation( $include );

        if( ! in_array( $product_id, $include ) ){
            wc_add_notice( __( get_the_title( $product_id ) . ' is not allowed to purchase on this campaign.', 'woocommerce' ), 'error' );
            $passed = false;
        }
    }


    return $passed;
}

// on successfull payment
// plugin not always has payment complete hook, change to woocommerce_thankyou for now
// add_action( 'woocommerce_payment_complete', 'weber_landing_on_payment_complete' );
add_action('weber_thankyou', 'weber_landing_on_payment_complete', 10, 1);
function weber_landing_on_payment_complete( $order_id ){

    // valid order
    if ( ! $order_id )
        return;

    // ensure the order has been paid
    /* $order = wc_get_order( $order_id );
    if( ! $order->is_paid() )
        return;
    */

    // if ever pushed to custom db
    if( get_post_meta( $order_id, '_weber_pushed_to_campaign_log', true ) == 'yes' ){
        return;
    }

    // store to session has purchase = 1
    // to hide add to cart functionality
    $_SESSION['weber_partner']['has_purchase'] = 1;

    // store to database
    weber_add_customer_to_campaign_log( $order_id );

    // only run once indicator
    update_post_meta( $order_id, '_weber_pushed_to_campaign_log', 'yes' );

    

}

// store to custom database
function weber_add_customer_to_campaign_log( $order_id ){

    $order = wc_get_order( $order_id );

    global $wpdb;
    $wpdb->insert( 
        $wpdb->prefix . 'weber_campaign_log', 
        array( 
            'business_code' => $_SESSION['weber_partner']['business_code'], 
            'billing_email' => $_SESSION['weber_partner']['email_address'],
            'billing_firstname' => $order->get_billing_first_name(),
            'billing_lastname' => $order->get_billing_last_name(),
            'billing_postcode' => $order->get_billing_postcode(),
            'order_id' => $order_id,
            'partner_id' => $_SESSION['weber_partner']['partner_id'],
            'campaign_id' => $_SESSION['weber_partner']['campaign_id'],
            'campaign_name' => $_SESSION['weber_partner']['campaign_name'],
            'campaign_start_date' => $_SESSION['weber_partner']['start_date'],
            'campaign_end_date' => $_SESSION['weber_partner']['end_date'],
            'order_total' => $order->get_total(),
            'order_date' => $order->get_date_created()
        )
    );

    // update business code table
    $wpdb->update(
        $wpdb->prefix . 'weber_business_code',
        array(
            'status' => 'redeemed',
        ),
        array(
            'business_code' => $_SESSION['weber_partner']['business_code']
        )
    );

}

// enable session on visit
add_action('init', 'weber_wc_session_enabler');
function weber_wc_session_enabler(){
    if ( ! is_admin() && WC()->session != null && ! WC()->session->has_session() ) {
        WC()->session->set_customer_session_cookie( true );
    }
}

// Hide product for specific product
// To do: dev this
//dd_action( 'woocommerce_product_query', 'bbloomer_hide_product_if_country_new', 9999, 2 );
function bbloomer_hide_product_if_country_new( $q, $query ) {

    if ( is_admin() ) return;

    $location = WC_Geolocation::geolocate_ip();
    $hide_products = array( 21, 32 );   
    $country = $location['country'];   
    if ( $country === "US" ) {
        $q->set( 'post__not_in', $hide_products );
    } 

}

// reorder checkout field
add_filter( 'woocommerce_checkout_fields', 'weber_reorder_checkout_fields', 10, 1 );
function weber_reorder_checkout_fields( $checkout_fields ) {


    $checkout_fields['billing']['billing_email']['custom_attributes'] = array('readonly'=>'readonly');
    $checkout_fields['billing']['billing_postcode']['custom_attributes'] = array('readonly'=>'readonly');

    $checkout_fields['yourdetails']['type'] = 'html';
    
    return $checkout_fields;
}

// html field type
add_filter('woocommerce_form_field_text','weber_woocommerce_form_field_text', 9999999, 4);
function weber_woocommerce_form_field_text($field, $key, $args, $value){

    if( 'yourdetails' == $key){
        return '<p class="form-row form-row-wide yourdetails"><span>Your Details</span></p>';
    }

    return $field;

}

/**
 * Remove Woocommerce Select2 - Woocommerce 3.2.1+
 */
function woo_dequeue_select2() {
    if ( class_exists( 'woocommerce' ) ) {
        wp_dequeue_style( 'select2' );
        wp_deregister_style( 'select2' );

        wp_dequeue_script( 'selectWoo');
        wp_deregister_script('selectWoo');
    } 
}
add_action( 'wp_enqueue_scripts', 'woo_dequeue_select2', 100 );


// disable order notes
add_filter( 'woocommerce_enable_order_notes_field', 'weber_woocommerce_enable_order_notes_field', 9999999, 4);
function weber_woocommerce_enable_order_notes_field(){
    return false;
}


// check whether product is ready to stock
function weber_warehouse_instock( $product_id ){

    $warehouse = strtolower( $_SESSION['weber_partner']['warehouse'] );

    $key = 'inventory-au-'.$warehouse;
    $instock = get_post_meta( $product_id, $key, true );
    $low_stock_threshold = get_post_meta( $product_id, 'product_low_stock_threshold', true );

    // low stock has been set on parent product, check if parents has it
    $product = wc_get_product($product_id);
    $type = $product->get_type();
    if($type == 'variation'){

        $parent = $product->get_parent_id();
        $low_stock_threshold = get_post_meta( $parent, 'product_low_stock_threshold', true );
        $low_stock_variation_id = get_post_meta( $product_id, 'product_low_stock_threshold', true );

        // if variation has different low stock use that otherwise use parent
        $low_stock_threshold = trim($low_stock_variation_id) != '' ? $low_stock_variation_id : $low_stock_threshold;

    }

    $low_stock_threshold = trim( $low_stock_threshold ) != '' ? $low_stock_threshold : 1; // default is 1

    if( $instock > $low_stock_threshold ){
        return true;
    }

    // try to see whether main warehouse have it
    $instock_main_warehouse = get_post_meta( $product_id, 'inventory-au', true );
    if( $instock_main_warehouse > $low_stock_threshold ){
        return true;
    }

    return false;

}

// check whether campaign is allow this product to be purchase
function weber_partner_is_partner_can_access( $product ){

    $product_id = $product->get_id();
    $get_excluded_product = (array) get_field( 'exclude_products', $_SESSION['weber_partner']['partner_id'] );
    if( in_array( $product_id, $get_excluded_product) ){
        return false;
    }

    return true;

}

// hide variation product
add_filter( 'woocommerce_get_children', 'weber_partner_variation_is_active', 10, 3 );
function weber_partner_variation_is_active( $children, $var, $variation ) {

    $new_children = array();
    foreach( $children as $variation_id){
        $product_id = $variation_id;
        $get_excluded_product = (array) get_field( 'exclude_products', $_SESSION['weber_partner']['partner_id'] );
        if( in_array( $product_id, $get_excluded_product) ){
            continue;
        }

        // check if variation is in stock
        $is_in_stock = weber_warehouse_instock( $product_id );
        if(! $is_in_stock ){
            continue;
        }

        $new_children[] = $variation_id;
    }   

    return $new_children;
}

// hide single product if its hidden from campaign
add_action('template_redirect', 'weber_hidden_product_for_partner');
function weber_hidden_product_for_partner(){

    if( is_product() ){

        $product_id = get_queried_object_id();
        $get_excluded_product = (array) get_field( 'exclude_products', $_SESSION['weber_partner']['partner_id'] );
        if( in_array( $product_id, $get_excluded_product) ){
            global $wp_query;
            $wp_query->set_404();
        }
        
    }
}

// show all product
add_filter( 'loop_shop_per_page', 'weber_loop_shop_per_page', 20 );
function weber_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = -1;
  return $cols;
}

// prevent user to buy if the stock recently changed from available to not available
add_action( 'woocommerce_check_cart_items', 'weber_check_cart_items_validation', 1 );
function weber_check_cart_items_validation(){

    $return = true;

    foreach( WC()->cart->get_cart() as $cart_item ) {

        // split whether we use variation id or product id
        $product_id = isset( $cart_item['variation_id'] ) && $cart_item['variation_id'] != 0 ? $cart_item['variation_id'] : $cart_item['product_id'];
        $product = $cart_item['data'];

        if ( ! weber_warehouse_instock( $product_id ) ) {
            $return = false;
            $error_message = sprintf( __( 'Sorry, "%s" is not in stock. Please edit your cart and try again. We apologize for any inconvenience caused.', 'woocommerce' ), $product->get_name() );

            wc_add_notice( $error_message, 'error' );
            break;
        }
    }

    return $return;

}

// change pay now text
add_filter( 'woocommerce_order_button_text', 'weber_woocommerce_order_button_text' );
function weber_woocommerce_order_button_text(){

    return 'Pay Now';

}

// Function to change email address
add_filter( 'wp_mail_from', 'wpb_sender_email' );
function wpb_sender_email( $original_email_address ) {
    return 'noreply@weberpartners.com.au';
}
 
// Function to change sender name
add_filter( 'wp_mail_from_name', 'wpb_sender_name' );
function wpb_sender_name( $original_email_from ) {
    return 'Weber Partners';
}

// debug
add_filter( 'wp_mail', 'weber_sandbox_mail_filter', 150, 1 );
function weber_sandbox_mail_filter( $args ) {

	$get_site_url = get_bloginfo('url');
	$url = parse_url($get_site_url);
	$is_main_site = $url['host'] == 'www.weberpartners.com.au' || $url['host'] == 'weberpartners.com.au' ? true : false;
	if( !$is_main_site ){
		$original_subject = $args['subject'];
		$args['subject'] = '[SENT FROM SANDBOX] '. $original_subject;
	}		

	/*ob_start();
	print_r($args['message']);
    $output =  ob_get_clean() ;
    
    $title = $args['subject'];
    $to = $args['to'];

	$wp_root_path = str_replace('/wp-content/themes', '', get_theme_root());
	$myFile = $wp_root_path."/". sanitize_title($to). " -- " . sanitize_title($title) .".html"; // or .php   
	$fh = fopen($myFile, 'w'); // or die("error");  
	$stringData = $output;   
	fwrite($fh, $stringData);
	fclose($fh);*/
	
	return $args;
}



// Add source of warehouse when adding to cart
add_filter( 'woocommerce_add_cart_item_data', 'weber_add_source_wareohuse', 10, 4 );
function weber_add_source_wareohuse( $cart_item_data, $product_id, $variation_id, $quantity ) {
    
    $product_id = isset( $variation_id ) && $variation_id != 0 ? $variation_id : $product_id;

    $cart_item_data['_source_warehouse'] = weber_warehouse_source( $product_id );
    
    return $cart_item_data;
}

// save cart item to have source of warehouse
add_action( 'woocommerce_checkout_create_order_line_item', 'weber_woocommerce_checkout_create_order_line_item', 10, 4 );
function weber_woocommerce_checkout_create_order_line_item( $item, $cart_item_key, $values, $order ) {

    $meta_key = '_source_warehouse';
    $item->update_meta_data( $meta_key, $values[$meta_key] );

    // only if set
    $item->update_meta_data( '_da', $values['da'] );
        
}

// hide the meta data from frontend
add_filter('woocommerce_hidden_order_itemmeta', 'weber_woocommerce_hidden_order_itemmeta', 50);
function weber_woocommerce_hidden_order_itemmeta($args) {

    $args[] = '_source_warehouse';
    $args[] = '_da';

    return $args;

}

// this is copy from weber check stock per warehouse
// we use this to specify where the stock come from
function weber_warehouse_source( $product_id ){

    $warehouse = strtolower( $_SESSION['weber_partner']['warehouse'] );

    $key = 'inventory-au-'.$warehouse;
    $instock = get_post_meta( $product_id, $key, true );
    $low_stock_threshold = get_post_meta( $product_id, 'product_low_stock_threshold', true );

    // low stock has been set on parent product, check if parents has it
    $product = wc_get_product($product_id);
    $type = $product->get_type();
    if($type == 'variation'){

        $parent = $product->get_parent_id();
        $low_stock_threshold = get_post_meta( $parent, 'product_low_stock_threshold', true );
        $low_stock_variation_id = get_post_meta( $product_id, 'product_low_stock_threshold', true );

        // if variation has different low stock use that otherwise use parent
        $low_stock_threshold = trim($low_stock_variation_id) != '' ? $low_stock_variation_id : $low_stock_threshold;

    }

    $low_stock_threshold = trim( $low_stock_threshold ) != '' ? $low_stock_threshold : 1; // default is 1

    if( $instock > $low_stock_threshold ){
        return $key;
    }

    // try to see whether main warehouse have it
    $instock_main_warehouse = get_post_meta( $product_id, 'inventory-au', true );
    if( $instock_main_warehouse > $low_stock_threshold ){
        return 'inventory-au';
    }

    // this is imposible but add this, just in case
    return 'not-set';

}


// helper get product id by sku
function weber_get_product_id_by_sku( $sku ){

    global $wpdb;
    $product_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_sku' AND meta_value='%s' LIMIT 1", $sku ) );

    if(is_numeric($product_id) && $product_id != 0){
        return $product_id;
    }

    return false;

}


// update the custom stock before it get replaced
add_action('woocommerce_checkout_update_order_meta','weber_woocommerce_checkout_update_order_meta', 50, 2);
function weber_woocommerce_checkout_update_order_meta( $order_id, $data = array() ){

    if ( is_a( $order_id, 'WC_Order' ) ) {
		$order    = $order_id;
		$order_id = $order->get_id();
	} else {
		$order = wc_get_order( $order_id );
	}
    
    // Loop over all item
    foreach ( $order->get_items() as $item_id => $item ) {

		if ( ! $item->is_type( 'line_item' ) ) {
			continue;
        }

        // warehouse
        $_source_warehouse = wc_get_order_item_meta( $item_id, '_source_warehouse', true ); 
        $_qty = $item->get_quantity();

        $product = $item->get_product();
        $sku = $product->get_sku();
        $_id = weber_get_product_id_by_sku($sku);
        
        // reduce the custom stock level
        if( $_id ){
            $current_stock = get_post_meta( $_id, $_source_warehouse, true );
            $reduced = $current_stock - $_qty; 
            update_post_meta( $_id, $_source_warehouse, $reduced );
        }
    
    }

    // update partners session to meta
    $partner_data = weber_get_partner_details();
    update_post_meta( $order_id, '_weber_partner_promo', $partner_data );

}

// format price with 2 decimal
// using wc_price based but remove the html
function weber_price( $price, $args = array() ) {
    $args = apply_filters(
      'wc_price_args',
      wp_parse_args(
        $args,
        array(
          'ex_tax_label'       => false,
          'currency'           => '',
          'decimal_separator'  => wc_get_price_decimal_separator(),
          'thousand_separator' => '',
          'decimals'           => wc_get_price_decimals(),
          'price_format'       => get_woocommerce_price_format(),
        )
      )
    );
  
    $unformatted_price = $price;
    $negative          = $price < 0;
    $price             = apply_filters( 'raw_woocommerce_price', floatval( $negative ? $price * -1 : $price ) );
    $price             = apply_filters( 'formatted_woocommerce_price', number_format( $price, $args['decimals'], $args['decimal_separator'], $args['thousand_separator'] ), $price, $args['decimals'], $args['decimal_separator'], $args['thousand_separator'] );
  
    if ( apply_filters( 'woocommerce_price_trim_zeros', false ) && $args['decimals'] > 0 ) {
      $price = wc_trim_zeros( $price );
    }
  
    return $price;

  }

  // calculate tax and net price based on gross price
  function weber_get_net_price( $gross_price, $tax = 0.1){

    if(!is_numeric($gross_price)){
        return;
    }

    $net_price = $gross_price / (1 + $tax);
    $net_price = weber_price( $net_price );

    return array(
        'net' => weber_price( $net_price ),
        'tax' => weber_price( $gross_price - $net_price ),
    );

  }

// on failed to push to pronto, notify admin
add_action('weber_pronto_failed_send_xml', 'weber_pronto_failed_send_xml_callback', 10, 1);
function weber_pronto_failed_send_xml_callback( $order_id ){

    $wc_email = WC()->mailer()->get_emails();
    $new_order = $wc_email['WC_Email_New_Order'];
    $recipient = $new_order->settings['recipient'];

    $subject = 'Failed Push XML Order to Pronto - B2B'.$order_id.'.xml';
    $content = 'There is a problem when trying to push the order to Pronto. <br>';
    $content .= 'Please ensure the SFTP details can access Pronto server, then login to the backend and send it manually.<br>';
    $content .= '<a target="_blank" href="'. get_edit_post_link(  $order_id) .'">Click here</a> to open the order detail page.';

    wp_mail($recipient, $subject, $title);

}

// Order metabox to resend the xml to pronto
/**
 * Register meta box(es).
 */
add_action( 'add_meta_boxes', 'weber_pronto_register_meta_boxes' );
function weber_pronto_register_meta_boxes() {
    add_meta_box( 'weber-pronto-status-box', __( 'Pronto Status', 'weber' ), 'weber_pronto_metabox', 'shop_order', 'side', 'high' );
}

 
/**
 * Meta box display callback.
 *
 * @param WP_Post $post Current post object.
 */
function weber_pronto_metabox( $post ) {
    // Display code/markup goes here. Don't forget to include nonces!
   $pronto_status = get_post_meta( $post->ID, '_pushed_to_pronto', true );
   if( $pronto_status == 'yes' ){
       echo 'Order has been sent to Pronto.';
   }else{
       echo 'There was a problem to push the XML file to Pronto when customer created an order, please hit the button below to resend.<br><br>';
        echo '<button name="resend-to-pronto" class="button button-primary" data-order-id="'. $post->ID .'">Send to Pronto <span style="display:none"><img src="'.THEME_URL.'/img/loader.gif" alt=""/></span></button>';
        echo '<div id="resend-to-pronto-results"></div>';
   }
}

// ensure class exists
use phpseclib\Net\SFTP;

// Re push to Pronto
add_action( 'wp_ajax_weber_re_push_xml_pronto', 'weber_re_push_xml_pronto' );
add_action( 'wp_ajax_nopriv_weber_re_push_xml_pronto', 'weber_re_push_xml_pronto' );
function weber_re_push_xml_pronto() {

    $order_id = $_POST['order_id'];
    if( empty($order_id) || ! is_numeric($order_id)){
        echo json_encode(array(
            'result' => 'error',
            'message' => 'Wrong Order ID',
        ));
        exit;
    }

    $rootPath = str_replace(ABSPATH,"",getcwd());
    $filename = "B2B".$order_id.".xml";

        # write file to a FTP server.
        $sftp = new SFTP( SFTP_HOST );
        $user = SFTP_USER;
        $pass = SFTP_PASS;
        if (!$sftp->login($user, $pass)) {

            $result = 'error';
            $message = 'Push FAILED, please check whether SFTP can access Pronto server';

        } else {
            
            $finalFilename = $filename;
            $dataFile = $rootPath.'/xml-record/'.$finalFilename;
            $fileContent = file_get_contents($dataFile);
            $sftp->put( SFTP_DESTINATION . $finalFilename, $fileContent);

            // push to pronto works, save a custom field to indicate
            update_post_meta( $order_id, '_pushed_to_pronto', 'yes' );

            $result = 'success';
            $message = 'Order has been sent to Pronto.';

        }

    echo json_encode(array(
        'result' => $result,
        'message' => $message
    ));
    exit;

}