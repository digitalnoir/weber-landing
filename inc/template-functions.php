<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Digital_Noir_Starter_Pack
 */

/**
 * Register widget area.
 */
add_action( 'widgets_init', 'digitalnoir_widgets_init' );
function digitalnoir_widgets_init() {
	
	//register main sidebar
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'digitalnoir' ),
		'id'            => 'sidebar-main',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	
	// register footer widget
	register_sidebar( array(
		'name'          => __( 'Footer 1', 'digitalnoir' ),
		'id'            => 'footer-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer 2', 'digitalnoir' ),
		'id'            => 'footer-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer 3', 'digitalnoir' ),
		'id'            => 'footer-3',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer 4', 'digitalnoir' ),
		'id'            => 'footer-4',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}


// Register Partner
add_action( 'init', 'codex_partner_init' );
function codex_partner_init() {
	$labels = array(
		'name'               => _x( 'Partners', 'post type general name', 'weber' ),
		'singular_name'      => _x( 'Partner', 'post type singular name', 'weber' ),
		'menu_name'          => _x( 'Partners', 'admin menu', 'weber' ),
		'name_admin_bar'     => _x( 'Partner', 'add new on admin bar', 'weber' ),
		'add_new'            => _x( 'Add New', 'partner', 'weber' ),
		'add_new_item'       => __( 'Add New Partner', 'weber' ),
		'new_item'           => __( 'New Partner', 'weber' ),
		'edit_item'          => __( 'Edit Partner', 'weber' ),
		'view_item'          => __( 'View Partner', 'weber' ),
		'all_items'          => __( 'All Partners', 'weber' ),
		'search_items'       => __( 'Search Partners', 'weber' ),
		'parent_item_colon'  => __( 'Parent Partners:', 'weber' ),
		'not_found'          => __( 'No partners found.', 'weber' ),
		'not_found_in_trash' => __( 'No partners found in Trash.', 'weber' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'partner', 'with_front' => false ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => 5,
		'supports'           => array( 'title' )
	);

	register_post_type( 'partner', $args );
}

add_action('admin_menu','partner_submenu');
function partner_submenu(){
	add_submenu_page(
        'edit.php?post_type=partner',
        'Campaign Report',
        'Campaign Report',
        'edit_posts',
        'report-campaign-code',
		'weber_manage_report_campaign'
    );
}

//
add_filter( 'post_type_link', 'wpex_remove_cpt_slug', 10, 3 );
function wpex_remove_cpt_slug( $post_link, $post, $leavename ) {
	if ( 'partner' != $post->post_type || 'publish' != $post->post_status ) {
		return $post_link;
	}
	$post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
	return $post_link;
}

// get post
add_action( 'pre_get_posts', 'wpex_parse_request_tricksy' );
function wpex_parse_request_tricksy( $query ) {
	// Only noop the main query
	if ( ! $query->is_main_query() )
		return;
	// Only noop our very specific rewrite rule match
	if ( 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
		return;
	}
	// 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
	if ( ! empty( $query->query['name'] ) ) {
		$query->set( 'post_type', array( 'post', 'partner', 'page' ) );
	}
}


// custom column
add_action( 'manage_partner_posts_custom_column' , 'weber_partner_custom_column', 10, 2 );
function weber_partner_custom_column( $column, $post_id ) {

	switch ( $column ) {

		case 'discount':
			$discount = get_post_meta( $post_id, 'partner_discount', true);
			echo $discount . '%';
			break;
	}
}

// add the column
add_filter( 'manage_partner_posts_columns' , 'weber_add_discount_column' );
function weber_add_discount_column( $columns ) {

	$return = array();
	foreach($columns as $key => $column){

		$return[$key] = $column;

		if( $key == 'title' ){

			$return['discount'] = 'Discount';

		}

	}

    return $return;
}


// weber generate code here
add_action( 'wp_ajax_weber_do_generate_business_code', 'weber_do_generate_business_code');
function weber_do_generate_business_code(){

	$partner_id = $_POST['post_id'];
	$amount = $_POST['amount'];
	$discount = get_post_meta($partner_id, 'partner_discount', true);
	$partner_name = get_the_title( $partner_id );
	$campaign_name = $_POST['name'];
	$campaign_start_date = $_POST['start_date'];
	$campaign_end_date = $_POST['end_date'];

	// insert to main database
	global $wpdb;
	$wpdb->insert( 'wp_weber_business_campaign',
		array(
			'status' => 'available',
			'discount' => $discount,
			'partner_id' => $partner_id,
			'partner_name' => $partner_name,
			'campaign_name' => $campaign_name,
			'total_code' => $amount,
			'start_date' => $campaign_start_date . ' 00:00:00',
			'end_date' => $campaign_end_date . ' 23:59:59',
		)
	);
	// grab campaign id
	$campaign_id = $wpdb->insert_id;
	
    // Edit Start  //
	// iterate add to database
	/*global $wpdb;
	for($i=0; $i < $amount; $i++ ){

		// unuique code
		$unique_code = weber_check_create_unique_code( 8 ); // length 8 character

		$wpdb->insert( 'wp_weber_business_code',
			array(
				'campaign_id' => $campaign_id,
				'business_code' => $unique_code,
				'status' => 'available',
				'discount' => $discount,
				'partner_id' => $partner_id,
				'partner_name' => $partner_name,
				'campaign_name' => $campaign_name,
				'start_date' => $campaign_start_date . ' 00:00:00',
				'end_date' => $campaign_end_date . ' 23:59:59',
			)
		);
	}*/
    // Edit End  //

	// return the html
	ob_start();
	echo weber_unique_code_tr( $partner_id );
	$html = ob_get_clean();

	echo json_encode(array(
		'html' => $html
	));
	exit;

}


// generate code
function weber_check_create_unique_code( $length )  {

	$char = "abcdefghijklmnpqrstuvwxyz123456789";
	$char = str_shuffle($char);
	for($i = 0, $rand = '', $l = strlen($char) - 1; $i < $length; $i ++) {
		$rand .= $char{mt_rand(0, $l)};
	}

	// ensure its unique
	$is_exist = weber_check_is_code_exist( $rand );
	if( $is_exist ){
		return weber_check_create_unique_code( $length );
	}

	return $rand;
}

// check whether the code is exits
function weber_check_is_code_exist( $string ){

	$string = strtolower($string);

	global $wpdb;
	$sql = "
		SELECT * FROM wp_weber_business_code
		WHERE business_code = '{$string}'
	";

	$result = $wpdb->get_row($sql);
	return !empty($result) > 0 ? true : false;

}

// Edit Start//
// templating results
function weber_unique_code_tr( $partner_id ){

	global $wpdb;
	$results = $wpdb->get_results( "
							SELECT * FROM wp_weber_business_campaign
							WHERE partner_id = {$partner_id}
							AND status != 'trash'
							ORDER BY end_date ASC
	 ", ARRAY_A);


		$html = '';
		 
	 	if( !empty($results)){
			$html = '<tbody id="replace--results">';
			$i = 0;
			foreach( (array) $results as $row){ $i++;

				$campaign_id = $row['ID'];
				$count_available = $wpdb->get_var(" SELECT COUNT(ID) FROM wp_weber_business_code WHERE status = 'available' AND campaign_id = {$campaign_id} ");
                

				$html .= '<tr>
					<td class="acf-row-handle order"><span>'.$i.'</span></td>
					<td class="campaign-item">'. $row['campaign_name'] .'</td>
					<td class="campaign-item">'. $row['discount'] .'%</td>
					<td class="campaign-item">'. date('d F Y', strtotime($row['start_date'])) .'</td>
					<td class="campaign-item">'. date('d F Y', strtotime($row['end_date'])) .'</td>
					<td class="campaign-item">';
					
					if(strtotime(current_time('mysql')) < strtotime($row['end_date'])){
						//$html .=	'<a href="#" data-id="'. $row['ID'] .'" data-action="pause" class="acf-button button campaign-action-btn button-secondary">Pause</a> &nbsp;';
					}

					// manage
					$html .= '<a target="_blank" href="edit.php?post_type=partner&page=report-campaign-code&campaign_id='. $campaign_id .'" class="acf-button button button-primary">Report</a> &nbsp;';

					$html .= '<a href="#" data-id="'. $row['ID'] .'" data-partner="'. $row['partner_id'] .'" data-action="delete" class="acf-button button campaign-action-btn button-secondary">Delete</a></td>
				</tr>';

			}

			$html .= '</tbody>';
		}

	return $html;

}
// Edit End  //

// delete or pause
add_action('wp_ajax_weber_business_code_action','weber_business_code_action');
function weber_business_code_action(){

	$campaign_id = $_POST['campaign_id'];
	$action = $_POST['run_action'];
	$partner_id = $_POST['partner_id'];

	global $wpdb;

	$html = '';

	// pause action
	if($action == 'pause'){

		// main access code database
		$wpdb->update(
            'wp_weber_business_code',
            array(
                'status' => 'paused',
            ),
            array( 'campaign_id' => $campaign_id, 'status' => 'available' )
		);

		// campaign database
		$wpdb->update(
            'wp_weber_business_campaign',
            array(
                'status' => 'paused',
            ),
            array( 'ID' => $campaign_id )
		);
		
	}

// Edit Start  //	
    // delete
	if($action == 'delete'){
		$wpdb->delete( 'wp_weber_business_code', array( 'campaign_id' => $campaign_id ) );
		$wpdb->delete( 'wp_weber_business_campaign', array( 'ID' => $campaign_id ) );
        $wpdb->delete( $wpdb->prefix.'weber_partnerform_log', array( 'campaign_id' => $campaign_id ) );
	}

    $all_campaign = weber_unique_code_tr( $partner_id );
	ob_start();
    if( trim($all_campaign) == ''){
        echo '<tbody id="replace--results"><tr><td colspan="7" align="center">No campaign found</td></tr></tbody>';
    }else{
        echo $all_campaign;
    }
	$html = ob_get_clean();

	$json = array(
		'results' => 'success',
		'html' => $html
	);
    if(trim($all_campaign) != ''){$json["noaddnew"]=1;}
    else{$json["html2"]=weber_addnewCampaign_html($partner_id);}
    echo json_encode($json);
	exit;
// Edit End//
}

// download all csv
add_action('admin_init','weber_campaign_action_download_codes',1,1);
function weber_campaign_action_download_codes(){

	// download campaign code
    if( isset($_REQUEST['action']) && $_REQUEST['action'] == 'download_all_code_weber_partner_campaign'){

		$campaign_id = $_REQUEST['campaign_id'];
    
		require get_template_directory().'/inc/class-php-export-data.php';

		global $wpdb;

		$campaign_details = $wpdb->get_row(" SELECT * FROM wp_weber_business_campaign WHERE ID = {$campaign_id} ", ARRAY_A);
		$campaign_name = $campaign_details['partner_name'] . ' ' . $campaign_details['campaign_name']. ' ' . date('Y-m-d', strtotime($campaign_details['start_date'])) .' to '. date('Y-m-d', strtotime($campaign_details['end_date']));
		

        // filename
        $filename = sanitize_title($campaign_name);
        

        // begin csv generator
        $exporter = new ExportDataCSV('browser');
        $exporter->filename = $filename . '.csv';

		$exporter->initialize(); // starts streaming data to web browser
		
		// Title
		$exporter->addRow(array(
            "Partner", $campaign_details['partner_name']
		));

		$exporter->addRow(array(
            "Start Date", $campaign_details['start_date']
		));

		$exporter->addRow(array(
            "End Date", $campaign_details['end_date']
		));

		// empty line
		$exporter->addRow(array(
            " ",
		));
		$exporter->addRow(array(
            " ",
		));

		
		$results = $wpdb->get_results( "SELECT * FROM wp_weber_business_code WHERE campaign_id = {$campaign_id} ", ARRAY_A );

        foreach((array) $results as $row){
            // Heading
            $exporter->addRow(array( 
                strtoupper($row['business_code']),
            ));
        }

        
        $exporter->finalize(); // writes the footer, flushes remaining data to browser.
        
        exit();

    
	}
	
	// download csv campaign report
	if( isset($_REQUEST['action']) && $_REQUEST['action'] == 'download-report-all-campaign-csv'){

		$campaign_id = $_REQUEST['campaign_id'];
    
		require get_template_directory().'/inc/class-php-export-data.php';

		global $wpdb;

		$campaign_details = $wpdb->get_row(" SELECT * FROM wp_weber_business_campaign WHERE ID = {$campaign_id} ", ARRAY_A);
		$campaign_name = $campaign_details['partner_name'] . ' ' . $campaign_details['campaign_name']. ' ' . date('Y-m-d', strtotime($campaign_details['start_date'])) .' to '. date('Y-m-d', strtotime($campaign_details['end_date']));
		

        // filename
        $filename = 'campaign-report-'.sanitize_title($campaign_name).'-'.date('Y-m-d-H-i-s');
        

        // begin csv generator
        $exporter = new ExportDataCSV('browser');
        $exporter->filename = $filename . '.csv';

		$exporter->initialize(); // starts streaming data to web browser
		
		// Title
		$exporter->addRow(array(
            "Partner", $campaign_details['partner_name']
		));

		// Campaign Name
		$exporter->addRow(array(
            "Campaign Name", $campaign_details['campaign_name']
		));

		$exporter->addRow(array(
            "Start Date", $campaign_details['start_date']
		));

		$exporter->addRow(array(
            "End Date", $campaign_details['end_date']
		));


		$sql = "
			SELECT 
                bcode.business_code,
                bcode.status,
                bcode.start_date,
                bcode.end_date,
                clog.billing_email,
                clog.billing_firstname,
                clog.billing_lastname,
                clog.billing_postcode,
                clog.order_id,
                clog.order_total,
                clog.order_date

            FROM {$wpdb->prefix}weber_business_code bcode 
            LEFT JOIN {$wpdb->prefix}weber_campaign_log clog
            ON bcode.business_code = clog.business_code 
			WHERE bcode.campaign_id = {$campaign_id}
			ORDER BY clog.order_date DESC
		";

		$results = $wpdb->get_results( $sql, ARRAY_A );

		$total = sizeof( (array) $results );
		$redeemed = 0;
		$revenue = 0;
		foreach($results as $business_code){
			if( $business_code['status'] == 'redeemed'){
				$redeemed++;
			}
			$revenue += $business_code['order_total'];
		}

		// Redeemed
		$exporter->addRow(array(
            "Redeemed Code", $redeemed.' from '. $total
		));

		// Total Sales
		$exporter->addRow(array(
            "Total Sales", $revenue
		));

		// empty line
		$exporter->addRow(array(
            " ",
		));
		$exporter->addRow(array(
            " ",
		));

		// header
		$exporter->addRow(array( 
			'Business Code',
			'Status',
			'Order ID',
			'Email',
			'First Name',
			'Last Name',
			'Order Total',
			'Order Date'
		));

        foreach((array) $results as $row){
            // Heading
            $exporter->addRow(array( 
				strtoupper($row['business_code']),
				$row['status'],
				$row['order_id'],
				$row['billing_email'],
				$row['billing_firstname'],
				$row['billing_lastname'],
				$row['order_total'],
				$row['order_date']
            ));
        }

        
        $exporter->finalize(); // writes the footer, flushes remaining data to browser.
        
        exit();


	}

}

// only show parent cat
add_filter('acf/fields/taxonomy/wp_list_categories', 'weber_parent_product_cat_only', 10, 2);
function weber_parent_product_cat_only( $args, $field ){

	if( $field['key'] != 'field_5f6c19bff9496' ){
		return $args;
	}

	$args['depth'] = 1;
	$args['exclude'] = array(15);

	
    return $args;
}