<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php wp_title( '|', true, 'right' ); ?></title>
<?php wp_head(); ?>
<style>
    .partner--login .xoo-wsc-modal{display:none}
</style>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TXPSS5V');</script>
<!-- End Google Tag Manager -->
</head>

<body <?php body_class('partner--login'); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TXPSS5V"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="page-wraper">

<?php 

	// Please change the scss file on \assets\production\sass\header\_header.scss
	// Use the correct scss

	//include THEME_DIR . "/blocks/headers/menu-dropdown.php";
	//include THEME_DIR . "/blocks/headers/menu-side.php";
	include THEME_DIR . "/blocks/headers/menu-blocky.php";

?>


<div id="content" class="page-content-wrapper">
