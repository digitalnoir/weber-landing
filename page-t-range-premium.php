<?php
/*
Template Name: Range Template - Premium Gas
*/

get_header(); ?>
<div class="site-content ux2-wrapper">
	<div id="content" class="content-area">
		<main id="main" class="site-main" >
			<?php while ( have_posts() ) : the_post(); ?>
			<article>

				<?php
				# Parameter
				$the_query_args = array (
					'post_type' => array( 'product', ),
					'posts_per_page'  => -1,  # -1 for all
					'order'   => 'DESC',  # Newest
					'orderby' => 'date',  # 'rand' 'post__in'
					'tax_query' => array(
					   'relation' => 'OR', # ('AND','OR')

					   array(
					      'field'    => 'term_id',           #  ('term_id', 'name', 'slug', 'term_taxonomy_id')
					      'taxonomy' => 'range',     #  Taxonomy Name
					      'operator' => 'IN',                #  ('IN', 'NOT IN', 'AND', 'EXISTS' and 'NOT EXISTS')
					      'terms'    =>  array( 81,),        #  (int, string, array)

					      # Select all Terms
					      # 'terms'    =>  get_terms(array('taxonomy' => 'taxonomy-name','hide_empty' => false,'fields' => 'names'));
					      # 'terms'    =>  get_terms(array('taxonomy' => 'taxonomy-name','hide_empty' => false,'fields' => 'id=>slug'));
					      # 'terms'    =>  get_terms(array('taxonomy' => 'taxonomy-name','hide_empty' => false,'fields' => 'tt_ids'));
					   ),
					),
				);

				# Connect Loop to Parameter
				$the_query_query = new WP_Query( $the_query_args );
				?>

				<?php
				# Loop
				if ( $the_query_query->have_posts() ) : ?>
					<section id="ranges-filter">
						<div class="filter-galleries">
							<div class="container">
								<div class="row">
									 	<?php while ( $the_query_query->have_posts() ) : $the_query_query->the_post(); ?>
									 		<div class="col-xs-12 col-md-4 " style="text-align: center;margin-bottom: 20px;">
									 		    <a href="<?php echo get_the_permalink(); ?>">
									 		    	  <?php  $post_featured_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));; ?>
									 		    	  <?php if($post_featured_image): ?>
									 		    	     <img src="<?php echo $post_featured_image; ?>" title="<?php get_the_title(); ?>" alt="<?php get_the_title(); ?>">
									 		    	  <?php endif; ?>
									 		    	  <div>
									 		    	  	<strong><?php the_title(); ?></strong>
									 		    	  </div>
									 		    </a>
									 		</div>
									 	<?php endwhile; ?>
								    
								</div>
							</div>
						</div>
					
					</section>	
					<?php wp_reset_query(); ?>
				<?php else : ?>
				   <?php # Template Part | Blog
				   get_template_part('template-parts/general/content-no-post'); ?>
				<?php endif; ?>
				
			</article>
			<?php dn_post_edit_link(); ?>
			<?php endwhile; // end of the loop. ?>
		</main>
	</div>
</div>

<div class="section-how how-premium">
	<div class="how-to-text match-height">
		<h2 class="acc-title">How It Works</h2>
		<h3 class="subtitle">Weber Gas Barbecue Cooking System</h3>
		<div class="separator"><img src="<?php echo get_template_directory_uri(); ?>/img/how-line.png" alt="How it works - line" width="240" height="9" /></div>
		<div class="desc">At the heart of every Weber gas barbecue is a unique burner system found on no other barbecue in the world. It’s this unique, revolutionary and patented Flavorizer™ system that distinguishes our barbecues from all the others. Specially angled Flavorizer bars form a roof over the gas burner tubes so that no meat juice or fat comes in contact with the gas flame. Follow our directions and this system allows for healthy, fat free cooking with incredible flavour. Meat juices sizzle and smoke on the hot bars, yet excess fats fall harmlessly into the disposable drip tray below. Not many people know that there is one other feature of our cooking system that separates our barbecues from all the others. It’s the scientifically vented hood. This enhances flavour in a way that you can’t get when you cook in the open. Cooking with a Weber hood down ensures that the smoke trapped underneath it is circulated all around the meat before leaving the barbecue. This is how you get that famous Weber flavour. On those occasions when you want to create a beautiful roast or finish off the perfect steak, simply turn off the centre burner(s) directly under the food (this is our indirect barbecue cooking) and allow the heat to circulate barbecue smoke evenly all around the meat for unbelievable flavour. </div>
	</div>
</div>

<?php get_footer(); ?>