<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Digital_Noir_Starter_Pack
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">

		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="widget-1 col-md-2 col-xs-12"><img src="<?php echo THEME_URL ?>/img/header-logo.png" alt="Weber Logo" /></div>
					<div class="widget-2 col-md-4 col-xs-12"><?php dynamic_sidebar('footer-2'); ?></div>
					   <div class="widget-3 col-md-3 col-xs-12">
   						<aside class="widget">
   							<h3 class="widget-title">Social</h3>
   							<div class="textwidget"><ul class="social-footer">
   								<li class="email"><a href="mailto:custserv@weberbbq.com.au" target="_blank"><?php echo dn_get_svg('icon-email.svg') ?></a></li>
   								<li class="facebook"><a href="http://www.facebook.com/WeberBBQAusNZ" target="_blank" class="set"><?php echo dn_get_svg('icon-facebook.svg') ?></a></li>
   								<li class="instagram"><a href="http://www.instagram.com/WeberBBQAusNZ" target="_blank" class="set"><?php echo dn_get_svg('icon-instagram.svg') ?></a></li>
   								</ul>
   							</div>
   						</aside>
   					</div>
					<div class="widget-4 col-md-3 col-xs-12"><?php dynamic_sidebar( 'footer-4' ); ?></div>
				</div>
			</div>
		</div>
		
		
		
		<div class="container">
			<div class="row">
				<div class="site-info col-md-12">
	
					<div class="weblink-footer">
						<?php
							$terms = get_field('footer_page_link', 'options');
							if( !empty( $terms ) ){
								echo '<p>';
								echo 'Copyright &copy; '.date('Y').' Weber &nbsp;';
								foreach( $terms as $page_id){
									echo ' | &nbsp;<a class="special-link" href="'. get_permalink( $page_id ) .'" target="_blank">'. get_the_title( $page_id ) .'</a> &nbsp;';
								}
								echo '</p>';
							}
						?>
						<p><a target="_blank" rel="nofollow" href="http://www.digital-noir.com"><img src="<?php echo THEME_URL ?>/img/dn.svg" alt="Digital Noir" /></a></p>
					</div>
				</div>
			</div>
		</div>
	</footer>
		
</div><!-- #page -->


<?php wp_footer(); ?>

</body>
</html>