<?php

    wp_enqueue_script('jquery-dataTables');    

    // get campaign
    global $wpdb;
    $partner = $wpdb->get_row(" SELECT * FROM {$wpdb->prefix}weber_business_campaign WHERE ID = {$campaign_id} ");
    if(empty($partner)){
        return;
    }
    $total = sizeof( (array) $results );
    $redeemed = 0;
    $revenue = 0;
    foreach($results as $business_code){
        if( $business_code['status'] == 'redeemed'){
            $redeemed++;
        }
        $revenue += $business_code['order_total'];
    }
?>

<table class="acf-table acf-repeater campaign-summary-table">
    <tr>
        <td style="width:180px"><strong>Partner Name</strong></td>
        <td style="width:30%"><?php echo $partner->partner_name ?> <a href="<?php echo get_edit_post_link( $partner->partner_id ) ?>">[back to partner page]</a></td>
        <td class="summ" rowspan="4">
            <strong>Available Code</strong>: <?php echo $total - $redeemed ?><br>
            <strong>Redeemed Code</strong>: <?php echo $redeemed ?> <br><br>
            <strong>Total Sales</strong><br>
            <h3><?php echo wc_price( $revenue ) ?></h3>
        </td>
    </tr>
    <tr>
        <td><strong>Campaign Name</strong></td>
        <td><?php echo $partner->campaign_name ?></strong></td>
    </tr>
    <tr>
        <td><strong>Discount</strong></td>
        <td><?php echo $partner->discount ?>%</td>
    </tr>
    <tr>
        <td><strong>Campaign Date</strong></td>
        <td><?php echo date('d F Y', strtotime($partner->start_date)).' - ' . date('d F Y', strtotime($partner->end_date)) ?></td>
    </tr>
</table>

<link rel="stylesheet" href="<?php echo  THEME_URL ?>/assets/dist/css/jquery.dataTables.min.css">
<style>
    .dataTables_wrapper, .box-wrapper {
        padding:20px;
        background:#fff;
        border: 1px solid #cecece;
        margin-top: 30px;
    }
    table.dataTable{
        padding: 20px 0 0 0 ;
        margin-bottom: 20px;
    }
    table.dataTable thead th{
        padding:10px;
        text-align:left
    }
    div.invite-all {
        float: right;
        margin-right: 20px;
    }
</style>
<script>
jQuery(document).ready(function($) {
    $('#example').DataTable( {
        "order": [[ 7, 'desc' ]],
        "iDisplayLength": 50,
        "bProcessing": true,
        "serverSide": true,
        "columnDefs": [
            { "orderable": false, "targets": 0 },
            { "orderable": false, "targets": 1 },
            { "orderable": false, "targets": 2 },
            { "orderable": false, "targets": 3 },
            { "orderable": false, "targets": 4 },
            { "orderable": false, "targets": 5 },
            { "orderable": false, "targets": 6 },
        ],
        //"ordering": false,
        "ajax":{
            url : ajaxurl, // json datasource
            type: "POST", // type of method  ,GET/POST/DELETE
            data : {action: 'weber_campaign_report_datatable', campaign_id: <?php echo $campaign_id ?> } ,   
          },
        "dom": 'fl<"invite-all">rtip'
    } );

    $("div.invite-all").html('<a href="edit.php?post_type=partner&page=report-campaign-code&campaign_id=<?php echo $campaign_id ?>&action=download-report-all-campaign-csv" class="button button-primary">Download Report</a>');


} );
</script>


    
    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Business Code</th>
                <th>Status</th>
                <th>Order ID</th>
                <th>Email</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Order Total</th>
                <th>Order Date</th>
            </tr>
        </thead>
    </table>
