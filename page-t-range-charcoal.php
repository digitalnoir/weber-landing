<?php
/*
Template Name: Range Template - Charcoal
*/

get_header(); ?>
<div class="site-content ux2-wrapper">
	<div id="content" class="content-area">
		<main id="main" class="site-main" >
			<?php while ( have_posts() ) : the_post(); ?>
			<article>

				<?php
				# Parameter
				$the_query_args = array (
					'post_type' => array( 'product', ),
					'posts_per_page'  => -1,  # -1 for all
					'order'   => 'DESC',  # Newest
					'orderby' => 'date',  # 'rand' 'post__in'
					'tax_query' => array(
					   'relation' => 'OR', # ('AND','OR')

					   array(
					      'field'    => 'term_id',           #  ('term_id', 'name', 'slug', 'term_taxonomy_id')
					      'taxonomy' => 'range',     #  Taxonomy Name
					      'operator' => 'IN',                #  ('IN', 'NOT IN', 'AND', 'EXISTS' and 'NOT EXISTS')
					      'terms'    =>  array( 91,),        #  (int, string, array)

					      # Select all Terms
					      # 'terms'    =>  get_terms(array('taxonomy' => 'taxonomy-name','hide_empty' => false,'fields' => 'names'));
					      # 'terms'    =>  get_terms(array('taxonomy' => 'taxonomy-name','hide_empty' => false,'fields' => 'id=>slug'));
					      # 'terms'    =>  get_terms(array('taxonomy' => 'taxonomy-name','hide_empty' => false,'fields' => 'tt_ids'));
					   ),
					),
				);

				# Connect Loop to Parameter
				$the_query_query = new WP_Query( $the_query_args );
				?>

				<?php
				# Loop
				if ( $the_query_query->have_posts() ) : ?>
					<section id="ranges-filter">
						<div class="filter-galleries">
							<div class="container">
								<div class="row">
									 	<?php while ( $the_query_query->have_posts() ) : $the_query_query->the_post(); ?>
									 		<div class="col-xs-12 col-md-4 " style="text-align: center;margin-bottom: 20px;">
									 		    <a href="<?php echo get_the_permalink(); ?>">
									 		    	  <?php  $post_featured_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));; ?>
									 		    	  <?php if($post_featured_image): ?>
									 		    	     <img src="<?php echo $post_featured_image; ?>" title="<?php get_the_title(); ?>" alt="<?php get_the_title(); ?>">
									 		    	  <?php endif; ?>
									 		    	  <div>
									 		    	  	<strong><?php the_title(); ?></strong>
									 		    	  </div>
									 		    </a>
									 		</div>
									 	<?php endwhile; ?>
								    
								</div>
							</div>
						</div>
					
					</section>	
					<?php wp_reset_query(); ?>
				<?php else : ?>
				   <?php # Template Part | Blog
				   get_template_part('template-parts/general/content-no-post'); ?>
				<?php endif; ?>
				
			</article>
			<?php dn_post_edit_link(); ?>
			<?php endwhile; // end of the loop. ?>
		</main>
	</div>
</div>

<div class="section-how how-charcoal">
	<h2 class="acc-title">How It Works</h2>
	<h3 class="subtitle">The Weber Kettle Cooking System</h3>
	<div class="separator"><img src="<?php echo get_template_directory_uri(); ?>/img/how-line.png" alt="" width="240" height="9" /></div>
	<div class="desc">
		<p>We use two fires instead of one. The fat from the turkey will drop into the disposable drip tray below. This means that you get much healthier, fat free roasts, without any flare ups. We call this ‘indirect cooking’ because the food is cooked using natural convection without any fire directly underneath it. Apart from the unforgettable flavour, the great advantage of using this method of cooking is that you don’t even have to turn the meat. Your barbecue cooks the most extraordinary roast, every time, all by itself.</p>
	</div>
	<div class="snapshot-range">
		<div class="snapshot snap1" style="width: 350.866px; left: 436.104px; top: 336.979px;"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-charcoal/snap1.png" width="354" height="473" data-top="340" data-left="440" alt="Original Weber Flavour" /></div>
		<div class="snapshot snap1-mob mobile"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-charcoal/snap1-mob.png" width="161" height="270" data-top="280" data-left="540" alt="Original Weber Flavour" /></div>
		<div class="snapshot snap2" style="width: 333.025px; left: 1115.04px; top: 336.979px;"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-charcoal/snap2.png" width="336" height="323" data-top="340" data-left="1125" alt="World Famous Outdoor Roast" /></div>
		<div class="snapshot snap2-mob mobile"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-charcoal/snap2-mob.png" width="134" height="297" data-top="280" data-left="1225" alt="World Famous Outdoor Roast" /></div>
		<div class="snapshot snap3" style="width: 157.592px; left: 594.688px; top: 614.491px;"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-charcoal/snap3.png" width="159" height="416" data-top="620" data-left="600" alt="Smoke" /></div>
		<div class="snapshot snap4" style="width: 126.867px; left: 1174.51px; top: 614.491px;"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-charcoal/snap4.png" width="128" height="416" data-top="620" data-left="1185" alt="Smoke" /></div>
		<div class="snapshot snap5" style="width: 490.617px; left: 708.669px; top: 986.159px;"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-charcoal/snap5.png" width="495" height="174" data-top="995" data-left="715" alt="Cooke over charcoal for the authentic barbecue experience" /></div>
		<div class="main-image"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-charcoal/how-work-charcoal.png" alt="Weber Kettle Barbecue" width="1920" height="1238" data-margin="-310" /></div>
	</div>
</div>
<?php get_footer(); ?>