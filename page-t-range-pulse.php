<?php
/*
Template Name: Range Template - Weber Pulse
*/

get_header(); ?>
<div class="site-content ux2-wrapper">
	<div id="content" class="content-area">
		<main id="main" class="site-main" >
			<?php while ( have_posts() ) : the_post(); ?>
			<article>

				<?php
				# Parameter
				$the_query_args = array (
					'post_type' => array( 'product', ),
					'posts_per_page'  => -1,  # -1 for all
					'order'   => 'DESC',  # Newest
					'orderby' => 'date',  # 'rand' 'post__in'
					'tax_query' => array(
					   'relation' => 'OR', # ('AND','OR')

					   array(
					      'field'    => 'term_id',           #  ('term_id', 'name', 'slug', 'term_taxonomy_id')
					      'taxonomy' => 'range',     #  Taxonomy Name
					      'operator' => 'IN',                #  ('IN', 'NOT IN', 'AND', 'EXISTS' and 'NOT EXISTS')
					      'terms'    =>  array( 89,),        #  (int, string, array)

					      # Select all Terms
					      # 'terms'    =>  get_terms(array('taxonomy' => 'taxonomy-name','hide_empty' => false,'fields' => 'names'));
					      # 'terms'    =>  get_terms(array('taxonomy' => 'taxonomy-name','hide_empty' => false,'fields' => 'id=>slug'));
					      # 'terms'    =>  get_terms(array('taxonomy' => 'taxonomy-name','hide_empty' => false,'fields' => 'tt_ids'));
					   ),
					),
				);

				# Connect Loop to Parameter
				$the_query_query = new WP_Query( $the_query_args );
				?>

				<?php
				# Loop
				if ( $the_query_query->have_posts() ) : ?>
					<section id="ranges-filter">
						<div class="filter-galleries">
							<div class="container">
								<div class="row">
									 	<?php while ( $the_query_query->have_posts() ) : $the_query_query->the_post(); ?>
									 		<div class="col-xs-12 col-md-4 " style="text-align: center;margin-bottom: 20px;">
									 		    <a href="<?php echo get_the_permalink(); ?>">
									 		    	  <?php  $post_featured_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));; ?>
									 		    	  <?php if($post_featured_image): ?>
									 		    	     <img src="<?php echo $post_featured_image; ?>" title="<?php get_the_title(); ?>" alt="<?php get_the_title(); ?>">
									 		    	  <?php endif; ?>
									 		    	  <div>
									 		    	  	<strong><?php the_title(); ?></strong>
									 		    	  </div>
									 		    </a>
									 		</div>
									 	<?php endwhile; ?>
								    
								</div>
							</div>
						</div>
					
					</section>	
					<?php wp_reset_query(); ?>
				<?php else : ?>
				   <?php # Template Part | Blog
				   get_template_part('template-parts/general/content-no-post'); ?>
				<?php endif; ?>
				
			</article>
			<?php dn_post_edit_link(); ?>
			<?php endwhile; // end of the loop. ?>
		</main>
	</div>
</div>

<div class="content-t-range content-pulse-range">
	
	<div class="section-how how-weber-q how-pulse">
		<h2 class="acc-title">How It Works</h2>
		<h3 class="subtitle">Weber Pulse Cooking System</h3>
		<div class="separator"><img src="<?php echo get_template_directory_uri(); ?>/img/how-line.png" alt="" width="240" height="9" /></div>
		<div class="desc">This innovative electric barbecue is the perfect companion for anyone who wants an authentic barbecue experience, but with the convenience of electric power. While the 1000 series delivers all you need to cook up a feast for your special someone or closest friends, the larger 2000 series model boasts a bigger cooking space and dual zone cooking capability. The revolutionary dual zone barbecuing feature allows you to simultaneously cook different foods in two temperature zones, just as you would on a charcoal or gas barbecue.</div>
		<div class="snapshot-range pulse-snap-range">
			<div class="snapshot snap1"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-pulse/pulse-detail-update.png" width="1282" height="907" alt="Easy to use - Total control at your fingertips" /></div>
			<div class="main-image"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-pulse/pulse-big.png" alt="Weber Pulse barbecue" width="1282" height="907" /></div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
