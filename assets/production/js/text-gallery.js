jQuery(function($){
  
        $('.text-gallery-slider').each(function(){
            
            var $responsive = [];
            var $this = $(this)

            // responsive option
            if($this.hasClass('responsive-centered')){
                $responsive = [
                    {
                        breakpoint: 991,
                        settings: {
                            dots: false,
                            arrows: false,
                            infinite: false,
                            autoplay: true,
                            centerMode: true,
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    }
                ]
            }

            $this.slick({
                arrows: false,
                dots: true,
                infinite: true,
                autoplay: true,
                adaptiveHeight: false,
                responsive: $responsive
            });

            /* $this.slick({
                dots: false,
                arrows: false,
                infinite: false,
                autoplay: true,
                centerMode: true,
                responsive: [
                    {
                        breakpoint: 9999,
                        settings: "unslick"
                    },
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    }
                ]
            }); */
        })
    
})