jQuery(function($) {

    $(".testimonial-slider").each(function() {

      $(this).slick({
        arrows: false,
        dots: true,
        infinite: true,
        autoplay: false,
        adaptiveHeight: true
      });
  
    })
  
  })
  