/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./wp-content/themes/weber-landing/assets/src/js/main.js":
/*!***************************************************************!*\
  !*** ./wp-content/themes/weber-landing/assets/src/js/main.js ***!
  \***************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _maps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ~/maps.js */ "./wp-content/themes/weber-landing/assets/src/js/maps.js");
/* harmony import */ var _maps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_maps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _sliding_gallery_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ~/sliding-gallery.js */ "./wp-content/themes/weber-landing/assets/src/js/sliding-gallery.js");
/* harmony import */ var _sliding_gallery_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_sliding_gallery_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _testimonial_slider_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ~/testimonial-slider.js */ "./wp-content/themes/weber-landing/assets/src/js/testimonial-slider.js");
/* harmony import */ var _testimonial_slider_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_testimonial_slider_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _text_gallery_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ~/text-gallery.js */ "./wp-content/themes/weber-landing/assets/src/js/text-gallery.js");
/* harmony import */ var _text_gallery_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_text_gallery_js__WEBPACK_IMPORTED_MODULE_3__);




jQuery(function ($) {
  $(document).ready(function (e) {
    HeaderMenuFunction();
    GravityFormFunctionality(); //StickHeaderFunctionality();
    //BurgerMenuFunctionality();
    //AccordianMenuFunctionality(); 
    //DropDownMenuFunctionalty();
    //SlideFullMenuFunctionality();
    //SlickSliderFunctionalty();
    //DownArrowFunctionalty();
    //GoogleMapsFunctionality();
    //MasonaryLayoutFunctionality();
    //DynamicLoadPostsFunctionality();
    //SearchDropFunctionality();
    //CartDropFunctionality();
    //WooShopIsotopeFunctionality();
    //WooTabFunctionality();
    //InstagramFunctionality();
    //CookiesFooterFunctionality();
  }); // on change variation

  $(document).on('change', '.trigger-select-update', function () {
    var $this = $(this);
    var $value = $this.val();
    $(document).find('select[name="' + $this.attr('data-name') + '"]').val($value).change();

    if (weber_data_variation_price != false) {
      var selectedVariation = $('input[name="variation_id"]').val();
      var thePrice = weber_data_variation_price[selectedVariation];
      $('#weber-price-wrap').html('<p class="price">' + thePrice + '</p>');
    }
  }); // on load get selected variation

  $(document).ready(function () {
    var $this = $(document).find('.trigger-select-update');
    var selectedDefault = $(document).find('select[name="' + $this.attr('data-name') + '"]').val();

    if (selectedDefault != '') {
      $(document).find('.trigger-select-update[value="' + selectedDefault + '"]').attr('checked', 'checked');
    }
  }); // product category slider

  $('#related-accessories .inner-related-accessories ul.products').slick({
    arrows: true,
    dots: false,
    infinite: false,
    autoplay: false,
    adaptiveHeight: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    }, {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    } // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
  }); // popoup

  if ($('.open-video-popup').length > 0) {
    $('.open-video-popup').magnificPopup({
      type: 'iframe'
    });
  } // accessories


  if ($('.container-accessories').length > 0) {//$('.sidebar-filter, .container-loop').stick_in_parent({offset_top: 90});
  } // filter


  $('.sidebar-filter ul li a').click(function (e) {
    e.preventDefault(); // always move to top

    $('html, body').animate({
      scrollTop: $('.container-accessories').offset().top - 70
    });
    $(this).toggleClass('active');
    $('#ajax-append-target').addClass('loading');
    setTimeout(function () {
      doFiltering();
    }, 500);
  });

  function doFiltering() {
    if ($(document).find('.sidebar-filter .parent-cat ul li a.active').length > 0) {
      $('.sidebar-filter .child-cat ul li').hide();
    } else {
      $('.sidebar-filter .child-cat ul li').show();
    }

    var $parent_arr = [];
    $(document).find('.sidebar-filter .parent-cat ul li a.active').each(function (i, e) {
      var theFilter = $(this).attr('data-cat-id');
      $parent_arr.push(theFilter);
      $(document).find('.sidebar-filter .child-cat ul li a[data-parent-id="' + theFilter + '"]').parent().show();
    }); // map all active filter

    var $filter = [];
    $(document).find('.sidebar-filter ul li:visible a.active').each(function (i, e) {
      var $filterItem = $(this).attr('data-cat-id');

      if ($filterItem == '*') {
        $filterItem = '';
      } else {
        $filter.push($filterItem);
      }
    }); // if sub category clicked, remove parent from array to refine

    $(document).find('.sidebar-filter .child-cat ul li a.active').each(function () {
      var $_parent = $(this).attr('data-parent-id');
      $filter = removeItemAll($filter, $_parent);
    }); // reset no resulte

    $('li.product.not-found').hide();
    var allFilter = $filter.join(' ');

    if (allFilter != '') {
      $('.container-accessories ul.products li.product').each(function () {
        if ($(this).hasAnyClass(allFilter)) {
          $(this).show();
        } else {
          $(this).hide();
        }
      });
    } else {
      $('.container-accessories ul.products li.product').show();
    } // show no results


    if ($('.container-accessories ul.products li.product:visible:not(.not-found)').length < 1) {
      $('li.product.not-found').show();
    } else {
      $('li.product.not-found').hide();
    }

    setTimeout(function () {
      $('#ajax-append-target').removeClass('loading');
    }, 100);
  } // helper remove array


  function removeItemAll(arr, value) {
    var i = 0;

    while (i < arr.length) {
      if (arr[i] === value) {
        arr.splice(i, 1);
      } else {
        ++i;
      }
    }

    return arr;
  } // helper for filter


  $.fn.hasAnyClass = function () {
    for (var i = 0; i < arguments.length; i++) {
      var classes = arguments[i].split(" ");

      for (var j = 0; j < classes.length; j++) {
        if (this.hasClass(classes[j])) {
          return true;
        }
      }
    }

    return false;
  };

  $(document).on('click', '.sort-action', function (e) {
    e.preventDefault(); // styling

    $(this).parent().siblings('li').find('a').removeClass('active');
    $(this).addClass('active');
    $('#ajax-append-target').addClass('loading');
    var sortBy = $(this).attr('data-sort');
    var orientation = $(this).attr('data-order');
    setTimeout(function () {
      if (sortBy == 'alpha' && orientation == 'asc') {
        tinysort('ul.products > li', {
          attr: 'data-alpha',
          order: 'asc'
        });
      }

      if (sortBy == 'alpha' && orientation == 'desc') {
        tinysort('ul.products > li', {
          attr: 'data-alpha',
          order: 'desc'
        });
      }

      if (sortBy == 'price' && orientation == 'asc') {
        tinysort('ul.products > li', {
          attr: 'data-price',
          order: 'asc'
        });
      }

      if (sortBy == 'price' && orientation == 'desc') {
        tinysort('ul.products > li', {
          attr: 'data-price',
          order: 'desc'
        });
      }

      $('#ajax-append-target').removeClass('loading');
    }, 500);
  });
  $('.triger-filter').click(function (e) {
    e.preventDefault();
    $('.sidebar-filter').addClass('open-filter');
  });
  $('.container-accessories .sidebar-filter .close-filter a').click(function (e) {
    e.preventDefault();
    $('.sidebar-filter').removeClass('open-filter');
  });
  $('.next-button a.dn-button').click(function (e) {
    e.preventDefault(); // validation

    var _return = true;
    $('.validate-required input, .validate-required select').each(function (i, e) {
      if ($(this).val().trim() == '') {
        _return = false;
        $(this).parents('.form-row').addClass('woocommerce-invalid');
        return false; // breaks
      }
    });

    if (_return == false) {
      $('html,body').animate({
        scrollTop: $(document).find('.woocommerce-invalid:first').offset().top - 80
      });
      return;
    }

    var $next = parseFloat($(this).attr('data-next'));
    var pageLength = $('.checkout-steps ul li').length; // heading

    $('.checkout-steps ul li').removeClass('active');
    $('.checkout-steps ul li[data-page="' + $next + '"]').addClass('active');

    for (var i = 0; i < $next; i++) {
      $('.checkout-steps ul li:nth-child(' + i + ')').addClass('passed');
    } // content


    $('.woocommerce-checkout div[data-page]').hide();
    $('.woocommerce-checkout div[data-page="' + $next + '"]').show(); // scroll to top

    $('html,body').animate({
      scrollTop: $('.checkout-head').offset().top - 80
    });
  }); // All Header Options

  function HeaderMenuFunction() {
    // tell when to run the stuck
    var $offset = $('#masthead').attr('data-offset') == 'true' ? $('#masthead').height() + 10 : 30; // Sticky header

    $(window).scroll(function () {
      if ($(document).scrollTop() > $offset) {
        $('body').addClass('header-stuck');
      } else {
        $('body').removeClass('header-stuck');
      }
    }); // Burger menu toggle

    $(document).on('click', 'button.js-hamburger', function (e) {
      e.preventDefault();
      $(this).toggleClass('is-active');
      $('body').toggleClass('mobile-menu-open'); // hook so other function can runnig

      $(window).trigger('hamburger-menu-click');
    }); // Disable parent menu to be clickable

    $('.main-navigation li.menu-item-has-children > a').on('click', function (e) {
      e.preventDefault();
    }); // Open close search

    $('.menu-search').click(function (e) {
      e.preventDefault();
      $('body').toggleClass('search-open');
      $('.header-search-form .search-field').focus(); // fullscreen style

      if ($('.header-search-form').hasClass('fullscreen')) {
        $('.header-search-form').fadeToggle(200, function () {
          // focus the search
          $('.header-search-form .search-field').focus();
        });
      }
    }); // close fullscreen

    $('.fullscreen-shader').click(function (e) {
      e.preventDefault();
      $('body').removeClass('search-open');
      $('.header-search-form').fadeOut(200);
    });
  } // Gravity Label Functionality


  function GravityFormFunctionalitySetup() {
    // label hover style
    $('li.gfield input, li.gfield textarea').on('change keyup focusout', function () {
      var isEmpty = $(this).val();

      if (isEmpty == '') {
        $(this).parents('li.gfield').removeClass('filled');
      } else {
        $(this).parents('li.gfield').addClass('filled');
      }
    }); // trigger by default

    $('li.gfield input, li.gfield textarea').trigger('focusout'); // label hover style

    $('li.gfield input, li.gfield textarea').on('focusin', function () {
      $(this).parents('li.gfield').addClass('filled');
    }); // Scroll the user to the top of the form if there is an error on this page

    if ($(".gform_wrapper.gform_validation_error").length > 0) {
      $('html, body').animate({
        scrollTop: $(".gform_wrapper").offset().top - 140
      }, 1);
    } // Setup this label for movement
    // Form choice placeholder


    $(".form-label-move").find(".ginput_container.ginput_container_text, .ginput_container.ginput_container_textarea, .ginput_container.ginput_container_email, .dn-form-input-container").each(function () {
      // Set this label up for greateness
      $(this).siblings("label").addClass("text-field"); // On focusing of this elements

      $(this).find("input, textarea").focusin(function () {
        $(this).parent().siblings("label").addClass("focus");
      }); // If any of these fields already have content

      $(this).find("input, textarea").each(function () {
        if ($(this).val() != '') {
          $(this).parent().siblings("label").addClass("focus");
        }
      }); // On stop focusing of this elements

      $(this).find("input, textarea").focusout(function () {
        // If this field has no contents
        if ($(this).val() == '') {
          $(this).parent().siblings("label").removeClass("focus");
        }
      });
    }); // Setup live form validation
    // Add required fields in place of grav forms classes

    $(".gfield_contains_required").find('input[type="text"], input[type="email"], input[type="url"], input[type="password"], input[type="search"], input[type="number"], input[type="tel"],  input[type="date"], input[type="month"], input[type="week"], input[type="time"], input[type="datetime"], input[type="datetime-local"], input[type="color"], textarea').addClass("required"); // On unclicking a form field

    $("input.required:not(.ignore), textarea.required:not(.ignore), .gfield_contains_required select").focusout(function () {
      if ($(this).val() == '') {
        $(this).addClass("error");
      } else {
        $(this).removeClass("error");
        $(this).parents(".gfield.gfield_error").removeClass("gfield_error");
      }
    });
  } // service accordion


  $(document).on('click', '.dn-services .service-item .heading:not(.disable-accordion)', function (e) {
    e.preventDefault();
    $(this).parent().toggleClass('active');
    $(this).siblings('.content').slideToggle();
  }); // slide menu

  function SlideFullMenuFunctionality() {
    if ($('.header-slide-full').length > 0) {
      var count = 0;
      $('.header-slide-full .the-menu .menu > li').each(function (i, e) {
        $(this).addClass('delay-' + i);
        count++;
      }); // the link

      $('.header-slide-full .links').addClass('delay-' + (count + 1));
    }
  } // Creates functionality for burger menus


  function BurgerMenuFunctionality() {
    if ($('#burger,#burger-blocky').length > 0) {
      var MenuOpenReady = true;
      $('#burger,#burger-blocky').click(function () {
        if (MenuOpenReady) {
          MenuOpenReady = false;
          setTimeout(function () {
            MenuOpenReady = true;
          }, 250);

          if ($('#burger,#burger-blocky').hasClass("open")) {
            CloseMenu();
          } else {
            OpenMenu();
          }
        }
      });
    }
  }

  function OpenMenu() {
    // Close the search
    CloseSearchDrop(); // Close the side-cart

    CloseSideCart();
    $("#header-menu").addClass("appear");
    $("#masthead").addClass("filled-menu");
    $("body").addClass("mobile-menu-open");
    $(".hamburger-blush").addClass('is-active');
    $("#burger,#burger-blocky").addClass('open');
    ShowMenuShader();
    LockPageScroll();
  }

  function CloseMenu() {
    $("#header-menu").removeClass("appear");
    $("body").removeClass("mobile-menu-open");
    $("#masthead").removeClass("filled-menu");
    $(".hamburger-blush").removeClass('is-active');
    $("#burger,#burger-blocky").removeClass('open');
    HideMenuShader();
    UnlockPageScroll();
  } // Creates functionality for accordian menus


  function AccordianMenuFunctionality() {
    // For each accordian
    $(".accordian").each(function () {
      // Hide the icons for those menu elements that do not have children
      $(this).find("li").each(function () {
        if (!$(this).find("ul.children, ul.sub-menu").length > 0) {
          $(this).find("i").hide();
        }
      }); // Setup the menu

      $(this).find(".sub-menu, .children").each(function () {
        $(this).hide();
        $(this).parent().addClass("accord-parent");
      }); // CLick functionality

      $(this).find("li i:last-of-type()").hide();
      $(this).find("li i").mousedown(function () {
        $(this).toggle();
        $(this).siblings("i").toggle();
        $(this).parent().find(".sub-menu, .children").slideToggle(200);
      });
    });
    $(".accordian-menu-disable-parent").each(function () {
      // Hide the icons for those menu elements that do not have children
      $(this).find("li").each(function () {
        if (!$(this).find("ul.children, ul.sub-menu").length > 0) {
          $(this).find("i").hide();
        }
      }); // Setup the menu

      $(this).find(".sub-menu, .children").each(function () {
        $(this).hide();
        $(this).parent().addClass("accord-parent");
      }); // CLick functionality

      $(this).find("li i:last-of-type()").hide();
      $(this).find("li i").mousedown(function () {
        $(this).toggle();
        $(this).siblings("i").toggle();
        $(this).parent().find(".sub-menu, .children").slideToggle(200);
      });
      $(this).find("ul.menu-item-has-children > li > a").on('click', function (e) {
        e.preventDefault();
        $(this).siblings("i").toggle();
        $(this).parent().find(".sub-menu, .children").slideToggle(200);
      });
    }); // For each accordian

    $(".accordian-category .toggle-open").click(function (e) {
      e.preventDefault();
      var $this = $(this);

      if ($this.hasClass('icon-plus')) {
        $this.removeClass('icon-plus');
        $this.addClass('icon-minus');
        $this.siblings('.children').slideDown();
      } else {
        $this.removeClass('icon-minus');
        $this.addClass('icon-plus');
        $this.siblings('.children').slideUp();
      } // reset the others


      $this.parent().siblings('li').find('.children').slideUp();
      $this.parent().siblings('li').find('.toggle-open').removeClass('icon-minus');
      $this.parent().siblings('li').find('.toggle-open').addClass('icon-plus');
    }); // Open by default

    if ($(document).find('.accordian-category .current-cat').length > 0) {
      var $current_cat = $(document).find('.accordian-category .current-cat');
      $current_cat.parent().siblings('.toggle-open').trigger('click');

      if ($current_cat.parent().hasClass('accordian-category')) {
        $current_cat.find('.toggle-open').trigger('click');
      }
    }
  } // Creates functionality for dropdown menus


  function DropDownMenuFunctionalty() {
    if ($(".menu-container.drop-menu").length > 0) {
      // Hide all other sub-menu's
      var ClearCurrentSubMenu = function ClearCurrentSubMenu() {
        $(".menu-container.drop-menu li.menu-item-has-children.dropped .sub-menu").hide();
        $(".menu-container.drop-menu li.menu-item-has-children.dropped").removeClass("dropped");
      };

      // Hide those & sub-menus icons properly
      $(".menu-container.drop-menu .sub-menu").hide();
      $(".menu-container.drop-menu").removeClass("hidden-sub"); // Setup hover effects

      $(".menu-container.drop-menu li.menu-item-has-children").mouseenter(function () {
        ClearCurrentSubMenu(); // Setup this class

        $(this).addClass("dropped");
        $(this).find(".sub-menu").show();
      });
      $(".menu-container.drop-menu li.menu-item-has-children").mouseleave(function () {
        ClearCurrentSubMenu();
      }); // Make top level menu items not clickable in the header (on desktop)

      $(".menu-container.drop-menu li.menu-item-has-children > a").on('click', function (e) {
        e.preventDefault();
      });
    }
  } // Sets up various slick sliders


  function SlickSliderFunctionalty() {
    // Regular slider container
    if ($(".slick-slider-container").length > 0) {
      if ($(".slick-slider-container").children().length > 1) {
        $(".slick-slider-container").each(function () {
          var $this = $(this);

          if ($this.hasClass('testimonial-slider')) {
            $this.slick({
              arrows: false,
              dots: true,
              infinite: true,
              autoplay: false,
              adaptiveHeight: true
            });
            /* $this.on('setPosition', function () {
            	$this.find('.slick-slide').matchHeight();
            }) */
          } else {
            $this.slick({
              arrows: false,
              dots: true,
              infinite: true,
              autoplay: true,
              adaptiveHeight: true
            });
          }
        });
      }
    } // text gallery


    if ($('.text-gallery-slider').length > 0) {
      $('.text-gallery-slider').each(function () {
        var $responsive = [];
        var $this = $(this); // responsive option

        if ($this.hasClass('responsive-centered')) {
          $responsive = [{
            breakpoint: 991,
            settings: {
              dots: false,
              arrows: false,
              infinite: false,
              autoplay: true,
              centerMode: true,
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }];
        }

        $this.slick({
          arrows: false,
          dots: true,
          infinite: true,
          autoplay: true,
          adaptiveHeight: false,
          responsive: $responsive
        });
        /* $this.slick({
        	dots: false,
        	arrows: false,
        	infinite: false,
        	autoplay: true,
        	centerMode: true,
        	responsive: [
        		{
        			breakpoint: 9999,
        			settings: "unslick"
        		},
        		{
        			breakpoint: 991,
        			settings: {
        				slidesToShow: 1,
        				slidesToScroll: 1,
        			}
        		}
        	]
        }); */
      });
    } // Mega slider container


    if ($(".mega-slick-slider-container").length > 0) {
      if ($(".mega-slick-slider-container").children().length > 1) {
        $('.mega-slick-slider-container').slick({
          dots: true,
          infinite: true,
          autoplay: true,
          prevArrow: "<i class='icon-chevron-left slick-prev'></i>",
          nextArrow: "<i class='icon-chevron-right slick-next'></i>"
        });
      }
    } // Feature 


    if ($(".dn-feature-carousel").length > 0) {
      // Main slick slider
      $(".dn-feature-carousel .main-image").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.dn-feature-carousel .selector'
      }); // Carousel slider

      $(".dn-feature-carousel .selector").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: "<i class='icon-chevron-left slick-prev'></i>",
        nextArrow: "<i class='icon-chevron-right slick-next'></i>",
        asNavFor: '.dn-feature-carousel .main-image',
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        responsive: [{
          breakpoint: 991,
          settings: {
            slidesToShow: 2
          }
        }]
      }); // Main lightboxes

      $('.dn-feature-carousel .main-image .image-container').magnificPopup({
        type: 'image',
        gallery: {
          enabled: true
        }
      }); // Carousel lightboxes

      $('.dn-feature-carousel .selector .image-container').magnificPopup({
        type: 'image',
        gallery: {
          enabled: true
        }
      });
    } // Product 


    if ($(".dn-product-carousel").length > 0) {
      $(".dn-product-carousel .main-image").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        infinite: false,
        asNavFor: '.dn-product-carousel .selector',
        prevArrow: "<i class='icon-chevron-left slick-prev'></i>",
        nextArrow: "<i class='icon-chevron-right slick-next'></i>"
      });
      $(".dn-product-carousel .selector").slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        infinite: false,
        asNavFor: '.dn-product-carousel .main-image',
        dots: false,
        vertical: true,
        arrows: false,
        centerMode: false,
        focusOnSelect: true,
        responsive: [{
          breakpoint: 991,
          settings: {
            slidesToShow: 2
          }
        }]
      }); // Create lightboxes

      $('.dn-product-carousel .main-image .image-container').magnificPopup({
        type: 'image'
      });
    } // Product-mini 


    if ($(".dn-product-carousel-mini").length > 0) {
      $(".dn-product-carousel-mini .slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        fade: true,
        asNavFor: '.dn-product-carousel .selector'
      }); // Create lightboxes

      $('.dn-product-carousel .main-image .image-container').magnificPopup({
        type: 'image',
        gallery: {
          enabled: true
        }
      });
    }
  } // Creates funcitonality for the down arrow


  function DownArrowFunctionalty() {
    // If the user clicks on the next section arrow
    $("#dn-next-sec-arrow").mousedown(function () {
      // Find the scrolltop height for this objects parent
      var offSetTop = $(this).parent().offset().top + $(window).height() // Add the window height
      - $("#masthead").outerHeight(); // minus the menu top height
      // Scroll to the next section

      $('html, body').animate({
        scrollTop: offSetTop
      }, 500);
    });
  }

  function OpenNewsFilter() {
    // Close the search & mini-cart
    CloseSearchDrop();
    CloseSideCart(); // fade in the BG

    $("#post-filter-bg").fadeIn(250); // Bring in the news filter from the side

    $("#post-filter-container").addClass("appear");
    $("#filter-bottom").addClass("appear"); // Lock the background

    LockPageScroll();
  }

  function CloseNewsFilter() {
    // Create the final list of categories to display
    var finalCatList = "";
    var finalCatListNice = ""; // Scan the list for the categories

    $("#post-filter-container .middle a.selected").each(function () {
      if (finalCatList != "") {
        finalCatList += ", ";
      }

      finalCatList += "." + RegexReplace($(this).text());

      if (finalCatListNice != "") {
        finalCatListNice += ", ";
      }

      finalCatListNice += $(this).text();
    }); // Show only those in the Masonary display
    // Display all cats

    if (finalCatList == "") {
      $(".masonary-layout-container").isotope({
        filter: "*"
      }); // Display just the requires cats
    } else {
      $(".masonary-layout-container").isotope({
        filter: finalCatList
      });
    } // Show/Hide no posts


    if (!$(".masonary-layout-container").data('isotope').filteredItems.length) {
      $("#no-posts").show();
    } else {
      $("#no-posts").hide();
    } // console.log( finalCatList );
    // Update the Filter news with the current cats showing


    if (finalCatListNice != "") {
      $("#post-filter .results").text(': ' + finalCatListNice);
    } else {
      $("#post-filter .results").text("");
    } // fade in the BG


    $("#post-filter-bg").fadeOut(250); // Bring in the news filter from the side

    $("#post-filter-container").removeClass("appear");
    $("#filter-bottom").removeClass("appear"); // Unlock the background

    UnlockPageScroll(); // Scroll user back to the top of the section

    $('html, body').animate({
      scrollTop: $("#main").offset().top - 100
    }, 500);
  }

  function DynamicLoadPostsFunctionality() {
    // Keep track of how many posts have loaded
    $("#dn-dynamic-load-posts .dn-button").hide();
    $("#dn-dynamic-load-posts .dn-button").removeClass("hidden");
    CheckForLoadPosts();
    $(window).scroll(function () {
      CheckForLoadPosts();
    });
    $("#dn-dynamic-load-posts .dn-button").click(function (e) {
      // Hide the button
      e.preventDefault();
      $(this).hide();
      PostLoaderFunctionality();
    });

    function CheckForLoadPosts() {
      if ($("#dn-dynamic-load-posts").length > 0 && !LoadingMorePosts) {
        // Calculate the pixel distance of the bottom of the screen
        var windowBottomDistanceCalc = $(window).scrollTop() + $(window).height(); // If this is the first 2 times we're autoloading posts

        if (PostsLoaded < PostLoadsBeforeButton) {
          // If we've reached the final blog post and we're not currently loading other blog posts
          if (windowBottomDistanceCalc > $("#dn-dynamic-load-posts").offset().top) {
            PostLoaderFunctionality();
          }
        }
      }
    }

    function PostLoaderFunctionality() {
      PostsLoaded++; // indicate we're loading more blog posts

      LoadingMorePosts = true; // Show the post loading element

      $("#dn-dynamic-load-posts .post-loading").removeClass("hidden"); // Indicate the miniumum loading time has not been met yet

      MinLoadingWaitingTimeCompleted = false; // Load the blog posts

      LoadMorePosts(5); // Gives a minimum loading time to show users the loading thing

      setTimeout(function () {
        MinLoadingWaitingTimeCompleted = true;
      }, MinLoadingTime);
    }
  }

  function LoadMorePosts(NumberOfPostsToLoad) {
    // find the number of blog articles here
    var PostCount = $(".dn-single-post").length; // If this is a categories page

    var cattype = "";

    if ($("#cat-type").length > 0) {
      cattype = $("#cat-type").attr("data-cattype");
    }

    jQuery.ajax({
      url: dn_variable.ajax_url,
      type: 'post',
      data: {
        action: 'post_entries_find_entries',
        current_posts: PostCount,
        post_type: 'post',
        cattype: cattype,
        fetch_amount: NumberOfPostsToLoad
      },
      success: function success(data) {
        LoadMorePosts_Success(data);
      },
      error: function error(data) {
        console.log("AJAX request failed");
      }
    });
  }

  function LoadMorePosts_Success(data) {
    CompleteLoadingFunctionality();

    function CompleteLoadingFunctionality() {
      // if the minmum laoding time has been met
      if (MinLoadingWaitingTimeCompleted) {
        // Hide the post loading element
        $("#dn-dynamic-load-posts .post-loading").addClass("hidden"); // Post the new blog posts data into the page

        var json = JSON.parse(data); // Paste the actual data ino 

        $("#dn-dynamic-load-posts").before(json.html); // If there are not more blog posts to load, delete the loader element

        if (json.is_final) {
          $("#dn-dynamic-load-posts").remove();
        } // Indicate we're finished loading blog posts


        LoadingMorePosts = false; // Show the button if the posts loading is appropriate

        if (PostsLoaded >= PostLoadsBeforeButton) {
          $("#dn-dynamic-load-posts .dn-button").show();
        }
      } else {
        // Check again shortly
        setTimeout(function () {
          CompleteLoadingFunctionality();
        }, 100);
      }
    }
  }

  function SearchDropFunctionality() {
    if ($("#dn-search-drop").length > 0 && $("#dn-search-drop-button").length > 0) {
      // close button
      $('#dn-search-drop.full-style .title-close a, #dn-search-drop.full-style .shade').click(function (e) {
        e.preventDefault();
        $('#dn-search-drop').toggleClass('open-full-search');
      });
      $("#dn-search-drop-button").click(function () {
        if ($(this).hasClass('full-page')) {
          // Open full drop search
          $('#dn-search-drop').toggleClass('open-full-search');
        } else {
          // Open the search
          if (!$("#dn-search-drop").hasClass("dropped")) {
            OpenSearchDrop(); // Close the search
          } else {
            CloseSearchDrop();
          }
        }
      });
    }
  }

  function OpenSearchDrop() {
    // Close the menu
    CloseMenu(); // Close the side-cart

    CloseSideCart();
    $("#dn-search-drop").addClass("dropped");
  }

  function CloseSearchDrop() {
    $("#dn-search-drop").removeClass("dropped");
  }

  function CartDropFunctionality() {
    if ($("#dn-side-cart-menu").length > 0 && $("#dn-cart-pop-button").length > 0) {
      $(document).on("click touchstart", "#dn-cart-pop-button", function () {
        OpenSideCart();
      });
      $("#close-dn-side-cart-menu").click(function () {
        CloseSideCart();
      });
    }
  }

  function OpenSideCart() {
    // Close the search
    CloseSearchDrop(); // Close the menu

    CloseMenu(); // Open the cart

    $("#dn-side-cart-menu").addClass("appear"); // Lock the page scrolling

    LockPageScroll();
  }

  function CloseSideCart() {
    // Close the cart
    $("#dn-side-cart-menu").removeClass("appear"); // Allow page to scroll again

    UnlockPageScroll();
  }

  function GravityFormFunctionality() {
    // Run once on startup
    GravityFormFunctionalitySetup(); // Run on failed sbumissions

    jQuery(document).bind('gform_post_render', function () {
      if ($(".validation_error").length > 0) {
        GravityFormFunctionalitySetup();
      }
    });
  }

  var scrollPosition;
  var scrollLocked = false;

  function LockPageScroll() {
    /* scrollPosition = [
    	self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
    	self.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop
    ];
    var html = jQuery('body'); // it would make more sense to apply this to body, but IE7 won't have that
    scrollLocked = true;
    html.css({
    	'position': 'fixed',
    	"left": "0",
    	"top": "-" + scrollPosition[1] + "px",
    	"overflow": "hidden",
    	"width": "100%",
    }); */
  }

  function UnlockPageScroll() {
    /* 
    		// If the page scroll is locked
    		if (scrollLocked) {
    			var html = jQuery('body');
    			scrollLocked = false;
    			html.css({
    				'position': 'static',
    				"left": "auto",
    				"top": "auto",
    				"overflow": "visible",
    				"width": "auto",
    			});
    			window.scrollTo(scrollPosition[0], scrollPosition[1]);
    		} */
  }

  function WooShopIsotopeFunctionality() {
    // if this is the woo shop page
    if ($("body.post-type-archive-product.woocommerce").length > 0) {
      // ASSIGN ISOTOPE
      var $container = $('.product-loop-container');
      var buttonFilter = '*';
      $container.isotope({
        itemSelector: '.product',
        resizable: false,
        // disable normal resizing
        layoutMode: 'fitRows',
        filter: function filter() {
          var $this = $(this);
          var rangeFilter = $('#woo-filter-price');
          var RangeMin = parseFloat(rangeFilter.attr('data-filter-min'));
          var RangeMax = parseFloat(rangeFilter.attr('data-filter-max'));
          var price = parseFloat($this.attr('data-price'));
          var isPriceRange = price <= RangeMax && price >= RangeMin;
          return $this.is(buttonFilter) && isPriceRange;
        }
      }); // FILTER CATEGORY

      $('.dn-woo-category-filter li a').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        var btnFilter = [];
        $('.dn-woo-category-filter li a.active').each(function () {
          btnFilter.push($(this).attr('data-filter'));
        });
        buttonFilter = btnFilter.join(',');

        if (buttonFilter == '') {
          buttonFilter = '*';
        }

        $container.isotope();
      }); // FILTER PRICE

      var $priceFilterContainer = $('#woo-filter-price');
      var $priceMin = parseFloat($priceFilterContainer.attr('data-min'));
      var $priceMax = parseFloat($priceFilterContainer.attr('data-max'));
      $priceFilterContainer.slider({
        range: true,
        min: $priceMin,
        max: $priceMax,
        values: [$priceMin, $priceMax],
        create: function create(e, ui) {
          $priceFilterContainer.find('span.ui-slider-handle:eq(0)').addClass('range-min').html('<span class="slide-price">$' + $priceFilterContainer.slider("values", 0) + '</span>');
          $priceFilterContainer.find('span.ui-slider-handle:eq(1)').addClass('range-max').html('<span class="slide-price">$' + $priceFilterContainer.slider("values", 1) + '</span>');
        },
        slide: function slide(event, ui) {
          $(ui.handle).html('<span class="slide-price">$' + ui.value + '</span>');
          $priceFilterContainer.attr('data-filter-min', $priceFilterContainer.slider("values", 0));
          $priceFilterContainer.attr('data-filter-max', $priceFilterContainer.slider("values", 1));
        },
        stop: function stop(event, ui) {
          $container.isotope();
        }
      });
    }
  }

  function WooTabFunctionality() {
    ///////////////////////////
    // Desktop functionality //
    ///////////////////////////
    // Set the first tab to active
    $(".dn-wc-tabs > div:first-child").addClass("active-desktop");
    $(".dn-tabs-panel:first").addClass("active-desktop"); // If a user clicks on a tab

    $('.tabs-accordion-header a').click(function (e) {
      // Prevent default behaviour
      e.preventDefault(); // Remove the currently active class

      $('.tabs-accordion-header').removeClass("active-desktop");
      $(".dn-tabs-panel").removeClass("active-desktop"); // Make this button & tab active

      var ItemPosition = $(this).parent().index();
      $(this).parent().addClass("active-desktop");
      $(".dn-tabs-panel").eq(ItemPosition).addClass("active-desktop");
    }); ////////////////////////
    // Resp functionality //
    ////////////////////////
    // Set the first tab to active

    $('.dn-tabs-panel:first .tabs-accordion-header a').addClass("active-resp");
    $(".dn-tabs-panel:first .dn-tabs-inner").addClass("active-resp"); // If a user clicks on a tab

    $('.tabs-accordion-header-resp a').click(function (e) {
      // if this class is not currently active
      if (!$(this).hasClass(".active-resp")) {
        // Prevent default behaviour
        e.preventDefault(); // Remove the currently active class

        $('.tabs-accordion-header-resp a.active-resp').removeClass("active-resp");
        $(".dn-tabs-inner.active-resp").removeClass("active-resp"); // Make this the new active class

        $(this).addClass("active-resp");
        $(this).parent().siblings(".dn-tabs-inner").addClass("active-resp");
      }
    });
  }

  function RegexReplace(ArgString) {
    return ArgString.replace(/[\W\s\/]+/g, '_');
  }

  function InstagramFunctionality() {
    // If te instagram container is shown on the page
    if ($("#dn-instafeed").length > 0) {
      // Max of 12 available
      var ImagesToPull = 8;
      jQuery.ajax({
        url: dn_variable.ajax_url,
        type: 'post',
        data: {
          action: 'dn_insta_js_pull_func',
          ImagesToPull: ImagesToPull
        },
        success: function success(data) {
          // Convert the json data
          data = JSON.parse(data); // Loop through each of the images

          data['images'].forEach(function (element) {
            // Create strig to be added
            var temp_add_string = '<a href="https://www.instagram.com/p/' + element['shortcode'] + '" target="_blank" rel="nofollow" class="insta-image special-link" style="background-image: url(' + element['image'] + ');"></a>';
            $("#dn-instafeed").append(temp_add_string);
          });
        },
        error: function error(data) {
          console.log("AJAX request failed");
        }
      });
    }
  }

  function CookiesFooterFunctionality() {
    $("#cookies-footer-notice a.dn-button").click(function (e) {
      // Stop the button from doing it's thinggg
      e.preventDefault(); // Add a cookie to the page

      setCookie("site-cookies-agreement", "site-cookies-agreement-agreed", 99); // Make it dissapear

      $("#cookies-footer-notice").slideUp();
    });
  }

  function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
});
/**
   * DN fee update on cart page using ajax
   */

(function ($) {
  $(document).ready(function () {
    $(document).on('change keyup paste', '.woocommerce-cart input.delivery_assembly', function () {
      $('.cart_totals').block({
        message: null,
        overlayCSS: {
          background: '#fff',
          opacity: 0.6
        }
      });
      var cart_id = $(this).data('cart-id');

      if (jQuery(this).prop("checked") == true) {
        var dn = 'on';
      } else if (jQuery(this).prop("checked") == false) {
        var dn = '';
      }

      var cart_notes = $('#cart_notes_' + cart_id).val();
      var wp_ajax_url = dn_variable.ajax_url;
      var data = {
        action: 'prefix_update_cart_notes',
        'security': dn,
        'notes': cart_notes,
        'cart_id': cart_id
      };
      jQuery.post(wp_ajax_url, data, function (response) {
        console.log(response);
        location.reload(); //$('.cart_totals').unblock();
      });
    });
  });
})(jQuery);

/***/ }),

/***/ "./wp-content/themes/weber-landing/assets/src/js/maps.js":
/*!***************************************************************!*\
  !*** ./wp-content/themes/weber-landing/assets/src/js/maps.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// GMap Style
// Please refer to this site: https://snazzymaps.com/
var GMapStyle = [];
jQuery(function ($) {
  // Add Google Map
  $('.gmap').each(function () {
    initMap($(this).data("lat"), $(this).data("lng"), $(this).data("icon"), $(this));
  });

  function initMap(lat, lng, icon_url, $container) {
    var viewCentreLat = lat;
    var viewCentreLng = lng;

    if ($(window).width() > 991) {// viewCentreLat -= 0.0035;
      // viewCentreLng += 0.015;
    }

    var mapClass = $container;
    var map = new google.maps.Map(mapClass[0], {
      scrollwheel: false,
      zoom: 15,
      styles: GMapStyle,
      center: new google.maps.LatLng(viewCentreLat, viewCentreLng),
      mapTypeId: 'roadmap',
      disableDefaultUI: true
    }); // Disable map drag on mobile
    // if ( $(window).width() < 992 ) {
    // map.setOptions({draggable: false});
    // }

    var IconSize = new google.maps.Size(55, 72);

    if ($(window).width() < 992) {
      IconSize = new google.maps.Size(39, 52);
    }

    var feature = {
      position: new google.maps.LatLng(lat, lng)
    };
    var icon = {
      url: icon_url,
      scaledSize: IconSize
    };
    var marker = new google.maps.Marker({
      position: feature.position,
      icon: icon,
      scaledSize: new google.maps.Size(10, 10),
      map: map
    }); // create info window

    var infowindow = new google.maps.InfoWindow({
      content: $container.siblings(".bubble-info").html()
    }); // show info window when marker is clicked

    google.maps.event.addListener(marker, 'click', function () {
      infowindow.open(map, marker);
    });
  }
});

/***/ }),

/***/ "./wp-content/themes/weber-landing/assets/src/js/sliding-gallery.js":
/*!**************************************************************************!*\
  !*** ./wp-content/themes/weber-landing/assets/src/js/sliding-gallery.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

jQuery(function ($) {
  // Hero image repeater container
  if ($(".dn-image-hero-repeats").length > 0) {
    // Create lightboxes
    $('.dn-image-hero-repeats .image-container').magnificPopup({
      type: 'image',
      gallery: {
        enabled: true
      },
      callbacks: {
        elementParse: function elementParse(item) {
          // the class name
          if ($(item.el.context).hasClass('video-link')) {
            item.type = 'iframe';
          } else {
            item.type = 'image';
          }
        }
      }
    }); // Don't run the slider if

    if ($(window).width() >= 768) {
      // Desktop
      // If there's less than 8 images
      if ($(".dn-image-hero-repeats").children().length < 5) {
        return;
      }
    } else {
      // Mobile
      // If there's only 1 image
      if ($(".dn-image-hero-repeats").children().length < 2) {
        return;
      }
    } // Determine the type of slider it will be


    var ImageHeroCount = $(".dn-image-hero-repeats").children().length;
    var HeroImagerows = 2; // If there are very few images or we are on mobile go for a single row

    if (ImageHeroCount < 8 || $(window).width() < 768) {
      HeroImagerows = 1;
    } // Remove the default flex container


    $(".dn-image-hero-repeats").removeClass("flex-container");

    if (HeroImagerows > 1) {
      // Setup the slider
      $('.dn-image-hero-repeats').slick({
        "rows": 2,
        "slidesPerRow": 4,
        "slidesToScroll": 1 / 4,
        infinite: true,
        prevArrow: "<i class='icon-chevron-left slick-prev'></i>",
        nextArrow: "<i class='icon-chevron-right slick-next'></i>",
        responsive: [{
          breakpoint: 1199,
          settings: {
            "slidesPerRow": 3,
            "slidesToScroll": 1 / 3
          }
        }, {
          breakpoint: 991,
          settings: {
            "slidesPerRow": 2,
            "slidesToScroll": 1 / 2
          }
        }, {
          breakpoint: 767,
          settings: {
            "slidesPerRow": 1,
            "slidesToScroll": 1
          }
        }]
      });
    } else {
      // Setup the slider
      $('.dn-image-hero-repeats').slick({
        "slidesToShow": 4,
        "slidesToScroll": 1,
        infinite: true,
        prevArrow: "<i class='icon-chevron-left slick-prev'></i>",
        nextArrow: "<i class='icon-chevron-right slick-next'></i>",
        responsive: [{
          breakpoint: 1199,
          settings: {
            "slidesToShow": 3
          }
        }, {
          breakpoint: 991,
          settings: {
            "slidesToShow": 2
          }
        }, {
          breakpoint: 767,
          settings: {
            "slidesToShow": 1
          }
        }]
      });
    }
  }
});

/***/ }),

/***/ "./wp-content/themes/weber-landing/assets/src/js/testimonial-slider.js":
/*!*****************************************************************************!*\
  !*** ./wp-content/themes/weber-landing/assets/src/js/testimonial-slider.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

jQuery(function ($) {
  $(".testimonial-slider").each(function () {
    $(this).slick({
      arrows: false,
      dots: true,
      infinite: true,
      autoplay: false,
      adaptiveHeight: true
    });
  });
});

/***/ }),

/***/ "./wp-content/themes/weber-landing/assets/src/js/text-gallery.js":
/*!***********************************************************************!*\
  !*** ./wp-content/themes/weber-landing/assets/src/js/text-gallery.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

jQuery(function ($) {
  $('.text-gallery-slider').each(function () {
    var $responsive = [];
    var $this = $(this); // responsive option

    if ($this.hasClass('responsive-centered')) {
      $responsive = [{
        breakpoint: 991,
        settings: {
          dots: false,
          arrows: false,
          infinite: false,
          autoplay: true,
          centerMode: true,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }];
    }

    $this.slick({
      arrows: false,
      dots: true,
      infinite: true,
      autoplay: true,
      adaptiveHeight: false,
      responsive: $responsive
    });
    /* $this.slick({
        dots: false,
        arrows: false,
        infinite: false,
        autoplay: true,
        centerMode: true,
        responsive: [
            {
                breakpoint: 9999,
                settings: "unslick"
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    }); */
  });
});

/***/ }),

/***/ 1:
/*!*********************************************************************!*\
  !*** multi ./wp-content/themes/weber-landing/assets/src/js/main.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Applications/MAMP/htdocs/weber/wp-content/themes/weber-landing/assets/src/js/main.js */"./wp-content/themes/weber-landing/assets/src/js/main.js");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vd3AtY29udGVudC90aGVtZXMvd2ViZXItbGFuZGluZy9hc3NldHMvc3JjL2pzL21haW4uanMiLCJ3ZWJwYWNrOi8vLy4vd3AtY29udGVudC90aGVtZXMvd2ViZXItbGFuZGluZy9hc3NldHMvc3JjL2pzL21hcHMuanMiLCJ3ZWJwYWNrOi8vLy4vd3AtY29udGVudC90aGVtZXMvd2ViZXItbGFuZGluZy9hc3NldHMvc3JjL2pzL3NsaWRpbmctZ2FsbGVyeS5qcyIsIndlYnBhY2s6Ly8vLi93cC1jb250ZW50L3RoZW1lcy93ZWJlci1sYW5kaW5nL2Fzc2V0cy9zcmMvanMvdGVzdGltb25pYWwtc2xpZGVyLmpzIiwid2VicGFjazovLy8uL3dwLWNvbnRlbnQvdGhlbWVzL3dlYmVyLWxhbmRpbmcvYXNzZXRzL3NyYy9qcy90ZXh0LWdhbGxlcnkuanMiXSwibmFtZXMiOlsialF1ZXJ5IiwiJCIsImRvY3VtZW50IiwicmVhZHkiLCJlIiwiSGVhZGVyTWVudUZ1bmN0aW9uIiwiR3Jhdml0eUZvcm1GdW5jdGlvbmFsaXR5Iiwib24iLCIkdGhpcyIsIiR2YWx1ZSIsInZhbCIsImZpbmQiLCJhdHRyIiwiY2hhbmdlIiwid2ViZXJfZGF0YV92YXJpYXRpb25fcHJpY2UiLCJzZWxlY3RlZFZhcmlhdGlvbiIsInRoZVByaWNlIiwiaHRtbCIsInNlbGVjdGVkRGVmYXVsdCIsInNsaWNrIiwiYXJyb3dzIiwiZG90cyIsImluZmluaXRlIiwiYXV0b3BsYXkiLCJhZGFwdGl2ZUhlaWdodCIsInNsaWRlc1RvU2hvdyIsInNsaWRlc1RvU2Nyb2xsIiwicmVzcG9uc2l2ZSIsImJyZWFrcG9pbnQiLCJzZXR0aW5ncyIsImxlbmd0aCIsIm1hZ25pZmljUG9wdXAiLCJ0eXBlIiwiY2xpY2siLCJwcmV2ZW50RGVmYXVsdCIsImFuaW1hdGUiLCJzY3JvbGxUb3AiLCJvZmZzZXQiLCJ0b3AiLCJ0b2dnbGVDbGFzcyIsImFkZENsYXNzIiwic2V0VGltZW91dCIsImRvRmlsdGVyaW5nIiwiaGlkZSIsInNob3ciLCIkcGFyZW50X2FyciIsImVhY2giLCJpIiwidGhlRmlsdGVyIiwicHVzaCIsInBhcmVudCIsIiRmaWx0ZXIiLCIkZmlsdGVySXRlbSIsIiRfcGFyZW50IiwicmVtb3ZlSXRlbUFsbCIsImFsbEZpbHRlciIsImpvaW4iLCJoYXNBbnlDbGFzcyIsInJlbW92ZUNsYXNzIiwiYXJyIiwidmFsdWUiLCJzcGxpY2UiLCJmbiIsImFyZ3VtZW50cyIsImNsYXNzZXMiLCJzcGxpdCIsImoiLCJoYXNDbGFzcyIsInNpYmxpbmdzIiwic29ydEJ5Iiwib3JpZW50YXRpb24iLCJ0aW55c29ydCIsIm9yZGVyIiwiX3JldHVybiIsInRyaW0iLCJwYXJlbnRzIiwiJG5leHQiLCJwYXJzZUZsb2F0IiwicGFnZUxlbmd0aCIsIiRvZmZzZXQiLCJoZWlnaHQiLCJ3aW5kb3ciLCJzY3JvbGwiLCJ0cmlnZ2VyIiwiZm9jdXMiLCJmYWRlVG9nZ2xlIiwiZmFkZU91dCIsIkdyYXZpdHlGb3JtRnVuY3Rpb25hbGl0eVNldHVwIiwiaXNFbXB0eSIsImZvY3VzaW4iLCJmb2N1c291dCIsInNsaWRlVG9nZ2xlIiwiU2xpZGVGdWxsTWVudUZ1bmN0aW9uYWxpdHkiLCJjb3VudCIsIkJ1cmdlck1lbnVGdW5jdGlvbmFsaXR5IiwiTWVudU9wZW5SZWFkeSIsIkNsb3NlTWVudSIsIk9wZW5NZW51IiwiQ2xvc2VTZWFyY2hEcm9wIiwiQ2xvc2VTaWRlQ2FydCIsIlNob3dNZW51U2hhZGVyIiwiTG9ja1BhZ2VTY3JvbGwiLCJIaWRlTWVudVNoYWRlciIsIlVubG9ja1BhZ2VTY3JvbGwiLCJBY2NvcmRpYW5NZW51RnVuY3Rpb25hbGl0eSIsIm1vdXNlZG93biIsInRvZ2dsZSIsInNsaWRlRG93biIsInNsaWRlVXAiLCIkY3VycmVudF9jYXQiLCJEcm9wRG93bk1lbnVGdW5jdGlvbmFsdHkiLCJDbGVhckN1cnJlbnRTdWJNZW51IiwibW91c2VlbnRlciIsIm1vdXNlbGVhdmUiLCJTbGlja1NsaWRlckZ1bmN0aW9uYWx0eSIsImNoaWxkcmVuIiwiJHJlc3BvbnNpdmUiLCJjZW50ZXJNb2RlIiwicHJldkFycm93IiwibmV4dEFycm93IiwiZmFkZSIsImFzTmF2Rm9yIiwiZm9jdXNPblNlbGVjdCIsImdhbGxlcnkiLCJlbmFibGVkIiwidmVydGljYWwiLCJEb3duQXJyb3dGdW5jdGlvbmFsdHkiLCJvZmZTZXRUb3AiLCJvdXRlckhlaWdodCIsIk9wZW5OZXdzRmlsdGVyIiwiZmFkZUluIiwiQ2xvc2VOZXdzRmlsdGVyIiwiZmluYWxDYXRMaXN0IiwiZmluYWxDYXRMaXN0TmljZSIsIlJlZ2V4UmVwbGFjZSIsInRleHQiLCJpc290b3BlIiwiZmlsdGVyIiwiZGF0YSIsImZpbHRlcmVkSXRlbXMiLCJEeW5hbWljTG9hZFBvc3RzRnVuY3Rpb25hbGl0eSIsIkNoZWNrRm9yTG9hZFBvc3RzIiwiUG9zdExvYWRlckZ1bmN0aW9uYWxpdHkiLCJMb2FkaW5nTW9yZVBvc3RzIiwid2luZG93Qm90dG9tRGlzdGFuY2VDYWxjIiwiUG9zdHNMb2FkZWQiLCJQb3N0TG9hZHNCZWZvcmVCdXR0b24iLCJNaW5Mb2FkaW5nV2FpdGluZ1RpbWVDb21wbGV0ZWQiLCJMb2FkTW9yZVBvc3RzIiwiTWluTG9hZGluZ1RpbWUiLCJOdW1iZXJPZlBvc3RzVG9Mb2FkIiwiUG9zdENvdW50IiwiY2F0dHlwZSIsImFqYXgiLCJ1cmwiLCJkbl92YXJpYWJsZSIsImFqYXhfdXJsIiwiYWN0aW9uIiwiY3VycmVudF9wb3N0cyIsInBvc3RfdHlwZSIsImZldGNoX2Ftb3VudCIsInN1Y2Nlc3MiLCJMb2FkTW9yZVBvc3RzX1N1Y2Nlc3MiLCJlcnJvciIsImNvbnNvbGUiLCJsb2ciLCJDb21wbGV0ZUxvYWRpbmdGdW5jdGlvbmFsaXR5IiwianNvbiIsIkpTT04iLCJwYXJzZSIsImJlZm9yZSIsImlzX2ZpbmFsIiwicmVtb3ZlIiwiU2VhcmNoRHJvcEZ1bmN0aW9uYWxpdHkiLCJPcGVuU2VhcmNoRHJvcCIsIkNhcnREcm9wRnVuY3Rpb25hbGl0eSIsIk9wZW5TaWRlQ2FydCIsImJpbmQiLCJzY3JvbGxQb3NpdGlvbiIsInNjcm9sbExvY2tlZCIsIldvb1Nob3BJc290b3BlRnVuY3Rpb25hbGl0eSIsIiRjb250YWluZXIiLCJidXR0b25GaWx0ZXIiLCJpdGVtU2VsZWN0b3IiLCJyZXNpemFibGUiLCJsYXlvdXRNb2RlIiwicmFuZ2VGaWx0ZXIiLCJSYW5nZU1pbiIsIlJhbmdlTWF4IiwicHJpY2UiLCJpc1ByaWNlUmFuZ2UiLCJpcyIsImJ0bkZpbHRlciIsIiRwcmljZUZpbHRlckNvbnRhaW5lciIsIiRwcmljZU1pbiIsIiRwcmljZU1heCIsInNsaWRlciIsInJhbmdlIiwibWluIiwibWF4IiwidmFsdWVzIiwiY3JlYXRlIiwidWkiLCJzbGlkZSIsImV2ZW50IiwiaGFuZGxlIiwic3RvcCIsIldvb1RhYkZ1bmN0aW9uYWxpdHkiLCJJdGVtUG9zaXRpb24iLCJpbmRleCIsImVxIiwiQXJnU3RyaW5nIiwicmVwbGFjZSIsIkluc3RhZ3JhbUZ1bmN0aW9uYWxpdHkiLCJJbWFnZXNUb1B1bGwiLCJmb3JFYWNoIiwiZWxlbWVudCIsInRlbXBfYWRkX3N0cmluZyIsImFwcGVuZCIsIkNvb2tpZXNGb290ZXJGdW5jdGlvbmFsaXR5Iiwic2V0Q29va2llIiwiY25hbWUiLCJjdmFsdWUiLCJleGRheXMiLCJkIiwiRGF0ZSIsInNldFRpbWUiLCJnZXRUaW1lIiwiZXhwaXJlcyIsInRvVVRDU3RyaW5nIiwiY29va2llIiwiYmxvY2siLCJtZXNzYWdlIiwib3ZlcmxheUNTUyIsImJhY2tncm91bmQiLCJvcGFjaXR5IiwiY2FydF9pZCIsInByb3AiLCJkbiIsImNhcnRfbm90ZXMiLCJ3cF9hamF4X3VybCIsInBvc3QiLCJyZXNwb25zZSIsImxvY2F0aW9uIiwicmVsb2FkIiwiR01hcFN0eWxlIiwiaW5pdE1hcCIsImxhdCIsImxuZyIsImljb25fdXJsIiwidmlld0NlbnRyZUxhdCIsInZpZXdDZW50cmVMbmciLCJ3aWR0aCIsIm1hcENsYXNzIiwibWFwIiwiZ29vZ2xlIiwibWFwcyIsIk1hcCIsInNjcm9sbHdoZWVsIiwiem9vbSIsInN0eWxlcyIsImNlbnRlciIsIkxhdExuZyIsIm1hcFR5cGVJZCIsImRpc2FibGVEZWZhdWx0VUkiLCJJY29uU2l6ZSIsIlNpemUiLCJmZWF0dXJlIiwicG9zaXRpb24iLCJpY29uIiwic2NhbGVkU2l6ZSIsIm1hcmtlciIsIk1hcmtlciIsImluZm93aW5kb3ciLCJJbmZvV2luZG93IiwiY29udGVudCIsImFkZExpc3RlbmVyIiwib3BlbiIsImNhbGxiYWNrcyIsImVsZW1lbnRQYXJzZSIsIml0ZW0iLCJlbCIsImNvbnRleHQiLCJJbWFnZUhlcm9Db3VudCIsIkhlcm9JbWFnZXJvd3MiXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQUEsTUFBTSxDQUFDLFVBQVVDLENBQVYsRUFBYTtBQUVuQkEsR0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWUMsS0FBWixDQUFrQixVQUFVQyxDQUFWLEVBQWE7QUFFOUJDLHNCQUFrQjtBQUVsQkMsNEJBQXdCLEdBSk0sQ0FNOUI7QUFFQTtBQUVBO0FBRUE7QUFFQTtBQUVBO0FBRUE7QUFFQTtBQUVBO0FBRUE7QUFFQTtBQUVBO0FBRUE7QUFFQTtBQUVBO0FBRUE7QUFFQSxHQXRDRCxFQUZtQixDQTBDbkI7O0FBQ0FMLEdBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlLLEVBQVosQ0FBZSxRQUFmLEVBQXdCLHdCQUF4QixFQUFpRCxZQUFVO0FBRTFELFFBQUlDLEtBQUssR0FBR1AsQ0FBQyxDQUFDLElBQUQsQ0FBYjtBQUNBLFFBQUlRLE1BQU0sR0FBR0QsS0FBSyxDQUFDRSxHQUFOLEVBQWI7QUFDQVQsS0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWVMsSUFBWixDQUFpQixrQkFBaUJILEtBQUssQ0FBQ0ksSUFBTixDQUFXLFdBQVgsQ0FBakIsR0FBMEMsSUFBM0QsRUFBaUVGLEdBQWpFLENBQXFFRCxNQUFyRSxFQUE2RUksTUFBN0U7O0FBRUEsUUFBR0MsMEJBQTBCLElBQUksS0FBakMsRUFBdUM7QUFDdEMsVUFBSUMsaUJBQWlCLEdBQUdkLENBQUMsQ0FBQyw0QkFBRCxDQUFELENBQWdDUyxHQUFoQyxFQUF4QjtBQUNBLFVBQUlNLFFBQVEsR0FBR0YsMEJBQTBCLENBQUNDLGlCQUFELENBQXpDO0FBQ0FkLE9BQUMsQ0FBQyxtQkFBRCxDQUFELENBQXVCZ0IsSUFBdkIsQ0FBNEIsc0JBQXFCRCxRQUFyQixHQUErQixNQUEzRDtBQUNBO0FBRUQsR0FaRCxFQTNDbUIsQ0F5RG5COztBQUNBZixHQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZQyxLQUFaLENBQWtCLFlBQVU7QUFFM0IsUUFBSUssS0FBSyxHQUFHUCxDQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZUyxJQUFaLENBQWlCLHdCQUFqQixDQUFaO0FBQ0EsUUFBSU8sZUFBZSxHQUFHakIsQ0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWVMsSUFBWixDQUFpQixrQkFBaUJILEtBQUssQ0FBQ0ksSUFBTixDQUFXLFdBQVgsQ0FBakIsR0FBMEMsSUFBM0QsRUFBaUVGLEdBQWpFLEVBQXRCOztBQUNBLFFBQUdRLGVBQWUsSUFBSSxFQUF0QixFQUF5QjtBQUN4QmpCLE9BQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlTLElBQVosQ0FBaUIsbUNBQWtDTyxlQUFsQyxHQUFtRCxJQUFwRSxFQUEwRU4sSUFBMUUsQ0FBK0UsU0FBL0UsRUFBeUYsU0FBekY7QUFDQTtBQUVELEdBUkQsRUExRG1CLENBb0VuQjs7QUFDQVgsR0FBQyxDQUFDLDZEQUFELENBQUQsQ0FBaUVrQixLQUFqRSxDQUF1RTtBQUNoRUMsVUFBTSxFQUFFLElBRHdEO0FBRWhFQyxRQUFJLEVBQUUsS0FGMEQ7QUFHaEVDLFlBQVEsRUFBRSxLQUhzRDtBQUloRUMsWUFBUSxFQUFFLEtBSnNEO0FBS3RFQyxrQkFBYyxFQUFFLElBTHNEO0FBTXRFQyxnQkFBWSxFQUFFLENBTndEO0FBT3RFQyxrQkFBYyxFQUFFLENBUHNEO0FBUXRFQyxjQUFVLEVBQUUsQ0FBQztBQUNaQyxnQkFBVSxFQUFFLElBREE7QUFFWkMsY0FBUSxFQUFFO0FBQ1RKLG9CQUFZLEVBQUUsQ0FETDtBQUVUQyxzQkFBYyxFQUFFO0FBRlA7QUFGRSxLQUFELEVBTVg7QUFDQUUsZ0JBQVUsRUFBRSxHQURaO0FBRUFDLGNBQVEsRUFBRTtBQUNUSixvQkFBWSxFQUFFLENBREw7QUFFVEMsc0JBQWMsRUFBRTtBQUZQO0FBRlYsS0FOVyxFQVlYO0FBQ0FFLGdCQUFVLEVBQUUsR0FEWjtBQUVBQyxjQUFRLEVBQUU7QUFDVEosb0JBQVksRUFBRSxDQURMO0FBRVRDLHNCQUFjLEVBQUU7QUFGUDtBQUZWLEtBWlcsQ0FrQlg7QUFDQTtBQUNBO0FBcEJXO0FBUjBELEdBQXZFLEVBckVtQixDQXFHbkI7O0FBQ0EsTUFBR3pCLENBQUMsQ0FBQyxtQkFBRCxDQUFELENBQXVCNkIsTUFBdkIsR0FBZ0MsQ0FBbkMsRUFBcUM7QUFDcEM3QixLQUFDLENBQUMsbUJBQUQsQ0FBRCxDQUF1QjhCLGFBQXZCLENBQXFDO0FBQ3BDQyxVQUFJLEVBQUU7QUFEOEIsS0FBckM7QUFHQSxHQTFHa0IsQ0E0R25COzs7QUFDQSxNQUFJL0IsQ0FBQyxDQUFDLHdCQUFELENBQUQsQ0FBNEI2QixNQUE1QixHQUFxQyxDQUF6QyxFQUE0QyxDQUMzQztBQUNBLEdBL0drQixDQWlIbkI7OztBQUNBN0IsR0FBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkJnQyxLQUE3QixDQUFtQyxVQUFTN0IsQ0FBVCxFQUFXO0FBQzdDQSxLQUFDLENBQUM4QixjQUFGLEdBRDZDLENBRzdDOztBQUNBakMsS0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQmtDLE9BQWhCLENBQXdCO0FBQ3ZCQyxlQUFTLEVBQUVuQyxDQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0Qm9DLE1BQTVCLEdBQXFDQyxHQUFyQyxHQUEyQztBQUQvQixLQUF4QjtBQUlBckMsS0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRc0MsV0FBUixDQUFvQixRQUFwQjtBQUNBdEMsS0FBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUJ1QyxRQUF6QixDQUFrQyxTQUFsQztBQUVBQyxjQUFVLENBQUMsWUFBVTtBQUNwQkMsaUJBQVc7QUFDWCxLQUZTLEVBRVIsR0FGUSxDQUFWO0FBS0EsR0FoQkQ7O0FBa0JBLFdBQVNBLFdBQVQsR0FBc0I7QUFFckIsUUFBSXpDLENBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlTLElBQVosQ0FBaUIsNENBQWpCLEVBQStEbUIsTUFBL0QsR0FBd0UsQ0FBNUUsRUFBOEU7QUFDN0U3QixPQUFDLENBQUMsa0NBQUQsQ0FBRCxDQUFzQzBDLElBQXRDO0FBQ0EsS0FGRCxNQUVLO0FBQ0oxQyxPQUFDLENBQUMsa0NBQUQsQ0FBRCxDQUFzQzJDLElBQXRDO0FBQ0E7O0FBRUQsUUFBSUMsV0FBVyxHQUFHLEVBQWxCO0FBQ0E1QyxLQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZUyxJQUFaLENBQWlCLDRDQUFqQixFQUErRG1DLElBQS9ELENBQW9FLFVBQVNDLENBQVQsRUFBVzNDLENBQVgsRUFBYTtBQUNoRixVQUFJNEMsU0FBUyxHQUFHL0MsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRVyxJQUFSLENBQWEsYUFBYixDQUFoQjtBQUNBaUMsaUJBQVcsQ0FBQ0ksSUFBWixDQUFpQkQsU0FBakI7QUFDQS9DLE9BQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlTLElBQVosQ0FBaUIsd0RBQXVEcUMsU0FBdkQsR0FBa0UsSUFBbkYsRUFBeUZFLE1BQXpGLEdBQWtHTixJQUFsRztBQUNBLEtBSkQsRUFUcUIsQ0FnQnJCOztBQUNBLFFBQUlPLE9BQU8sR0FBRyxFQUFkO0FBQ0FsRCxLQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZUyxJQUFaLENBQWlCLHdDQUFqQixFQUEyRG1DLElBQTNELENBQWdFLFVBQVNDLENBQVQsRUFBVzNDLENBQVgsRUFBYTtBQUM1RSxVQUFJZ0QsV0FBVyxHQUFHbkQsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRVyxJQUFSLENBQWEsYUFBYixDQUFsQjs7QUFDQSxVQUFJd0MsV0FBVyxJQUFJLEdBQW5CLEVBQXdCO0FBQ3ZCQSxtQkFBVyxHQUFHLEVBQWQ7QUFDQSxPQUZELE1BRU87QUFDTkQsZUFBTyxDQUFDRixJQUFSLENBQWFHLFdBQWI7QUFDQTtBQUNELEtBUEQsRUFsQnFCLENBMkJyQjs7QUFDQW5ELEtBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlTLElBQVosQ0FBaUIsMkNBQWpCLEVBQThEbUMsSUFBOUQsQ0FBbUUsWUFBVTtBQUM1RSxVQUFJTyxRQUFRLEdBQUdwRCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFXLElBQVIsQ0FBYSxnQkFBYixDQUFmO0FBQ0F1QyxhQUFPLEdBQUdHLGFBQWEsQ0FBQ0gsT0FBRCxFQUFVRSxRQUFWLENBQXZCO0FBQ0EsS0FIRCxFQTVCcUIsQ0FpQ3JCOztBQUNBcEQsS0FBQyxDQUFDLHNCQUFELENBQUQsQ0FBMEIwQyxJQUExQjtBQUVBLFFBQUlZLFNBQVMsR0FBR0osT0FBTyxDQUFDSyxJQUFSLENBQWEsR0FBYixDQUFoQjs7QUFDQSxRQUFHRCxTQUFTLElBQUksRUFBaEIsRUFBbUI7QUFDbEJ0RCxPQUFDLENBQUMsK0NBQUQsQ0FBRCxDQUFtRDZDLElBQW5ELENBQXdELFlBQVU7QUFDakUsWUFBSTdDLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXdELFdBQVIsQ0FBb0JGLFNBQXBCLENBQUosRUFBb0M7QUFDbkN0RCxXQUFDLENBQUMsSUFBRCxDQUFELENBQVEyQyxJQUFSO0FBQ0EsU0FGRCxNQUVLO0FBQ0ozQyxXQUFDLENBQUMsSUFBRCxDQUFELENBQVEwQyxJQUFSO0FBQ0E7QUFDRCxPQU5EO0FBT0EsS0FSRCxNQVFLO0FBQ0oxQyxPQUFDLENBQUMsK0NBQUQsQ0FBRCxDQUFtRDJDLElBQW5EO0FBQ0EsS0EvQ29CLENBaURyQjs7O0FBQ0EsUUFBSTNDLENBQUMsQ0FBQyx1RUFBRCxDQUFELENBQTJFNkIsTUFBM0UsR0FBb0YsQ0FBeEYsRUFBMkY7QUFDMUY3QixPQUFDLENBQUMsc0JBQUQsQ0FBRCxDQUEwQjJDLElBQTFCO0FBQ0EsS0FGRCxNQUVLO0FBQ0ozQyxPQUFDLENBQUMsc0JBQUQsQ0FBRCxDQUEwQjBDLElBQTFCO0FBQ0E7O0FBRURGLGNBQVUsQ0FBQyxZQUFVO0FBQ3BCeEMsT0FBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUJ5RCxXQUF6QixDQUFxQyxTQUFyQztBQUNBLEtBRlMsRUFFUCxHQUZPLENBQVY7QUFJQSxHQWhNa0IsQ0FrTW5COzs7QUFDQSxXQUFTSixhQUFULENBQXVCSyxHQUF2QixFQUE0QkMsS0FBNUIsRUFBbUM7QUFDbEMsUUFBSWIsQ0FBQyxHQUFHLENBQVI7O0FBQ0EsV0FBT0EsQ0FBQyxHQUFHWSxHQUFHLENBQUM3QixNQUFmLEVBQXVCO0FBQ3JCLFVBQUk2QixHQUFHLENBQUNaLENBQUQsQ0FBSCxLQUFXYSxLQUFmLEVBQXNCO0FBQ3ZCRCxXQUFHLENBQUNFLE1BQUosQ0FBV2QsQ0FBWCxFQUFjLENBQWQ7QUFDRSxPQUZELE1BRU87QUFDUixVQUFFQSxDQUFGO0FBQ0U7QUFDRjs7QUFDRCxXQUFPWSxHQUFQO0FBQ0UsR0E3TWdCLENBK01uQjs7O0FBQ0ExRCxHQUFDLENBQUM2RCxFQUFGLENBQUtMLFdBQUwsR0FBbUIsWUFBVztBQUM3QixTQUFLLElBQUlWLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdnQixTQUFTLENBQUNqQyxNQUE5QixFQUFzQ2lCLENBQUMsRUFBdkMsRUFBMkM7QUFDMUMsVUFBSWlCLE9BQU8sR0FBR0QsU0FBUyxDQUFDaEIsQ0FBRCxDQUFULENBQWFrQixLQUFiLENBQW1CLEdBQW5CLENBQWQ7O0FBQ0EsV0FBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHRixPQUFPLENBQUNsQyxNQUE1QixFQUFvQ29DLENBQUMsRUFBckMsRUFBeUM7QUFDeEMsWUFBSSxLQUFLQyxRQUFMLENBQWNILE9BQU8sQ0FBQ0UsQ0FBRCxDQUFyQixDQUFKLEVBQStCO0FBQzlCLGlCQUFPLElBQVA7QUFDQTtBQUNEO0FBQ0Q7O0FBQ0QsV0FBTyxLQUFQO0FBQ0EsR0FWRDs7QUFZQWpFLEdBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlLLEVBQVosQ0FBZSxPQUFmLEVBQXVCLGNBQXZCLEVBQXNDLFVBQVNILENBQVQsRUFBVztBQUNoREEsS0FBQyxDQUFDOEIsY0FBRixHQURnRCxDQUdoRDs7QUFDQWpDLEtBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlELE1BQVIsR0FBaUJrQixRQUFqQixDQUEwQixJQUExQixFQUFnQ3pELElBQWhDLENBQXFDLEdBQXJDLEVBQTBDK0MsV0FBMUMsQ0FBc0QsUUFBdEQ7QUFDQXpELEtBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXVDLFFBQVIsQ0FBaUIsUUFBakI7QUFFQXZDLEtBQUMsQ0FBQyxxQkFBRCxDQUFELENBQXlCdUMsUUFBekIsQ0FBa0MsU0FBbEM7QUFFQSxRQUFJNkIsTUFBTSxHQUFHcEUsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRVyxJQUFSLENBQWEsV0FBYixDQUFiO0FBQ0EsUUFBSTBELFdBQVcsR0FBR3JFLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVcsSUFBUixDQUFhLFlBQWIsQ0FBbEI7QUFFQTZCLGNBQVUsQ0FBQyxZQUFVO0FBQ3BCLFVBQUc0QixNQUFNLElBQUksT0FBVixJQUFxQkMsV0FBVyxJQUFJLEtBQXZDLEVBQTZDO0FBQzVDQyxnQkFBUSxDQUFDLGtCQUFELEVBQW9CO0FBQUMzRCxjQUFJLEVBQUMsWUFBTjtBQUFvQjRELGVBQUssRUFBQztBQUExQixTQUFwQixDQUFSO0FBQ0E7O0FBRUQsVUFBR0gsTUFBTSxJQUFJLE9BQVYsSUFBcUJDLFdBQVcsSUFBSSxNQUF2QyxFQUE4QztBQUM3Q0MsZ0JBQVEsQ0FBQyxrQkFBRCxFQUFvQjtBQUFDM0QsY0FBSSxFQUFDLFlBQU47QUFBb0I0RCxlQUFLLEVBQUM7QUFBMUIsU0FBcEIsQ0FBUjtBQUNBOztBQUVELFVBQUdILE1BQU0sSUFBSSxPQUFWLElBQXFCQyxXQUFXLElBQUksS0FBdkMsRUFBNkM7QUFDNUNDLGdCQUFRLENBQUMsa0JBQUQsRUFBb0I7QUFBQzNELGNBQUksRUFBQyxZQUFOO0FBQW9CNEQsZUFBSyxFQUFDO0FBQTFCLFNBQXBCLENBQVI7QUFDQTs7QUFFRCxVQUFHSCxNQUFNLElBQUksT0FBVixJQUFxQkMsV0FBVyxJQUFJLE1BQXZDLEVBQThDO0FBQzdDQyxnQkFBUSxDQUFDLGtCQUFELEVBQW9CO0FBQUMzRCxjQUFJLEVBQUMsWUFBTjtBQUFvQjRELGVBQUssRUFBQztBQUExQixTQUFwQixDQUFSO0FBQ0E7O0FBRUR2RSxPQUFDLENBQUMscUJBQUQsQ0FBRCxDQUF5QnlELFdBQXpCLENBQXFDLFNBQXJDO0FBQ0EsS0FsQlMsRUFrQlIsR0FsQlEsQ0FBVjtBQW1CQSxHQS9CRDtBQWlDQXpELEdBQUMsQ0FBQyxnQkFBRCxDQUFELENBQW9CZ0MsS0FBcEIsQ0FBMEIsVUFBUzdCLENBQVQsRUFBVztBQUNwQ0EsS0FBQyxDQUFDOEIsY0FBRjtBQUNBakMsS0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJ1QyxRQUFyQixDQUE4QixhQUE5QjtBQUNBLEdBSEQ7QUFLQXZDLEdBQUMsQ0FBQyx3REFBRCxDQUFELENBQTREZ0MsS0FBNUQsQ0FBa0UsVUFBUzdCLENBQVQsRUFBVztBQUM1RUEsS0FBQyxDQUFDOEIsY0FBRjtBQUNBakMsS0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJ5RCxXQUFyQixDQUFpQyxhQUFqQztBQUNBLEdBSEQ7QUFNQXpELEdBQUMsQ0FBQywwQkFBRCxDQUFELENBQThCZ0MsS0FBOUIsQ0FBb0MsVUFBUzdCLENBQVQsRUFBVztBQUM5Q0EsS0FBQyxDQUFDOEIsY0FBRixHQUQ4QyxDQUc5Qzs7QUFDQSxRQUFJdUMsT0FBTyxHQUFHLElBQWQ7QUFDQXhFLEtBQUMsQ0FBQyxxREFBRCxDQUFELENBQXlENkMsSUFBekQsQ0FBOEQsVUFBU0MsQ0FBVCxFQUFXM0MsQ0FBWCxFQUFhO0FBQzFFLFVBQUlILENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVMsR0FBUixHQUFjZ0UsSUFBZCxNQUF3QixFQUE1QixFQUFnQztBQUMvQkQsZUFBTyxHQUFHLEtBQVY7QUFDQXhFLFNBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTBFLE9BQVIsQ0FBZ0IsV0FBaEIsRUFBNkJuQyxRQUE3QixDQUFzQyxxQkFBdEM7QUFDQSxlQUFPLEtBQVAsQ0FIK0IsQ0FHakI7QUFDZDtBQUNELEtBTkQ7O0FBUUEsUUFBR2lDLE9BQU8sSUFBSSxLQUFkLEVBQW9CO0FBQ25CeEUsT0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFla0MsT0FBZixDQUF1QjtBQUN0QkMsaUJBQVMsRUFBRW5DLENBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlTLElBQVosQ0FBaUIsNEJBQWpCLEVBQStDMEIsTUFBL0MsR0FBd0RDLEdBQXhELEdBQThEO0FBRG5ELE9BQXZCO0FBR0E7QUFDQTs7QUFFRCxRQUFJc0MsS0FBSyxHQUFHQyxVQUFVLENBQUU1RSxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFXLElBQVIsQ0FBYSxXQUFiLENBQUYsQ0FBdEI7QUFDQSxRQUFJa0UsVUFBVSxHQUFHN0UsQ0FBQyxDQUFDLHVCQUFELENBQUQsQ0FBMkI2QixNQUE1QyxDQXJCOEMsQ0F1QjlDOztBQUNBN0IsS0FBQyxDQUFDLHVCQUFELENBQUQsQ0FBMkJ5RCxXQUEzQixDQUF1QyxRQUF2QztBQUNBekQsS0FBQyxDQUFDLHNDQUFxQzJFLEtBQXJDLEdBQTRDLElBQTdDLENBQUQsQ0FBb0RwQyxRQUFwRCxDQUE2RCxRQUE3RDs7QUFDQSxTQUFJLElBQUlPLENBQUMsR0FBQyxDQUFWLEVBQVlBLENBQUMsR0FBQzZCLEtBQWQsRUFBb0I3QixDQUFDLEVBQXJCLEVBQXdCO0FBQ3ZCOUMsT0FBQyxDQUFDLHFDQUFtQzhDLENBQW5DLEdBQXFDLEdBQXRDLENBQUQsQ0FBNENQLFFBQTVDLENBQXFELFFBQXJEO0FBQ0EsS0E1QjZDLENBOEI5Qzs7O0FBQ0F2QyxLQUFDLENBQUMsc0NBQUQsQ0FBRCxDQUEwQzBDLElBQTFDO0FBQ0ExQyxLQUFDLENBQUMsMENBQXdDMkUsS0FBeEMsR0FBOEMsSUFBL0MsQ0FBRCxDQUFzRGhDLElBQXRELEdBaEM4QyxDQWtDOUM7O0FBQ0EzQyxLQUFDLENBQUMsV0FBRCxDQUFELENBQWVrQyxPQUFmLENBQXVCO0FBQ3RCQyxlQUFTLEVBQUVuQyxDQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQm9DLE1BQXBCLEdBQTZCQyxHQUE3QixHQUFtQztBQUR4QixLQUF2QjtBQUlBLEdBdkNELEVBeFFtQixDQW1UbkI7O0FBQ0EsV0FBU2pDLGtCQUFULEdBQTZCO0FBRTVCO0FBQ0EsUUFBSTBFLE9BQU8sR0FBRzlFLENBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZVcsSUFBZixDQUFvQixhQUFwQixLQUFzQyxNQUF0QyxHQUErQ1gsQ0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlK0UsTUFBZixLQUEwQixFQUF6RSxHQUE4RSxFQUE1RixDQUg0QixDQUs1Qjs7QUFDQS9FLEtBQUMsQ0FBQ2dGLE1BQUQsQ0FBRCxDQUFVQyxNQUFWLENBQWlCLFlBQVk7QUFDNUIsVUFBR2pGLENBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlrQyxTQUFaLEtBQTBCMkMsT0FBN0IsRUFBcUM7QUFDcEM5RSxTQUFDLENBQUMsTUFBRCxDQUFELENBQVV1QyxRQUFWLENBQW1CLGNBQW5CO0FBQ0EsT0FGRCxNQUVLO0FBQ0p2QyxTQUFDLENBQUMsTUFBRCxDQUFELENBQVV5RCxXQUFWLENBQXNCLGNBQXRCO0FBQ0E7QUFDRCxLQU5ELEVBTjRCLENBYzVCOztBQUNBekQsS0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWUssRUFBWixDQUFlLE9BQWYsRUFBd0IscUJBQXhCLEVBQStDLFVBQVNILENBQVQsRUFBVztBQUN6REEsT0FBQyxDQUFDOEIsY0FBRjtBQUNBakMsT0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRc0MsV0FBUixDQUFvQixXQUFwQjtBQUNBdEMsT0FBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVc0MsV0FBVixDQUFzQixrQkFBdEIsRUFIeUQsQ0FLekQ7O0FBQ0F0QyxPQUFDLENBQUNnRixNQUFELENBQUQsQ0FBVUUsT0FBVixDQUFrQixzQkFBbEI7QUFDQSxLQVBELEVBZjRCLENBd0I1Qjs7QUFDQWxGLEtBQUMsQ0FBQyxnREFBRCxDQUFELENBQW9ETSxFQUFwRCxDQUF1RCxPQUF2RCxFQUErRCxVQUFTSCxDQUFULEVBQVc7QUFDekVBLE9BQUMsQ0FBQzhCLGNBQUY7QUFDQSxLQUZELEVBekI0QixDQThCNUI7O0FBQ0FqQyxLQUFDLENBQUMsY0FBRCxDQUFELENBQWtCZ0MsS0FBbEIsQ0FBd0IsVUFBUzdCLENBQVQsRUFBVztBQUNsQ0EsT0FBQyxDQUFDOEIsY0FBRjtBQUNBakMsT0FBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVc0MsV0FBVixDQUFzQixhQUF0QjtBQUNBdEMsT0FBQyxDQUFDLG1DQUFELENBQUQsQ0FBdUNtRixLQUF2QyxHQUhrQyxDQUtsQzs7QUFDQSxVQUFHbkYsQ0FBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUJrRSxRQUF6QixDQUFrQyxZQUFsQyxDQUFILEVBQW1EO0FBQ2xEbEUsU0FBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUJvRixVQUF6QixDQUFvQyxHQUFwQyxFQUF3QyxZQUFVO0FBQ2pEO0FBQ0FwRixXQUFDLENBQUMsbUNBQUQsQ0FBRCxDQUF1Q21GLEtBQXZDO0FBQ0EsU0FIRDtBQUlBO0FBQ0QsS0FaRCxFQS9CNEIsQ0E2QzVCOztBQUNBbkYsS0FBQyxDQUFDLG9CQUFELENBQUQsQ0FBd0JnQyxLQUF4QixDQUE4QixVQUFTN0IsQ0FBVCxFQUFXO0FBQ3hDQSxPQUFDLENBQUM4QixjQUFGO0FBQ0FqQyxPQUFDLENBQUMsTUFBRCxDQUFELENBQVV5RCxXQUFWLENBQXNCLGFBQXRCO0FBQ0F6RCxPQUFDLENBQUMscUJBQUQsQ0FBRCxDQUF5QnFGLE9BQXpCLENBQWlDLEdBQWpDO0FBQ0EsS0FKRDtBQU1BLEdBeFdrQixDQTJXbkI7OztBQUNBLFdBQVNDLDZCQUFULEdBQXlDO0FBRXhDO0FBQ0F0RixLQUFDLENBQUMscUNBQUQsQ0FBRCxDQUF5Q00sRUFBekMsQ0FBNEMsdUJBQTVDLEVBQXFFLFlBQVU7QUFFOUUsVUFBSWlGLE9BQU8sR0FBR3ZGLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVMsR0FBUixFQUFkOztBQUNBLFVBQUc4RSxPQUFPLElBQUksRUFBZCxFQUFpQjtBQUNoQnZGLFNBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTBFLE9BQVIsQ0FBZ0IsV0FBaEIsRUFBNkJqQixXQUE3QixDQUF5QyxRQUF6QztBQUNBLE9BRkQsTUFFSztBQUNKekQsU0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRMEUsT0FBUixDQUFnQixXQUFoQixFQUE2Qm5DLFFBQTdCLENBQXNDLFFBQXRDO0FBQ0E7QUFFRCxLQVRELEVBSHdDLENBY3hDOztBQUNBdkMsS0FBQyxDQUFDLHFDQUFELENBQUQsQ0FBeUNrRixPQUF6QyxDQUFpRCxVQUFqRCxFQWZ3QyxDQWlCeEM7O0FBQ0FsRixLQUFDLENBQUMscUNBQUQsQ0FBRCxDQUF5Q00sRUFBekMsQ0FBNEMsU0FBNUMsRUFBdUQsWUFBVTtBQUVoRU4sT0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRMEUsT0FBUixDQUFnQixXQUFoQixFQUE2Qm5DLFFBQTdCLENBQXNDLFFBQXRDO0FBRUEsS0FKRCxFQWxCd0MsQ0F3QnhDOztBQUNBLFFBQUl2QyxDQUFDLENBQUMsdUNBQUQsQ0FBRCxDQUEyQzZCLE1BQTNDLEdBQW9ELENBQXhELEVBQTJEO0FBRTFEN0IsT0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQmtDLE9BQWhCLENBQXdCO0FBQ3ZCQyxpQkFBUyxFQUFFbkMsQ0FBQyxDQUFDLGdCQUFELENBQUQsQ0FBb0JvQyxNQUFwQixHQUE2QkMsR0FBN0IsR0FBbUM7QUFEdkIsT0FBeEIsRUFFRyxDQUZIO0FBR0EsS0E5QnVDLENBZ0N4QztBQUNBOzs7QUFDQXJDLEtBQUMsQ0FBQyxrQkFBRCxDQUFELENBQXNCVSxJQUF0QixDQUEyQiwwSkFBM0IsRUFBdUxtQyxJQUF2TCxDQUE0TCxZQUFZO0FBRXZNO0FBQ0E3QyxPQUFDLENBQUMsSUFBRCxDQUFELENBQVFtRSxRQUFSLENBQWlCLE9BQWpCLEVBQTBCNUIsUUFBMUIsQ0FBbUMsWUFBbkMsRUFIdU0sQ0FLdk07O0FBQ0F2QyxPQUFDLENBQUMsSUFBRCxDQUFELENBQVFVLElBQVIsQ0FBYSxpQkFBYixFQUFnQzhFLE9BQWhDLENBQXdDLFlBQVk7QUFDbkR4RixTQUFDLENBQUMsSUFBRCxDQUFELENBQVFpRCxNQUFSLEdBQWlCa0IsUUFBakIsQ0FBMEIsT0FBMUIsRUFBbUM1QixRQUFuQyxDQUE0QyxPQUE1QztBQUNBLE9BRkQsRUFOdU0sQ0FVdk07O0FBQ0F2QyxPQUFDLENBQUMsSUFBRCxDQUFELENBQVFVLElBQVIsQ0FBYSxpQkFBYixFQUFnQ21DLElBQWhDLENBQXFDLFlBQVk7QUFFaEQsWUFBSTdDLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVMsR0FBUixNQUFpQixFQUFyQixFQUF5QjtBQUN4QlQsV0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRaUQsTUFBUixHQUFpQmtCLFFBQWpCLENBQTBCLE9BQTFCLEVBQW1DNUIsUUFBbkMsQ0FBNEMsT0FBNUM7QUFDQTtBQUNELE9BTEQsRUFYdU0sQ0FrQnZNOztBQUNBdkMsT0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRVSxJQUFSLENBQWEsaUJBQWIsRUFBZ0MrRSxRQUFoQyxDQUF5QyxZQUFZO0FBRXBEO0FBQ0EsWUFBSXpGLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVMsR0FBUixNQUFpQixFQUFyQixFQUF5QjtBQUN4QlQsV0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRaUQsTUFBUixHQUFpQmtCLFFBQWpCLENBQTBCLE9BQTFCLEVBQW1DVixXQUFuQyxDQUErQyxPQUEvQztBQUNBO0FBQ0QsT0FORDtBQVFBLEtBM0JELEVBbEN3QyxDQStEeEM7QUFDQTs7QUFDQXpELEtBQUMsQ0FBQywyQkFBRCxDQUFELENBQStCVSxJQUEvQixDQUFvQywwVEFBcEMsRUFBZ1c2QixRQUFoVyxDQUF5VyxVQUF6VyxFQWpFd0MsQ0FtRXhDOztBQUNBdkMsS0FBQyxDQUFDLCtGQUFELENBQUQsQ0FBbUd5RixRQUFuRyxDQUE0RyxZQUFZO0FBRXZILFVBQUl6RixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFTLEdBQVIsTUFBaUIsRUFBckIsRUFBeUI7QUFDeEJULFNBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXVDLFFBQVIsQ0FBaUIsT0FBakI7QUFDQSxPQUZELE1BRU87QUFDTnZDLFNBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXlELFdBQVIsQ0FBb0IsT0FBcEI7QUFDQXpELFNBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTBFLE9BQVIsQ0FBZ0Isc0JBQWhCLEVBQXdDakIsV0FBeEMsQ0FBb0QsY0FBcEQ7QUFDQTtBQUNELEtBUkQ7QUFTQSxHQXpia0IsQ0FpZG5COzs7QUFDQXpELEdBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlLLEVBQVosQ0FBZSxPQUFmLEVBQXVCLDZEQUF2QixFQUFxRixVQUFTSCxDQUFULEVBQVc7QUFFL0ZBLEtBQUMsQ0FBQzhCLGNBQUY7QUFDQWpDLEtBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlELE1BQVIsR0FBaUJYLFdBQWpCLENBQTZCLFFBQTdCO0FBQ0F0QyxLQUFDLENBQUMsSUFBRCxDQUFELENBQVFtRSxRQUFSLENBQWlCLFVBQWpCLEVBQTZCdUIsV0FBN0I7QUFFQSxHQU5ELEVBbGRtQixDQTBkbkI7O0FBQ0EsV0FBU0MsMEJBQVQsR0FBcUM7QUFFcEMsUUFBSTNGLENBQUMsQ0FBQyxvQkFBRCxDQUFELENBQXdCNkIsTUFBeEIsR0FBaUMsQ0FBckMsRUFBd0M7QUFFdkMsVUFBSStELEtBQUssR0FBRyxDQUFaO0FBQ0E1RixPQUFDLENBQUMseUNBQUQsQ0FBRCxDQUE2QzZDLElBQTdDLENBQWtELFVBQVNDLENBQVQsRUFBVzNDLENBQVgsRUFBYTtBQUM5REgsU0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRdUMsUUFBUixDQUFpQixXQUFTTyxDQUExQjtBQUNBOEMsYUFBSztBQUNMLE9BSEQsRUFIdUMsQ0FRdkM7O0FBQ0E1RixPQUFDLENBQUMsMkJBQUQsQ0FBRCxDQUErQnVDLFFBQS9CLENBQXdDLFlBQVdxRCxLQUFLLEdBQUMsQ0FBakIsQ0FBeEM7QUFFQTtBQUVELEdBMWVrQixDQWdmbkI7OztBQUNBLFdBQVNDLHVCQUFULEdBQW1DO0FBRWxDLFFBQUk3RixDQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0QjZCLE1BQTVCLEdBQXFDLENBQXpDLEVBQTRDO0FBRTNDLFVBQUlpRSxhQUFhLEdBQUcsSUFBcEI7QUFFQTlGLE9BQUMsQ0FBQyx3QkFBRCxDQUFELENBQTRCZ0MsS0FBNUIsQ0FBa0MsWUFBWTtBQUU3QyxZQUFJOEQsYUFBSixFQUFtQjtBQUVsQkEsdUJBQWEsR0FBRyxLQUFoQjtBQUNBdEQsb0JBQVUsQ0FBQyxZQUFZO0FBQ3RCc0QseUJBQWEsR0FBRyxJQUFoQjtBQUNBLFdBRlMsRUFFUCxHQUZPLENBQVY7O0FBSUEsY0FBSTlGLENBQUMsQ0FBQyx3QkFBRCxDQUFELENBQTRCa0UsUUFBNUIsQ0FBcUMsTUFBckMsQ0FBSixFQUFrRDtBQUNqRDZCLHFCQUFTO0FBRVQsV0FIRCxNQUdPO0FBQ05DLG9CQUFRO0FBQ1I7QUFDRDtBQUNELE9BaEJEO0FBa0JBO0FBRUQ7O0FBRUQsV0FBU0EsUUFBVCxHQUFvQjtBQUVuQjtBQUNBQyxtQkFBZSxHQUhJLENBS25COztBQUNBQyxpQkFBYTtBQUVibEcsS0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQnVDLFFBQWxCLENBQTJCLFFBQTNCO0FBQ0F2QyxLQUFDLENBQUMsV0FBRCxDQUFELENBQWV1QyxRQUFmLENBQXdCLGFBQXhCO0FBQ0F2QyxLQUFDLENBQUMsTUFBRCxDQUFELENBQVV1QyxRQUFWLENBQW1CLGtCQUFuQjtBQUNBdkMsS0FBQyxDQUFDLGtCQUFELENBQUQsQ0FBc0J1QyxRQUF0QixDQUErQixXQUEvQjtBQUNBdkMsS0FBQyxDQUFDLHdCQUFELENBQUQsQ0FBNEJ1QyxRQUE1QixDQUFxQyxNQUFyQztBQUNBNEQsa0JBQWM7QUFDZEMsa0JBQWM7QUFDZDs7QUFFRCxXQUFTTCxTQUFULEdBQXFCO0FBQ3BCL0YsS0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQnlELFdBQWxCLENBQThCLFFBQTlCO0FBQ0F6RCxLQUFDLENBQUMsTUFBRCxDQUFELENBQVV5RCxXQUFWLENBQXNCLGtCQUF0QjtBQUNBekQsS0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFleUQsV0FBZixDQUEyQixhQUEzQjtBQUNBekQsS0FBQyxDQUFDLGtCQUFELENBQUQsQ0FBc0J5RCxXQUF0QixDQUFrQyxXQUFsQztBQUNBekQsS0FBQyxDQUFDLHdCQUFELENBQUQsQ0FBNEJ5RCxXQUE1QixDQUF3QyxNQUF4QztBQUNBNEMsa0JBQWM7QUFDZEMsb0JBQWdCO0FBQ2hCLEdBdGlCa0IsQ0F3aUJuQjs7O0FBQ0EsV0FBU0MsMEJBQVQsR0FBc0M7QUFFckM7QUFDQXZHLEtBQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0I2QyxJQUFoQixDQUFxQixZQUFZO0FBRWhDO0FBQ0E3QyxPQUFDLENBQUMsSUFBRCxDQUFELENBQVFVLElBQVIsQ0FBYSxJQUFiLEVBQW1CbUMsSUFBbkIsQ0FBd0IsWUFBWTtBQUVuQyxZQUFJLENBQUM3QyxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFVLElBQVIsQ0FBYSwwQkFBYixFQUF5Q21CLE1BQTFDLEdBQW1ELENBQXZELEVBQTBEO0FBQ3pEN0IsV0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRVSxJQUFSLENBQWEsR0FBYixFQUFrQmdDLElBQWxCO0FBQ0E7QUFFRCxPQU5ELEVBSGdDLENBV2hDOztBQUNBMUMsT0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRVSxJQUFSLENBQWEsc0JBQWIsRUFBcUNtQyxJQUFyQyxDQUEwQyxZQUFZO0FBQ3JEN0MsU0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRMEMsSUFBUjtBQUNBMUMsU0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRaUQsTUFBUixHQUFpQlYsUUFBakIsQ0FBMEIsZUFBMUI7QUFDQSxPQUhELEVBWmdDLENBaUJoQzs7QUFDQXZDLE9BQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVUsSUFBUixDQUFhLHFCQUFiLEVBQW9DZ0MsSUFBcEM7QUFDQTFDLE9BQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVUsSUFBUixDQUFhLE1BQWIsRUFBcUI4RixTQUFyQixDQUErQixZQUFZO0FBRTFDeEcsU0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFReUcsTUFBUjtBQUNBekcsU0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRbUUsUUFBUixDQUFpQixHQUFqQixFQUFzQnNDLE1BQXRCO0FBRUF6RyxTQUFDLENBQUMsSUFBRCxDQUFELENBQVFpRCxNQUFSLEdBQWlCdkMsSUFBakIsQ0FBc0Isc0JBQXRCLEVBQThDZ0YsV0FBOUMsQ0FBMEQsR0FBMUQ7QUFFQSxPQVBEO0FBU0EsS0E1QkQ7QUE4QkExRixLQUFDLENBQUMsZ0NBQUQsQ0FBRCxDQUFvQzZDLElBQXBDLENBQXlDLFlBQVk7QUFFcEQ7QUFDQTdDLE9BQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVUsSUFBUixDQUFhLElBQWIsRUFBbUJtQyxJQUFuQixDQUF3QixZQUFZO0FBRW5DLFlBQUksQ0FBQzdDLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVUsSUFBUixDQUFhLDBCQUFiLEVBQXlDbUIsTUFBMUMsR0FBbUQsQ0FBdkQsRUFBMEQ7QUFDekQ3QixXQUFDLENBQUMsSUFBRCxDQUFELENBQVFVLElBQVIsQ0FBYSxHQUFiLEVBQWtCZ0MsSUFBbEI7QUFDQTtBQUVELE9BTkQsRUFIb0QsQ0FXcEQ7O0FBQ0ExQyxPQUFDLENBQUMsSUFBRCxDQUFELENBQVFVLElBQVIsQ0FBYSxzQkFBYixFQUFxQ21DLElBQXJDLENBQTBDLFlBQVk7QUFDckQ3QyxTQUFDLENBQUMsSUFBRCxDQUFELENBQVEwQyxJQUFSO0FBQ0ExQyxTQUFDLENBQUMsSUFBRCxDQUFELENBQVFpRCxNQUFSLEdBQWlCVixRQUFqQixDQUEwQixlQUExQjtBQUNBLE9BSEQsRUFab0QsQ0FpQnBEOztBQUNBdkMsT0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRVSxJQUFSLENBQWEscUJBQWIsRUFBb0NnQyxJQUFwQztBQUNBMUMsT0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRVSxJQUFSLENBQWEsTUFBYixFQUFxQjhGLFNBQXJCLENBQStCLFlBQVk7QUFFMUN4RyxTQUFDLENBQUMsSUFBRCxDQUFELENBQVF5RyxNQUFSO0FBQ0F6RyxTQUFDLENBQUMsSUFBRCxDQUFELENBQVFtRSxRQUFSLENBQWlCLEdBQWpCLEVBQXNCc0MsTUFBdEI7QUFFQXpHLFNBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlELE1BQVIsR0FBaUJ2QyxJQUFqQixDQUFzQixzQkFBdEIsRUFBOENnRixXQUE5QyxDQUEwRCxHQUExRDtBQUVBLE9BUEQ7QUFTQTFGLE9BQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVUsSUFBUixDQUFhLG9DQUFiLEVBQW1ESixFQUFuRCxDQUFzRCxPQUF0RCxFQUErRCxVQUFVSCxDQUFWLEVBQWE7QUFFM0VBLFNBQUMsQ0FBQzhCLGNBQUY7QUFFQWpDLFNBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUW1FLFFBQVIsQ0FBaUIsR0FBakIsRUFBc0JzQyxNQUF0QjtBQUVBekcsU0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRaUQsTUFBUixHQUFpQnZDLElBQWpCLENBQXNCLHNCQUF0QixFQUE4Q2dGLFdBQTlDLENBQTBELEdBQTFEO0FBRUEsT0FSRDtBQVVBLEtBdENELEVBakNxQyxDQTBFckM7O0FBQ0ExRixLQUFDLENBQUMsa0NBQUQsQ0FBRCxDQUFzQ2dDLEtBQXRDLENBQTRDLFVBQVU3QixDQUFWLEVBQWE7QUFDeERBLE9BQUMsQ0FBQzhCLGNBQUY7QUFFQSxVQUFJMUIsS0FBSyxHQUFHUCxDQUFDLENBQUMsSUFBRCxDQUFiOztBQUNBLFVBQUtPLEtBQUQsQ0FBUTJELFFBQVIsQ0FBaUIsV0FBakIsQ0FBSixFQUFtQztBQUNsQzNELGFBQUssQ0FBQ2tELFdBQU4sQ0FBa0IsV0FBbEI7QUFDQWxELGFBQUssQ0FBQ2dDLFFBQU4sQ0FBZSxZQUFmO0FBQ0FoQyxhQUFLLENBQUM0RCxRQUFOLENBQWUsV0FBZixFQUE0QnVDLFNBQTVCO0FBQ0EsT0FKRCxNQUlPO0FBQ05uRyxhQUFLLENBQUNrRCxXQUFOLENBQWtCLFlBQWxCO0FBQ0FsRCxhQUFLLENBQUNnQyxRQUFOLENBQWUsV0FBZjtBQUNBaEMsYUFBSyxDQUFDNEQsUUFBTixDQUFlLFdBQWYsRUFBNEJ3QyxPQUE1QjtBQUNBLE9BWnVELENBY3hEOzs7QUFDQXBHLFdBQUssQ0FBQzBDLE1BQU4sR0FBZWtCLFFBQWYsQ0FBd0IsSUFBeEIsRUFBOEJ6RCxJQUE5QixDQUFtQyxXQUFuQyxFQUFnRGlHLE9BQWhEO0FBQ0FwRyxXQUFLLENBQUMwQyxNQUFOLEdBQWVrQixRQUFmLENBQXdCLElBQXhCLEVBQThCekQsSUFBOUIsQ0FBbUMsY0FBbkMsRUFBbUQrQyxXQUFuRCxDQUErRCxZQUEvRDtBQUNBbEQsV0FBSyxDQUFDMEMsTUFBTixHQUFla0IsUUFBZixDQUF3QixJQUF4QixFQUE4QnpELElBQTlCLENBQW1DLGNBQW5DLEVBQW1ENkIsUUFBbkQsQ0FBNEQsV0FBNUQ7QUFHQSxLQXBCRCxFQTNFcUMsQ0FpR3JDOztBQUNBLFFBQUl2QyxDQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZUyxJQUFaLENBQWlCLGtDQUFqQixFQUFxRG1CLE1BQXJELEdBQThELENBQWxFLEVBQXFFO0FBQ3BFLFVBQUkrRSxZQUFZLEdBQUc1RyxDQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZUyxJQUFaLENBQWlCLGtDQUFqQixDQUFuQjtBQUNBa0csa0JBQVksQ0FBQzNELE1BQWIsR0FBc0JrQixRQUF0QixDQUErQixjQUEvQixFQUErQ2UsT0FBL0MsQ0FBdUQsT0FBdkQ7O0FBQ0EsVUFBSTBCLFlBQVksQ0FBQzNELE1BQWIsR0FBc0JpQixRQUF0QixDQUErQixvQkFBL0IsQ0FBSixFQUEwRDtBQUN6RDBDLG9CQUFZLENBQUNsRyxJQUFiLENBQWtCLGNBQWxCLEVBQWtDd0UsT0FBbEMsQ0FBMEMsT0FBMUM7QUFDQTtBQUNEO0FBQ0QsR0FscEJrQixDQW9wQm5COzs7QUFDQSxXQUFTMkIsd0JBQVQsR0FBb0M7QUFFbkMsUUFBSTdHLENBQUMsQ0FBQywyQkFBRCxDQUFELENBQStCNkIsTUFBL0IsR0FBd0MsQ0FBNUMsRUFBK0M7QUEwQjlDO0FBMUI4QyxVQTJCckNpRixtQkEzQnFDLEdBMkI5QyxTQUFTQSxtQkFBVCxHQUErQjtBQUM5QjlHLFNBQUMsQ0FBQyx1RUFBRCxDQUFELENBQTJFMEMsSUFBM0U7QUFDQTFDLFNBQUMsQ0FBQyw2REFBRCxDQUFELENBQWlFeUQsV0FBakUsQ0FBNkUsU0FBN0U7QUFDQSxPQTlCNkM7O0FBRTlDO0FBQ0F6RCxPQUFDLENBQUMscUNBQUQsQ0FBRCxDQUF5QzBDLElBQXpDO0FBQ0ExQyxPQUFDLENBQUMsMkJBQUQsQ0FBRCxDQUErQnlELFdBQS9CLENBQTJDLFlBQTNDLEVBSjhDLENBTTlDOztBQUNBekQsT0FBQyxDQUFDLHFEQUFELENBQUQsQ0FBeUQrRyxVQUF6RCxDQUFvRSxZQUFZO0FBRS9FRCwyQkFBbUIsR0FGNEQsQ0FJL0U7O0FBQ0E5RyxTQUFDLENBQUMsSUFBRCxDQUFELENBQVF1QyxRQUFSLENBQWlCLFNBQWpCO0FBQ0F2QyxTQUFDLENBQUMsSUFBRCxDQUFELENBQVFVLElBQVIsQ0FBYSxXQUFiLEVBQTBCaUMsSUFBMUI7QUFFQSxPQVJEO0FBVUEzQyxPQUFDLENBQUMscURBQUQsQ0FBRCxDQUF5RGdILFVBQXpELENBQW9FLFlBQVk7QUFDL0VGLDJCQUFtQjtBQUNuQixPQUZELEVBakI4QyxDQXFCOUM7O0FBQ0E5RyxPQUFDLENBQUMseURBQUQsQ0FBRCxDQUE2RE0sRUFBN0QsQ0FBZ0UsT0FBaEUsRUFBeUUsVUFBVUgsQ0FBVixFQUFhO0FBQ3JGQSxTQUFDLENBQUM4QixjQUFGO0FBQ0EsT0FGRDtBQVNBO0FBQ0QsR0F2ckJrQixDQXlyQm5COzs7QUFDQSxXQUFTZ0YsdUJBQVQsR0FBbUM7QUFFbEM7QUFDQSxRQUFJakgsQ0FBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkI2QixNQUE3QixHQUFzQyxDQUExQyxFQUE2QztBQUM1QyxVQUFJN0IsQ0FBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkJrSCxRQUE3QixHQUF3Q3JGLE1BQXhDLEdBQWlELENBQXJELEVBQXdEO0FBQ3ZEN0IsU0FBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkI2QyxJQUE3QixDQUFrQyxZQUFZO0FBQzdDLGNBQUl0QyxLQUFLLEdBQUdQLENBQUMsQ0FBQyxJQUFELENBQWI7O0FBQ0EsY0FBSU8sS0FBSyxDQUFDMkQsUUFBTixDQUFlLG9CQUFmLENBQUosRUFBMEM7QUFFekMzRCxpQkFBSyxDQUFDVyxLQUFOLENBQVk7QUFDWEMsb0JBQU0sRUFBRSxLQURHO0FBRVhDLGtCQUFJLEVBQUUsSUFGSztBQUdYQyxzQkFBUSxFQUFFLElBSEM7QUFJWEMsc0JBQVEsRUFBRSxLQUpDO0FBS1hDLDRCQUFjLEVBQUU7QUFMTCxhQUFaO0FBUUE7OztBQUlBLFdBZEQsTUFjTztBQUNOaEIsaUJBQUssQ0FBQ1csS0FBTixDQUFZO0FBQ1hDLG9CQUFNLEVBQUUsS0FERztBQUVYQyxrQkFBSSxFQUFFLElBRks7QUFHWEMsc0JBQVEsRUFBRSxJQUhDO0FBSVhDLHNCQUFRLEVBQUUsSUFKQztBQUtYQyw0QkFBYyxFQUFFO0FBTEwsYUFBWjtBQU9BO0FBRUQsU0ExQkQ7QUE0QkE7QUFDRCxLQWxDaUMsQ0FvQ2xDOzs7QUFDQSxRQUFHdkIsQ0FBQyxDQUFDLHNCQUFELENBQUQsQ0FBMEI2QixNQUExQixHQUFtQyxDQUF0QyxFQUF3QztBQUN2QzdCLE9BQUMsQ0FBQyxzQkFBRCxDQUFELENBQTBCNkMsSUFBMUIsQ0FBK0IsWUFBVTtBQUV4QyxZQUFJc0UsV0FBVyxHQUFHLEVBQWxCO0FBQ0EsWUFBSTVHLEtBQUssR0FBR1AsQ0FBQyxDQUFDLElBQUQsQ0FBYixDQUh3QyxDQUt4Qzs7QUFDQSxZQUFHTyxLQUFLLENBQUMyRCxRQUFOLENBQWUscUJBQWYsQ0FBSCxFQUF5QztBQUN4Q2lELHFCQUFXLEdBQUcsQ0FDYjtBQUNDeEYsc0JBQVUsRUFBRSxHQURiO0FBRUNDLG9CQUFRLEVBQUU7QUFDVFIsa0JBQUksRUFBRSxLQURHO0FBRVRELG9CQUFNLEVBQUUsS0FGQztBQUdURSxzQkFBUSxFQUFFLEtBSEQ7QUFJVEMsc0JBQVEsRUFBRSxJQUpEO0FBS1Q4Rix3QkFBVSxFQUFFLElBTEg7QUFNVDVGLDBCQUFZLEVBQUUsQ0FOTDtBQU9UQyw0QkFBYyxFQUFFO0FBUFA7QUFGWCxXQURhLENBQWQ7QUFjQTs7QUFFRGxCLGFBQUssQ0FBQ1csS0FBTixDQUFZO0FBQ1hDLGdCQUFNLEVBQUUsS0FERztBQUVYQyxjQUFJLEVBQUUsSUFGSztBQUdYQyxrQkFBUSxFQUFFLElBSEM7QUFJWEMsa0JBQVEsRUFBRSxJQUpDO0FBS1hDLHdCQUFjLEVBQUUsS0FMTDtBQU1YRyxvQkFBVSxFQUFFeUY7QUFORCxTQUFaO0FBU0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBb0JBLE9BcEREO0FBcURBLEtBM0ZpQyxDQTZGbEM7OztBQUNBLFFBQUluSCxDQUFDLENBQUMsOEJBQUQsQ0FBRCxDQUFrQzZCLE1BQWxDLEdBQTJDLENBQS9DLEVBQWtEO0FBQ2pELFVBQUk3QixDQUFDLENBQUMsOEJBQUQsQ0FBRCxDQUFrQ2tILFFBQWxDLEdBQTZDckYsTUFBN0MsR0FBc0QsQ0FBMUQsRUFBNkQ7QUFDNUQ3QixTQUFDLENBQUMsOEJBQUQsQ0FBRCxDQUFrQ2tCLEtBQWxDLENBQXdDO0FBQ3ZDRSxjQUFJLEVBQUUsSUFEaUM7QUFFdkNDLGtCQUFRLEVBQUUsSUFGNkI7QUFHdkNDLGtCQUFRLEVBQUUsSUFINkI7QUFJdkMrRixtQkFBUyxFQUFFLDhDQUo0QjtBQUt2Q0MsbUJBQVMsRUFBRTtBQUw0QixTQUF4QztBQU9BO0FBQ0QsS0F4R2lDLENBNEdsQzs7O0FBQ0EsUUFBSXRILENBQUMsQ0FBQyxzQkFBRCxDQUFELENBQTBCNkIsTUFBMUIsR0FBbUMsQ0FBdkMsRUFBMEM7QUFFekM7QUFDQTdCLE9BQUMsQ0FBQyxrQ0FBRCxDQUFELENBQXNDa0IsS0FBdEMsQ0FBNEM7QUFDM0NNLG9CQUFZLEVBQUUsQ0FENkI7QUFFM0NDLHNCQUFjLEVBQUUsQ0FGMkI7QUFHM0NOLGNBQU0sRUFBRSxLQUhtQztBQUkzQ29HLFlBQUksRUFBRSxJQUpxQztBQUszQ0MsZ0JBQVEsRUFBRTtBQUxpQyxPQUE1QyxFQUh5QyxDQVd6Qzs7QUFDQXhILE9BQUMsQ0FBQyxnQ0FBRCxDQUFELENBQW9Da0IsS0FBcEMsQ0FBMEM7QUFDekNNLG9CQUFZLEVBQUUsQ0FEMkI7QUFFekNDLHNCQUFjLEVBQUUsQ0FGeUI7QUFHekM0RixpQkFBUyxFQUFFLDhDQUg4QjtBQUl6Q0MsaUJBQVMsRUFBRSwrQ0FKOEI7QUFLekNFLGdCQUFRLEVBQUUsa0NBTCtCO0FBTXpDcEcsWUFBSSxFQUFFLEtBTm1DO0FBT3pDZ0csa0JBQVUsRUFBRSxJQVA2QjtBQVF6Q0sscUJBQWEsRUFBRSxJQVIwQjtBQVN6Qy9GLGtCQUFVLEVBQUUsQ0FBQztBQUNaQyxvQkFBVSxFQUFFLEdBREE7QUFFWkMsa0JBQVEsRUFBRTtBQUNUSix3QkFBWSxFQUFFO0FBREw7QUFGRSxTQUFEO0FBVDZCLE9BQTFDLEVBWnlDLENBNkJ6Qzs7QUFDQXhCLE9BQUMsQ0FBQyxtREFBRCxDQUFELENBQXVEOEIsYUFBdkQsQ0FBcUU7QUFDcEVDLFlBQUksRUFBRSxPQUQ4RDtBQUVwRTJGLGVBQU8sRUFBRTtBQUNSQyxpQkFBTyxFQUFFO0FBREQ7QUFGMkQsT0FBckUsRUE5QnlDLENBcUN6Qzs7QUFDQTNILE9BQUMsQ0FBQyxpREFBRCxDQUFELENBQXFEOEIsYUFBckQsQ0FBbUU7QUFDbEVDLFlBQUksRUFBRSxPQUQ0RDtBQUVsRTJGLGVBQU8sRUFBRTtBQUNSQyxpQkFBTyxFQUFFO0FBREQ7QUFGeUQsT0FBbkU7QUFPQSxLQTFKaUMsQ0E0SmxDOzs7QUFDQSxRQUFJM0gsQ0FBQyxDQUFDLHNCQUFELENBQUQsQ0FBMEI2QixNQUExQixHQUFtQyxDQUF2QyxFQUEwQztBQUV6QzdCLE9BQUMsQ0FBQyxrQ0FBRCxDQUFELENBQXNDa0IsS0FBdEMsQ0FBNEM7QUFDM0NNLG9CQUFZLEVBQUUsQ0FENkI7QUFFM0NDLHNCQUFjLEVBQUUsQ0FGMkI7QUFHM0NOLGNBQU0sRUFBRSxJQUhtQztBQUkzQ29HLFlBQUksRUFBRSxJQUpxQztBQUszQ2xHLGdCQUFRLEVBQUUsS0FMaUM7QUFNM0NtRyxnQkFBUSxFQUFFLGdDQU5pQztBQU8zQ0gsaUJBQVMsRUFBRSw4Q0FQZ0M7QUFRM0NDLGlCQUFTLEVBQUU7QUFSZ0MsT0FBNUM7QUFVQXRILE9BQUMsQ0FBQyxnQ0FBRCxDQUFELENBQW9Da0IsS0FBcEMsQ0FBMEM7QUFDekNNLG9CQUFZLEVBQUUsQ0FEMkI7QUFFekNDLHNCQUFjLEVBQUUsQ0FGeUI7QUFHekNKLGdCQUFRLEVBQUUsS0FIK0I7QUFJekNtRyxnQkFBUSxFQUFFLGtDQUorQjtBQUt6Q3BHLFlBQUksRUFBRSxLQUxtQztBQU16Q3dHLGdCQUFRLEVBQUUsSUFOK0I7QUFPekN6RyxjQUFNLEVBQUUsS0FQaUM7QUFRekNpRyxrQkFBVSxFQUFFLEtBUjZCO0FBU3pDSyxxQkFBYSxFQUFFLElBVDBCO0FBVXpDL0Ysa0JBQVUsRUFBRSxDQUFDO0FBQ1pDLG9CQUFVLEVBQUUsR0FEQTtBQUVaQyxrQkFBUSxFQUFFO0FBQ1RKLHdCQUFZLEVBQUU7QUFETDtBQUZFLFNBQUQ7QUFWNkIsT0FBMUMsRUFaeUMsQ0E4QnpDOztBQUNBeEIsT0FBQyxDQUFDLG1EQUFELENBQUQsQ0FBdUQ4QixhQUF2RCxDQUFxRTtBQUNwRUMsWUFBSSxFQUFFO0FBRDhELE9BQXJFO0FBSUEsS0FoTWlDLENBa01sQzs7O0FBQ0EsUUFBSS9CLENBQUMsQ0FBQywyQkFBRCxDQUFELENBQStCNkIsTUFBL0IsR0FBd0MsQ0FBNUMsRUFBK0M7QUFFOUM3QixPQUFDLENBQUMsbUNBQUQsQ0FBRCxDQUF1Q2tCLEtBQXZDLENBQTZDO0FBQzVDTSxvQkFBWSxFQUFFLENBRDhCO0FBRTVDQyxzQkFBYyxFQUFFLENBRjRCO0FBRzVDTixjQUFNLEVBQUUsS0FIb0M7QUFJNUNDLFlBQUksRUFBRSxJQUpzQztBQUs1Q21HLFlBQUksRUFBRSxJQUxzQztBQU01Q0MsZ0JBQVEsRUFBRTtBQU5rQyxPQUE3QyxFQUY4QyxDQVc5Qzs7QUFDQXhILE9BQUMsQ0FBQyxtREFBRCxDQUFELENBQXVEOEIsYUFBdkQsQ0FBcUU7QUFDcEVDLFlBQUksRUFBRSxPQUQ4RDtBQUVwRTJGLGVBQU8sRUFBRTtBQUNSQyxpQkFBTyxFQUFFO0FBREQ7QUFGMkQsT0FBckU7QUFPQTtBQUNELEdBajVCa0IsQ0FtNUJuQjs7O0FBQ0EsV0FBU0UscUJBQVQsR0FBaUM7QUFFaEM7QUFDQTdILEtBQUMsQ0FBQyxvQkFBRCxDQUFELENBQXdCd0csU0FBeEIsQ0FBa0MsWUFBWTtBQUU3QztBQUNBLFVBQUlzQixTQUFTLEdBQUc5SCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFpRCxNQUFSLEdBQWlCYixNQUFqQixHQUEwQkMsR0FBMUIsR0FDZnJDLENBQUMsQ0FBQ2dGLE1BQUQsQ0FBRCxDQUFVRCxNQUFWLEVBRGUsQ0FDSTtBQURKLFFBR2YvRSxDQUFDLENBQUMsV0FBRCxDQUFELENBQWUrSCxXQUFmLEVBSEQsQ0FINkMsQ0FNZDtBQUUvQjs7QUFDQS9ILE9BQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0JrQyxPQUFoQixDQUF3QjtBQUN2QkMsaUJBQVMsRUFBRTJGO0FBRFksT0FBeEIsRUFFRyxHQUZIO0FBSUEsS0FiRDtBQWNBOztBQUtELFdBQVNFLGNBQVQsR0FBMEI7QUFFekI7QUFDQS9CLG1CQUFlO0FBQ2ZDLGlCQUFhLEdBSlksQ0FNekI7O0FBQ0FsRyxLQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQmlJLE1BQXJCLENBQTRCLEdBQTVCLEVBUHlCLENBU3pCOztBQUNBakksS0FBQyxDQUFDLHdCQUFELENBQUQsQ0FBNEJ1QyxRQUE1QixDQUFxQyxRQUFyQztBQUNBdkMsS0FBQyxDQUFDLGdCQUFELENBQUQsQ0FBb0J1QyxRQUFwQixDQUE2QixRQUE3QixFQVh5QixDQWF6Qjs7QUFDQTZELGtCQUFjO0FBQ2Q7O0FBRUQsV0FBUzhCLGVBQVQsR0FBMkI7QUFFMUI7QUFDQSxRQUFJQyxZQUFZLEdBQUcsRUFBbkI7QUFDQSxRQUFJQyxnQkFBZ0IsR0FBRyxFQUF2QixDQUowQixDQU0xQjs7QUFDQXBJLEtBQUMsQ0FBQywyQ0FBRCxDQUFELENBQStDNkMsSUFBL0MsQ0FBb0QsWUFBWTtBQUUvRCxVQUFJc0YsWUFBWSxJQUFJLEVBQXBCLEVBQXdCO0FBQ3ZCQSxvQkFBWSxJQUFJLElBQWhCO0FBQ0E7O0FBQ0RBLGtCQUFZLElBQUksTUFBTUUsWUFBWSxDQUFDckksQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRc0ksSUFBUixFQUFELENBQWxDOztBQUVBLFVBQUlGLGdCQUFnQixJQUFJLEVBQXhCLEVBQTRCO0FBQzNCQSx3QkFBZ0IsSUFBSSxJQUFwQjtBQUNBOztBQUNEQSxzQkFBZ0IsSUFBSXBJLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXNJLElBQVIsRUFBcEI7QUFDQSxLQVhELEVBUDBCLENBb0IxQjtBQUNBOztBQUNBLFFBQUlILFlBQVksSUFBSSxFQUFwQixFQUF3QjtBQUV2Qm5JLE9BQUMsQ0FBQyw0QkFBRCxDQUFELENBQWdDdUksT0FBaEMsQ0FBd0M7QUFDdkNDLGNBQU0sRUFBRTtBQUQrQixPQUF4QyxFQUZ1QixDQU12QjtBQUNBLEtBUEQsTUFPTztBQUVOeEksT0FBQyxDQUFDLDRCQUFELENBQUQsQ0FBZ0N1SSxPQUFoQyxDQUF3QztBQUN2Q0MsY0FBTSxFQUFFTDtBQUQrQixPQUF4QztBQUlBLEtBbkN5QixDQXFDMUI7OztBQUNBLFFBQUksQ0FBQ25JLENBQUMsQ0FBQyw0QkFBRCxDQUFELENBQWdDeUksSUFBaEMsQ0FBcUMsU0FBckMsRUFBZ0RDLGFBQWhELENBQThEN0csTUFBbkUsRUFBMkU7QUFDMUU3QixPQUFDLENBQUMsV0FBRCxDQUFELENBQWUyQyxJQUFmO0FBQ0EsS0FGRCxNQUVPO0FBQ04zQyxPQUFDLENBQUMsV0FBRCxDQUFELENBQWUwQyxJQUFmO0FBQ0EsS0ExQ3lCLENBNEMxQjtBQUVBOzs7QUFDQSxRQUFJMEYsZ0JBQWdCLElBQUksRUFBeEIsRUFBNEI7QUFDM0JwSSxPQUFDLENBQUMsdUJBQUQsQ0FBRCxDQUEyQnNJLElBQTNCLENBQWdDLE9BQU9GLGdCQUF2QztBQUNBLEtBRkQsTUFFTztBQUNOcEksT0FBQyxDQUFDLHVCQUFELENBQUQsQ0FBMkJzSSxJQUEzQixDQUFnQyxFQUFoQztBQUNBLEtBbkR5QixDQXFEMUI7OztBQUNBdEksS0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJxRixPQUFyQixDQUE2QixHQUE3QixFQXREMEIsQ0F3RDFCOztBQUNBckYsS0FBQyxDQUFDLHdCQUFELENBQUQsQ0FBNEJ5RCxXQUE1QixDQUF3QyxRQUF4QztBQUNBekQsS0FBQyxDQUFDLGdCQUFELENBQUQsQ0FBb0J5RCxXQUFwQixDQUFnQyxRQUFoQyxFQTFEMEIsQ0E0RDFCOztBQUNBNkMsb0JBQWdCLEdBN0RVLENBK0QxQjs7QUFDQXRHLEtBQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0JrQyxPQUFoQixDQUF3QjtBQUN2QkMsZUFBUyxFQUFFbkMsQ0FBQyxDQUFDLE9BQUQsQ0FBRCxDQUFXb0MsTUFBWCxHQUFvQkMsR0FBcEIsR0FBMEI7QUFEZCxLQUF4QixFQUVHLEdBRkg7QUFHQTs7QUFFRCxXQUFTc0csNkJBQVQsR0FBeUM7QUFFeEM7QUFDQTNJLEtBQUMsQ0FBQyxtQ0FBRCxDQUFELENBQXVDMEMsSUFBdkM7QUFDQTFDLEtBQUMsQ0FBQyxtQ0FBRCxDQUFELENBQXVDeUQsV0FBdkMsQ0FBbUQsUUFBbkQ7QUFFQW1GLHFCQUFpQjtBQUNqQjVJLEtBQUMsQ0FBQ2dGLE1BQUQsQ0FBRCxDQUFVQyxNQUFWLENBQWlCLFlBQVk7QUFDNUIyRCx1QkFBaUI7QUFDakIsS0FGRDtBQUlBNUksS0FBQyxDQUFDLG1DQUFELENBQUQsQ0FBdUNnQyxLQUF2QyxDQUE2QyxVQUFVN0IsQ0FBVixFQUFhO0FBRXpEO0FBQ0FBLE9BQUMsQ0FBQzhCLGNBQUY7QUFDQWpDLE9BQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTBDLElBQVI7QUFFQW1HLDZCQUF1QjtBQUV2QixLQVJEOztBQVVBLGFBQVNELGlCQUFULEdBQTZCO0FBRTVCLFVBQUk1SSxDQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0QjZCLE1BQTVCLEdBQXFDLENBQXJDLElBQTBDLENBQUNpSCxnQkFBL0MsRUFBaUU7QUFFaEU7QUFDQSxZQUFJQyx3QkFBd0IsR0FBRy9JLENBQUMsQ0FBQ2dGLE1BQUQsQ0FBRCxDQUFVN0MsU0FBVixLQUF3Qm5DLENBQUMsQ0FBQ2dGLE1BQUQsQ0FBRCxDQUFVRCxNQUFWLEVBQXZELENBSGdFLENBS2hFOztBQUNBLFlBQUlpRSxXQUFXLEdBQUdDLHFCQUFsQixFQUF5QztBQUV4QztBQUNBLGNBQUlGLHdCQUF3QixHQUFHL0ksQ0FBQyxDQUFDLHdCQUFELENBQUQsQ0FBNEJvQyxNQUE1QixHQUFxQ0MsR0FBcEUsRUFBeUU7QUFFeEV3RyxtQ0FBdUI7QUFDdkI7QUFFRDtBQUVEO0FBQ0Q7O0FBRUQsYUFBU0EsdUJBQVQsR0FBbUM7QUFFbENHLGlCQUFXLEdBRnVCLENBSWxDOztBQUNBRixzQkFBZ0IsR0FBRyxJQUFuQixDQUxrQyxDQU9sQzs7QUFDQTlJLE9BQUMsQ0FBQyxzQ0FBRCxDQUFELENBQTBDeUQsV0FBMUMsQ0FBc0QsUUFBdEQsRUFSa0MsQ0FVbEM7O0FBQ0F5RixvQ0FBOEIsR0FBRyxLQUFqQyxDQVhrQyxDQWFsQzs7QUFDQUMsbUJBQWEsQ0FBQyxDQUFELENBQWIsQ0Fka0MsQ0FnQmxDOztBQUNBM0csZ0JBQVUsQ0FBQyxZQUFZO0FBQ3RCMEcsc0NBQThCLEdBQUcsSUFBakM7QUFDQSxPQUZTLEVBRVBFLGNBRk8sQ0FBVjtBQUdBO0FBQ0Q7O0FBRUQsV0FBU0QsYUFBVCxDQUF1QkUsbUJBQXZCLEVBQTRDO0FBRTNDO0FBQ0EsUUFBSUMsU0FBUyxHQUFHdEosQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUI2QixNQUFyQyxDQUgyQyxDQUszQzs7QUFDQSxRQUFJMEgsT0FBTyxHQUFHLEVBQWQ7O0FBQ0EsUUFBSXZKLENBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZTZCLE1BQWYsR0FBd0IsQ0FBNUIsRUFBK0I7QUFDOUIwSCxhQUFPLEdBQUd2SixDQUFDLENBQUMsV0FBRCxDQUFELENBQWVXLElBQWYsQ0FBb0IsY0FBcEIsQ0FBVjtBQUNBOztBQUVEWixVQUFNLENBQUN5SixJQUFQLENBQVk7QUFDWEMsU0FBRyxFQUFFQyxXQUFXLENBQUNDLFFBRE47QUFFWDVILFVBQUksRUFBRSxNQUZLO0FBR1gwRyxVQUFJLEVBQUU7QUFDTG1CLGNBQU0sRUFBRSwyQkFESDtBQUVMQyxxQkFBYSxFQUFFUCxTQUZWO0FBR0xRLGlCQUFTLEVBQUUsTUFITjtBQUlMUCxlQUFPLEVBQUVBLE9BSko7QUFLTFEsb0JBQVksRUFBRVY7QUFMVCxPQUhLO0FBVVhXLGFBQU8sRUFBRSxpQkFBVXZCLElBQVYsRUFBZ0I7QUFDeEJ3Qiw2QkFBcUIsQ0FBQ3hCLElBQUQsQ0FBckI7QUFDQSxPQVpVO0FBYVh5QixXQUFLLEVBQUUsZUFBVXpCLElBQVYsRUFBZ0I7QUFDdEIwQixlQUFPLENBQUNDLEdBQVIsQ0FBWSxxQkFBWjtBQUNBO0FBZlUsS0FBWjtBQWlCQTs7QUFFRCxXQUFTSCxxQkFBVCxDQUErQnhCLElBQS9CLEVBQXFDO0FBRXBDNEIsZ0NBQTRCOztBQUU1QixhQUFTQSw0QkFBVCxHQUF3QztBQUV2QztBQUNBLFVBQUluQiw4QkFBSixFQUFvQztBQUVuQztBQUNBbEosU0FBQyxDQUFDLHNDQUFELENBQUQsQ0FBMEN1QyxRQUExQyxDQUFtRCxRQUFuRCxFQUhtQyxDQUtuQzs7QUFDQSxZQUFJK0gsSUFBSSxHQUFHQyxJQUFJLENBQUNDLEtBQUwsQ0FBVy9CLElBQVgsQ0FBWCxDQU5tQyxDQVFuQzs7QUFDQXpJLFNBQUMsQ0FBQyx3QkFBRCxDQUFELENBQTRCeUssTUFBNUIsQ0FBbUNILElBQUksQ0FBQ3RKLElBQXhDLEVBVG1DLENBV25DOztBQUNBLFlBQUlzSixJQUFJLENBQUNJLFFBQVQsRUFBbUI7QUFDbEIxSyxXQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0QjJLLE1BQTVCO0FBQ0EsU0Fka0MsQ0FnQm5DOzs7QUFDQTdCLHdCQUFnQixHQUFHLEtBQW5CLENBakJtQyxDQW1CbkM7O0FBQ0EsWUFBSUUsV0FBVyxJQUFJQyxxQkFBbkIsRUFBMEM7QUFDekNqSixXQUFDLENBQUMsbUNBQUQsQ0FBRCxDQUF1QzJDLElBQXZDO0FBQ0E7QUFFRCxPQXhCRCxNQXdCTztBQUVOO0FBQ0FILGtCQUFVLENBQUMsWUFBWTtBQUN0QjZILHNDQUE0QjtBQUM1QixTQUZTLEVBRVAsR0FGTyxDQUFWO0FBR0E7QUFDRDtBQUNEOztBQUVELFdBQVNPLHVCQUFULEdBQW1DO0FBRWxDLFFBQUk1SyxDQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQjZCLE1BQXJCLEdBQThCLENBQTlCLElBQW1DN0IsQ0FBQyxDQUFDLHdCQUFELENBQUQsQ0FBNEI2QixNQUE1QixHQUFxQyxDQUE1RSxFQUErRTtBQUU5RTtBQUNBN0IsT0FBQyxDQUFDLDhFQUFELENBQUQsQ0FBa0ZnQyxLQUFsRixDQUF3RixVQUFTN0IsQ0FBVCxFQUFXO0FBQ2xHQSxTQUFDLENBQUM4QixjQUFGO0FBQ0FqQyxTQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQnNDLFdBQXJCLENBQWlDLGtCQUFqQztBQUNBLE9BSEQ7QUFLQXRDLE9BQUMsQ0FBQyx3QkFBRCxDQUFELENBQTRCZ0MsS0FBNUIsQ0FBa0MsWUFBWTtBQUU3QyxZQUFJaEMsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRa0UsUUFBUixDQUFpQixXQUFqQixDQUFKLEVBQW1DO0FBRWxDO0FBQ0FsRSxXQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQnNDLFdBQXJCLENBQWlDLGtCQUFqQztBQUVBLFNBTEQsTUFLSztBQUNKO0FBQ0EsY0FBSSxDQUFDdEMsQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJrRSxRQUFyQixDQUE4QixTQUE5QixDQUFMLEVBQStDO0FBRTlDMkcsMEJBQWMsR0FGZ0MsQ0FJOUM7QUFDQSxXQUxELE1BS087QUFFTjVFLDJCQUFlO0FBRWY7QUFDRDtBQUVELE9BckJEO0FBdUJBO0FBQ0Q7O0FBRUQsV0FBUzRFLGNBQVQsR0FBMEI7QUFFekI7QUFDQTlFLGFBQVMsR0FIZ0IsQ0FLekI7O0FBQ0FHLGlCQUFhO0FBRWJsRyxLQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQnVDLFFBQXJCLENBQThCLFNBQTlCO0FBQ0E7O0FBRUQsV0FBUzBELGVBQVQsR0FBMkI7QUFDMUJqRyxLQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQnlELFdBQXJCLENBQWlDLFNBQWpDO0FBQ0E7O0FBRUQsV0FBU3FILHFCQUFULEdBQWlDO0FBRWhDLFFBQUk5SyxDQUFDLENBQUMsb0JBQUQsQ0FBRCxDQUF3QjZCLE1BQXhCLEdBQWlDLENBQWpDLElBQXNDN0IsQ0FBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUI2QixNQUF6QixHQUFrQyxDQUE1RSxFQUErRTtBQUU5RTdCLE9BQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlLLEVBQVosQ0FBZSxrQkFBZixFQUFtQyxxQkFBbkMsRUFBMEQsWUFBWTtBQUNyRXlLLG9CQUFZO0FBQ1osT0FGRDtBQUlBL0ssT0FBQyxDQUFDLDBCQUFELENBQUQsQ0FBOEJnQyxLQUE5QixDQUFvQyxZQUFZO0FBRS9Da0UscUJBQWE7QUFDYixPQUhEO0FBS0E7QUFFRDs7QUFFRCxXQUFTNkUsWUFBVCxHQUF3QjtBQUV2QjtBQUNBOUUsbUJBQWUsR0FIUSxDQUt2Qjs7QUFDQUYsYUFBUyxHQU5jLENBUXZCOztBQUNBL0YsS0FBQyxDQUFDLG9CQUFELENBQUQsQ0FBd0J1QyxRQUF4QixDQUFpQyxRQUFqQyxFQVR1QixDQVd2Qjs7QUFDQTZELGtCQUFjO0FBQ2Q7O0FBRUQsV0FBU0YsYUFBVCxHQUF5QjtBQUV4QjtBQUNBbEcsS0FBQyxDQUFDLG9CQUFELENBQUQsQ0FBd0J5RCxXQUF4QixDQUFvQyxRQUFwQyxFQUh3QixDQUt4Qjs7QUFDQTZDLG9CQUFnQjtBQUNoQjs7QUFFRCxXQUFTakcsd0JBQVQsR0FBb0M7QUFFbkM7QUFDQWlGLGlDQUE2QixHQUhNLENBS25DOztBQUNBdkYsVUFBTSxDQUFDRSxRQUFELENBQU4sQ0FBaUIrSyxJQUFqQixDQUFzQixtQkFBdEIsRUFBMkMsWUFBWTtBQUN0RCxVQUFJaEwsQ0FBQyxDQUFDLG1CQUFELENBQUQsQ0FBdUI2QixNQUF2QixHQUFnQyxDQUFwQyxFQUF1QztBQUN0Q3lELHFDQUE2QjtBQUM3QjtBQUNELEtBSkQ7QUFLQTs7QUFJRCxNQUFJMkYsY0FBSjtBQUNBLE1BQUlDLFlBQVksR0FBRyxLQUFuQjs7QUFFQSxXQUFTOUUsY0FBVCxHQUEwQjtBQUN6Qjs7Ozs7Ozs7Ozs7OztBQWFBOztBQUVELFdBQVNFLGdCQUFULEdBQTRCO0FBQzdCOzs7Ozs7Ozs7Ozs7OztBQWNFOztBQUVELFdBQVM2RSwyQkFBVCxHQUF1QztBQUV0QztBQUNBLFFBQUluTCxDQUFDLENBQUMsNENBQUQsQ0FBRCxDQUFnRDZCLE1BQWhELEdBQXlELENBQTdELEVBQWdFO0FBRS9EO0FBQ0EsVUFBSXVKLFVBQVUsR0FBR3BMLENBQUMsQ0FBQyx5QkFBRCxDQUFsQjtBQUNBLFVBQUlxTCxZQUFZLEdBQUcsR0FBbkI7QUFDQUQsZ0JBQVUsQ0FBQzdDLE9BQVgsQ0FBbUI7QUFDbEIrQyxvQkFBWSxFQUFFLFVBREk7QUFFbEJDLGlCQUFTLEVBQUUsS0FGTztBQUVBO0FBQ2xCQyxrQkFBVSxFQUFFLFNBSE07QUFJbEJoRCxjQUFNLEVBQUUsa0JBQVk7QUFDbkIsY0FBSWpJLEtBQUssR0FBR1AsQ0FBQyxDQUFDLElBQUQsQ0FBYjtBQUNBLGNBQUl5TCxXQUFXLEdBQUd6TCxDQUFDLENBQUMsbUJBQUQsQ0FBbkI7QUFDQSxjQUFJMEwsUUFBUSxHQUFHOUcsVUFBVSxDQUFDNkcsV0FBVyxDQUFDOUssSUFBWixDQUFpQixpQkFBakIsQ0FBRCxDQUF6QjtBQUNBLGNBQUlnTCxRQUFRLEdBQUcvRyxVQUFVLENBQUM2RyxXQUFXLENBQUM5SyxJQUFaLENBQWlCLGlCQUFqQixDQUFELENBQXpCO0FBQ0EsY0FBSWlMLEtBQUssR0FBR2hILFVBQVUsQ0FBQ3JFLEtBQUssQ0FBQ0ksSUFBTixDQUFXLFlBQVgsQ0FBRCxDQUF0QjtBQUNBLGNBQUlrTCxZQUFZLEdBQUdELEtBQUssSUFBSUQsUUFBVCxJQUFxQkMsS0FBSyxJQUFJRixRQUFqRDtBQUVBLGlCQUFPbkwsS0FBSyxDQUFDdUwsRUFBTixDQUFTVCxZQUFULEtBQTJCUSxZQUFsQztBQUNBO0FBYmlCLE9BQW5CLEVBTCtELENBcUIvRDs7QUFDQTdMLE9BQUMsQ0FBQyw4QkFBRCxDQUFELENBQWtDZ0MsS0FBbEMsQ0FBd0MsVUFBVTdCLENBQVYsRUFBYTtBQUNwREEsU0FBQyxDQUFDOEIsY0FBRjtBQUNBakMsU0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRc0MsV0FBUixDQUFvQixRQUFwQjtBQUNBLFlBQUl5SixTQUFTLEdBQUcsRUFBaEI7QUFDQS9MLFNBQUMsQ0FBQyxxQ0FBRCxDQUFELENBQXlDNkMsSUFBekMsQ0FBOEMsWUFBWTtBQUN6RGtKLG1CQUFTLENBQUMvSSxJQUFWLENBQWVoRCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFXLElBQVIsQ0FBYSxhQUFiLENBQWY7QUFDQSxTQUZEO0FBR0EwSyxvQkFBWSxHQUFHVSxTQUFTLENBQUN4SSxJQUFWLENBQWUsR0FBZixDQUFmOztBQUNBLFlBQUk4SCxZQUFZLElBQUksRUFBcEIsRUFBd0I7QUFDdkJBLHNCQUFZLEdBQUcsR0FBZjtBQUNBOztBQUNERCxrQkFBVSxDQUFDN0MsT0FBWDtBQUNBLE9BWkQsRUF0QitELENBb0MvRDs7QUFDQSxVQUFJeUQscUJBQXFCLEdBQUdoTSxDQUFDLENBQUMsbUJBQUQsQ0FBN0I7QUFDQSxVQUFJaU0sU0FBUyxHQUFHckgsVUFBVSxDQUFDb0gscUJBQXFCLENBQUNyTCxJQUF0QixDQUEyQixVQUEzQixDQUFELENBQTFCO0FBQ0EsVUFBSXVMLFNBQVMsR0FBR3RILFVBQVUsQ0FBQ29ILHFCQUFxQixDQUFDckwsSUFBdEIsQ0FBMkIsVUFBM0IsQ0FBRCxDQUExQjtBQUNBcUwsMkJBQXFCLENBQUNHLE1BQXRCLENBQTZCO0FBQzVCQyxhQUFLLEVBQUUsSUFEcUI7QUFFNUJDLFdBQUcsRUFBRUosU0FGdUI7QUFHNUJLLFdBQUcsRUFBRUosU0FIdUI7QUFJNUJLLGNBQU0sRUFBRSxDQUFDTixTQUFELEVBQVlDLFNBQVosQ0FKb0I7QUFLNUJNLGNBQU0sRUFBRSxnQkFBVXJNLENBQVYsRUFBYXNNLEVBQWIsRUFBaUI7QUFDeEJULCtCQUFxQixDQUFDdEwsSUFBdEIsQ0FBMkIsNkJBQTNCLEVBQTBENkIsUUFBMUQsQ0FBbUUsV0FBbkUsRUFBZ0Z2QixJQUFoRixDQUFxRixnQ0FBZ0NnTCxxQkFBcUIsQ0FBQ0csTUFBdEIsQ0FBNkIsUUFBN0IsRUFBdUMsQ0FBdkMsQ0FBaEMsR0FBNEUsU0FBaks7QUFDQUgsK0JBQXFCLENBQUN0TCxJQUF0QixDQUEyQiw2QkFBM0IsRUFBMEQ2QixRQUExRCxDQUFtRSxXQUFuRSxFQUFnRnZCLElBQWhGLENBQXFGLGdDQUFnQ2dMLHFCQUFxQixDQUFDRyxNQUF0QixDQUE2QixRQUE3QixFQUF1QyxDQUF2QyxDQUFoQyxHQUE0RSxTQUFqSztBQUNBLFNBUjJCO0FBVTVCTyxhQUFLLEVBQUUsZUFBVUMsS0FBVixFQUFpQkYsRUFBakIsRUFBcUI7QUFDM0J6TSxXQUFDLENBQUN5TSxFQUFFLENBQUNHLE1BQUosQ0FBRCxDQUFhNUwsSUFBYixDQUFrQixnQ0FBZ0N5TCxFQUFFLENBQUM5SSxLQUFuQyxHQUEyQyxTQUE3RDtBQUNBcUksK0JBQXFCLENBQUNyTCxJQUF0QixDQUEyQixpQkFBM0IsRUFBOENxTCxxQkFBcUIsQ0FBQ0csTUFBdEIsQ0FBNkIsUUFBN0IsRUFBdUMsQ0FBdkMsQ0FBOUM7QUFDQUgsK0JBQXFCLENBQUNyTCxJQUF0QixDQUEyQixpQkFBM0IsRUFBOENxTCxxQkFBcUIsQ0FBQ0csTUFBdEIsQ0FBNkIsUUFBN0IsRUFBdUMsQ0FBdkMsQ0FBOUM7QUFDQSxTQWQyQjtBQWdCNUJVLFlBQUksRUFBRSxjQUFVRixLQUFWLEVBQWlCRixFQUFqQixFQUFxQjtBQUMxQnJCLG9CQUFVLENBQUM3QyxPQUFYO0FBQ0E7QUFsQjJCLE9BQTdCO0FBc0JBO0FBQ0Q7O0FBRUQsV0FBU3VFLG1CQUFULEdBQStCO0FBRTlCO0FBQ0E7QUFDQTtBQUVBO0FBQ0E5TSxLQUFDLENBQUMsK0JBQUQsQ0FBRCxDQUFtQ3VDLFFBQW5DLENBQTRDLGdCQUE1QztBQUNBdkMsS0FBQyxDQUFDLHNCQUFELENBQUQsQ0FBMEJ1QyxRQUExQixDQUFtQyxnQkFBbkMsRUFSOEIsQ0FVOUI7O0FBQ0F2QyxLQUFDLENBQUMsMEJBQUQsQ0FBRCxDQUE4QmdDLEtBQTlCLENBQW9DLFVBQVU3QixDQUFWLEVBQWE7QUFFaEQ7QUFDQUEsT0FBQyxDQUFDOEIsY0FBRixHQUhnRCxDQUtoRDs7QUFDQWpDLE9BQUMsQ0FBQyx3QkFBRCxDQUFELENBQTRCeUQsV0FBNUIsQ0FBd0MsZ0JBQXhDO0FBQ0F6RCxPQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQnlELFdBQXBCLENBQWdDLGdCQUFoQyxFQVBnRCxDQVNoRDs7QUFDQSxVQUFJc0osWUFBWSxHQUFHL00sQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRaUQsTUFBUixHQUFpQitKLEtBQWpCLEVBQW5CO0FBQ0FoTixPQUFDLENBQUMsSUFBRCxDQUFELENBQVFpRCxNQUFSLEdBQWlCVixRQUFqQixDQUEwQixnQkFBMUI7QUFDQXZDLE9BQUMsQ0FBQyxnQkFBRCxDQUFELENBQW9CaU4sRUFBcEIsQ0FBdUJGLFlBQXZCLEVBQXFDeEssUUFBckMsQ0FBOEMsZ0JBQTlDO0FBRUEsS0FkRCxFQVg4QixDQTJCOUI7QUFDQTtBQUNBO0FBRUE7O0FBQ0F2QyxLQUFDLENBQUMsK0NBQUQsQ0FBRCxDQUFtRHVDLFFBQW5ELENBQTRELGFBQTVEO0FBQ0F2QyxLQUFDLENBQUMscUNBQUQsQ0FBRCxDQUF5Q3VDLFFBQXpDLENBQWtELGFBQWxELEVBakM4QixDQW1DOUI7O0FBQ0F2QyxLQUFDLENBQUMsK0JBQUQsQ0FBRCxDQUFtQ2dDLEtBQW5DLENBQXlDLFVBQVU3QixDQUFWLEVBQWE7QUFFckQ7QUFDQSxVQUFJLENBQUNILENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWtFLFFBQVIsQ0FBaUIsY0FBakIsQ0FBTCxFQUF1QztBQUV0QztBQUNBL0QsU0FBQyxDQUFDOEIsY0FBRixHQUhzQyxDQUt0Qzs7QUFDQWpDLFNBQUMsQ0FBQywyQ0FBRCxDQUFELENBQStDeUQsV0FBL0MsQ0FBMkQsYUFBM0Q7QUFDQXpELFNBQUMsQ0FBQyw0QkFBRCxDQUFELENBQWdDeUQsV0FBaEMsQ0FBNEMsYUFBNUMsRUFQc0MsQ0FTdEM7O0FBQ0F6RCxTQUFDLENBQUMsSUFBRCxDQUFELENBQVF1QyxRQUFSLENBQWlCLGFBQWpCO0FBQ0F2QyxTQUFDLENBQUMsSUFBRCxDQUFELENBQVFpRCxNQUFSLEdBQWlCa0IsUUFBakIsQ0FBMEIsZ0JBQTFCLEVBQTRDNUIsUUFBNUMsQ0FBcUQsYUFBckQ7QUFFQTtBQUVELEtBbEJEO0FBb0JBOztBQUVELFdBQVM4RixZQUFULENBQXNCNkUsU0FBdEIsRUFBaUM7QUFFaEMsV0FBT0EsU0FBUyxDQUFDQyxPQUFWLENBQWtCLFlBQWxCLEVBQWdDLEdBQWhDLENBQVA7QUFFQTs7QUFJRCxXQUFTQyxzQkFBVCxHQUFrQztBQUVqQztBQUNBLFFBQUlwTixDQUFDLENBQUMsZUFBRCxDQUFELENBQW1CNkIsTUFBbkIsR0FBNEIsQ0FBaEMsRUFBbUM7QUFFbEM7QUFDQSxVQUFJd0wsWUFBWSxHQUFHLENBQW5CO0FBRUF0TixZQUFNLENBQUN5SixJQUFQLENBQVk7QUFDWEMsV0FBRyxFQUFFQyxXQUFXLENBQUNDLFFBRE47QUFFWDVILFlBQUksRUFBRSxNQUZLO0FBR1gwRyxZQUFJLEVBQUU7QUFDTG1CLGdCQUFNLEVBQUUsdUJBREg7QUFFTHlELHNCQUFZLEVBQUVBO0FBRlQsU0FISztBQU9YckQsZUFBTyxFQUFFLGlCQUFVdkIsSUFBVixFQUFnQjtBQUV4QjtBQUNBQSxjQUFJLEdBQUc4QixJQUFJLENBQUNDLEtBQUwsQ0FBVy9CLElBQVgsQ0FBUCxDQUh3QixDQUt4Qjs7QUFDQUEsY0FBSSxDQUFDLFFBQUQsQ0FBSixDQUFlNkUsT0FBZixDQUF1QixVQUFVQyxPQUFWLEVBQW1CO0FBRXpDO0FBQ0EsZ0JBQUlDLGVBQWUsR0FBRywwQ0FBMENELE9BQU8sQ0FBQyxXQUFELENBQWpELEdBQWlFLGlHQUFqRSxHQUFxS0EsT0FBTyxDQUFDLE9BQUQsQ0FBNUssR0FBd0wsVUFBOU07QUFFQXZOLGFBQUMsQ0FBQyxlQUFELENBQUQsQ0FBbUJ5TixNQUFuQixDQUEwQkQsZUFBMUI7QUFFQSxXQVBEO0FBU0EsU0F0QlU7QUF1Qlh0RCxhQUFLLEVBQUUsZUFBVXpCLElBQVYsRUFBZ0I7QUFDdEIwQixpQkFBTyxDQUFDQyxHQUFSLENBQVkscUJBQVo7QUFDQTtBQXpCVSxPQUFaO0FBNEJBO0FBRUQ7O0FBRUQsV0FBU3NELDBCQUFULEdBQXNDO0FBRXJDMU4sS0FBQyxDQUFDLG9DQUFELENBQUQsQ0FBd0NnQyxLQUF4QyxDQUE4QyxVQUFTN0IsQ0FBVCxFQUFXO0FBRXhEO0FBQ0FBLE9BQUMsQ0FBQzhCLGNBQUYsR0FId0QsQ0FLeEQ7O0FBQ0EwTCxlQUFTLENBQUUsd0JBQUYsRUFBNEIsK0JBQTVCLEVBQTZELEVBQTdELENBQVQsQ0FOd0QsQ0FReEQ7O0FBQ0EzTixPQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0QjJHLE9BQTVCO0FBRUEsS0FYRDtBQWFBOztBQUVELFdBQVNnSCxTQUFULENBQW1CQyxLQUFuQixFQUEwQkMsTUFBMUIsRUFBa0NDLE1BQWxDLEVBQTBDO0FBQ3pDLFFBQUlDLENBQUMsR0FBRyxJQUFJQyxJQUFKLEVBQVI7QUFDQUQsS0FBQyxDQUFDRSxPQUFGLENBQVVGLENBQUMsQ0FBQ0csT0FBRixLQUFlSixNQUFNLEdBQUMsRUFBUCxHQUFVLEVBQVYsR0FBYSxFQUFiLEdBQWdCLElBQXpDO0FBQ0EsUUFBSUssT0FBTyxHQUFHLGFBQVlKLENBQUMsQ0FBQ0ssV0FBRixFQUExQjtBQUNBbk8sWUFBUSxDQUFDb08sTUFBVCxHQUFrQlQsS0FBSyxHQUFHLEdBQVIsR0FBY0MsTUFBZCxHQUF1QixHQUF2QixHQUE2Qk0sT0FBN0IsR0FBdUMsU0FBekQ7QUFDQTtBQUVELENBNzlDSyxDQUFOO0FBKzlDQTs7OztBQUdFLENBQUMsVUFBVW5PLENBQVYsRUFBYTtBQUNaQSxHQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZQyxLQUFaLENBQWtCLFlBQVk7QUFDNUJGLEtBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlLLEVBQVosQ0FBZSxvQkFBZixFQUFvQywyQ0FBcEMsRUFBaUYsWUFBWTtBQUMzRk4sT0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQnNPLEtBQWxCLENBQXdCO0FBQ3RCQyxlQUFPLEVBQUUsSUFEYTtBQUV0QkMsa0JBQVUsRUFBRTtBQUNWQyxvQkFBVSxFQUFFLE1BREY7QUFFVkMsaUJBQU8sRUFBRTtBQUZDO0FBRlUsT0FBeEI7QUFPQSxVQUFJQyxPQUFPLEdBQUczTyxDQUFDLENBQUMsSUFBRCxDQUFELENBQVF5SSxJQUFSLENBQWEsU0FBYixDQUFkOztBQUNBLFVBQUkxSSxNQUFNLENBQUMsSUFBRCxDQUFOLENBQWE2TyxJQUFiLENBQWtCLFNBQWxCLEtBQWdDLElBQXBDLEVBQTBDO0FBQ3hDLFlBQUlDLEVBQUUsR0FBRyxJQUFUO0FBQ0QsT0FGRCxNQUVPLElBQUk5TyxNQUFNLENBQUMsSUFBRCxDQUFOLENBQWE2TyxJQUFiLENBQWtCLFNBQWxCLEtBQWdDLEtBQXBDLEVBQTJDO0FBRWhELFlBQUlDLEVBQUUsR0FBRyxFQUFUO0FBQ0Q7O0FBQ0QsVUFBSUMsVUFBVSxHQUFHOU8sQ0FBQyxDQUFDLGlCQUFpQjJPLE9BQWxCLENBQUQsQ0FBNEJsTyxHQUE1QixFQUFqQjtBQUNBLFVBQUlzTyxXQUFXLEdBQUdyRixXQUFXLENBQUNDLFFBQTlCO0FBQ0EsVUFBSWxCLElBQUksR0FBRztBQUNUbUIsY0FBTSxFQUFFLDBCQURDO0FBRVQsb0JBQVlpRixFQUZIO0FBR1QsaUJBQVNDLFVBSEE7QUFJVCxtQkFBV0g7QUFKRixPQUFYO0FBTUE1TyxZQUFNLENBQUNpUCxJQUFQLENBQVlELFdBQVosRUFBeUJ0RyxJQUF6QixFQUErQixVQUFVd0csUUFBVixFQUFvQjtBQUNqRDlFLGVBQU8sQ0FBQ0MsR0FBUixDQUFZNkUsUUFBWjtBQUNBQyxnQkFBUSxDQUFDQyxNQUFULEdBRmlELENBR2pEO0FBQ0QsT0FKRDtBQU1ELEtBN0JEO0FBOEJELEdBL0JEO0FBZ0NELENBakNELEVBaUNHcFAsTUFqQ0gsRTs7Ozs7Ozs7Ozs7QUN2K0NGO0FBQ0E7QUFDQSxJQUFJcVAsU0FBUyxHQUFHLEVBQWhCO0FBRUFyUCxNQUFNLENBQUMsVUFBU0MsQ0FBVCxFQUFXO0FBR2pCO0FBQ0FBLEdBQUMsQ0FBQyxPQUFELENBQUQsQ0FBVzZDLElBQVgsQ0FBZ0IsWUFBWTtBQUMzQndNLFdBQU8sQ0FBQ3JQLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXlJLElBQVIsQ0FBYSxLQUFiLENBQUQsRUFBc0J6SSxDQUFDLENBQUMsSUFBRCxDQUFELENBQVF5SSxJQUFSLENBQWEsS0FBYixDQUF0QixFQUEyQ3pJLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXlJLElBQVIsQ0FBYSxNQUFiLENBQTNDLEVBQWlFekksQ0FBQyxDQUFDLElBQUQsQ0FBbEUsQ0FBUDtBQUNBLEdBRkQ7O0FBTUEsV0FBU3FQLE9BQVQsQ0FBaUJDLEdBQWpCLEVBQXNCQyxHQUF0QixFQUEyQkMsUUFBM0IsRUFBcUNwRSxVQUFyQyxFQUFpRDtBQUVoRCxRQUFJcUUsYUFBYSxHQUFHSCxHQUFwQjtBQUNBLFFBQUlJLGFBQWEsR0FBR0gsR0FBcEI7O0FBQ0EsUUFBSXZQLENBQUMsQ0FBQ2dGLE1BQUQsQ0FBRCxDQUFVMkssS0FBVixLQUFvQixHQUF4QixFQUE2QixDQUM1QjtBQUNBO0FBQ0E7O0FBQ0QsUUFBSUMsUUFBUSxHQUFHeEUsVUFBZjtBQUNBLFFBQUl5RSxHQUFHLEdBQUcsSUFBSUMsTUFBTSxDQUFDQyxJQUFQLENBQVlDLEdBQWhCLENBQXFCSixRQUFRLENBQUMsQ0FBRCxDQUE3QixFQUFrQztBQUMzQ0ssaUJBQVcsRUFBRSxLQUQ4QjtBQUUzQ0MsVUFBSSxFQUFFLEVBRnFDO0FBRzNDQyxZQUFNLEVBQUVmLFNBSG1DO0FBSTNDZ0IsWUFBTSxFQUFFLElBQUlOLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZTSxNQUFoQixDQUF1QlosYUFBdkIsRUFBc0NDLGFBQXRDLENBSm1DO0FBSzNDWSxlQUFTLEVBQUUsU0FMZ0M7QUFNM0NDLHNCQUFnQixFQUFFO0FBTnlCLEtBQWxDLENBQVYsQ0FUZ0QsQ0FrQmhEO0FBQ0E7QUFDQTtBQUNBOztBQUVBLFFBQUlDLFFBQVEsR0FBRyxJQUFJVixNQUFNLENBQUNDLElBQVAsQ0FBWVUsSUFBaEIsQ0FBcUIsRUFBckIsRUFBeUIsRUFBekIsQ0FBZjs7QUFDQSxRQUFJelEsQ0FBQyxDQUFDZ0YsTUFBRCxDQUFELENBQVUySyxLQUFWLEtBQW9CLEdBQXhCLEVBQTZCO0FBQzVCYSxjQUFRLEdBQUcsSUFBSVYsTUFBTSxDQUFDQyxJQUFQLENBQVlVLElBQWhCLENBQXFCLEVBQXJCLEVBQXlCLEVBQXpCLENBQVg7QUFDQTs7QUFFRCxRQUFJQyxPQUFPLEdBQUc7QUFDYkMsY0FBUSxFQUFFLElBQUliLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZTSxNQUFoQixDQUF1QmYsR0FBdkIsRUFBNEJDLEdBQTVCO0FBREcsS0FBZDtBQUdBLFFBQUlxQixJQUFJLEdBQUc7QUFDVm5ILFNBQUcsRUFBRStGLFFBREs7QUFFVnFCLGdCQUFVLEVBQUVMO0FBRkYsS0FBWDtBQUlBLFFBQUlNLE1BQU0sR0FBRyxJQUFJaEIsTUFBTSxDQUFDQyxJQUFQLENBQVlnQixNQUFoQixDQUF1QjtBQUNuQ0osY0FBUSxFQUFFRCxPQUFPLENBQUNDLFFBRGlCO0FBRW5DQyxVQUFJLEVBQUVBLElBRjZCO0FBR25DQyxnQkFBVSxFQUFFLElBQUlmLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZVSxJQUFoQixDQUFxQixFQUFyQixFQUF5QixFQUF6QixDQUh1QjtBQUluQ1osU0FBRyxFQUFFQTtBQUo4QixLQUF2QixDQUFiLENBbkNnRCxDQTBDaEQ7O0FBQ0EsUUFBSW1CLFVBQVUsR0FBRyxJQUFJbEIsTUFBTSxDQUFDQyxJQUFQLENBQVlrQixVQUFoQixDQUEyQjtBQUMzQ0MsYUFBTyxFQUFFOUYsVUFBVSxDQUFDakgsUUFBWCxDQUFvQixjQUFwQixFQUFvQ25ELElBQXBDO0FBRGtDLEtBQTNCLENBQWpCLENBM0NnRCxDQStDaEQ7O0FBQ0E4TyxVQUFNLENBQUNDLElBQVAsQ0FBWXBELEtBQVosQ0FBa0J3RSxXQUFsQixDQUE4QkwsTUFBOUIsRUFBc0MsT0FBdEMsRUFBK0MsWUFBWTtBQUUxREUsZ0JBQVUsQ0FBQ0ksSUFBWCxDQUFnQnZCLEdBQWhCLEVBQXFCaUIsTUFBckI7QUFFQSxLQUpEO0FBS0E7QUFDRCxDQWhFSyxDQUFOLEM7Ozs7Ozs7Ozs7O0FDSkEvUSxNQUFNLENBQUMsVUFBU0MsQ0FBVCxFQUFZO0FBQ2xCO0FBQ0EsTUFBSUEsQ0FBQyxDQUFDLHdCQUFELENBQUQsQ0FBNEI2QixNQUE1QixHQUFxQyxDQUF6QyxFQUE0QztBQUUxQztBQUNBN0IsS0FBQyxDQUFDLHlDQUFELENBQUQsQ0FBNkM4QixhQUE3QyxDQUEyRDtBQUM1REMsVUFBSSxFQUFFLE9BRHNEO0FBRTVEMkYsYUFBTyxFQUFFO0FBQ1BDLGVBQU8sRUFBRTtBQURGLE9BRm1EO0FBSzVEMEosZUFBUyxFQUFFO0FBQ1RDLG9CQUFZLEVBQUUsc0JBQVNDLElBQVQsRUFBZTtBQUM5QjtBQUVBLGNBQUl2UixDQUFDLENBQUN1UixJQUFJLENBQUNDLEVBQUwsQ0FBUUMsT0FBVCxDQUFELENBQW1Cdk4sUUFBbkIsQ0FBNEIsWUFBNUIsQ0FBSixFQUErQztBQUM3Q3FOLGdCQUFJLENBQUN4UCxJQUFMLEdBQVksUUFBWjtBQUNELFdBRkQsTUFFTztBQUNMd1AsZ0JBQUksQ0FBQ3hQLElBQUwsR0FBWSxPQUFaO0FBQ0Q7QUFDQztBQVRRO0FBTGlELEtBQTNELEVBSDBDLENBcUIxQzs7QUFDQSxRQUFJL0IsQ0FBQyxDQUFDZ0YsTUFBRCxDQUFELENBQVUySyxLQUFWLE1BQXFCLEdBQXpCLEVBQThCO0FBQUU7QUFDakM7QUFDQSxVQUFJM1AsQ0FBQyxDQUFDLHdCQUFELENBQUQsQ0FBNEJrSCxRQUE1QixHQUF1Q3JGLE1BQXZDLEdBQWdELENBQXBELEVBQXVEO0FBQ3JEO0FBQ0Q7QUFDQyxLQUxELE1BS087QUFBRTtBQUNWO0FBQ0EsVUFBSTdCLENBQUMsQ0FBQyx3QkFBRCxDQUFELENBQTRCa0gsUUFBNUIsR0FBdUNyRixNQUF2QyxHQUFnRCxDQUFwRCxFQUF1RDtBQUNyRDtBQUNEO0FBQ0MsS0FoQ3lDLENBa0MxQzs7O0FBQ0EsUUFBSTZQLGNBQWMsR0FBRzFSLENBQUMsQ0FBQyx3QkFBRCxDQUFELENBQTRCa0gsUUFBNUIsR0FBdUNyRixNQUE1RDtBQUNBLFFBQUk4UCxhQUFhLEdBQUcsQ0FBcEIsQ0FwQzBDLENBcUMxQzs7QUFDQSxRQUFJRCxjQUFjLEdBQUcsQ0FBakIsSUFBc0IxUixDQUFDLENBQUNnRixNQUFELENBQUQsQ0FBVTJLLEtBQVYsS0FBb0IsR0FBOUMsRUFBbUQ7QUFDcERnQyxtQkFBYSxHQUFHLENBQWhCO0FBQ0UsS0F4Q3lDLENBMEMxQzs7O0FBQ0EzUixLQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0QnlELFdBQTVCLENBQXdDLGdCQUF4Qzs7QUFFQSxRQUFJa08sYUFBYSxHQUFHLENBQXBCLEVBQXVCO0FBRXhCO0FBQ0EzUixPQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0QmtCLEtBQTVCLENBQWtDO0FBQ2hDLGdCQUFRLENBRHdCO0FBRWhDLHdCQUFnQixDQUZnQjtBQUdoQywwQkFBa0IsSUFBSSxDQUhVO0FBSWhDRyxnQkFBUSxFQUFFLElBSnNCO0FBS2hDZ0csaUJBQVMsRUFBRSw4Q0FMcUI7QUFNaENDLGlCQUFTLEVBQUUsK0NBTnFCO0FBT2hDNUYsa0JBQVUsRUFBRSxDQUFDO0FBQ1pDLG9CQUFVLEVBQUUsSUFEQTtBQUVaQyxrQkFBUSxFQUFFO0FBQ1gsNEJBQWdCLENBREw7QUFFWCw4QkFBa0IsSUFBSTtBQUZYO0FBRkUsU0FBRCxFQU9iO0FBQ0VELG9CQUFVLEVBQUUsR0FEZDtBQUVFQyxrQkFBUSxFQUFFO0FBQ1gsNEJBQWdCLENBREw7QUFFWCw4QkFBa0IsSUFBSTtBQUZYO0FBRlosU0FQYSxFQWNiO0FBQ0VELG9CQUFVLEVBQUUsR0FEZDtBQUVFQyxrQkFBUSxFQUFFO0FBQ1gsNEJBQWdCLENBREw7QUFFWCw4QkFBa0I7QUFGUDtBQUZaLFNBZGE7QUFQb0IsT0FBbEM7QUErQkUsS0FsQ0QsTUFrQ087QUFFUjtBQUNBNUIsT0FBQyxDQUFDLHdCQUFELENBQUQsQ0FBNEJrQixLQUE1QixDQUFrQztBQUNoQyx3QkFBZ0IsQ0FEZ0I7QUFFaEMsMEJBQWtCLENBRmM7QUFHaENHLGdCQUFRLEVBQUUsSUFIc0I7QUFJaENnRyxpQkFBUyxFQUFFLDhDQUpxQjtBQUtoQ0MsaUJBQVMsRUFBRSwrQ0FMcUI7QUFNaEM1RixrQkFBVSxFQUFFLENBQUM7QUFDWkMsb0JBQVUsRUFBRSxJQURBO0FBRVpDLGtCQUFRLEVBQUU7QUFDWCw0QkFBZ0I7QUFETDtBQUZFLFNBQUQsRUFNYjtBQUNFRCxvQkFBVSxFQUFFLEdBRGQ7QUFFRUMsa0JBQVEsRUFBRTtBQUNYLDRCQUFnQjtBQURMO0FBRlosU0FOYSxFQVliO0FBQ0VELG9CQUFVLEVBQUUsR0FEZDtBQUVFQyxrQkFBUSxFQUFFO0FBQ1gsNEJBQWdCO0FBREw7QUFGWixTQVphO0FBTm9CLE9BQWxDO0FBMkJFO0FBQ0Y7QUFDRCxDQWpISyxDQUFOLEM7Ozs7Ozs7Ozs7O0FDQUE3QixNQUFNLENBQUMsVUFBU0MsQ0FBVCxFQUFZO0FBRWZBLEdBQUMsQ0FBQyxxQkFBRCxDQUFELENBQXlCNkMsSUFBekIsQ0FBOEIsWUFBVztBQUV2QzdDLEtBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWtCLEtBQVIsQ0FBYztBQUNaQyxZQUFNLEVBQUUsS0FESTtBQUVaQyxVQUFJLEVBQUUsSUFGTTtBQUdaQyxjQUFRLEVBQUUsSUFIRTtBQUlaQyxjQUFRLEVBQUUsS0FKRTtBQUtaQyxvQkFBYyxFQUFFO0FBTEosS0FBZDtBQVFELEdBVkQ7QUFZRCxDQWRHLENBQU4sQzs7Ozs7Ozs7Ozs7QUNBQXhCLE1BQU0sQ0FBQyxVQUFTQyxDQUFULEVBQVc7QUFFVkEsR0FBQyxDQUFDLHNCQUFELENBQUQsQ0FBMEI2QyxJQUExQixDQUErQixZQUFVO0FBRXJDLFFBQUlzRSxXQUFXLEdBQUcsRUFBbEI7QUFDQSxRQUFJNUcsS0FBSyxHQUFHUCxDQUFDLENBQUMsSUFBRCxDQUFiLENBSHFDLENBS3JDOztBQUNBLFFBQUdPLEtBQUssQ0FBQzJELFFBQU4sQ0FBZSxxQkFBZixDQUFILEVBQXlDO0FBQ3JDaUQsaUJBQVcsR0FBRyxDQUNWO0FBQ0l4RixrQkFBVSxFQUFFLEdBRGhCO0FBRUlDLGdCQUFRLEVBQUU7QUFDTlIsY0FBSSxFQUFFLEtBREE7QUFFTkQsZ0JBQU0sRUFBRSxLQUZGO0FBR05FLGtCQUFRLEVBQUUsS0FISjtBQUlOQyxrQkFBUSxFQUFFLElBSko7QUFLTjhGLG9CQUFVLEVBQUUsSUFMTjtBQU1ONUYsc0JBQVksRUFBRSxDQU5SO0FBT05DLHdCQUFjLEVBQUU7QUFQVjtBQUZkLE9BRFUsQ0FBZDtBQWNIOztBQUVEbEIsU0FBSyxDQUFDVyxLQUFOLENBQVk7QUFDUkMsWUFBTSxFQUFFLEtBREE7QUFFUkMsVUFBSSxFQUFFLElBRkU7QUFHUkMsY0FBUSxFQUFFLElBSEY7QUFJUkMsY0FBUSxFQUFFLElBSkY7QUFLUkMsb0JBQWMsRUFBRSxLQUxSO0FBTVJHLGdCQUFVLEVBQUV5RjtBQU5KLEtBQVo7QUFTQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQkgsR0FwREQ7QUFzRFAsQ0F4REssQ0FBTixDIiwiZmlsZSI6Ii9qcy9tYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCIvXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSAxKTtcbiIsImltcG9ydCAnfi9tYXBzLmpzJ1xyXG5pbXBvcnQgJ34vc2xpZGluZy1nYWxsZXJ5LmpzJ1xyXG5pbXBvcnQgJ34vdGVzdGltb25pYWwtc2xpZGVyLmpzJ1xyXG5pbXBvcnQgJ34vdGV4dC1nYWxsZXJ5LmpzJ1xyXG5cclxualF1ZXJ5KGZ1bmN0aW9uICgkKSB7XHJcblxyXG5cdCQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uIChlKSB7XHJcblxyXG5cdFx0SGVhZGVyTWVudUZ1bmN0aW9uKCk7XHJcblxyXG5cdFx0R3Jhdml0eUZvcm1GdW5jdGlvbmFsaXR5KCk7XHJcblxyXG5cdFx0Ly9TdGlja0hlYWRlckZ1bmN0aW9uYWxpdHkoKTtcclxuXHJcblx0XHQvL0J1cmdlck1lbnVGdW5jdGlvbmFsaXR5KCk7XHJcblxyXG5cdFx0Ly9BY2NvcmRpYW5NZW51RnVuY3Rpb25hbGl0eSgpOyBcclxuXHJcblx0XHQvL0Ryb3BEb3duTWVudUZ1bmN0aW9uYWx0eSgpO1xyXG5cclxuXHRcdC8vU2xpZGVGdWxsTWVudUZ1bmN0aW9uYWxpdHkoKTtcclxuXHJcblx0XHQvL1NsaWNrU2xpZGVyRnVuY3Rpb25hbHR5KCk7XHJcbiBcclxuXHRcdC8vRG93bkFycm93RnVuY3Rpb25hbHR5KCk7XHJcbiBcclxuXHRcdC8vR29vZ2xlTWFwc0Z1bmN0aW9uYWxpdHkoKTtcclxuXHJcblx0XHQvL01hc29uYXJ5TGF5b3V0RnVuY3Rpb25hbGl0eSgpO1xyXG5cclxuXHRcdC8vRHluYW1pY0xvYWRQb3N0c0Z1bmN0aW9uYWxpdHkoKTtcclxuXHJcblx0XHQvL1NlYXJjaERyb3BGdW5jdGlvbmFsaXR5KCk7XHJcblxyXG5cdFx0Ly9DYXJ0RHJvcEZ1bmN0aW9uYWxpdHkoKTtcclxuXHJcblx0XHQvL1dvb1Nob3BJc290b3BlRnVuY3Rpb25hbGl0eSgpO1xyXG5cclxuXHRcdC8vV29vVGFiRnVuY3Rpb25hbGl0eSgpO1xyXG5cclxuXHRcdC8vSW5zdGFncmFtRnVuY3Rpb25hbGl0eSgpO1xyXG5cclxuXHRcdC8vQ29va2llc0Zvb3RlckZ1bmN0aW9uYWxpdHkoKTtcclxuXHJcblx0fSlcclxuXHJcblx0Ly8gb24gY2hhbmdlIHZhcmlhdGlvblxyXG5cdCQoZG9jdW1lbnQpLm9uKCdjaGFuZ2UnLCcudHJpZ2dlci1zZWxlY3QtdXBkYXRlJyxmdW5jdGlvbigpe1xyXG5cclxuXHRcdHZhciAkdGhpcyA9ICQodGhpcylcclxuXHRcdHZhciAkdmFsdWUgPSAkdGhpcy52YWwoKVxyXG5cdFx0JChkb2N1bWVudCkuZmluZCgnc2VsZWN0W25hbWU9XCInKyAkdGhpcy5hdHRyKCdkYXRhLW5hbWUnKSArJ1wiXScpLnZhbCgkdmFsdWUpLmNoYW5nZSgpXHJcblx0XHRcclxuXHRcdGlmKHdlYmVyX2RhdGFfdmFyaWF0aW9uX3ByaWNlICE9IGZhbHNlKXtcclxuXHRcdFx0dmFyIHNlbGVjdGVkVmFyaWF0aW9uID0gJCgnaW5wdXRbbmFtZT1cInZhcmlhdGlvbl9pZFwiXScpLnZhbCgpXHJcblx0XHRcdHZhciB0aGVQcmljZSA9IHdlYmVyX2RhdGFfdmFyaWF0aW9uX3ByaWNlW3NlbGVjdGVkVmFyaWF0aW9uXVxyXG5cdFx0XHQkKCcjd2ViZXItcHJpY2Utd3JhcCcpLmh0bWwoJzxwIGNsYXNzPVwicHJpY2VcIj4nKyB0aGVQcmljZSArJzwvcD4nKVxyXG5cdFx0fVxyXG5cclxuXHR9KVxyXG5cclxuXHQvLyBvbiBsb2FkIGdldCBzZWxlY3RlZCB2YXJpYXRpb25cclxuXHQkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpe1xyXG5cclxuXHRcdHZhciAkdGhpcyA9ICQoZG9jdW1lbnQpLmZpbmQoJy50cmlnZ2VyLXNlbGVjdC11cGRhdGUnKVxyXG5cdFx0dmFyIHNlbGVjdGVkRGVmYXVsdCA9ICQoZG9jdW1lbnQpLmZpbmQoJ3NlbGVjdFtuYW1lPVwiJysgJHRoaXMuYXR0cignZGF0YS1uYW1lJykgKydcIl0nKS52YWwoKVxyXG5cdFx0aWYoc2VsZWN0ZWREZWZhdWx0ICE9ICcnKXtcclxuXHRcdFx0JChkb2N1bWVudCkuZmluZCgnLnRyaWdnZXItc2VsZWN0LXVwZGF0ZVt2YWx1ZT1cIicrIHNlbGVjdGVkRGVmYXVsdCArJ1wiXScpLmF0dHIoJ2NoZWNrZWQnLCdjaGVja2VkJylcclxuXHRcdH1cclxuXHJcblx0fSlcclxuXHJcblx0Ly8gcHJvZHVjdCBjYXRlZ29yeSBzbGlkZXJcclxuXHQkKCcjcmVsYXRlZC1hY2Nlc3NvcmllcyAuaW5uZXItcmVsYXRlZC1hY2Nlc3NvcmllcyB1bC5wcm9kdWN0cycpLnNsaWNrKHtcclxuICAgICAgICBhcnJvd3M6IHRydWUsXHJcbiAgICAgICAgZG90czogZmFsc2UsXHJcbiAgICAgICAgaW5maW5pdGU6IGZhbHNlLFxyXG4gICAgICAgIGF1dG9wbGF5OiBmYWxzZSxcclxuXHRcdGFkYXB0aXZlSGVpZ2h0OiB0cnVlLFxyXG5cdFx0c2xpZGVzVG9TaG93OiA0LFxyXG5cdFx0c2xpZGVzVG9TY3JvbGw6IDQsXHJcblx0XHRyZXNwb25zaXZlOiBbe1xyXG5cdFx0XHRicmVha3BvaW50OiAxMjAwLFxyXG5cdFx0XHRzZXR0aW5nczoge1xyXG5cdFx0XHRcdHNsaWRlc1RvU2hvdzogMyxcclxuXHRcdFx0XHRzbGlkZXNUb1Njcm9sbDogMyxcclxuXHRcdFx0fX0sXHJcblx0XHRcdHtcclxuXHRcdFx0YnJlYWtwb2ludDogOTkxLFxyXG5cdFx0XHRzZXR0aW5nczoge1xyXG5cdFx0XHRcdHNsaWRlc1RvU2hvdzogMixcclxuXHRcdFx0XHRzbGlkZXNUb1Njcm9sbDogMlxyXG5cdFx0XHR9fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRicmVha3BvaW50OiA0ODAsXHJcblx0XHRcdHNldHRpbmdzOiB7XHJcblx0XHRcdFx0c2xpZGVzVG9TaG93OiAxLFxyXG5cdFx0XHRcdHNsaWRlc1RvU2Nyb2xsOiAxXHJcblx0XHRcdH19XHJcblx0XHRcdC8vIFlvdSBjYW4gdW5zbGljayBhdCBhIGdpdmVuIGJyZWFrcG9pbnQgbm93IGJ5IGFkZGluZzpcclxuXHRcdFx0Ly8gc2V0dGluZ3M6IFwidW5zbGlja1wiXHJcblx0XHRcdC8vIGluc3RlYWQgb2YgYSBzZXR0aW5ncyBvYmplY3RcclxuXHRcdF0gIFxyXG5cdH0pO1xyXG5cdFxyXG5cdC8vIHBvcG91cFxyXG5cdGlmKCQoJy5vcGVuLXZpZGVvLXBvcHVwJykubGVuZ3RoID4gMCl7XHJcblx0XHQkKCcub3Blbi12aWRlby1wb3B1cCcpLm1hZ25pZmljUG9wdXAoe1xyXG5cdFx0XHR0eXBlOiAnaWZyYW1lJyxcclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblx0Ly8gYWNjZXNzb3JpZXNcclxuXHRpZiggJCgnLmNvbnRhaW5lci1hY2Nlc3NvcmllcycpLmxlbmd0aCA+IDAgKXtcclxuXHRcdC8vJCgnLnNpZGViYXItZmlsdGVyLCAuY29udGFpbmVyLWxvb3AnKS5zdGlja19pbl9wYXJlbnQoe29mZnNldF90b3A6IDkwfSk7XHJcblx0fVxyXG5cclxuXHQvLyBmaWx0ZXJcclxuXHQkKCcuc2lkZWJhci1maWx0ZXIgdWwgbGkgYScpLmNsaWNrKGZ1bmN0aW9uKGUpe1xyXG5cdFx0ZS5wcmV2ZW50RGVmYXVsdCgpXHJcblxyXG5cdFx0Ly8gYWx3YXlzIG1vdmUgdG8gdG9wXHJcblx0XHQkKCdodG1sLCBib2R5JykuYW5pbWF0ZSh7XHJcblx0XHRcdHNjcm9sbFRvcDogJCgnLmNvbnRhaW5lci1hY2Nlc3NvcmllcycpLm9mZnNldCgpLnRvcCAtIDcwXHJcblx0XHR9KVxyXG5cclxuXHRcdCQodGhpcykudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpXHJcblx0XHQkKCcjYWpheC1hcHBlbmQtdGFyZ2V0JykuYWRkQ2xhc3MoJ2xvYWRpbmcnKVxyXG5cclxuXHRcdHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuXHRcdFx0ZG9GaWx0ZXJpbmcoKVxyXG5cdFx0fSw1MDApXHJcblx0XHRcclxuXHRcdFxyXG5cdH0pXHJcblxyXG5cdGZ1bmN0aW9uIGRvRmlsdGVyaW5nKCl7XHJcblxyXG5cdFx0aWYoICQoZG9jdW1lbnQpLmZpbmQoJy5zaWRlYmFyLWZpbHRlciAucGFyZW50LWNhdCB1bCBsaSBhLmFjdGl2ZScpLmxlbmd0aCA+IDApe1xyXG5cdFx0XHQkKCcuc2lkZWJhci1maWx0ZXIgLmNoaWxkLWNhdCB1bCBsaScpLmhpZGUoKVxyXG5cdFx0fWVsc2V7XHJcblx0XHRcdCQoJy5zaWRlYmFyLWZpbHRlciAuY2hpbGQtY2F0IHVsIGxpJykuc2hvdygpXHJcblx0XHR9XHJcblxyXG5cdFx0dmFyICRwYXJlbnRfYXJyID0gW11cclxuXHRcdCQoZG9jdW1lbnQpLmZpbmQoJy5zaWRlYmFyLWZpbHRlciAucGFyZW50LWNhdCB1bCBsaSBhLmFjdGl2ZScpLmVhY2goZnVuY3Rpb24oaSxlKXtcclxuXHRcdFx0dmFyIHRoZUZpbHRlciA9ICQodGhpcykuYXR0cignZGF0YS1jYXQtaWQnKVxyXG5cdFx0XHQkcGFyZW50X2Fyci5wdXNoKHRoZUZpbHRlcilcclxuXHRcdFx0JChkb2N1bWVudCkuZmluZCgnLnNpZGViYXItZmlsdGVyIC5jaGlsZC1jYXQgdWwgbGkgYVtkYXRhLXBhcmVudC1pZD1cIicrIHRoZUZpbHRlciArJ1wiXScpLnBhcmVudCgpLnNob3coKVxyXG5cdFx0fSlcclxuXHJcblxyXG5cdFx0Ly8gbWFwIGFsbCBhY3RpdmUgZmlsdGVyXHJcblx0XHR2YXIgJGZpbHRlciA9IFtdXHJcblx0XHQkKGRvY3VtZW50KS5maW5kKCcuc2lkZWJhci1maWx0ZXIgdWwgbGk6dmlzaWJsZSBhLmFjdGl2ZScpLmVhY2goZnVuY3Rpb24oaSxlKXtcclxuXHRcdFx0dmFyICRmaWx0ZXJJdGVtID0gJCh0aGlzKS5hdHRyKCdkYXRhLWNhdC1pZCcpXHJcblx0XHRcdGlmICgkZmlsdGVySXRlbSA9PSAnKicpIHtcclxuXHRcdFx0XHQkZmlsdGVySXRlbSA9ICcnXHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0JGZpbHRlci5wdXNoKCRmaWx0ZXJJdGVtKVxyXG5cdFx0XHR9XHJcblx0XHR9KVxyXG5cclxuXHRcdC8vIGlmIHN1YiBjYXRlZ29yeSBjbGlja2VkLCByZW1vdmUgcGFyZW50IGZyb20gYXJyYXkgdG8gcmVmaW5lXHJcblx0XHQkKGRvY3VtZW50KS5maW5kKCcuc2lkZWJhci1maWx0ZXIgLmNoaWxkLWNhdCB1bCBsaSBhLmFjdGl2ZScpLmVhY2goZnVuY3Rpb24oKXtcclxuXHRcdFx0dmFyICRfcGFyZW50ID0gJCh0aGlzKS5hdHRyKCdkYXRhLXBhcmVudC1pZCcpXHJcblx0XHRcdCRmaWx0ZXIgPSByZW1vdmVJdGVtQWxsKCRmaWx0ZXIsICRfcGFyZW50KVxyXG5cdFx0fSlcclxuXHJcblx0XHQvLyByZXNldCBubyByZXN1bHRlXHJcblx0XHQkKCdsaS5wcm9kdWN0Lm5vdC1mb3VuZCcpLmhpZGUoKVxyXG5cclxuXHRcdHZhciBhbGxGaWx0ZXIgPSAkZmlsdGVyLmpvaW4oJyAnKSBcclxuXHRcdGlmKGFsbEZpbHRlciAhPSAnJyl7XHJcblx0XHRcdCQoJy5jb250YWluZXItYWNjZXNzb3JpZXMgdWwucHJvZHVjdHMgbGkucHJvZHVjdCcpLmVhY2goZnVuY3Rpb24oKXtcclxuXHRcdFx0XHRpZiggJCh0aGlzKS5oYXNBbnlDbGFzcyhhbGxGaWx0ZXIpICl7XHJcblx0XHRcdFx0XHQkKHRoaXMpLnNob3coKVxyXG5cdFx0XHRcdH1lbHNle1xyXG5cdFx0XHRcdFx0JCh0aGlzKS5oaWRlKClcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pXHJcblx0XHR9ZWxzZXtcclxuXHRcdFx0JCgnLmNvbnRhaW5lci1hY2Nlc3NvcmllcyB1bC5wcm9kdWN0cyBsaS5wcm9kdWN0Jykuc2hvdygpXHJcblx0XHR9XHJcblxyXG5cdFx0Ly8gc2hvdyBubyByZXN1bHRzXHJcblx0XHRpZiggJCgnLmNvbnRhaW5lci1hY2Nlc3NvcmllcyB1bC5wcm9kdWN0cyBsaS5wcm9kdWN0OnZpc2libGU6bm90KC5ub3QtZm91bmQpJykubGVuZ3RoIDwgMSApe1xyXG5cdFx0XHQkKCdsaS5wcm9kdWN0Lm5vdC1mb3VuZCcpLnNob3coKVxyXG5cdFx0fWVsc2V7XHJcblx0XHRcdCQoJ2xpLnByb2R1Y3Qubm90LWZvdW5kJykuaGlkZSgpXHJcblx0XHR9XHJcblxyXG5cdFx0c2V0VGltZW91dChmdW5jdGlvbigpe1xyXG5cdFx0XHQkKCcjYWpheC1hcHBlbmQtdGFyZ2V0JykucmVtb3ZlQ2xhc3MoJ2xvYWRpbmcnKVxyXG5cdFx0fSwgMTAwKVxyXG5cclxuXHR9XHJcblxyXG5cdC8vIGhlbHBlciByZW1vdmUgYXJyYXlcclxuXHRmdW5jdGlvbiByZW1vdmVJdGVtQWxsKGFyciwgdmFsdWUpIHtcclxuXHRcdHZhciBpID0gMDtcclxuXHRcdHdoaWxlIChpIDwgYXJyLmxlbmd0aCkge1xyXG5cdFx0ICBpZiAoYXJyW2ldID09PSB2YWx1ZSkge1xyXG5cdFx0XHRhcnIuc3BsaWNlKGksIDEpO1xyXG5cdFx0ICB9IGVsc2Uge1xyXG5cdFx0XHQrK2k7XHJcblx0XHQgIH1cclxuXHRcdH1cclxuXHRcdHJldHVybiBhcnI7XHJcblx0ICB9XHJcblxyXG5cdC8vIGhlbHBlciBmb3IgZmlsdGVyXHJcblx0JC5mbi5oYXNBbnlDbGFzcyA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0dmFyIGNsYXNzZXMgPSBhcmd1bWVudHNbaV0uc3BsaXQoXCIgXCIpO1xyXG5cdFx0XHRmb3IgKHZhciBqID0gMDsgaiA8IGNsYXNzZXMubGVuZ3RoOyBqKyspIHtcclxuXHRcdFx0XHRpZiAodGhpcy5oYXNDbGFzcyhjbGFzc2VzW2pdKSkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRyZXR1cm4gZmFsc2U7XHJcblx0fVxyXG5cclxuXHQkKGRvY3VtZW50KS5vbignY2xpY2snLCcuc29ydC1hY3Rpb24nLGZ1bmN0aW9uKGUpe1xyXG5cdFx0ZS5wcmV2ZW50RGVmYXVsdCgpXHJcblxyXG5cdFx0Ly8gc3R5bGluZ1xyXG5cdFx0JCh0aGlzKS5wYXJlbnQoKS5zaWJsaW5ncygnbGknKS5maW5kKCdhJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpXHJcblx0XHQkKHRoaXMpLmFkZENsYXNzKCdhY3RpdmUnKVxyXG5cclxuXHRcdCQoJyNhamF4LWFwcGVuZC10YXJnZXQnKS5hZGRDbGFzcygnbG9hZGluZycpXHJcblxyXG5cdFx0dmFyIHNvcnRCeSA9ICQodGhpcykuYXR0cignZGF0YS1zb3J0JylcclxuXHRcdHZhciBvcmllbnRhdGlvbiA9ICQodGhpcykuYXR0cignZGF0YS1vcmRlcicpXHJcblxyXG5cdFx0c2V0VGltZW91dChmdW5jdGlvbigpe1xyXG5cdFx0XHRpZihzb3J0QnkgPT0gJ2FscGhhJyAmJiBvcmllbnRhdGlvbiA9PSAnYXNjJyl7XHJcblx0XHRcdFx0dGlueXNvcnQoJ3VsLnByb2R1Y3RzID4gbGknLHthdHRyOidkYXRhLWFscGhhJywgb3JkZXI6J2FzYyd9KTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aWYoc29ydEJ5ID09ICdhbHBoYScgJiYgb3JpZW50YXRpb24gPT0gJ2Rlc2MnKXtcclxuXHRcdFx0XHR0aW55c29ydCgndWwucHJvZHVjdHMgPiBsaScse2F0dHI6J2RhdGEtYWxwaGEnLCBvcmRlcjonZGVzYyd9KTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aWYoc29ydEJ5ID09ICdwcmljZScgJiYgb3JpZW50YXRpb24gPT0gJ2FzYycpe1xyXG5cdFx0XHRcdHRpbnlzb3J0KCd1bC5wcm9kdWN0cyA+IGxpJyx7YXR0cjonZGF0YS1wcmljZScsIG9yZGVyOidhc2MnfSk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmKHNvcnRCeSA9PSAncHJpY2UnICYmIG9yaWVudGF0aW9uID09ICdkZXNjJyl7XHJcblx0XHRcdFx0dGlueXNvcnQoJ3VsLnByb2R1Y3RzID4gbGknLHthdHRyOidkYXRhLXByaWNlJywgb3JkZXI6J2Rlc2MnfSk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdCQoJyNhamF4LWFwcGVuZC10YXJnZXQnKS5yZW1vdmVDbGFzcygnbG9hZGluZycpXHJcblx0XHR9LDUwMClcclxuXHR9KVxyXG5cclxuXHQkKCcudHJpZ2VyLWZpbHRlcicpLmNsaWNrKGZ1bmN0aW9uKGUpe1xyXG5cdFx0ZS5wcmV2ZW50RGVmYXVsdCgpXHJcblx0XHQkKCcuc2lkZWJhci1maWx0ZXInKS5hZGRDbGFzcygnb3Blbi1maWx0ZXInKVxyXG5cdH0pXHJcblxyXG5cdCQoJy5jb250YWluZXItYWNjZXNzb3JpZXMgLnNpZGViYXItZmlsdGVyIC5jbG9zZS1maWx0ZXIgYScpLmNsaWNrKGZ1bmN0aW9uKGUpe1xyXG5cdFx0ZS5wcmV2ZW50RGVmYXVsdCgpXHJcblx0XHQkKCcuc2lkZWJhci1maWx0ZXInKS5yZW1vdmVDbGFzcygnb3Blbi1maWx0ZXInKVxyXG5cdH0pXHJcblxyXG5cclxuXHQkKCcubmV4dC1idXR0b24gYS5kbi1idXR0b24nKS5jbGljayhmdW5jdGlvbihlKXtcclxuXHRcdGUucHJldmVudERlZmF1bHQoKVxyXG5cclxuXHRcdC8vIHZhbGlkYXRpb25cclxuXHRcdHZhciBfcmV0dXJuID0gdHJ1ZTtcclxuXHRcdCQoJy52YWxpZGF0ZS1yZXF1aXJlZCBpbnB1dCwgLnZhbGlkYXRlLXJlcXVpcmVkIHNlbGVjdCcpLmVhY2goZnVuY3Rpb24oaSxlKXtcclxuXHRcdFx0aWYoICQodGhpcykudmFsKCkudHJpbSgpID09ICcnICl7XHJcblx0XHRcdFx0X3JldHVybiA9IGZhbHNlO1xyXG5cdFx0XHRcdCQodGhpcykucGFyZW50cygnLmZvcm0tcm93JykuYWRkQ2xhc3MoJ3dvb2NvbW1lcmNlLWludmFsaWQnKVxyXG5cdFx0XHRcdHJldHVybiBmYWxzZTsgLy8gYnJlYWtzXHJcblx0XHRcdH1cclxuXHRcdH0pXHJcblxyXG5cdFx0aWYoX3JldHVybiA9PSBmYWxzZSl7XHJcblx0XHRcdCQoJ2h0bWwsYm9keScpLmFuaW1hdGUoe1xyXG5cdFx0XHRcdHNjcm9sbFRvcDogJChkb2N1bWVudCkuZmluZCgnLndvb2NvbW1lcmNlLWludmFsaWQ6Zmlyc3QnKS5vZmZzZXQoKS50b3AgLSA4MFxyXG5cdFx0XHR9KVxyXG5cdFx0XHRyZXR1cm47XHJcblx0XHR9XHJcblxyXG5cdFx0dmFyICRuZXh0ID0gcGFyc2VGbG9hdCggJCh0aGlzKS5hdHRyKCdkYXRhLW5leHQnKSApXHJcblx0XHR2YXIgcGFnZUxlbmd0aCA9ICQoJy5jaGVja291dC1zdGVwcyB1bCBsaScpLmxlbmd0aFxyXG5cclxuXHRcdC8vIGhlYWRpbmdcclxuXHRcdCQoJy5jaGVja291dC1zdGVwcyB1bCBsaScpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKVxyXG5cdFx0JCgnLmNoZWNrb3V0LXN0ZXBzIHVsIGxpW2RhdGEtcGFnZT1cIicrICRuZXh0ICsnXCJdJykuYWRkQ2xhc3MoJ2FjdGl2ZScpXHJcblx0XHRmb3IodmFyIGk9MDtpPCRuZXh0O2krKyl7XHJcblx0XHRcdCQoJy5jaGVja291dC1zdGVwcyB1bCBsaTpudGgtY2hpbGQoJytpKycpJykuYWRkQ2xhc3MoJ3Bhc3NlZCcpXHJcblx0XHR9XHJcblxyXG5cdFx0Ly8gY29udGVudFxyXG5cdFx0JCgnLndvb2NvbW1lcmNlLWNoZWNrb3V0IGRpdltkYXRhLXBhZ2VdJykuaGlkZSgpXHJcblx0XHQkKCcud29vY29tbWVyY2UtY2hlY2tvdXQgZGl2W2RhdGEtcGFnZT1cIicrJG5leHQrJ1wiXScpLnNob3coKVxyXG5cclxuXHRcdC8vIHNjcm9sbCB0byB0b3BcclxuXHRcdCQoJ2h0bWwsYm9keScpLmFuaW1hdGUoe1xyXG5cdFx0XHRzY3JvbGxUb3A6ICQoJy5jaGVja291dC1oZWFkJykub2Zmc2V0KCkudG9wIC0gODBcclxuXHRcdH0pXHJcblxyXG5cdH0pXHJcblxyXG5cdFxyXG5cdFxyXG5cdC8vIEFsbCBIZWFkZXIgT3B0aW9uc1xyXG5cdGZ1bmN0aW9uIEhlYWRlck1lbnVGdW5jdGlvbigpe1xyXG5cclxuXHRcdC8vIHRlbGwgd2hlbiB0byBydW4gdGhlIHN0dWNrXHJcblx0XHR2YXIgJG9mZnNldCA9ICQoJyNtYXN0aGVhZCcpLmF0dHIoJ2RhdGEtb2Zmc2V0JykgPT0gJ3RydWUnID8gJCgnI21hc3RoZWFkJykuaGVpZ2h0KCkgKyAxMCA6IDMwXHJcblxyXG5cdFx0Ly8gU3RpY2t5IGhlYWRlclxyXG5cdFx0JCh3aW5kb3cpLnNjcm9sbChmdW5jdGlvbiAoKSB7XHJcblx0XHRcdGlmKCQoZG9jdW1lbnQpLnNjcm9sbFRvcCgpID4gJG9mZnNldCl7XHJcblx0XHRcdFx0JCgnYm9keScpLmFkZENsYXNzKCdoZWFkZXItc3R1Y2snKVxyXG5cdFx0XHR9ZWxzZXtcclxuXHRcdFx0XHQkKCdib2R5JykucmVtb3ZlQ2xhc3MoJ2hlYWRlci1zdHVjaycpXHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cclxuXHRcdC8vIEJ1cmdlciBtZW51IHRvZ2dsZVxyXG5cdFx0JChkb2N1bWVudCkub24oJ2NsaWNrJywgJ2J1dHRvbi5qcy1oYW1idXJnZXInLCBmdW5jdGlvbihlKXtcclxuXHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpXHJcblx0XHRcdCQodGhpcykudG9nZ2xlQ2xhc3MoJ2lzLWFjdGl2ZScpXHJcblx0XHRcdCQoJ2JvZHknKS50b2dnbGVDbGFzcygnbW9iaWxlLW1lbnUtb3BlbicpXHJcblx0XHRcdFxyXG5cdFx0XHQvLyBob29rIHNvIG90aGVyIGZ1bmN0aW9uIGNhbiBydW5uaWdcclxuXHRcdFx0JCh3aW5kb3cpLnRyaWdnZXIoJ2hhbWJ1cmdlci1tZW51LWNsaWNrJylcclxuXHRcdH0pXHJcblxyXG5cdFx0Ly8gRGlzYWJsZSBwYXJlbnQgbWVudSB0byBiZSBjbGlja2FibGVcclxuXHRcdCQoJy5tYWluLW5hdmlnYXRpb24gbGkubWVudS1pdGVtLWhhcy1jaGlsZHJlbiA+IGEnKS5vbignY2xpY2snLGZ1bmN0aW9uKGUpe1xyXG5cdFx0XHRlLnByZXZlbnREZWZhdWx0KClcclxuXHRcdH0pXHJcblxyXG5cclxuXHRcdC8vIE9wZW4gY2xvc2Ugc2VhcmNoXHJcblx0XHQkKCcubWVudS1zZWFyY2gnKS5jbGljayhmdW5jdGlvbihlKXtcclxuXHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpXHJcblx0XHRcdCQoJ2JvZHknKS50b2dnbGVDbGFzcygnc2VhcmNoLW9wZW4nKVxyXG5cdFx0XHQkKCcuaGVhZGVyLXNlYXJjaC1mb3JtIC5zZWFyY2gtZmllbGQnKS5mb2N1cygpXHJcblxyXG5cdFx0XHQvLyBmdWxsc2NyZWVuIHN0eWxlXHJcblx0XHRcdGlmKCQoJy5oZWFkZXItc2VhcmNoLWZvcm0nKS5oYXNDbGFzcygnZnVsbHNjcmVlbicpKXtcclxuXHRcdFx0XHQkKCcuaGVhZGVyLXNlYXJjaC1mb3JtJykuZmFkZVRvZ2dsZSgyMDAsZnVuY3Rpb24oKXtcclxuXHRcdFx0XHRcdC8vIGZvY3VzIHRoZSBzZWFyY2hcclxuXHRcdFx0XHRcdCQoJy5oZWFkZXItc2VhcmNoLWZvcm0gLnNlYXJjaC1maWVsZCcpLmZvY3VzKClcclxuXHRcdFx0XHR9KVxyXG5cdFx0XHR9XHJcblx0XHR9KVxyXG5cclxuXHRcdC8vIGNsb3NlIGZ1bGxzY3JlZW5cclxuXHRcdCQoJy5mdWxsc2NyZWVuLXNoYWRlcicpLmNsaWNrKGZ1bmN0aW9uKGUpe1xyXG5cdFx0XHRlLnByZXZlbnREZWZhdWx0KClcclxuXHRcdFx0JCgnYm9keScpLnJlbW92ZUNsYXNzKCdzZWFyY2gtb3BlbicpXHJcblx0XHRcdCQoJy5oZWFkZXItc2VhcmNoLWZvcm0nKS5mYWRlT3V0KDIwMClcclxuXHRcdH0pXHJcblxyXG5cdH1cclxuXHJcblxyXG5cdC8vIEdyYXZpdHkgTGFiZWwgRnVuY3Rpb25hbGl0eVxyXG5cdGZ1bmN0aW9uIEdyYXZpdHlGb3JtRnVuY3Rpb25hbGl0eVNldHVwKCkge1xyXG5cclxuXHRcdC8vIGxhYmVsIGhvdmVyIHN0eWxlXHJcblx0XHQkKCdsaS5nZmllbGQgaW5wdXQsIGxpLmdmaWVsZCB0ZXh0YXJlYScpLm9uKCdjaGFuZ2Uga2V5dXAgZm9jdXNvdXQnLCBmdW5jdGlvbigpe1xyXG5cdFx0XHRcclxuXHRcdFx0dmFyIGlzRW1wdHkgPSAkKHRoaXMpLnZhbCgpXHJcblx0XHRcdGlmKGlzRW1wdHkgPT0gJycpe1xyXG5cdFx0XHRcdCQodGhpcykucGFyZW50cygnbGkuZ2ZpZWxkJykucmVtb3ZlQ2xhc3MoJ2ZpbGxlZCcpXHJcblx0XHRcdH1lbHNle1xyXG5cdFx0XHRcdCQodGhpcykucGFyZW50cygnbGkuZ2ZpZWxkJykuYWRkQ2xhc3MoJ2ZpbGxlZCcpXHJcblx0XHRcdH1cclxuXHJcblx0XHR9KVxyXG5cclxuXHRcdC8vIHRyaWdnZXIgYnkgZGVmYXVsdFxyXG5cdFx0JCgnbGkuZ2ZpZWxkIGlucHV0LCBsaS5nZmllbGQgdGV4dGFyZWEnKS50cmlnZ2VyKCdmb2N1c291dCcpXHJcblxyXG5cdFx0Ly8gbGFiZWwgaG92ZXIgc3R5bGVcclxuXHRcdCQoJ2xpLmdmaWVsZCBpbnB1dCwgbGkuZ2ZpZWxkIHRleHRhcmVhJykub24oJ2ZvY3VzaW4nLCBmdW5jdGlvbigpe1xyXG5cdFx0XHRcclxuXHRcdFx0JCh0aGlzKS5wYXJlbnRzKCdsaS5nZmllbGQnKS5hZGRDbGFzcygnZmlsbGVkJylcclxuXHRcdFx0XHJcblx0XHR9KVxyXG5cclxuXHRcdC8vIFNjcm9sbCB0aGUgdXNlciB0byB0aGUgdG9wIG9mIHRoZSBmb3JtIGlmIHRoZXJlIGlzIGFuIGVycm9yIG9uIHRoaXMgcGFnZVxyXG5cdFx0aWYgKCQoXCIuZ2Zvcm1fd3JhcHBlci5nZm9ybV92YWxpZGF0aW9uX2Vycm9yXCIpLmxlbmd0aCA+IDApIHtcclxuXHJcblx0XHRcdCQoJ2h0bWwsIGJvZHknKS5hbmltYXRlKHtcclxuXHRcdFx0XHRzY3JvbGxUb3A6ICQoXCIuZ2Zvcm1fd3JhcHBlclwiKS5vZmZzZXQoKS50b3AgLSAxNDBcclxuXHRcdFx0fSwgMSk7XHJcblx0XHR9XHJcblxyXG5cdFx0Ly8gU2V0dXAgdGhpcyBsYWJlbCBmb3IgbW92ZW1lbnRcclxuXHRcdC8vIEZvcm0gY2hvaWNlIHBsYWNlaG9sZGVyXHJcblx0XHQkKFwiLmZvcm0tbGFiZWwtbW92ZVwiKS5maW5kKFwiLmdpbnB1dF9jb250YWluZXIuZ2lucHV0X2NvbnRhaW5lcl90ZXh0LCAuZ2lucHV0X2NvbnRhaW5lci5naW5wdXRfY29udGFpbmVyX3RleHRhcmVhLCAuZ2lucHV0X2NvbnRhaW5lci5naW5wdXRfY29udGFpbmVyX2VtYWlsLCAuZG4tZm9ybS1pbnB1dC1jb250YWluZXJcIikuZWFjaChmdW5jdGlvbiAoKSB7XHJcblxyXG5cdFx0XHQvLyBTZXQgdGhpcyBsYWJlbCB1cCBmb3IgZ3JlYXRlbmVzc1xyXG5cdFx0XHQkKHRoaXMpLnNpYmxpbmdzKFwibGFiZWxcIikuYWRkQ2xhc3MoXCJ0ZXh0LWZpZWxkXCIpO1xyXG5cclxuXHRcdFx0Ly8gT24gZm9jdXNpbmcgb2YgdGhpcyBlbGVtZW50c1xyXG5cdFx0XHQkKHRoaXMpLmZpbmQoXCJpbnB1dCwgdGV4dGFyZWFcIikuZm9jdXNpbihmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0JCh0aGlzKS5wYXJlbnQoKS5zaWJsaW5ncyhcImxhYmVsXCIpLmFkZENsYXNzKFwiZm9jdXNcIik7XHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdFx0Ly8gSWYgYW55IG9mIHRoZXNlIGZpZWxkcyBhbHJlYWR5IGhhdmUgY29udGVudFxyXG5cdFx0XHQkKHRoaXMpLmZpbmQoXCJpbnB1dCwgdGV4dGFyZWFcIikuZWFjaChmdW5jdGlvbiAoKSB7XHJcblxyXG5cdFx0XHRcdGlmICgkKHRoaXMpLnZhbCgpICE9ICcnKSB7XHJcblx0XHRcdFx0XHQkKHRoaXMpLnBhcmVudCgpLnNpYmxpbmdzKFwibGFiZWxcIikuYWRkQ2xhc3MoXCJmb2N1c1wiKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdFx0Ly8gT24gc3RvcCBmb2N1c2luZyBvZiB0aGlzIGVsZW1lbnRzXHJcblx0XHRcdCQodGhpcykuZmluZChcImlucHV0LCB0ZXh0YXJlYVwiKS5mb2N1c291dChmdW5jdGlvbiAoKSB7XHJcblxyXG5cdFx0XHRcdC8vIElmIHRoaXMgZmllbGQgaGFzIG5vIGNvbnRlbnRzXHJcblx0XHRcdFx0aWYgKCQodGhpcykudmFsKCkgPT0gJycpIHtcclxuXHRcdFx0XHRcdCQodGhpcykucGFyZW50KCkuc2libGluZ3MoXCJsYWJlbFwiKS5yZW1vdmVDbGFzcyhcImZvY3VzXCIpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0fSk7XHJcblxyXG5cdFx0Ly8gU2V0dXAgbGl2ZSBmb3JtIHZhbGlkYXRpb25cclxuXHRcdC8vIEFkZCByZXF1aXJlZCBmaWVsZHMgaW4gcGxhY2Ugb2YgZ3JhdiBmb3JtcyBjbGFzc2VzXHJcblx0XHQkKFwiLmdmaWVsZF9jb250YWluc19yZXF1aXJlZFwiKS5maW5kKCdpbnB1dFt0eXBlPVwidGV4dFwiXSwgaW5wdXRbdHlwZT1cImVtYWlsXCJdLCBpbnB1dFt0eXBlPVwidXJsXCJdLCBpbnB1dFt0eXBlPVwicGFzc3dvcmRcIl0sIGlucHV0W3R5cGU9XCJzZWFyY2hcIl0sIGlucHV0W3R5cGU9XCJudW1iZXJcIl0sIGlucHV0W3R5cGU9XCJ0ZWxcIl0sICBpbnB1dFt0eXBlPVwiZGF0ZVwiXSwgaW5wdXRbdHlwZT1cIm1vbnRoXCJdLCBpbnB1dFt0eXBlPVwid2Vla1wiXSwgaW5wdXRbdHlwZT1cInRpbWVcIl0sIGlucHV0W3R5cGU9XCJkYXRldGltZVwiXSwgaW5wdXRbdHlwZT1cImRhdGV0aW1lLWxvY2FsXCJdLCBpbnB1dFt0eXBlPVwiY29sb3JcIl0sIHRleHRhcmVhJykuYWRkQ2xhc3MoXCJyZXF1aXJlZFwiKTtcclxuXHJcblx0XHQvLyBPbiB1bmNsaWNraW5nIGEgZm9ybSBmaWVsZFxyXG5cdFx0JChcImlucHV0LnJlcXVpcmVkOm5vdCguaWdub3JlKSwgdGV4dGFyZWEucmVxdWlyZWQ6bm90KC5pZ25vcmUpLCAuZ2ZpZWxkX2NvbnRhaW5zX3JlcXVpcmVkIHNlbGVjdFwiKS5mb2N1c291dChmdW5jdGlvbiAoKSB7XHJcblxyXG5cdFx0XHRpZiAoJCh0aGlzKS52YWwoKSA9PSAnJykge1xyXG5cdFx0XHRcdCQodGhpcykuYWRkQ2xhc3MoXCJlcnJvclwiKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHQkKHRoaXMpLnJlbW92ZUNsYXNzKFwiZXJyb3JcIik7XHJcblx0XHRcdFx0JCh0aGlzKS5wYXJlbnRzKFwiLmdmaWVsZC5nZmllbGRfZXJyb3JcIikucmVtb3ZlQ2xhc3MoXCJnZmllbGRfZXJyb3JcIik7XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblx0XHJcblxyXG5cclxuXHJcblx0Ly8gc2VydmljZSBhY2NvcmRpb25cclxuXHQkKGRvY3VtZW50KS5vbignY2xpY2snLCcuZG4tc2VydmljZXMgLnNlcnZpY2UtaXRlbSAuaGVhZGluZzpub3QoLmRpc2FibGUtYWNjb3JkaW9uKScsZnVuY3Rpb24oZSl7XHJcblx0XHJcblx0XHRlLnByZXZlbnREZWZhdWx0KClcclxuXHRcdCQodGhpcykucGFyZW50KCkudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpXHJcblx0XHQkKHRoaXMpLnNpYmxpbmdzKCcuY29udGVudCcpLnNsaWRlVG9nZ2xlKClcclxuXHJcblx0fSlcclxuXHJcblx0Ly8gc2xpZGUgbWVudVxyXG5cdGZ1bmN0aW9uIFNsaWRlRnVsbE1lbnVGdW5jdGlvbmFsaXR5KCl7XHJcblxyXG5cdFx0aWYoICQoJy5oZWFkZXItc2xpZGUtZnVsbCcpLmxlbmd0aCA+IDAgKXtcclxuXHJcblx0XHRcdHZhciBjb3VudCA9IDA7XHJcblx0XHRcdCQoJy5oZWFkZXItc2xpZGUtZnVsbCAudGhlLW1lbnUgLm1lbnUgPiBsaScpLmVhY2goZnVuY3Rpb24oaSxlKXtcclxuXHRcdFx0XHQkKHRoaXMpLmFkZENsYXNzKCdkZWxheS0nK2kpXHJcblx0XHRcdFx0Y291bnQrK1xyXG5cdFx0XHR9KVxyXG5cclxuXHRcdFx0Ly8gdGhlIGxpbmtcclxuXHRcdFx0JCgnLmhlYWRlci1zbGlkZS1mdWxsIC5saW5rcycpLmFkZENsYXNzKCdkZWxheS0nKyAoY291bnQrMSkgKVxyXG5cclxuXHRcdH1cclxuXHJcblx0fVxyXG5cclxuXHJcblxyXG5cclxuXHJcblx0Ly8gQ3JlYXRlcyBmdW5jdGlvbmFsaXR5IGZvciBidXJnZXIgbWVudXNcclxuXHRmdW5jdGlvbiBCdXJnZXJNZW51RnVuY3Rpb25hbGl0eSgpIHtcclxuXHJcblx0XHRpZiAoJCgnI2J1cmdlciwjYnVyZ2VyLWJsb2NreScpLmxlbmd0aCA+IDApIHtcclxuXHJcblx0XHRcdHZhciBNZW51T3BlblJlYWR5ID0gdHJ1ZTtcclxuXHJcblx0XHRcdCQoJyNidXJnZXIsI2J1cmdlci1ibG9ja3knKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcblxyXG5cdFx0XHRcdGlmIChNZW51T3BlblJlYWR5KSB7XHJcblxyXG5cdFx0XHRcdFx0TWVudU9wZW5SZWFkeSA9IGZhbHNlO1xyXG5cdFx0XHRcdFx0c2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0XHRcdE1lbnVPcGVuUmVhZHkgPSB0cnVlO1xyXG5cdFx0XHRcdFx0fSwgMjUwKTtcclxuXHJcblx0XHRcdFx0XHRpZiAoJCgnI2J1cmdlciwjYnVyZ2VyLWJsb2NreScpLmhhc0NsYXNzKFwib3BlblwiKSkge1xyXG5cdFx0XHRcdFx0XHRDbG9zZU1lbnUoKTtcclxuXHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHRPcGVuTWVudSgpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0fVxyXG5cclxuXHR9XHJcblxyXG5cdGZ1bmN0aW9uIE9wZW5NZW51KCkge1xyXG5cclxuXHRcdC8vIENsb3NlIHRoZSBzZWFyY2hcclxuXHRcdENsb3NlU2VhcmNoRHJvcCgpO1xyXG5cclxuXHRcdC8vIENsb3NlIHRoZSBzaWRlLWNhcnRcclxuXHRcdENsb3NlU2lkZUNhcnQoKTtcclxuXHJcblx0XHQkKFwiI2hlYWRlci1tZW51XCIpLmFkZENsYXNzKFwiYXBwZWFyXCIpO1xyXG5cdFx0JChcIiNtYXN0aGVhZFwiKS5hZGRDbGFzcyhcImZpbGxlZC1tZW51XCIpO1xyXG5cdFx0JChcImJvZHlcIikuYWRkQ2xhc3MoXCJtb2JpbGUtbWVudS1vcGVuXCIpO1xyXG5cdFx0JChcIi5oYW1idXJnZXItYmx1c2hcIikuYWRkQ2xhc3MoJ2lzLWFjdGl2ZScpO1xyXG5cdFx0JChcIiNidXJnZXIsI2J1cmdlci1ibG9ja3lcIikuYWRkQ2xhc3MoJ29wZW4nKTtcclxuXHRcdFNob3dNZW51U2hhZGVyKCk7XHJcblx0XHRMb2NrUGFnZVNjcm9sbCgpO1xyXG5cdH1cclxuXHJcblx0ZnVuY3Rpb24gQ2xvc2VNZW51KCkge1xyXG5cdFx0JChcIiNoZWFkZXItbWVudVwiKS5yZW1vdmVDbGFzcyhcImFwcGVhclwiKTtcclxuXHRcdCQoXCJib2R5XCIpLnJlbW92ZUNsYXNzKFwibW9iaWxlLW1lbnUtb3BlblwiKTtcclxuXHRcdCQoXCIjbWFzdGhlYWRcIikucmVtb3ZlQ2xhc3MoXCJmaWxsZWQtbWVudVwiKTtcclxuXHRcdCQoXCIuaGFtYnVyZ2VyLWJsdXNoXCIpLnJlbW92ZUNsYXNzKCdpcy1hY3RpdmUnKTtcclxuXHRcdCQoXCIjYnVyZ2VyLCNidXJnZXItYmxvY2t5XCIpLnJlbW92ZUNsYXNzKCdvcGVuJyk7XHJcblx0XHRIaWRlTWVudVNoYWRlcigpO1xyXG5cdFx0VW5sb2NrUGFnZVNjcm9sbCgpO1xyXG5cdH1cclxuXHJcblx0Ly8gQ3JlYXRlcyBmdW5jdGlvbmFsaXR5IGZvciBhY2NvcmRpYW4gbWVudXNcclxuXHRmdW5jdGlvbiBBY2NvcmRpYW5NZW51RnVuY3Rpb25hbGl0eSgpIHtcclxuXHJcblx0XHQvLyBGb3IgZWFjaCBhY2NvcmRpYW5cclxuXHRcdCQoXCIuYWNjb3JkaWFuXCIpLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cclxuXHRcdFx0Ly8gSGlkZSB0aGUgaWNvbnMgZm9yIHRob3NlIG1lbnUgZWxlbWVudHMgdGhhdCBkbyBub3QgaGF2ZSBjaGlsZHJlblxyXG5cdFx0XHQkKHRoaXMpLmZpbmQoXCJsaVwiKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHJcblx0XHRcdFx0aWYgKCEkKHRoaXMpLmZpbmQoXCJ1bC5jaGlsZHJlbiwgdWwuc3ViLW1lbnVcIikubGVuZ3RoID4gMCkge1xyXG5cdFx0XHRcdFx0JCh0aGlzKS5maW5kKFwiaVwiKS5oaWRlKCk7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0XHQvLyBTZXR1cCB0aGUgbWVudVxyXG5cdFx0XHQkKHRoaXMpLmZpbmQoXCIuc3ViLW1lbnUsIC5jaGlsZHJlblwiKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHQkKHRoaXMpLmhpZGUoKTtcclxuXHRcdFx0XHQkKHRoaXMpLnBhcmVudCgpLmFkZENsYXNzKFwiYWNjb3JkLXBhcmVudFwiKTtcclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0XHQvLyBDTGljayBmdW5jdGlvbmFsaXR5XHJcblx0XHRcdCQodGhpcykuZmluZChcImxpIGk6bGFzdC1vZi10eXBlKClcIikuaGlkZSgpO1xyXG5cdFx0XHQkKHRoaXMpLmZpbmQoXCJsaSBpXCIpLm1vdXNlZG93bihmdW5jdGlvbiAoKSB7XHJcblxyXG5cdFx0XHRcdCQodGhpcykudG9nZ2xlKCk7XHJcblx0XHRcdFx0JCh0aGlzKS5zaWJsaW5ncyhcImlcIikudG9nZ2xlKCk7XHJcblxyXG5cdFx0XHRcdCQodGhpcykucGFyZW50KCkuZmluZChcIi5zdWItbWVudSwgLmNoaWxkcmVuXCIpLnNsaWRlVG9nZ2xlKDIwMCk7XHJcblxyXG5cdFx0XHR9KTtcclxuXHJcblx0XHR9KTtcclxuXHJcblx0XHQkKFwiLmFjY29yZGlhbi1tZW51LWRpc2FibGUtcGFyZW50XCIpLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cclxuXHRcdFx0Ly8gSGlkZSB0aGUgaWNvbnMgZm9yIHRob3NlIG1lbnUgZWxlbWVudHMgdGhhdCBkbyBub3QgaGF2ZSBjaGlsZHJlblxyXG5cdFx0XHQkKHRoaXMpLmZpbmQoXCJsaVwiKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHJcblx0XHRcdFx0aWYgKCEkKHRoaXMpLmZpbmQoXCJ1bC5jaGlsZHJlbiwgdWwuc3ViLW1lbnVcIikubGVuZ3RoID4gMCkge1xyXG5cdFx0XHRcdFx0JCh0aGlzKS5maW5kKFwiaVwiKS5oaWRlKCk7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0XHQvLyBTZXR1cCB0aGUgbWVudVxyXG5cdFx0XHQkKHRoaXMpLmZpbmQoXCIuc3ViLW1lbnUsIC5jaGlsZHJlblwiKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHQkKHRoaXMpLmhpZGUoKTtcclxuXHRcdFx0XHQkKHRoaXMpLnBhcmVudCgpLmFkZENsYXNzKFwiYWNjb3JkLXBhcmVudFwiKTtcclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0XHQvLyBDTGljayBmdW5jdGlvbmFsaXR5XHJcblx0XHRcdCQodGhpcykuZmluZChcImxpIGk6bGFzdC1vZi10eXBlKClcIikuaGlkZSgpO1xyXG5cdFx0XHQkKHRoaXMpLmZpbmQoXCJsaSBpXCIpLm1vdXNlZG93bihmdW5jdGlvbiAoKSB7XHJcblxyXG5cdFx0XHRcdCQodGhpcykudG9nZ2xlKCk7XHJcblx0XHRcdFx0JCh0aGlzKS5zaWJsaW5ncyhcImlcIikudG9nZ2xlKCk7XHJcblxyXG5cdFx0XHRcdCQodGhpcykucGFyZW50KCkuZmluZChcIi5zdWItbWVudSwgLmNoaWxkcmVuXCIpLnNsaWRlVG9nZ2xlKDIwMCk7XHJcblxyXG5cdFx0XHR9KTtcclxuXHJcblx0XHRcdCQodGhpcykuZmluZChcInVsLm1lbnUtaXRlbS1oYXMtY2hpbGRyZW4gPiBsaSA+IGFcIikub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcclxuXHJcblx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuXHRcdFx0XHQkKHRoaXMpLnNpYmxpbmdzKFwiaVwiKS50b2dnbGUoKTtcclxuXHJcblx0XHRcdFx0JCh0aGlzKS5wYXJlbnQoKS5maW5kKFwiLnN1Yi1tZW51LCAuY2hpbGRyZW5cIikuc2xpZGVUb2dnbGUoMjAwKTtcclxuXHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdH0pO1xyXG5cclxuXHJcblx0XHQvLyBGb3IgZWFjaCBhY2NvcmRpYW5cclxuXHRcdCQoXCIuYWNjb3JkaWFuLWNhdGVnb3J5IC50b2dnbGUtb3BlblwiKS5jbGljayhmdW5jdGlvbiAoZSkge1xyXG5cdFx0XHRlLnByZXZlbnREZWZhdWx0KClcclxuXHJcblx0XHRcdHZhciAkdGhpcyA9ICQodGhpcylcclxuXHRcdFx0aWYgKCgkdGhpcykuaGFzQ2xhc3MoJ2ljb24tcGx1cycpKSB7XHJcblx0XHRcdFx0JHRoaXMucmVtb3ZlQ2xhc3MoJ2ljb24tcGx1cycpXHJcblx0XHRcdFx0JHRoaXMuYWRkQ2xhc3MoJ2ljb24tbWludXMnKVxyXG5cdFx0XHRcdCR0aGlzLnNpYmxpbmdzKCcuY2hpbGRyZW4nKS5zbGlkZURvd24oKVxyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdCR0aGlzLnJlbW92ZUNsYXNzKCdpY29uLW1pbnVzJylcclxuXHRcdFx0XHQkdGhpcy5hZGRDbGFzcygnaWNvbi1wbHVzJylcclxuXHRcdFx0XHQkdGhpcy5zaWJsaW5ncygnLmNoaWxkcmVuJykuc2xpZGVVcCgpXHJcblx0XHRcdH1cclxuXHJcblx0XHRcdC8vIHJlc2V0IHRoZSBvdGhlcnNcclxuXHRcdFx0JHRoaXMucGFyZW50KCkuc2libGluZ3MoJ2xpJykuZmluZCgnLmNoaWxkcmVuJykuc2xpZGVVcCgpXHJcblx0XHRcdCR0aGlzLnBhcmVudCgpLnNpYmxpbmdzKCdsaScpLmZpbmQoJy50b2dnbGUtb3BlbicpLnJlbW92ZUNsYXNzKCdpY29uLW1pbnVzJylcclxuXHRcdFx0JHRoaXMucGFyZW50KCkuc2libGluZ3MoJ2xpJykuZmluZCgnLnRvZ2dsZS1vcGVuJykuYWRkQ2xhc3MoJ2ljb24tcGx1cycpXHJcblxyXG5cclxuXHRcdH0pXHJcblxyXG5cdFx0Ly8gT3BlbiBieSBkZWZhdWx0XHJcblx0XHRpZiAoJChkb2N1bWVudCkuZmluZCgnLmFjY29yZGlhbi1jYXRlZ29yeSAuY3VycmVudC1jYXQnKS5sZW5ndGggPiAwKSB7XHJcblx0XHRcdHZhciAkY3VycmVudF9jYXQgPSAkKGRvY3VtZW50KS5maW5kKCcuYWNjb3JkaWFuLWNhdGVnb3J5IC5jdXJyZW50LWNhdCcpO1xyXG5cdFx0XHQkY3VycmVudF9jYXQucGFyZW50KCkuc2libGluZ3MoJy50b2dnbGUtb3BlbicpLnRyaWdnZXIoJ2NsaWNrJylcclxuXHRcdFx0aWYgKCRjdXJyZW50X2NhdC5wYXJlbnQoKS5oYXNDbGFzcygnYWNjb3JkaWFuLWNhdGVnb3J5JykpIHtcclxuXHRcdFx0XHQkY3VycmVudF9jYXQuZmluZCgnLnRvZ2dsZS1vcGVuJykudHJpZ2dlcignY2xpY2snKVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQvLyBDcmVhdGVzIGZ1bmN0aW9uYWxpdHkgZm9yIGRyb3Bkb3duIG1lbnVzXHJcblx0ZnVuY3Rpb24gRHJvcERvd25NZW51RnVuY3Rpb25hbHR5KCkge1xyXG5cclxuXHRcdGlmICgkKFwiLm1lbnUtY29udGFpbmVyLmRyb3AtbWVudVwiKS5sZW5ndGggPiAwKSB7XHJcblxyXG5cdFx0XHQvLyBIaWRlIHRob3NlICYgc3ViLW1lbnVzIGljb25zIHByb3Blcmx5XHJcblx0XHRcdCQoXCIubWVudS1jb250YWluZXIuZHJvcC1tZW51IC5zdWItbWVudVwiKS5oaWRlKCk7XHJcblx0XHRcdCQoXCIubWVudS1jb250YWluZXIuZHJvcC1tZW51XCIpLnJlbW92ZUNsYXNzKFwiaGlkZGVuLXN1YlwiKTtcclxuXHJcblx0XHRcdC8vIFNldHVwIGhvdmVyIGVmZmVjdHNcclxuXHRcdFx0JChcIi5tZW51LWNvbnRhaW5lci5kcm9wLW1lbnUgbGkubWVudS1pdGVtLWhhcy1jaGlsZHJlblwiKS5tb3VzZWVudGVyKGZ1bmN0aW9uICgpIHtcclxuXHJcblx0XHRcdFx0Q2xlYXJDdXJyZW50U3ViTWVudSgpO1xyXG5cclxuXHRcdFx0XHQvLyBTZXR1cCB0aGlzIGNsYXNzXHJcblx0XHRcdFx0JCh0aGlzKS5hZGRDbGFzcyhcImRyb3BwZWRcIik7XHJcblx0XHRcdFx0JCh0aGlzKS5maW5kKFwiLnN1Yi1tZW51XCIpLnNob3coKTtcclxuXHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdFx0JChcIi5tZW51LWNvbnRhaW5lci5kcm9wLW1lbnUgbGkubWVudS1pdGVtLWhhcy1jaGlsZHJlblwiKS5tb3VzZWxlYXZlKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRDbGVhckN1cnJlbnRTdWJNZW51KCk7XHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdFx0Ly8gTWFrZSB0b3AgbGV2ZWwgbWVudSBpdGVtcyBub3QgY2xpY2thYmxlIGluIHRoZSBoZWFkZXIgKG9uIGRlc2t0b3ApXHJcblx0XHRcdCQoXCIubWVudS1jb250YWluZXIuZHJvcC1tZW51IGxpLm1lbnUtaXRlbS1oYXMtY2hpbGRyZW4gPiBhXCIpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XHJcblx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHR9KTtcclxuXHJcblx0XHRcdC8vIEhpZGUgYWxsIG90aGVyIHN1Yi1tZW51J3NcclxuXHRcdFx0ZnVuY3Rpb24gQ2xlYXJDdXJyZW50U3ViTWVudSgpIHtcclxuXHRcdFx0XHQkKFwiLm1lbnUtY29udGFpbmVyLmRyb3AtbWVudSBsaS5tZW51LWl0ZW0taGFzLWNoaWxkcmVuLmRyb3BwZWQgLnN1Yi1tZW51XCIpLmhpZGUoKTtcclxuXHRcdFx0XHQkKFwiLm1lbnUtY29udGFpbmVyLmRyb3AtbWVudSBsaS5tZW51LWl0ZW0taGFzLWNoaWxkcmVuLmRyb3BwZWRcIikucmVtb3ZlQ2xhc3MoXCJkcm9wcGVkXCIpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQvLyBTZXRzIHVwIHZhcmlvdXMgc2xpY2sgc2xpZGVyc1xyXG5cdGZ1bmN0aW9uIFNsaWNrU2xpZGVyRnVuY3Rpb25hbHR5KCkge1xyXG5cclxuXHRcdC8vIFJlZ3VsYXIgc2xpZGVyIGNvbnRhaW5lclxyXG5cdFx0aWYgKCQoXCIuc2xpY2stc2xpZGVyLWNvbnRhaW5lclwiKS5sZW5ndGggPiAwKSB7XHJcblx0XHRcdGlmICgkKFwiLnNsaWNrLXNsaWRlci1jb250YWluZXJcIikuY2hpbGRyZW4oKS5sZW5ndGggPiAxKSB7XHJcblx0XHRcdFx0JChcIi5zbGljay1zbGlkZXItY29udGFpbmVyXCIpLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0dmFyICR0aGlzID0gJCh0aGlzKVxyXG5cdFx0XHRcdFx0aWYgKCR0aGlzLmhhc0NsYXNzKCd0ZXN0aW1vbmlhbC1zbGlkZXInKSkge1xyXG5cclxuXHRcdFx0XHRcdFx0JHRoaXMuc2xpY2soe1xyXG5cdFx0XHRcdFx0XHRcdGFycm93czogZmFsc2UsXHJcblx0XHRcdFx0XHRcdFx0ZG90czogdHJ1ZSxcclxuXHRcdFx0XHRcdFx0XHRpbmZpbml0ZTogdHJ1ZSxcclxuXHRcdFx0XHRcdFx0XHRhdXRvcGxheTogZmFsc2UsXHJcblx0XHRcdFx0XHRcdFx0YWRhcHRpdmVIZWlnaHQ6IHRydWVcclxuXHRcdFx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdFx0XHQvKiAkdGhpcy5vbignc2V0UG9zaXRpb24nLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0XHRcdFx0JHRoaXMuZmluZCgnLnNsaWNrLXNsaWRlJykubWF0Y2hIZWlnaHQoKTtcclxuXHRcdFx0XHRcdFx0fSkgKi9cclxuXHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHQkdGhpcy5zbGljayh7XHJcblx0XHRcdFx0XHRcdFx0YXJyb3dzOiBmYWxzZSxcclxuXHRcdFx0XHRcdFx0XHRkb3RzOiB0cnVlLFxyXG5cdFx0XHRcdFx0XHRcdGluZmluaXRlOiB0cnVlLFxyXG5cdFx0XHRcdFx0XHRcdGF1dG9wbGF5OiB0cnVlLFxyXG5cdFx0XHRcdFx0XHRcdGFkYXB0aXZlSGVpZ2h0OiB0cnVlXHJcblx0XHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHR9KVxyXG5cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHRcdC8vIHRleHQgZ2FsbGVyeVxyXG5cdFx0aWYoJCgnLnRleHQtZ2FsbGVyeS1zbGlkZXInKS5sZW5ndGggPiAwKXtcclxuXHRcdFx0JCgnLnRleHQtZ2FsbGVyeS1zbGlkZXInKS5lYWNoKGZ1bmN0aW9uKCl7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0dmFyICRyZXNwb25zaXZlID0gW107XHJcblx0XHRcdFx0dmFyICR0aGlzID0gJCh0aGlzKVxyXG5cclxuXHRcdFx0XHQvLyByZXNwb25zaXZlIG9wdGlvblxyXG5cdFx0XHRcdGlmKCR0aGlzLmhhc0NsYXNzKCdyZXNwb25zaXZlLWNlbnRlcmVkJykpe1xyXG5cdFx0XHRcdFx0JHJlc3BvbnNpdmUgPSBbXHJcblx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRicmVha3BvaW50OiA5OTEsXHJcblx0XHRcdFx0XHRcdFx0c2V0dGluZ3M6IHtcclxuXHRcdFx0XHRcdFx0XHRcdGRvdHM6IGZhbHNlLFxyXG5cdFx0XHRcdFx0XHRcdFx0YXJyb3dzOiBmYWxzZSxcclxuXHRcdFx0XHRcdFx0XHRcdGluZmluaXRlOiBmYWxzZSxcclxuXHRcdFx0XHRcdFx0XHRcdGF1dG9wbGF5OiB0cnVlLFxyXG5cdFx0XHRcdFx0XHRcdFx0Y2VudGVyTW9kZTogdHJ1ZSxcclxuXHRcdFx0XHRcdFx0XHRcdHNsaWRlc1RvU2hvdzogMSxcclxuXHRcdFx0XHRcdFx0XHRcdHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XVxyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0JHRoaXMuc2xpY2soe1xyXG5cdFx0XHRcdFx0YXJyb3dzOiBmYWxzZSxcclxuXHRcdFx0XHRcdGRvdHM6IHRydWUsXHJcblx0XHRcdFx0XHRpbmZpbml0ZTogdHJ1ZSxcclxuXHRcdFx0XHRcdGF1dG9wbGF5OiB0cnVlLFxyXG5cdFx0XHRcdFx0YWRhcHRpdmVIZWlnaHQ6IGZhbHNlLFxyXG5cdFx0XHRcdFx0cmVzcG9uc2l2ZTogJHJlc3BvbnNpdmVcclxuXHRcdFx0XHR9KTtcclxuXHJcblx0XHRcdFx0LyogJHRoaXMuc2xpY2soe1xyXG5cdFx0XHRcdFx0ZG90czogZmFsc2UsXHJcblx0XHRcdFx0XHRhcnJvd3M6IGZhbHNlLFxyXG5cdFx0XHRcdFx0aW5maW5pdGU6IGZhbHNlLFxyXG5cdFx0XHRcdFx0YXV0b3BsYXk6IHRydWUsXHJcblx0XHRcdFx0XHRjZW50ZXJNb2RlOiB0cnVlLFxyXG5cdFx0XHRcdFx0cmVzcG9uc2l2ZTogW1xyXG5cdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0YnJlYWtwb2ludDogOTk5OSxcclxuXHRcdFx0XHRcdFx0XHRzZXR0aW5nczogXCJ1bnNsaWNrXCJcclxuXHRcdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdGJyZWFrcG9pbnQ6IDk5MSxcclxuXHRcdFx0XHRcdFx0XHRzZXR0aW5nczoge1xyXG5cdFx0XHRcdFx0XHRcdFx0c2xpZGVzVG9TaG93OiAxLFxyXG5cdFx0XHRcdFx0XHRcdFx0c2xpZGVzVG9TY3JvbGw6IDEsXHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRdXHJcblx0XHRcdFx0fSk7ICovXHJcblx0XHRcdH0pXHJcblx0XHR9XHJcblxyXG5cdFx0Ly8gTWVnYSBzbGlkZXIgY29udGFpbmVyXHJcblx0XHRpZiAoJChcIi5tZWdhLXNsaWNrLXNsaWRlci1jb250YWluZXJcIikubGVuZ3RoID4gMCkge1xyXG5cdFx0XHRpZiAoJChcIi5tZWdhLXNsaWNrLXNsaWRlci1jb250YWluZXJcIikuY2hpbGRyZW4oKS5sZW5ndGggPiAxKSB7XHJcblx0XHRcdFx0JCgnLm1lZ2Etc2xpY2stc2xpZGVyLWNvbnRhaW5lcicpLnNsaWNrKHtcclxuXHRcdFx0XHRcdGRvdHM6IHRydWUsXHJcblx0XHRcdFx0XHRpbmZpbml0ZTogdHJ1ZSxcclxuXHRcdFx0XHRcdGF1dG9wbGF5OiB0cnVlLFxyXG5cdFx0XHRcdFx0cHJldkFycm93OiBcIjxpIGNsYXNzPSdpY29uLWNoZXZyb24tbGVmdCBzbGljay1wcmV2Jz48L2k+XCIsXHJcblx0XHRcdFx0XHRuZXh0QXJyb3c6IFwiPGkgY2xhc3M9J2ljb24tY2hldnJvbi1yaWdodCBzbGljay1uZXh0Jz48L2k+XCIsXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHJcblx0XHRcclxuXHJcblx0XHQvLyBGZWF0dXJlIFxyXG5cdFx0aWYgKCQoXCIuZG4tZmVhdHVyZS1jYXJvdXNlbFwiKS5sZW5ndGggPiAwKSB7XHJcblxyXG5cdFx0XHQvLyBNYWluIHNsaWNrIHNsaWRlclxyXG5cdFx0XHQkKFwiLmRuLWZlYXR1cmUtY2Fyb3VzZWwgLm1haW4taW1hZ2VcIikuc2xpY2soe1xyXG5cdFx0XHRcdHNsaWRlc1RvU2hvdzogMSxcclxuXHRcdFx0XHRzbGlkZXNUb1Njcm9sbDogMSxcclxuXHRcdFx0XHRhcnJvd3M6IGZhbHNlLFxyXG5cdFx0XHRcdGZhZGU6IHRydWUsXHJcblx0XHRcdFx0YXNOYXZGb3I6ICcuZG4tZmVhdHVyZS1jYXJvdXNlbCAuc2VsZWN0b3InXHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdFx0Ly8gQ2Fyb3VzZWwgc2xpZGVyXHJcblx0XHRcdCQoXCIuZG4tZmVhdHVyZS1jYXJvdXNlbCAuc2VsZWN0b3JcIikuc2xpY2soe1xyXG5cdFx0XHRcdHNsaWRlc1RvU2hvdzogMyxcclxuXHRcdFx0XHRzbGlkZXNUb1Njcm9sbDogMSxcclxuXHRcdFx0XHRwcmV2QXJyb3c6IFwiPGkgY2xhc3M9J2ljb24tY2hldnJvbi1sZWZ0IHNsaWNrLXByZXYnPjwvaT5cIixcclxuXHRcdFx0XHRuZXh0QXJyb3c6IFwiPGkgY2xhc3M9J2ljb24tY2hldnJvbi1yaWdodCBzbGljay1uZXh0Jz48L2k+XCIsXHJcblx0XHRcdFx0YXNOYXZGb3I6ICcuZG4tZmVhdHVyZS1jYXJvdXNlbCAubWFpbi1pbWFnZScsXHJcblx0XHRcdFx0ZG90czogZmFsc2UsXHJcblx0XHRcdFx0Y2VudGVyTW9kZTogdHJ1ZSxcclxuXHRcdFx0XHRmb2N1c09uU2VsZWN0OiB0cnVlLFxyXG5cdFx0XHRcdHJlc3BvbnNpdmU6IFt7XHJcblx0XHRcdFx0XHRicmVha3BvaW50OiA5OTEsXHJcblx0XHRcdFx0XHRzZXR0aW5nczoge1xyXG5cdFx0XHRcdFx0XHRzbGlkZXNUb1Nob3c6IDIsXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSwgXVxyXG5cdFx0XHR9KTtcclxuXHJcblx0XHRcdC8vIE1haW4gbGlnaHRib3hlc1xyXG5cdFx0XHQkKCcuZG4tZmVhdHVyZS1jYXJvdXNlbCAubWFpbi1pbWFnZSAuaW1hZ2UtY29udGFpbmVyJykubWFnbmlmaWNQb3B1cCh7XHJcblx0XHRcdFx0dHlwZTogJ2ltYWdlJyxcclxuXHRcdFx0XHRnYWxsZXJ5OiB7XHJcblx0XHRcdFx0XHRlbmFibGVkOiB0cnVlXHJcblx0XHRcdFx0fSxcclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0XHQvLyBDYXJvdXNlbCBsaWdodGJveGVzXHJcblx0XHRcdCQoJy5kbi1mZWF0dXJlLWNhcm91c2VsIC5zZWxlY3RvciAuaW1hZ2UtY29udGFpbmVyJykubWFnbmlmaWNQb3B1cCh7XHJcblx0XHRcdFx0dHlwZTogJ2ltYWdlJyxcclxuXHRcdFx0XHRnYWxsZXJ5OiB7XHJcblx0XHRcdFx0XHRlbmFibGVkOiB0cnVlXHJcblx0XHRcdFx0fSxcclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0fVxyXG5cclxuXHRcdC8vIFByb2R1Y3QgXHJcblx0XHRpZiAoJChcIi5kbi1wcm9kdWN0LWNhcm91c2VsXCIpLmxlbmd0aCA+IDApIHtcclxuXHJcblx0XHRcdCQoXCIuZG4tcHJvZHVjdC1jYXJvdXNlbCAubWFpbi1pbWFnZVwiKS5zbGljayh7XHJcblx0XHRcdFx0c2xpZGVzVG9TaG93OiAxLFxyXG5cdFx0XHRcdHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG5cdFx0XHRcdGFycm93czogdHJ1ZSxcclxuXHRcdFx0XHRmYWRlOiB0cnVlLFxyXG5cdFx0XHRcdGluZmluaXRlOiBmYWxzZSxcclxuXHRcdFx0XHRhc05hdkZvcjogJy5kbi1wcm9kdWN0LWNhcm91c2VsIC5zZWxlY3RvcicsXHJcblx0XHRcdFx0cHJldkFycm93OiBcIjxpIGNsYXNzPSdpY29uLWNoZXZyb24tbGVmdCBzbGljay1wcmV2Jz48L2k+XCIsXHJcblx0XHRcdFx0bmV4dEFycm93OiBcIjxpIGNsYXNzPSdpY29uLWNoZXZyb24tcmlnaHQgc2xpY2stbmV4dCc+PC9pPlwiLFxyXG5cdFx0XHR9KTtcclxuXHRcdFx0JChcIi5kbi1wcm9kdWN0LWNhcm91c2VsIC5zZWxlY3RvclwiKS5zbGljayh7XHJcblx0XHRcdFx0c2xpZGVzVG9TaG93OiA0LFxyXG5cdFx0XHRcdHNsaWRlc1RvU2Nyb2xsOiA0LFxyXG5cdFx0XHRcdGluZmluaXRlOiBmYWxzZSxcclxuXHRcdFx0XHRhc05hdkZvcjogJy5kbi1wcm9kdWN0LWNhcm91c2VsIC5tYWluLWltYWdlJyxcclxuXHRcdFx0XHRkb3RzOiBmYWxzZSxcclxuXHRcdFx0XHR2ZXJ0aWNhbDogdHJ1ZSxcclxuXHRcdFx0XHRhcnJvd3M6IGZhbHNlLFxyXG5cdFx0XHRcdGNlbnRlck1vZGU6IGZhbHNlLFxyXG5cdFx0XHRcdGZvY3VzT25TZWxlY3Q6IHRydWUsXHJcblx0XHRcdFx0cmVzcG9uc2l2ZTogW3tcclxuXHRcdFx0XHRcdGJyZWFrcG9pbnQ6IDk5MSxcclxuXHRcdFx0XHRcdHNldHRpbmdzOiB7XHJcblx0XHRcdFx0XHRcdHNsaWRlc1RvU2hvdzogMixcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9LCBdXHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdFx0Ly8gQ3JlYXRlIGxpZ2h0Ym94ZXNcclxuXHRcdFx0JCgnLmRuLXByb2R1Y3QtY2Fyb3VzZWwgLm1haW4taW1hZ2UgLmltYWdlLWNvbnRhaW5lcicpLm1hZ25pZmljUG9wdXAoe1xyXG5cdFx0XHRcdHR5cGU6ICdpbWFnZScsXHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdH1cclxuXHJcblx0XHQvLyBQcm9kdWN0LW1pbmkgXHJcblx0XHRpZiAoJChcIi5kbi1wcm9kdWN0LWNhcm91c2VsLW1pbmlcIikubGVuZ3RoID4gMCkge1xyXG5cclxuXHRcdFx0JChcIi5kbi1wcm9kdWN0LWNhcm91c2VsLW1pbmkgLnNsaWRlclwiKS5zbGljayh7XHJcblx0XHRcdFx0c2xpZGVzVG9TaG93OiAxLFxyXG5cdFx0XHRcdHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG5cdFx0XHRcdGFycm93czogZmFsc2UsXHJcblx0XHRcdFx0ZG90czogdHJ1ZSxcclxuXHRcdFx0XHRmYWRlOiB0cnVlLFxyXG5cdFx0XHRcdGFzTmF2Rm9yOiAnLmRuLXByb2R1Y3QtY2Fyb3VzZWwgLnNlbGVjdG9yJyxcclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0XHQvLyBDcmVhdGUgbGlnaHRib3hlc1xyXG5cdFx0XHQkKCcuZG4tcHJvZHVjdC1jYXJvdXNlbCAubWFpbi1pbWFnZSAuaW1hZ2UtY29udGFpbmVyJykubWFnbmlmaWNQb3B1cCh7XHJcblx0XHRcdFx0dHlwZTogJ2ltYWdlJyxcclxuXHRcdFx0XHRnYWxsZXJ5OiB7XHJcblx0XHRcdFx0XHRlbmFibGVkOiB0cnVlXHJcblx0XHRcdFx0fSxcclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0Ly8gQ3JlYXRlcyBmdW5jaXRvbmFsaXR5IGZvciB0aGUgZG93biBhcnJvd1xyXG5cdGZ1bmN0aW9uIERvd25BcnJvd0Z1bmN0aW9uYWx0eSgpIHtcclxuXHJcblx0XHQvLyBJZiB0aGUgdXNlciBjbGlja3Mgb24gdGhlIG5leHQgc2VjdGlvbiBhcnJvd1xyXG5cdFx0JChcIiNkbi1uZXh0LXNlYy1hcnJvd1wiKS5tb3VzZWRvd24oZnVuY3Rpb24gKCkge1xyXG5cclxuXHRcdFx0Ly8gRmluZCB0aGUgc2Nyb2xsdG9wIGhlaWdodCBmb3IgdGhpcyBvYmplY3RzIHBhcmVudFxyXG5cdFx0XHR2YXIgb2ZmU2V0VG9wID0gJCh0aGlzKS5wYXJlbnQoKS5vZmZzZXQoKS50b3AgK1xyXG5cdFx0XHRcdCQod2luZG93KS5oZWlnaHQoKSAvLyBBZGQgdGhlIHdpbmRvdyBoZWlnaHRcclxuXHRcdFx0XHQtXHJcblx0XHRcdFx0JChcIiNtYXN0aGVhZFwiKS5vdXRlckhlaWdodCgpOyAvLyBtaW51cyB0aGUgbWVudSB0b3AgaGVpZ2h0XHJcblxyXG5cdFx0XHQvLyBTY3JvbGwgdG8gdGhlIG5leHQgc2VjdGlvblxyXG5cdFx0XHQkKCdodG1sLCBib2R5JykuYW5pbWF0ZSh7XHJcblx0XHRcdFx0c2Nyb2xsVG9wOiBvZmZTZXRUb3BcclxuXHRcdFx0fSwgNTAwKTtcclxuXHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cclxuXHJcblxyXG5cdGZ1bmN0aW9uIE9wZW5OZXdzRmlsdGVyKCkge1xyXG5cclxuXHRcdC8vIENsb3NlIHRoZSBzZWFyY2ggJiBtaW5pLWNhcnRcclxuXHRcdENsb3NlU2VhcmNoRHJvcCgpO1xyXG5cdFx0Q2xvc2VTaWRlQ2FydCgpO1xyXG5cclxuXHRcdC8vIGZhZGUgaW4gdGhlIEJHXHJcblx0XHQkKFwiI3Bvc3QtZmlsdGVyLWJnXCIpLmZhZGVJbigyNTApO1xyXG5cclxuXHRcdC8vIEJyaW5nIGluIHRoZSBuZXdzIGZpbHRlciBmcm9tIHRoZSBzaWRlXHJcblx0XHQkKFwiI3Bvc3QtZmlsdGVyLWNvbnRhaW5lclwiKS5hZGRDbGFzcyhcImFwcGVhclwiKTtcclxuXHRcdCQoXCIjZmlsdGVyLWJvdHRvbVwiKS5hZGRDbGFzcyhcImFwcGVhclwiKTtcclxuXHJcblx0XHQvLyBMb2NrIHRoZSBiYWNrZ3JvdW5kXHJcblx0XHRMb2NrUGFnZVNjcm9sbCgpO1xyXG5cdH1cclxuXHJcblx0ZnVuY3Rpb24gQ2xvc2VOZXdzRmlsdGVyKCkge1xyXG5cclxuXHRcdC8vIENyZWF0ZSB0aGUgZmluYWwgbGlzdCBvZiBjYXRlZ29yaWVzIHRvIGRpc3BsYXlcclxuXHRcdHZhciBmaW5hbENhdExpc3QgPSBcIlwiO1xyXG5cdFx0dmFyIGZpbmFsQ2F0TGlzdE5pY2UgPSBcIlwiO1xyXG5cclxuXHRcdC8vIFNjYW4gdGhlIGxpc3QgZm9yIHRoZSBjYXRlZ29yaWVzXHJcblx0XHQkKFwiI3Bvc3QtZmlsdGVyLWNvbnRhaW5lciAubWlkZGxlIGEuc2VsZWN0ZWRcIikuZWFjaChmdW5jdGlvbiAoKSB7XHJcblxyXG5cdFx0XHRpZiAoZmluYWxDYXRMaXN0ICE9IFwiXCIpIHtcclxuXHRcdFx0XHRmaW5hbENhdExpc3QgKz0gXCIsIFwiO1xyXG5cdFx0XHR9XHJcblx0XHRcdGZpbmFsQ2F0TGlzdCArPSBcIi5cIiArIFJlZ2V4UmVwbGFjZSgkKHRoaXMpLnRleHQoKSk7XHJcblxyXG5cdFx0XHRpZiAoZmluYWxDYXRMaXN0TmljZSAhPSBcIlwiKSB7XHJcblx0XHRcdFx0ZmluYWxDYXRMaXN0TmljZSArPSBcIiwgXCI7XHJcblx0XHRcdH1cclxuXHRcdFx0ZmluYWxDYXRMaXN0TmljZSArPSAkKHRoaXMpLnRleHQoKTtcclxuXHRcdH0pO1xyXG5cclxuXHRcdC8vIFNob3cgb25seSB0aG9zZSBpbiB0aGUgTWFzb25hcnkgZGlzcGxheVxyXG5cdFx0Ly8gRGlzcGxheSBhbGwgY2F0c1xyXG5cdFx0aWYgKGZpbmFsQ2F0TGlzdCA9PSBcIlwiKSB7XHJcblxyXG5cdFx0XHQkKFwiLm1hc29uYXJ5LWxheW91dC1jb250YWluZXJcIikuaXNvdG9wZSh7XHJcblx0XHRcdFx0ZmlsdGVyOiBcIipcIlxyXG5cdFx0XHR9KVxyXG5cclxuXHRcdFx0Ly8gRGlzcGxheSBqdXN0IHRoZSByZXF1aXJlcyBjYXRzXHJcblx0XHR9IGVsc2Uge1xyXG5cclxuXHRcdFx0JChcIi5tYXNvbmFyeS1sYXlvdXQtY29udGFpbmVyXCIpLmlzb3RvcGUoe1xyXG5cdFx0XHRcdGZpbHRlcjogZmluYWxDYXRMaXN0XHJcblx0XHRcdH0pXHJcblxyXG5cdFx0fVxyXG5cclxuXHRcdC8vIFNob3cvSGlkZSBubyBwb3N0c1xyXG5cdFx0aWYgKCEkKFwiLm1hc29uYXJ5LWxheW91dC1jb250YWluZXJcIikuZGF0YSgnaXNvdG9wZScpLmZpbHRlcmVkSXRlbXMubGVuZ3RoKSB7XHJcblx0XHRcdCQoXCIjbm8tcG9zdHNcIikuc2hvdygpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0JChcIiNuby1wb3N0c1wiKS5oaWRlKCk7XHJcblx0XHR9XHJcblxyXG5cdFx0Ly8gY29uc29sZS5sb2coIGZpbmFsQ2F0TGlzdCApO1xyXG5cclxuXHRcdC8vIFVwZGF0ZSB0aGUgRmlsdGVyIG5ld3Mgd2l0aCB0aGUgY3VycmVudCBjYXRzIHNob3dpbmdcclxuXHRcdGlmIChmaW5hbENhdExpc3ROaWNlICE9IFwiXCIpIHtcclxuXHRcdFx0JChcIiNwb3N0LWZpbHRlciAucmVzdWx0c1wiKS50ZXh0KCc6ICcgKyBmaW5hbENhdExpc3ROaWNlKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdCQoXCIjcG9zdC1maWx0ZXIgLnJlc3VsdHNcIikudGV4dChcIlwiKTtcclxuXHRcdH1cclxuXHJcblx0XHQvLyBmYWRlIGluIHRoZSBCR1xyXG5cdFx0JChcIiNwb3N0LWZpbHRlci1iZ1wiKS5mYWRlT3V0KDI1MCk7XHJcblxyXG5cdFx0Ly8gQnJpbmcgaW4gdGhlIG5ld3MgZmlsdGVyIGZyb20gdGhlIHNpZGVcclxuXHRcdCQoXCIjcG9zdC1maWx0ZXItY29udGFpbmVyXCIpLnJlbW92ZUNsYXNzKFwiYXBwZWFyXCIpO1xyXG5cdFx0JChcIiNmaWx0ZXItYm90dG9tXCIpLnJlbW92ZUNsYXNzKFwiYXBwZWFyXCIpO1xyXG5cclxuXHRcdC8vIFVubG9jayB0aGUgYmFja2dyb3VuZFxyXG5cdFx0VW5sb2NrUGFnZVNjcm9sbCgpO1xyXG5cclxuXHRcdC8vIFNjcm9sbCB1c2VyIGJhY2sgdG8gdGhlIHRvcCBvZiB0aGUgc2VjdGlvblxyXG5cdFx0JCgnaHRtbCwgYm9keScpLmFuaW1hdGUoe1xyXG5cdFx0XHRzY3JvbGxUb3A6ICQoXCIjbWFpblwiKS5vZmZzZXQoKS50b3AgLSAxMDBcclxuXHRcdH0sIDUwMCk7XHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiBEeW5hbWljTG9hZFBvc3RzRnVuY3Rpb25hbGl0eSgpIHtcclxuXHJcblx0XHQvLyBLZWVwIHRyYWNrIG9mIGhvdyBtYW55IHBvc3RzIGhhdmUgbG9hZGVkXHJcblx0XHQkKFwiI2RuLWR5bmFtaWMtbG9hZC1wb3N0cyAuZG4tYnV0dG9uXCIpLmhpZGUoKTtcclxuXHRcdCQoXCIjZG4tZHluYW1pYy1sb2FkLXBvc3RzIC5kbi1idXR0b25cIikucmVtb3ZlQ2xhc3MoXCJoaWRkZW5cIik7XHJcblxyXG5cdFx0Q2hlY2tGb3JMb2FkUG9zdHMoKTtcclxuXHRcdCQod2luZG93KS5zY3JvbGwoZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRDaGVja0ZvckxvYWRQb3N0cygpO1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0JChcIiNkbi1keW5hbWljLWxvYWQtcG9zdHMgLmRuLWJ1dHRvblwiKS5jbGljayhmdW5jdGlvbiAoZSkge1xyXG5cclxuXHRcdFx0Ly8gSGlkZSB0aGUgYnV0dG9uXHJcblx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0JCh0aGlzKS5oaWRlKCk7XHJcblxyXG5cdFx0XHRQb3N0TG9hZGVyRnVuY3Rpb25hbGl0eSgpO1xyXG5cclxuXHRcdH0pO1xyXG5cclxuXHRcdGZ1bmN0aW9uIENoZWNrRm9yTG9hZFBvc3RzKCkge1xyXG5cclxuXHRcdFx0aWYgKCQoXCIjZG4tZHluYW1pYy1sb2FkLXBvc3RzXCIpLmxlbmd0aCA+IDAgJiYgIUxvYWRpbmdNb3JlUG9zdHMpIHtcclxuXHJcblx0XHRcdFx0Ly8gQ2FsY3VsYXRlIHRoZSBwaXhlbCBkaXN0YW5jZSBvZiB0aGUgYm90dG9tIG9mIHRoZSBzY3JlZW5cclxuXHRcdFx0XHR2YXIgd2luZG93Qm90dG9tRGlzdGFuY2VDYWxjID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpICsgJCh3aW5kb3cpLmhlaWdodCgpO1xyXG5cclxuXHRcdFx0XHQvLyBJZiB0aGlzIGlzIHRoZSBmaXJzdCAyIHRpbWVzIHdlJ3JlIGF1dG9sb2FkaW5nIHBvc3RzXHJcblx0XHRcdFx0aWYgKFBvc3RzTG9hZGVkIDwgUG9zdExvYWRzQmVmb3JlQnV0dG9uKSB7XHJcblxyXG5cdFx0XHRcdFx0Ly8gSWYgd2UndmUgcmVhY2hlZCB0aGUgZmluYWwgYmxvZyBwb3N0IGFuZCB3ZSdyZSBub3QgY3VycmVudGx5IGxvYWRpbmcgb3RoZXIgYmxvZyBwb3N0c1xyXG5cdFx0XHRcdFx0aWYgKHdpbmRvd0JvdHRvbURpc3RhbmNlQ2FsYyA+ICQoXCIjZG4tZHluYW1pYy1sb2FkLXBvc3RzXCIpLm9mZnNldCgpLnRvcCkge1xyXG5cclxuXHRcdFx0XHRcdFx0UG9zdExvYWRlckZ1bmN0aW9uYWxpdHkoKTtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHRcdGZ1bmN0aW9uIFBvc3RMb2FkZXJGdW5jdGlvbmFsaXR5KCkge1xyXG5cclxuXHRcdFx0UG9zdHNMb2FkZWQrKztcclxuXHJcblx0XHRcdC8vIGluZGljYXRlIHdlJ3JlIGxvYWRpbmcgbW9yZSBibG9nIHBvc3RzXHJcblx0XHRcdExvYWRpbmdNb3JlUG9zdHMgPSB0cnVlO1xyXG5cclxuXHRcdFx0Ly8gU2hvdyB0aGUgcG9zdCBsb2FkaW5nIGVsZW1lbnRcclxuXHRcdFx0JChcIiNkbi1keW5hbWljLWxvYWQtcG9zdHMgLnBvc3QtbG9hZGluZ1wiKS5yZW1vdmVDbGFzcyhcImhpZGRlblwiKTtcclxuXHJcblx0XHRcdC8vIEluZGljYXRlIHRoZSBtaW5pdW11bSBsb2FkaW5nIHRpbWUgaGFzIG5vdCBiZWVuIG1ldCB5ZXRcclxuXHRcdFx0TWluTG9hZGluZ1dhaXRpbmdUaW1lQ29tcGxldGVkID0gZmFsc2U7XHJcblxyXG5cdFx0XHQvLyBMb2FkIHRoZSBibG9nIHBvc3RzXHJcblx0XHRcdExvYWRNb3JlUG9zdHMoNSk7XHJcblxyXG5cdFx0XHQvLyBHaXZlcyBhIG1pbmltdW0gbG9hZGluZyB0aW1lIHRvIHNob3cgdXNlcnMgdGhlIGxvYWRpbmcgdGhpbmdcclxuXHRcdFx0c2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0TWluTG9hZGluZ1dhaXRpbmdUaW1lQ29tcGxldGVkID0gdHJ1ZTtcclxuXHRcdFx0fSwgTWluTG9hZGluZ1RpbWUpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0ZnVuY3Rpb24gTG9hZE1vcmVQb3N0cyhOdW1iZXJPZlBvc3RzVG9Mb2FkKSB7XHJcblxyXG5cdFx0Ly8gZmluZCB0aGUgbnVtYmVyIG9mIGJsb2cgYXJ0aWNsZXMgaGVyZVxyXG5cdFx0dmFyIFBvc3RDb3VudCA9ICQoXCIuZG4tc2luZ2xlLXBvc3RcIikubGVuZ3RoO1xyXG5cclxuXHRcdC8vIElmIHRoaXMgaXMgYSBjYXRlZ29yaWVzIHBhZ2VcclxuXHRcdHZhciBjYXR0eXBlID0gXCJcIjtcclxuXHRcdGlmICgkKFwiI2NhdC10eXBlXCIpLmxlbmd0aCA+IDApIHtcclxuXHRcdFx0Y2F0dHlwZSA9ICQoXCIjY2F0LXR5cGVcIikuYXR0cihcImRhdGEtY2F0dHlwZVwiKTtcclxuXHRcdH1cclxuXHJcblx0XHRqUXVlcnkuYWpheCh7XHJcblx0XHRcdHVybDogZG5fdmFyaWFibGUuYWpheF91cmwsXHJcblx0XHRcdHR5cGU6ICdwb3N0JyxcclxuXHRcdFx0ZGF0YToge1xyXG5cdFx0XHRcdGFjdGlvbjogJ3Bvc3RfZW50cmllc19maW5kX2VudHJpZXMnLFxyXG5cdFx0XHRcdGN1cnJlbnRfcG9zdHM6IFBvc3RDb3VudCxcclxuXHRcdFx0XHRwb3N0X3R5cGU6ICdwb3N0JyxcclxuXHRcdFx0XHRjYXR0eXBlOiBjYXR0eXBlLFxyXG5cdFx0XHRcdGZldGNoX2Ftb3VudDogTnVtYmVyT2ZQb3N0c1RvTG9hZCxcclxuXHRcdFx0fSxcclxuXHRcdFx0c3VjY2VzczogZnVuY3Rpb24gKGRhdGEpIHtcclxuXHRcdFx0XHRMb2FkTW9yZVBvc3RzX1N1Y2Nlc3MoZGF0YSk7XHJcblx0XHRcdH0sXHJcblx0XHRcdGVycm9yOiBmdW5jdGlvbiAoZGF0YSkge1xyXG5cdFx0XHRcdGNvbnNvbGUubG9nKFwiQUpBWCByZXF1ZXN0IGZhaWxlZFwiKTtcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiBMb2FkTW9yZVBvc3RzX1N1Y2Nlc3MoZGF0YSkge1xyXG5cclxuXHRcdENvbXBsZXRlTG9hZGluZ0Z1bmN0aW9uYWxpdHkoKTtcclxuXHJcblx0XHRmdW5jdGlvbiBDb21wbGV0ZUxvYWRpbmdGdW5jdGlvbmFsaXR5KCkge1xyXG5cclxuXHRcdFx0Ly8gaWYgdGhlIG1pbm11bSBsYW9kaW5nIHRpbWUgaGFzIGJlZW4gbWV0XHJcblx0XHRcdGlmIChNaW5Mb2FkaW5nV2FpdGluZ1RpbWVDb21wbGV0ZWQpIHtcclxuXHJcblx0XHRcdFx0Ly8gSGlkZSB0aGUgcG9zdCBsb2FkaW5nIGVsZW1lbnRcclxuXHRcdFx0XHQkKFwiI2RuLWR5bmFtaWMtbG9hZC1wb3N0cyAucG9zdC1sb2FkaW5nXCIpLmFkZENsYXNzKFwiaGlkZGVuXCIpO1xyXG5cclxuXHRcdFx0XHQvLyBQb3N0IHRoZSBuZXcgYmxvZyBwb3N0cyBkYXRhIGludG8gdGhlIHBhZ2VcclxuXHRcdFx0XHR2YXIganNvbiA9IEpTT04ucGFyc2UoZGF0YSk7XHJcblxyXG5cdFx0XHRcdC8vIFBhc3RlIHRoZSBhY3R1YWwgZGF0YSBpbm8gXHJcblx0XHRcdFx0JChcIiNkbi1keW5hbWljLWxvYWQtcG9zdHNcIikuYmVmb3JlKGpzb24uaHRtbCk7XHJcblxyXG5cdFx0XHRcdC8vIElmIHRoZXJlIGFyZSBub3QgbW9yZSBibG9nIHBvc3RzIHRvIGxvYWQsIGRlbGV0ZSB0aGUgbG9hZGVyIGVsZW1lbnRcclxuXHRcdFx0XHRpZiAoanNvbi5pc19maW5hbCkge1xyXG5cdFx0XHRcdFx0JChcIiNkbi1keW5hbWljLWxvYWQtcG9zdHNcIikucmVtb3ZlKCk7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHQvLyBJbmRpY2F0ZSB3ZSdyZSBmaW5pc2hlZCBsb2FkaW5nIGJsb2cgcG9zdHNcclxuXHRcdFx0XHRMb2FkaW5nTW9yZVBvc3RzID0gZmFsc2U7XHJcblxyXG5cdFx0XHRcdC8vIFNob3cgdGhlIGJ1dHRvbiBpZiB0aGUgcG9zdHMgbG9hZGluZyBpcyBhcHByb3ByaWF0ZVxyXG5cdFx0XHRcdGlmIChQb3N0c0xvYWRlZCA+PSBQb3N0TG9hZHNCZWZvcmVCdXR0b24pIHtcclxuXHRcdFx0XHRcdCQoXCIjZG4tZHluYW1pYy1sb2FkLXBvc3RzIC5kbi1idXR0b25cIikuc2hvdygpO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdH0gZWxzZSB7XHJcblxyXG5cdFx0XHRcdC8vIENoZWNrIGFnYWluIHNob3J0bHlcclxuXHRcdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRcdENvbXBsZXRlTG9hZGluZ0Z1bmN0aW9uYWxpdHkoKTtcclxuXHRcdFx0XHR9LCAxMDApO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiBTZWFyY2hEcm9wRnVuY3Rpb25hbGl0eSgpIHtcclxuXHJcblx0XHRpZiAoJChcIiNkbi1zZWFyY2gtZHJvcFwiKS5sZW5ndGggPiAwICYmICQoXCIjZG4tc2VhcmNoLWRyb3AtYnV0dG9uXCIpLmxlbmd0aCA+IDApIHtcclxuXHJcblx0XHRcdC8vIGNsb3NlIGJ1dHRvblxyXG5cdFx0XHQkKCcjZG4tc2VhcmNoLWRyb3AuZnVsbC1zdHlsZSAudGl0bGUtY2xvc2UgYSwgI2RuLXNlYXJjaC1kcm9wLmZ1bGwtc3R5bGUgLnNoYWRlJykuY2xpY2soZnVuY3Rpb24oZSl7XHJcblx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpXHJcblx0XHRcdFx0JCgnI2RuLXNlYXJjaC1kcm9wJykudG9nZ2xlQ2xhc3MoJ29wZW4tZnVsbC1zZWFyY2gnKVxyXG5cdFx0XHR9KVxyXG5cclxuXHRcdFx0JChcIiNkbi1zZWFyY2gtZHJvcC1idXR0b25cIikuY2xpY2soZnVuY3Rpb24gKCkge1xyXG5cclxuXHRcdFx0XHRpZiggJCh0aGlzKS5oYXNDbGFzcygnZnVsbC1wYWdlJykgKXtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0Ly8gT3BlbiBmdWxsIGRyb3Agc2VhcmNoXHJcblx0XHRcdFx0XHQkKCcjZG4tc2VhcmNoLWRyb3AnKS50b2dnbGVDbGFzcygnb3Blbi1mdWxsLXNlYXJjaCcpXHJcblxyXG5cdFx0XHRcdH1lbHNle1xyXG5cdFx0XHRcdFx0Ly8gT3BlbiB0aGUgc2VhcmNoXHJcblx0XHRcdFx0XHRpZiAoISQoXCIjZG4tc2VhcmNoLWRyb3BcIikuaGFzQ2xhc3MoXCJkcm9wcGVkXCIpKSB7XHJcblxyXG5cdFx0XHRcdFx0XHRPcGVuU2VhcmNoRHJvcCgpO1xyXG5cclxuXHRcdFx0XHRcdFx0Ly8gQ2xvc2UgdGhlIHNlYXJjaFxyXG5cdFx0XHRcdFx0fSBlbHNlIHtcclxuXHJcblx0XHRcdFx0XHRcdENsb3NlU2VhcmNoRHJvcCgpO1xyXG5cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHR9KTtcclxuXHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiBPcGVuU2VhcmNoRHJvcCgpIHtcclxuXHJcblx0XHQvLyBDbG9zZSB0aGUgbWVudVxyXG5cdFx0Q2xvc2VNZW51KCk7XHJcblxyXG5cdFx0Ly8gQ2xvc2UgdGhlIHNpZGUtY2FydFxyXG5cdFx0Q2xvc2VTaWRlQ2FydCgpO1xyXG5cclxuXHRcdCQoXCIjZG4tc2VhcmNoLWRyb3BcIikuYWRkQ2xhc3MoXCJkcm9wcGVkXCIpO1xyXG5cdH1cclxuXHJcblx0ZnVuY3Rpb24gQ2xvc2VTZWFyY2hEcm9wKCkge1xyXG5cdFx0JChcIiNkbi1zZWFyY2gtZHJvcFwiKS5yZW1vdmVDbGFzcyhcImRyb3BwZWRcIik7XHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiBDYXJ0RHJvcEZ1bmN0aW9uYWxpdHkoKSB7XHJcblxyXG5cdFx0aWYgKCQoXCIjZG4tc2lkZS1jYXJ0LW1lbnVcIikubGVuZ3RoID4gMCAmJiAkKFwiI2RuLWNhcnQtcG9wLWJ1dHRvblwiKS5sZW5ndGggPiAwKSB7XHJcblxyXG5cdFx0XHQkKGRvY3VtZW50KS5vbihcImNsaWNrIHRvdWNoc3RhcnRcIiwgXCIjZG4tY2FydC1wb3AtYnV0dG9uXCIsIGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRPcGVuU2lkZUNhcnQoKTtcclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0XHQkKFwiI2Nsb3NlLWRuLXNpZGUtY2FydC1tZW51XCIpLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuXHJcblx0XHRcdFx0Q2xvc2VTaWRlQ2FydCgpO1xyXG5cdFx0XHR9KTtcclxuXHJcblx0XHR9XHJcblxyXG5cdH1cclxuXHJcblx0ZnVuY3Rpb24gT3BlblNpZGVDYXJ0KCkge1xyXG5cclxuXHRcdC8vIENsb3NlIHRoZSBzZWFyY2hcclxuXHRcdENsb3NlU2VhcmNoRHJvcCgpO1xyXG5cclxuXHRcdC8vIENsb3NlIHRoZSBtZW51XHJcblx0XHRDbG9zZU1lbnUoKTtcclxuXHJcblx0XHQvLyBPcGVuIHRoZSBjYXJ0XHJcblx0XHQkKFwiI2RuLXNpZGUtY2FydC1tZW51XCIpLmFkZENsYXNzKFwiYXBwZWFyXCIpO1xyXG5cclxuXHRcdC8vIExvY2sgdGhlIHBhZ2Ugc2Nyb2xsaW5nXHJcblx0XHRMb2NrUGFnZVNjcm9sbCgpO1xyXG5cdH1cclxuXHJcblx0ZnVuY3Rpb24gQ2xvc2VTaWRlQ2FydCgpIHtcclxuXHJcblx0XHQvLyBDbG9zZSB0aGUgY2FydFxyXG5cdFx0JChcIiNkbi1zaWRlLWNhcnQtbWVudVwiKS5yZW1vdmVDbGFzcyhcImFwcGVhclwiKTtcclxuXHJcblx0XHQvLyBBbGxvdyBwYWdlIHRvIHNjcm9sbCBhZ2FpblxyXG5cdFx0VW5sb2NrUGFnZVNjcm9sbCgpO1xyXG5cdH1cclxuXHJcblx0ZnVuY3Rpb24gR3Jhdml0eUZvcm1GdW5jdGlvbmFsaXR5KCkge1xyXG5cclxuXHRcdC8vIFJ1biBvbmNlIG9uIHN0YXJ0dXBcclxuXHRcdEdyYXZpdHlGb3JtRnVuY3Rpb25hbGl0eVNldHVwKCk7XHJcblxyXG5cdFx0Ly8gUnVuIG9uIGZhaWxlZCBzYnVtaXNzaW9uc1xyXG5cdFx0alF1ZXJ5KGRvY3VtZW50KS5iaW5kKCdnZm9ybV9wb3N0X3JlbmRlcicsIGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0aWYgKCQoXCIudmFsaWRhdGlvbl9lcnJvclwiKS5sZW5ndGggPiAwKSB7XHJcblx0XHRcdFx0R3Jhdml0eUZvcm1GdW5jdGlvbmFsaXR5U2V0dXAoKTtcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHRcclxuXHJcblx0dmFyIHNjcm9sbFBvc2l0aW9uO1xyXG5cdHZhciBzY3JvbGxMb2NrZWQgPSBmYWxzZTtcclxuXHJcblx0ZnVuY3Rpb24gTG9ja1BhZ2VTY3JvbGwoKSB7XHJcblx0XHQvKiBzY3JvbGxQb3NpdGlvbiA9IFtcclxuXHRcdFx0c2VsZi5wYWdlWE9mZnNldCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsTGVmdCB8fCBkb2N1bWVudC5ib2R5LnNjcm9sbExlZnQsXHJcblx0XHRcdHNlbGYucGFnZVlPZmZzZXQgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcCB8fCBkb2N1bWVudC5ib2R5LnNjcm9sbFRvcFxyXG5cdFx0XTtcclxuXHRcdHZhciBodG1sID0galF1ZXJ5KCdib2R5Jyk7IC8vIGl0IHdvdWxkIG1ha2UgbW9yZSBzZW5zZSB0byBhcHBseSB0aGlzIHRvIGJvZHksIGJ1dCBJRTcgd29uJ3QgaGF2ZSB0aGF0XHJcblx0XHRzY3JvbGxMb2NrZWQgPSB0cnVlO1xyXG5cdFx0aHRtbC5jc3Moe1xyXG5cdFx0XHQncG9zaXRpb24nOiAnZml4ZWQnLFxyXG5cdFx0XHRcImxlZnRcIjogXCIwXCIsXHJcblx0XHRcdFwidG9wXCI6IFwiLVwiICsgc2Nyb2xsUG9zaXRpb25bMV0gKyBcInB4XCIsXHJcblx0XHRcdFwib3ZlcmZsb3dcIjogXCJoaWRkZW5cIixcclxuXHRcdFx0XCJ3aWR0aFwiOiBcIjEwMCVcIixcclxuXHRcdH0pOyAqL1xyXG5cdH1cclxuXHJcblx0ZnVuY3Rpb24gVW5sb2NrUGFnZVNjcm9sbCgpIHtcclxuLyogXHJcblx0XHQvLyBJZiB0aGUgcGFnZSBzY3JvbGwgaXMgbG9ja2VkXHJcblx0XHRpZiAoc2Nyb2xsTG9ja2VkKSB7XHJcblx0XHRcdHZhciBodG1sID0galF1ZXJ5KCdib2R5Jyk7XHJcblx0XHRcdHNjcm9sbExvY2tlZCA9IGZhbHNlO1xyXG5cdFx0XHRodG1sLmNzcyh7XHJcblx0XHRcdFx0J3Bvc2l0aW9uJzogJ3N0YXRpYycsXHJcblx0XHRcdFx0XCJsZWZ0XCI6IFwiYXV0b1wiLFxyXG5cdFx0XHRcdFwidG9wXCI6IFwiYXV0b1wiLFxyXG5cdFx0XHRcdFwib3ZlcmZsb3dcIjogXCJ2aXNpYmxlXCIsXHJcblx0XHRcdFx0XCJ3aWR0aFwiOiBcImF1dG9cIixcclxuXHRcdFx0fSk7XHJcblx0XHRcdHdpbmRvdy5zY3JvbGxUbyhzY3JvbGxQb3NpdGlvblswXSwgc2Nyb2xsUG9zaXRpb25bMV0pO1xyXG5cdFx0fSAqL1xyXG5cdH1cclxuXHJcblx0ZnVuY3Rpb24gV29vU2hvcElzb3RvcGVGdW5jdGlvbmFsaXR5KCkge1xyXG5cclxuXHRcdC8vIGlmIHRoaXMgaXMgdGhlIHdvbyBzaG9wIHBhZ2VcclxuXHRcdGlmICgkKFwiYm9keS5wb3N0LXR5cGUtYXJjaGl2ZS1wcm9kdWN0Lndvb2NvbW1lcmNlXCIpLmxlbmd0aCA+IDApIHtcclxuXHJcblx0XHRcdC8vIEFTU0lHTiBJU09UT1BFXHJcblx0XHRcdHZhciAkY29udGFpbmVyID0gJCgnLnByb2R1Y3QtbG9vcC1jb250YWluZXInKTtcclxuXHRcdFx0dmFyIGJ1dHRvbkZpbHRlciA9ICcqJztcclxuXHRcdFx0JGNvbnRhaW5lci5pc290b3BlKHtcclxuXHRcdFx0XHRpdGVtU2VsZWN0b3I6ICcucHJvZHVjdCcsXHJcblx0XHRcdFx0cmVzaXphYmxlOiBmYWxzZSwgLy8gZGlzYWJsZSBub3JtYWwgcmVzaXppbmdcclxuXHRcdFx0XHRsYXlvdXRNb2RlOiAnZml0Um93cycsXHJcblx0XHRcdFx0ZmlsdGVyOiBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0XHR2YXIgJHRoaXMgPSAkKHRoaXMpO1xyXG5cdFx0XHRcdFx0dmFyIHJhbmdlRmlsdGVyID0gJCgnI3dvby1maWx0ZXItcHJpY2UnKTtcclxuXHRcdFx0XHRcdHZhciBSYW5nZU1pbiA9IHBhcnNlRmxvYXQocmFuZ2VGaWx0ZXIuYXR0cignZGF0YS1maWx0ZXItbWluJykpXHJcblx0XHRcdFx0XHR2YXIgUmFuZ2VNYXggPSBwYXJzZUZsb2F0KHJhbmdlRmlsdGVyLmF0dHIoJ2RhdGEtZmlsdGVyLW1heCcpKVxyXG5cdFx0XHRcdFx0dmFyIHByaWNlID0gcGFyc2VGbG9hdCgkdGhpcy5hdHRyKCdkYXRhLXByaWNlJykpXHJcblx0XHRcdFx0XHR2YXIgaXNQcmljZVJhbmdlID0gcHJpY2UgPD0gUmFuZ2VNYXggJiYgcHJpY2UgPj0gUmFuZ2VNaW5cclxuXHJcblx0XHRcdFx0XHRyZXR1cm4gJHRoaXMuaXMoYnV0dG9uRmlsdGVyKSAmJiAoaXNQcmljZVJhbmdlKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdFx0Ly8gRklMVEVSIENBVEVHT1JZXHJcblx0XHRcdCQoJy5kbi13b28tY2F0ZWdvcnktZmlsdGVyIGxpIGEnKS5jbGljayhmdW5jdGlvbiAoZSkge1xyXG5cdFx0XHRcdGUucHJldmVudERlZmF1bHQoKVxyXG5cdFx0XHRcdCQodGhpcykudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpXHJcblx0XHRcdFx0dmFyIGJ0bkZpbHRlciA9IFtdXHJcblx0XHRcdFx0JCgnLmRuLXdvby1jYXRlZ29yeS1maWx0ZXIgbGkgYS5hY3RpdmUnKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRcdGJ0bkZpbHRlci5wdXNoKCQodGhpcykuYXR0cignZGF0YS1maWx0ZXInKSlcclxuXHRcdFx0XHR9KVxyXG5cdFx0XHRcdGJ1dHRvbkZpbHRlciA9IGJ0bkZpbHRlci5qb2luKCcsJyk7XHJcblx0XHRcdFx0aWYgKGJ1dHRvbkZpbHRlciA9PSAnJykge1xyXG5cdFx0XHRcdFx0YnV0dG9uRmlsdGVyID0gJyonO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHQkY29udGFpbmVyLmlzb3RvcGUoKVxyXG5cdFx0XHR9KVxyXG5cclxuXHRcdFx0Ly8gRklMVEVSIFBSSUNFXHJcblx0XHRcdHZhciAkcHJpY2VGaWx0ZXJDb250YWluZXIgPSAkKCcjd29vLWZpbHRlci1wcmljZScpO1xyXG5cdFx0XHR2YXIgJHByaWNlTWluID0gcGFyc2VGbG9hdCgkcHJpY2VGaWx0ZXJDb250YWluZXIuYXR0cignZGF0YS1taW4nKSk7XHJcblx0XHRcdHZhciAkcHJpY2VNYXggPSBwYXJzZUZsb2F0KCRwcmljZUZpbHRlckNvbnRhaW5lci5hdHRyKCdkYXRhLW1heCcpKVxyXG5cdFx0XHQkcHJpY2VGaWx0ZXJDb250YWluZXIuc2xpZGVyKHtcclxuXHRcdFx0XHRyYW5nZTogdHJ1ZSxcclxuXHRcdFx0XHRtaW46ICRwcmljZU1pbixcclxuXHRcdFx0XHRtYXg6ICRwcmljZU1heCxcclxuXHRcdFx0XHR2YWx1ZXM6IFskcHJpY2VNaW4sICRwcmljZU1heF0sXHJcblx0XHRcdFx0Y3JlYXRlOiBmdW5jdGlvbiAoZSwgdWkpIHtcclxuXHRcdFx0XHRcdCRwcmljZUZpbHRlckNvbnRhaW5lci5maW5kKCdzcGFuLnVpLXNsaWRlci1oYW5kbGU6ZXEoMCknKS5hZGRDbGFzcygncmFuZ2UtbWluJykuaHRtbCgnPHNwYW4gY2xhc3M9XCJzbGlkZS1wcmljZVwiPiQnICsgJHByaWNlRmlsdGVyQ29udGFpbmVyLnNsaWRlcihcInZhbHVlc1wiLCAwKSArICc8L3NwYW4+JylcclxuXHRcdFx0XHRcdCRwcmljZUZpbHRlckNvbnRhaW5lci5maW5kKCdzcGFuLnVpLXNsaWRlci1oYW5kbGU6ZXEoMSknKS5hZGRDbGFzcygncmFuZ2UtbWF4JykuaHRtbCgnPHNwYW4gY2xhc3M9XCJzbGlkZS1wcmljZVwiPiQnICsgJHByaWNlRmlsdGVyQ29udGFpbmVyLnNsaWRlcihcInZhbHVlc1wiLCAxKSArICc8L3NwYW4+JylcclxuXHRcdFx0XHR9LFxyXG5cclxuXHRcdFx0XHRzbGlkZTogZnVuY3Rpb24gKGV2ZW50LCB1aSkge1xyXG5cdFx0XHRcdFx0JCh1aS5oYW5kbGUpLmh0bWwoJzxzcGFuIGNsYXNzPVwic2xpZGUtcHJpY2VcIj4kJyArIHVpLnZhbHVlICsgJzwvc3Bhbj4nKTtcclxuXHRcdFx0XHRcdCRwcmljZUZpbHRlckNvbnRhaW5lci5hdHRyKCdkYXRhLWZpbHRlci1taW4nLCAkcHJpY2VGaWx0ZXJDb250YWluZXIuc2xpZGVyKFwidmFsdWVzXCIsIDApKVxyXG5cdFx0XHRcdFx0JHByaWNlRmlsdGVyQ29udGFpbmVyLmF0dHIoJ2RhdGEtZmlsdGVyLW1heCcsICRwcmljZUZpbHRlckNvbnRhaW5lci5zbGlkZXIoXCJ2YWx1ZXNcIiwgMSkpXHJcblx0XHRcdFx0fSxcclxuXHJcblx0XHRcdFx0c3RvcDogZnVuY3Rpb24gKGV2ZW50LCB1aSkge1xyXG5cdFx0XHRcdFx0JGNvbnRhaW5lci5pc290b3BlKClcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHR9KVxyXG5cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdGZ1bmN0aW9uIFdvb1RhYkZ1bmN0aW9uYWxpdHkoKSB7XHJcblxyXG5cdFx0Ly8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXHJcblx0XHQvLyBEZXNrdG9wIGZ1bmN0aW9uYWxpdHkgLy9cclxuXHRcdC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xyXG5cclxuXHRcdC8vIFNldCB0aGUgZmlyc3QgdGFiIHRvIGFjdGl2ZVxyXG5cdFx0JChcIi5kbi13Yy10YWJzID4gZGl2OmZpcnN0LWNoaWxkXCIpLmFkZENsYXNzKFwiYWN0aXZlLWRlc2t0b3BcIik7XHJcblx0XHQkKFwiLmRuLXRhYnMtcGFuZWw6Zmlyc3RcIikuYWRkQ2xhc3MoXCJhY3RpdmUtZGVza3RvcFwiKTtcclxuXHJcblx0XHQvLyBJZiBhIHVzZXIgY2xpY2tzIG9uIGEgdGFiXHJcblx0XHQkKCcudGFicy1hY2NvcmRpb24taGVhZGVyIGEnKS5jbGljayhmdW5jdGlvbiAoZSkge1xyXG5cclxuXHRcdFx0Ly8gUHJldmVudCBkZWZhdWx0IGJlaGF2aW91clxyXG5cdFx0XHRlLnByZXZlbnREZWZhdWx0KClcclxuXHJcblx0XHRcdC8vIFJlbW92ZSB0aGUgY3VycmVudGx5IGFjdGl2ZSBjbGFzc1xyXG5cdFx0XHQkKCcudGFicy1hY2NvcmRpb24taGVhZGVyJykucmVtb3ZlQ2xhc3MoXCJhY3RpdmUtZGVza3RvcFwiKTtcclxuXHRcdFx0JChcIi5kbi10YWJzLXBhbmVsXCIpLnJlbW92ZUNsYXNzKFwiYWN0aXZlLWRlc2t0b3BcIik7XHJcblxyXG5cdFx0XHQvLyBNYWtlIHRoaXMgYnV0dG9uICYgdGFiIGFjdGl2ZVxyXG5cdFx0XHR2YXIgSXRlbVBvc2l0aW9uID0gJCh0aGlzKS5wYXJlbnQoKS5pbmRleCgpO1xyXG5cdFx0XHQkKHRoaXMpLnBhcmVudCgpLmFkZENsYXNzKFwiYWN0aXZlLWRlc2t0b3BcIik7XHJcblx0XHRcdCQoXCIuZG4tdGFicy1wYW5lbFwiKS5lcShJdGVtUG9zaXRpb24pLmFkZENsYXNzKFwiYWN0aXZlLWRlc2t0b3BcIik7XHJcblxyXG5cdFx0fSlcclxuXHJcblx0XHQvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cclxuXHRcdC8vIFJlc3AgZnVuY3Rpb25hbGl0eSAvL1xyXG5cdFx0Ly8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXHJcblxyXG5cdFx0Ly8gU2V0IHRoZSBmaXJzdCB0YWIgdG8gYWN0aXZlXHJcblx0XHQkKCcuZG4tdGFicy1wYW5lbDpmaXJzdCAudGFicy1hY2NvcmRpb24taGVhZGVyIGEnKS5hZGRDbGFzcyhcImFjdGl2ZS1yZXNwXCIpO1xyXG5cdFx0JChcIi5kbi10YWJzLXBhbmVsOmZpcnN0IC5kbi10YWJzLWlubmVyXCIpLmFkZENsYXNzKFwiYWN0aXZlLXJlc3BcIik7XHJcblxyXG5cdFx0Ly8gSWYgYSB1c2VyIGNsaWNrcyBvbiBhIHRhYlxyXG5cdFx0JCgnLnRhYnMtYWNjb3JkaW9uLWhlYWRlci1yZXNwIGEnKS5jbGljayhmdW5jdGlvbiAoZSkge1xyXG5cclxuXHRcdFx0Ly8gaWYgdGhpcyBjbGFzcyBpcyBub3QgY3VycmVudGx5IGFjdGl2ZVxyXG5cdFx0XHRpZiAoISQodGhpcykuaGFzQ2xhc3MoXCIuYWN0aXZlLXJlc3BcIikpIHtcclxuXHJcblx0XHRcdFx0Ly8gUHJldmVudCBkZWZhdWx0IGJlaGF2aW91clxyXG5cdFx0XHRcdGUucHJldmVudERlZmF1bHQoKVxyXG5cclxuXHRcdFx0XHQvLyBSZW1vdmUgdGhlIGN1cnJlbnRseSBhY3RpdmUgY2xhc3NcclxuXHRcdFx0XHQkKCcudGFicy1hY2NvcmRpb24taGVhZGVyLXJlc3AgYS5hY3RpdmUtcmVzcCcpLnJlbW92ZUNsYXNzKFwiYWN0aXZlLXJlc3BcIik7XHJcblx0XHRcdFx0JChcIi5kbi10YWJzLWlubmVyLmFjdGl2ZS1yZXNwXCIpLnJlbW92ZUNsYXNzKFwiYWN0aXZlLXJlc3BcIik7XHJcblxyXG5cdFx0XHRcdC8vIE1ha2UgdGhpcyB0aGUgbmV3IGFjdGl2ZSBjbGFzc1xyXG5cdFx0XHRcdCQodGhpcykuYWRkQ2xhc3MoXCJhY3RpdmUtcmVzcFwiKTtcclxuXHRcdFx0XHQkKHRoaXMpLnBhcmVudCgpLnNpYmxpbmdzKFwiLmRuLXRhYnMtaW5uZXJcIikuYWRkQ2xhc3MoXCJhY3RpdmUtcmVzcFwiKTtcclxuXHJcblx0XHRcdH1cclxuXHJcblx0XHR9KVxyXG5cclxuXHR9XHJcblxyXG5cdGZ1bmN0aW9uIFJlZ2V4UmVwbGFjZShBcmdTdHJpbmcpIHtcclxuXHJcblx0XHRyZXR1cm4gQXJnU3RyaW5nLnJlcGxhY2UoL1tcXFdcXHNcXC9dKy9nLCAnXycpO1xyXG5cclxuXHR9XHJcblxyXG5cdFxyXG5cclxuXHRmdW5jdGlvbiBJbnN0YWdyYW1GdW5jdGlvbmFsaXR5KCkge1xyXG5cclxuXHRcdC8vIElmIHRlIGluc3RhZ3JhbSBjb250YWluZXIgaXMgc2hvd24gb24gdGhlIHBhZ2VcclxuXHRcdGlmICgkKFwiI2RuLWluc3RhZmVlZFwiKS5sZW5ndGggPiAwKSB7XHJcblxyXG5cdFx0XHQvLyBNYXggb2YgMTIgYXZhaWxhYmxlXHJcblx0XHRcdHZhciBJbWFnZXNUb1B1bGwgPSA4O1xyXG5cclxuXHRcdFx0alF1ZXJ5LmFqYXgoe1xyXG5cdFx0XHRcdHVybDogZG5fdmFyaWFibGUuYWpheF91cmwsXHJcblx0XHRcdFx0dHlwZTogJ3Bvc3QnLFxyXG5cdFx0XHRcdGRhdGE6IHtcclxuXHRcdFx0XHRcdGFjdGlvbjogJ2RuX2luc3RhX2pzX3B1bGxfZnVuYycsXHJcblx0XHRcdFx0XHRJbWFnZXNUb1B1bGw6IEltYWdlc1RvUHVsbCxcclxuXHRcdFx0XHR9LFxyXG5cdFx0XHRcdHN1Y2Nlc3M6IGZ1bmN0aW9uIChkYXRhKSB7XHJcblxyXG5cdFx0XHRcdFx0Ly8gQ29udmVydCB0aGUganNvbiBkYXRhXHJcblx0XHRcdFx0XHRkYXRhID0gSlNPTi5wYXJzZShkYXRhKTtcclxuXHJcblx0XHRcdFx0XHQvLyBMb29wIHRocm91Z2ggZWFjaCBvZiB0aGUgaW1hZ2VzXHJcblx0XHRcdFx0XHRkYXRhWydpbWFnZXMnXS5mb3JFYWNoKGZ1bmN0aW9uIChlbGVtZW50KSB7XHJcblxyXG5cdFx0XHRcdFx0XHQvLyBDcmVhdGUgc3RyaWcgdG8gYmUgYWRkZWRcclxuXHRcdFx0XHRcdFx0dmFyIHRlbXBfYWRkX3N0cmluZyA9ICc8YSBocmVmPVwiaHR0cHM6Ly93d3cuaW5zdGFncmFtLmNvbS9wLycgKyBlbGVtZW50WydzaG9ydGNvZGUnXSArICdcIiB0YXJnZXQ9XCJfYmxhbmtcIiByZWw9XCJub2ZvbGxvd1wiIGNsYXNzPVwiaW5zdGEtaW1hZ2Ugc3BlY2lhbC1saW5rXCIgc3R5bGU9XCJiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJyArIGVsZW1lbnRbJ2ltYWdlJ10gKyAnKTtcIj48L2E+JztcclxuXHJcblx0XHRcdFx0XHRcdCQoXCIjZG4taW5zdGFmZWVkXCIpLmFwcGVuZCh0ZW1wX2FkZF9zdHJpbmcpO1xyXG5cclxuXHRcdFx0XHRcdH0pO1xyXG5cclxuXHRcdFx0XHR9LFxyXG5cdFx0XHRcdGVycm9yOiBmdW5jdGlvbiAoZGF0YSkge1xyXG5cdFx0XHRcdFx0Y29uc29sZS5sb2coXCJBSkFYIHJlcXVlc3QgZmFpbGVkXCIpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0fVxyXG5cclxuXHR9XHJcblxyXG5cdGZ1bmN0aW9uIENvb2tpZXNGb290ZXJGdW5jdGlvbmFsaXR5KCkge1xyXG5cclxuXHRcdCQoXCIjY29va2llcy1mb290ZXItbm90aWNlIGEuZG4tYnV0dG9uXCIpLmNsaWNrKGZ1bmN0aW9uKGUpe1xyXG5cclxuXHRcdFx0Ly8gU3RvcCB0aGUgYnV0dG9uIGZyb20gZG9pbmcgaXQncyB0aGluZ2dnXHJcblx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcblx0XHRcdC8vIEFkZCBhIGNvb2tpZSB0byB0aGUgcGFnZVxyXG5cdFx0XHRzZXRDb29raWUoIFwic2l0ZS1jb29raWVzLWFncmVlbWVudFwiLCBcInNpdGUtY29va2llcy1hZ3JlZW1lbnQtYWdyZWVkXCIsIDk5ICk7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBNYWtlIGl0IGRpc3NhcGVhclxyXG5cdFx0XHQkKFwiI2Nvb2tpZXMtZm9vdGVyLW5vdGljZVwiKS5zbGlkZVVwKCk7XHJcblx0XHRcdFxyXG5cdFx0fSk7XHJcblxyXG5cdH1cclxuXHJcblx0ZnVuY3Rpb24gc2V0Q29va2llKGNuYW1lLCBjdmFsdWUsIGV4ZGF5cykge1xyXG5cdFx0dmFyIGQgPSBuZXcgRGF0ZSgpO1xyXG5cdFx0ZC5zZXRUaW1lKGQuZ2V0VGltZSgpICsgKGV4ZGF5cyoyNCo2MCo2MCoxMDAwKSk7XHJcblx0XHR2YXIgZXhwaXJlcyA9IFwiZXhwaXJlcz1cIisgZC50b1VUQ1N0cmluZygpO1xyXG5cdFx0ZG9jdW1lbnQuY29va2llID0gY25hbWUgKyBcIj1cIiArIGN2YWx1ZSArIFwiO1wiICsgZXhwaXJlcyArIFwiO3BhdGg9L1wiO1xyXG5cdH1cclxuXHJcbn0pO1xyXG5cclxuLyoqXHJcbiAgICogRE4gZmVlIHVwZGF0ZSBvbiBjYXJ0IHBhZ2UgdXNpbmcgYWpheFxyXG4gICAqL1xyXG4gIChmdW5jdGlvbiAoJCkge1xyXG4gICAgJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xyXG4gICAgICAkKGRvY3VtZW50KS5vbignY2hhbmdlIGtleXVwIHBhc3RlJywnLndvb2NvbW1lcmNlLWNhcnQgaW5wdXQuZGVsaXZlcnlfYXNzZW1ibHknLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgJCgnLmNhcnRfdG90YWxzJykuYmxvY2soe1xyXG4gICAgICAgICAgbWVzc2FnZTogbnVsbCxcclxuICAgICAgICAgIG92ZXJsYXlDU1M6IHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogJyNmZmYnLFxyXG4gICAgICAgICAgICBvcGFjaXR5OiAwLjZcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICB2YXIgY2FydF9pZCA9ICQodGhpcykuZGF0YSgnY2FydC1pZCcpO1xyXG4gICAgICAgIGlmIChqUXVlcnkodGhpcykucHJvcChcImNoZWNrZWRcIikgPT0gdHJ1ZSkge1xyXG4gICAgICAgICAgdmFyIGRuID0gJ29uJztcclxuICAgICAgICB9IGVsc2UgaWYgKGpRdWVyeSh0aGlzKS5wcm9wKFwiY2hlY2tlZFwiKSA9PSBmYWxzZSkge1xyXG4gIFxyXG4gICAgICAgICAgdmFyIGRuID0gJyc7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHZhciBjYXJ0X25vdGVzID0gJCgnI2NhcnRfbm90ZXNfJyArIGNhcnRfaWQpLnZhbCgpO1xyXG4gICAgICAgIHZhciB3cF9hamF4X3VybCA9IGRuX3ZhcmlhYmxlLmFqYXhfdXJsO1xyXG4gICAgICAgIHZhciBkYXRhID0ge1xyXG4gICAgICAgICAgYWN0aW9uOiAncHJlZml4X3VwZGF0ZV9jYXJ0X25vdGVzJyxcclxuICAgICAgICAgICdzZWN1cml0eSc6IGRuLFxyXG4gICAgICAgICAgJ25vdGVzJzogY2FydF9ub3RlcyxcclxuICAgICAgICAgICdjYXJ0X2lkJzogY2FydF9pZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgalF1ZXJ5LnBvc3Qod3BfYWpheF91cmwsIGRhdGEsIGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xyXG4gICAgICAgICAgbG9jYXRpb24ucmVsb2FkKCk7XHJcbiAgICAgICAgICAvLyQoJy5jYXJ0X3RvdGFscycpLnVuYmxvY2soKTtcclxuICAgICAgICB9KTtcclxuICBcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9KShqUXVlcnkpOyIsIi8vIEdNYXAgU3R5bGVcclxuLy8gUGxlYXNlIHJlZmVyIHRvIHRoaXMgc2l0ZTogaHR0cHM6Ly9zbmF6enltYXBzLmNvbS9cclxudmFyIEdNYXBTdHlsZSA9IFtdO1xyXG5cclxualF1ZXJ5KGZ1bmN0aW9uKCQpe1xyXG4gICAgXHJcblxyXG5cdC8vIEFkZCBHb29nbGUgTWFwXHJcblx0JCgnLmdtYXAnKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHRcdGluaXRNYXAoJCh0aGlzKS5kYXRhKFwibGF0XCIpLCAkKHRoaXMpLmRhdGEoXCJsbmdcIiksICQodGhpcykuZGF0YShcImljb25cIiksICQodGhpcykgKTtcclxuXHR9KTtcclxuXHJcblx0XHJcblxyXG5cdGZ1bmN0aW9uIGluaXRNYXAobGF0LCBsbmcsIGljb25fdXJsLCAkY29udGFpbmVyKSB7XHJcblxyXG5cdFx0dmFyIHZpZXdDZW50cmVMYXQgPSBsYXQ7XHJcblx0XHR2YXIgdmlld0NlbnRyZUxuZyA9IGxuZztcclxuXHRcdGlmICgkKHdpbmRvdykud2lkdGgoKSA+IDk5MSkge1xyXG5cdFx0XHQvLyB2aWV3Q2VudHJlTGF0IC09IDAuMDAzNTtcclxuXHRcdFx0Ly8gdmlld0NlbnRyZUxuZyArPSAwLjAxNTtcclxuXHRcdH1cclxuXHRcdHZhciBtYXBDbGFzcyA9ICRjb250YWluZXJcclxuXHRcdHZhciBtYXAgPSBuZXcgZ29vZ2xlLm1hcHMuTWFwKCBtYXBDbGFzc1swXSwge1xyXG5cdFx0XHRzY3JvbGx3aGVlbDogZmFsc2UsXHJcblx0XHRcdHpvb206IDE1LFxyXG5cdFx0XHRzdHlsZXM6IEdNYXBTdHlsZSxcclxuXHRcdFx0Y2VudGVyOiBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKHZpZXdDZW50cmVMYXQsIHZpZXdDZW50cmVMbmcpLFxyXG5cdFx0XHRtYXBUeXBlSWQ6ICdyb2FkbWFwJyxcclxuXHRcdFx0ZGlzYWJsZURlZmF1bHRVSTogdHJ1ZVxyXG5cdFx0fSk7XHJcblxyXG5cdFx0Ly8gRGlzYWJsZSBtYXAgZHJhZyBvbiBtb2JpbGVcclxuXHRcdC8vIGlmICggJCh3aW5kb3cpLndpZHRoKCkgPCA5OTIgKSB7XHJcblx0XHQvLyBtYXAuc2V0T3B0aW9ucyh7ZHJhZ2dhYmxlOiBmYWxzZX0pO1xyXG5cdFx0Ly8gfVxyXG5cclxuXHRcdHZhciBJY29uU2l6ZSA9IG5ldyBnb29nbGUubWFwcy5TaXplKDU1LCA3Mik7XHJcblx0XHRpZiAoJCh3aW5kb3cpLndpZHRoKCkgPCA5OTIpIHtcclxuXHRcdFx0SWNvblNpemUgPSBuZXcgZ29vZ2xlLm1hcHMuU2l6ZSgzOSwgNTIpO1xyXG5cdFx0fVxyXG5cclxuXHRcdHZhciBmZWF0dXJlID0ge1xyXG5cdFx0XHRwb3NpdGlvbjogbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhsYXQsIGxuZyksXHJcblx0XHR9O1xyXG5cdFx0dmFyIGljb24gPSB7XHJcblx0XHRcdHVybDogaWNvbl91cmwsXHJcblx0XHRcdHNjYWxlZFNpemU6IEljb25TaXplLFxyXG5cdFx0fTtcclxuXHRcdHZhciBtYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHtcclxuXHRcdFx0cG9zaXRpb246IGZlYXR1cmUucG9zaXRpb24sXHJcblx0XHRcdGljb246IGljb24sXHJcblx0XHRcdHNjYWxlZFNpemU6IG5ldyBnb29nbGUubWFwcy5TaXplKDEwLCAxMCksXHJcblx0XHRcdG1hcDogbWFwXHJcblx0XHR9KTtcclxuXHJcblx0XHQvLyBjcmVhdGUgaW5mbyB3aW5kb3dcclxuXHRcdHZhciBpbmZvd2luZG93ID0gbmV3IGdvb2dsZS5tYXBzLkluZm9XaW5kb3coe1xyXG5cdFx0XHRjb250ZW50OiAkY29udGFpbmVyLnNpYmxpbmdzKFwiLmJ1YmJsZS1pbmZvXCIpLmh0bWwoKSxcclxuXHRcdH0pO1xyXG5cclxuXHRcdC8vIHNob3cgaW5mbyB3aW5kb3cgd2hlbiBtYXJrZXIgaXMgY2xpY2tlZFxyXG5cdFx0Z29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIobWFya2VyLCAnY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcblxyXG5cdFx0XHRpbmZvd2luZG93Lm9wZW4obWFwLCBtYXJrZXIpO1xyXG5cclxuXHRcdH0pO1xyXG5cdH1cclxufSkiLCJqUXVlcnkoZnVuY3Rpb24oJCkge1xyXG5cdC8vIEhlcm8gaW1hZ2UgcmVwZWF0ZXIgY29udGFpbmVyXHJcblx0aWYgKCQoXCIuZG4taW1hZ2UtaGVyby1yZXBlYXRzXCIpLmxlbmd0aCA+IDApIHtcclxuICBcclxuXHQgIC8vIENyZWF0ZSBsaWdodGJveGVzXHJcblx0ICAkKCcuZG4taW1hZ2UtaGVyby1yZXBlYXRzIC5pbWFnZS1jb250YWluZXInKS5tYWduaWZpY1BvcHVwKHtcclxuXHRcdHR5cGU6ICdpbWFnZScsXHJcblx0XHRnYWxsZXJ5OiB7XHJcblx0XHQgIGVuYWJsZWQ6IHRydWVcclxuXHRcdH0sXHJcblx0XHRjYWxsYmFja3M6IHtcclxuXHRcdCAgZWxlbWVudFBhcnNlOiBmdW5jdGlvbihpdGVtKSB7XHJcblx0XHRcdC8vIHRoZSBjbGFzcyBuYW1lXHJcbiAgXHJcblx0XHRcdGlmICgkKGl0ZW0uZWwuY29udGV4dCkuaGFzQ2xhc3MoJ3ZpZGVvLWxpbmsnKSkge1xyXG5cdFx0XHQgIGl0ZW0udHlwZSA9ICdpZnJhbWUnO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHQgIGl0ZW0udHlwZSA9ICdpbWFnZSc7XHJcblx0XHRcdH1cclxuXHRcdCAgfVxyXG5cdFx0fSxcclxuXHQgIH0pO1xyXG4gIFxyXG5cdCAgLy8gRG9uJ3QgcnVuIHRoZSBzbGlkZXIgaWZcclxuXHQgIGlmICgkKHdpbmRvdykud2lkdGgoKSA+PSA3NjgpIHsgLy8gRGVza3RvcFxyXG5cdFx0Ly8gSWYgdGhlcmUncyBsZXNzIHRoYW4gOCBpbWFnZXNcclxuXHRcdGlmICgkKFwiLmRuLWltYWdlLWhlcm8tcmVwZWF0c1wiKS5jaGlsZHJlbigpLmxlbmd0aCA8IDUpIHtcclxuXHRcdCAgcmV0dXJuO1xyXG5cdFx0fVxyXG5cdCAgfSBlbHNlIHsgLy8gTW9iaWxlXHJcblx0XHQvLyBJZiB0aGVyZSdzIG9ubHkgMSBpbWFnZVxyXG5cdFx0aWYgKCQoXCIuZG4taW1hZ2UtaGVyby1yZXBlYXRzXCIpLmNoaWxkcmVuKCkubGVuZ3RoIDwgMikge1xyXG5cdFx0ICByZXR1cm47XHJcblx0XHR9XHJcblx0ICB9XHJcbiAgXHJcblx0ICAvLyBEZXRlcm1pbmUgdGhlIHR5cGUgb2Ygc2xpZGVyIGl0IHdpbGwgYmVcclxuXHQgIHZhciBJbWFnZUhlcm9Db3VudCA9ICQoXCIuZG4taW1hZ2UtaGVyby1yZXBlYXRzXCIpLmNoaWxkcmVuKCkubGVuZ3RoO1xyXG5cdCAgdmFyIEhlcm9JbWFnZXJvd3MgPSAyO1xyXG5cdCAgLy8gSWYgdGhlcmUgYXJlIHZlcnkgZmV3IGltYWdlcyBvciB3ZSBhcmUgb24gbW9iaWxlIGdvIGZvciBhIHNpbmdsZSByb3dcclxuXHQgIGlmIChJbWFnZUhlcm9Db3VudCA8IDggfHwgJCh3aW5kb3cpLndpZHRoKCkgPCA3NjgpIHtcclxuXHRcdEhlcm9JbWFnZXJvd3MgPSAxO1xyXG5cdCAgfVxyXG4gIFxyXG5cdCAgLy8gUmVtb3ZlIHRoZSBkZWZhdWx0IGZsZXggY29udGFpbmVyXHJcblx0ICAkKFwiLmRuLWltYWdlLWhlcm8tcmVwZWF0c1wiKS5yZW1vdmVDbGFzcyhcImZsZXgtY29udGFpbmVyXCIpO1xyXG4gIFxyXG5cdCAgaWYgKEhlcm9JbWFnZXJvd3MgPiAxKSB7XHJcbiAgXHJcblx0XHQvLyBTZXR1cCB0aGUgc2xpZGVyXHJcblx0XHQkKCcuZG4taW1hZ2UtaGVyby1yZXBlYXRzJykuc2xpY2soe1xyXG5cdFx0ICBcInJvd3NcIjogMixcclxuXHRcdCAgXCJzbGlkZXNQZXJSb3dcIjogNCxcclxuXHRcdCAgXCJzbGlkZXNUb1Njcm9sbFwiOiAxIC8gNCxcclxuXHRcdCAgaW5maW5pdGU6IHRydWUsXHJcblx0XHQgIHByZXZBcnJvdzogXCI8aSBjbGFzcz0naWNvbi1jaGV2cm9uLWxlZnQgc2xpY2stcHJldic+PC9pPlwiLFxyXG5cdFx0ICBuZXh0QXJyb3c6IFwiPGkgY2xhc3M9J2ljb24tY2hldnJvbi1yaWdodCBzbGljay1uZXh0Jz48L2k+XCIsXHJcblx0XHQgIHJlc3BvbnNpdmU6IFt7XHJcblx0XHRcdCAgYnJlYWtwb2ludDogMTE5OSxcclxuXHRcdFx0ICBzZXR0aW5nczoge1xyXG5cdFx0XHRcdFwic2xpZGVzUGVyUm93XCI6IDMsXHJcblx0XHRcdFx0XCJzbGlkZXNUb1Njcm9sbFwiOiAxIC8gMyxcclxuXHRcdFx0ICB9XHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0ICBicmVha3BvaW50OiA5OTEsXHJcblx0XHRcdCAgc2V0dGluZ3M6IHtcclxuXHRcdFx0XHRcInNsaWRlc1BlclJvd1wiOiAyLFxyXG5cdFx0XHRcdFwic2xpZGVzVG9TY3JvbGxcIjogMSAvIDIsXHJcblx0XHRcdCAgfVxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdCAgYnJlYWtwb2ludDogNzY3LFxyXG5cdFx0XHQgIHNldHRpbmdzOiB7XHJcblx0XHRcdFx0XCJzbGlkZXNQZXJSb3dcIjogMSxcclxuXHRcdFx0XHRcInNsaWRlc1RvU2Nyb2xsXCI6IDEsXHJcblx0XHRcdCAgfVxyXG5cdFx0XHR9LFxyXG5cdFx0ICBdXHJcblx0XHR9KTtcclxuICBcclxuXHQgIH0gZWxzZSB7XHJcbiAgXHJcblx0XHQvLyBTZXR1cCB0aGUgc2xpZGVyXHJcblx0XHQkKCcuZG4taW1hZ2UtaGVyby1yZXBlYXRzJykuc2xpY2soe1xyXG5cdFx0ICBcInNsaWRlc1RvU2hvd1wiOiA0LFxyXG5cdFx0ICBcInNsaWRlc1RvU2Nyb2xsXCI6IDEsXHJcblx0XHQgIGluZmluaXRlOiB0cnVlLFxyXG5cdFx0ICBwcmV2QXJyb3c6IFwiPGkgY2xhc3M9J2ljb24tY2hldnJvbi1sZWZ0IHNsaWNrLXByZXYnPjwvaT5cIixcclxuXHRcdCAgbmV4dEFycm93OiBcIjxpIGNsYXNzPSdpY29uLWNoZXZyb24tcmlnaHQgc2xpY2stbmV4dCc+PC9pPlwiLFxyXG5cdFx0ICByZXNwb25zaXZlOiBbe1xyXG5cdFx0XHQgIGJyZWFrcG9pbnQ6IDExOTksXHJcblx0XHRcdCAgc2V0dGluZ3M6IHtcclxuXHRcdFx0XHRcInNsaWRlc1RvU2hvd1wiOiAzLFxyXG5cdFx0XHQgIH1cclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHQgIGJyZWFrcG9pbnQ6IDk5MSxcclxuXHRcdFx0ICBzZXR0aW5nczoge1xyXG5cdFx0XHRcdFwic2xpZGVzVG9TaG93XCI6IDIsXHJcblx0XHRcdCAgfVxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdCAgYnJlYWtwb2ludDogNzY3LFxyXG5cdFx0XHQgIHNldHRpbmdzOiB7XHJcblx0XHRcdFx0XCJzbGlkZXNUb1Nob3dcIjogMSxcclxuXHRcdFx0ICB9XHJcblx0XHRcdH0sXHJcblx0XHQgIF1cclxuXHRcdH0pO1xyXG4gIFxyXG5cdCAgfVxyXG5cdH1cclxufSlcclxuICAiLCJqUXVlcnkoZnVuY3Rpb24oJCkge1xyXG5cclxuICAgICQoXCIudGVzdGltb25pYWwtc2xpZGVyXCIpLmVhY2goZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAkKHRoaXMpLnNsaWNrKHtcclxuICAgICAgICBhcnJvd3M6IGZhbHNlLFxyXG4gICAgICAgIGRvdHM6IHRydWUsXHJcbiAgICAgICAgaW5maW5pdGU6IHRydWUsXHJcbiAgICAgICAgYXV0b3BsYXk6IGZhbHNlLFxyXG4gICAgICAgIGFkYXB0aXZlSGVpZ2h0OiB0cnVlXHJcbiAgICAgIH0pO1xyXG4gIFxyXG4gICAgfSlcclxuICBcclxuICB9KVxyXG4gICIsImpRdWVyeShmdW5jdGlvbigkKXtcclxuICBcclxuICAgICAgICAkKCcudGV4dC1nYWxsZXJ5LXNsaWRlcicpLmVhY2goZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHZhciAkcmVzcG9uc2l2ZSA9IFtdO1xyXG4gICAgICAgICAgICB2YXIgJHRoaXMgPSAkKHRoaXMpXHJcblxyXG4gICAgICAgICAgICAvLyByZXNwb25zaXZlIG9wdGlvblxyXG4gICAgICAgICAgICBpZigkdGhpcy5oYXNDbGFzcygncmVzcG9uc2l2ZS1jZW50ZXJlZCcpKXtcclxuICAgICAgICAgICAgICAgICRyZXNwb25zaXZlID0gW1xyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWtwb2ludDogOTkxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZG90czogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcnJvd3M6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaW5maW5pdGU6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXV0b3BsYXk6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjZW50ZXJNb2RlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICR0aGlzLnNsaWNrKHtcclxuICAgICAgICAgICAgICAgIGFycm93czogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBkb3RzOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgaW5maW5pdGU6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBhdXRvcGxheTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGFkYXB0aXZlSGVpZ2h0OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHJlc3BvbnNpdmU6ICRyZXNwb25zaXZlXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLyogJHRoaXMuc2xpY2soe1xyXG4gICAgICAgICAgICAgICAgZG90czogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBhcnJvd3M6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgaW5maW5pdGU6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgYXV0b3BsYXk6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBjZW50ZXJNb2RlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgcmVzcG9uc2l2ZTogW1xyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWtwb2ludDogOTk5OSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2V0dGluZ3M6IFwidW5zbGlja1wiXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDk5MSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2V0dGluZ3M6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICB9KTsgKi9cclxuICAgICAgICB9KVxyXG4gICAgXHJcbn0pIl0sInNvdXJjZVJvb3QiOiIifQ==