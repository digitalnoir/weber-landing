/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./wp-content/themes/weber-landing/assets/src/js/admin.js":
/*!****************************************************************!*\
  !*** ./wp-content/themes/weber-landing/assets/src/js/admin.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

//import './plugins/tinymce.js'
jQuery(function ($) {
  $(document).ready(function () {
    FocalPointControls();
  });
  $(document).on('mouseenter', '.acf-fc-popup li a', function () {
    var $this = $(this);
    var parent = $this.parents('.acf-fc-popup');
    var filename = $this.attr('data-layout');

    if (parent.find('.preview').length > 0) {
      parent.find('.preview').html('<div class="inner-preview"><img src="' + theme_var.upload + filename + '.jpg"/></div>');
    } else {
      parent.append('<div class="preview"><div class="inner-preview"><img src="' + theme_var.upload + filename + '.jpg"/></div></div>');
    }
  });
  $(document).on('mouseleave', '.acf-fc-popup', function () {
    $(this).find('.preview').empty();
  });

  function FocalPointControls() {
    // initialized on ACF events
    function initialize_field($el) {
      // Cache jquery selectors
      // Values to get/set
      var $id = $el.find('.acf-focal_point-id'),
          $top = $el.find('.acf-focal_point-top'),
          $left = $el.find('.acf-focal_point-left'),
          $right = $el.find('.acf-focal_point-right'),
          $bottom = $el.find('.acf-focal_point-bottom'),
          // Elements to get/set 
      $fp = $el.find('.acf-focal_point'),
          $img = $el.find('.acf-focal_point-image'),
          $canvas = $el.find('.acf-focal_point-canvas'),
          // Buttons to trigger events
      $add = $el.find('.add-image'),
          $del = $el.find('.acf-button-delete'); // Hold/get our values

      var values = {
        id: $id.val(),
        top: $top.val(),
        left: $left.val(),
        width: $right.val(),
        height: $bottom.val(),
        size: $fp.data('preview_size')
      }; // DOM elements

      var img = $img.get(0),
          canvas = $canvas.get(0); // To hold WP media frame

      var file_frame; // Vars for Canvas work

      var ctx = canvas.getContext("2d"),
          rect = {},
          mouseDown = false,
          canvasWidth,
          canvasHeight; // When we've loaded an image, draw the canvas.
      // (either on dom load or adding new image from WP media manager)

      $img.on("load", drawCanvas).each(function () {
        // Make sure to trigger load event by triggering load
        // after jquery has done it's iteration
        if (this.complete) {
          $(this).load();
        }
      }); // When resizing the page, redraw the canvas.

      $(window).on('resize', drawCanvas); // When we click the add image button...

      $add.on('click', function () {
        // If the media frame already exists, reopen it.
        if (file_frame) {
          file_frame.open();
          return;
        } // Create the media frame.


        file_frame = wp.media.frames.file_frame = wp.media({
          title: 'Select Image',
          button: {
            text: 'Select'
          }
        }); // When an image is selected..

        file_frame.on('select', function () {
          // Get selected image objects
          var attachment = file_frame.state().get('selection').first().toJSON(),
              src = attachment.sizes[values.size];
          console.log(attachment); // Make UI active (hide add image button, show canvas)

          $fp.addClass('active');

          if (src === undefined) {
            src = attachment;
          } // Set image to new src, triggering on load


          $img.attr('src', src.url); // Update our post values and values obj

          $id.val(attachment.id);
          values.id = attachment.id;
        }); // Finally, open the modal

        file_frame.open();
      }); // When we click the delete image button...

      $del.on('click', function () {
        // Reset DOM image attributes
        $img.removeAttr('src width height'); // Hide canvas and show add image button

        $fp.removeClass('active'); // Reset our post values

        $id.val('');
        $top.val('');
        $left.val('');
        $right.val('');
        $bottom.val(''); // And our values obj, but just one value (to check later) will do.

        values.top = null;
      }); // When we click and move on the canvas...

      canvas.addEventListener("mousemove", function (e) {
        if (mouseDown) {
          ApplyPointOvCanvas(e);
        }
      }, false);
      canvas.addEventListener("mousedown", function (e) {
        mouseDown = true;
        ApplyPointOvCanvas(e);
      }, false);
      canvas.addEventListener("mouseup", function () {
        mouseDown = false;
      }, false);

      function ApplyPointOvCanvas(e) {
        // Keep drawing image as bottom layer 
        // (otherwise we get multiple layers of the focus, making it opaque)
        drawImg(); // Set the point of the click

        rect.startX = e.layerX;
        rect.startY = e.layerY; // Create the distance we want from the click point

        rect.w = 8;
        rect.h = 8; // console.log( values.left );
        // console.log( values.top );
        // Put positions in our values object

        values.top = rect.startY / canvasHeight;
        values.left = rect.startX / canvasWidth;
        values.width = (rect.w + rect.startX) / canvasWidth;
        values.height = (rect.h + rect.startY) / canvasHeight; // Set post values

        $top.val(values.top.toFixed(2));
        $left.val(values.left.toFixed(2));
        $right.val(values.width.toFixed(2));
        $bottom.val(values.height.toFixed(2)); // draw focal point

        drawFocus(rect.startX, rect.startY, rect.w, rect.h);
      } //////////////////////////////////////
      // Old functions allow for dragging //
      //////////////////////////////////////
      // // When we click on canvas...
      // canvas.addEventListener("mousedown", function(e) {
      // 	// Track our position
      //     rect.startX = e.layerX;
      //     rect.startY = e.layerY;
      //     // And allow drawing
      //     mouseDown 	= true;
      // }, false);
      // When we stopped holding down mouse button, prevent further drawing.
      // canvas.addEventListener("mouseup", function() { mouseDown = false; }, false);
      // // When mouse button is down and we're moving the mouse
      // canvas.addEventListener("mousemove", function(e) {
      //     if (mouseDown) {
      //     	// Keep drawing image as bottom layer 
      //     	// (otherwise we get multiple layers of the focus, making it opaque)
      //         drawImg();
      //         // Get distance from when we first clicked on canvas
      //         rect.w 			= (e.layerX) - rect.startX;
      //         rect.h 			= (e.layerY) - rect.startY;
      //         // Put positions in our values object
      //         values.top 		= rect.startY / canvasHeight;
      //         values.left 	= rect.startX / canvasWidth;
      //         values.width 	= (rect.w + rect.startX) / canvasWidth;
      //         values.height 	= (rect.h + rect.startY) / canvasHeight;
      //         // Set post values
      //     	$top.val(values.top.toFixed(2));
      //     	$left.val(values.left.toFixed(2));
      //     	$right.val(values.width.toFixed(2));
      //     	$bottom.val(values.height.toFixed(2));
      //     	// draw focal point
      //         drawFocus(rect.startX, rect.startY, rect.w, rect.h);
      //     }
      // }, false);
      // Used to draw the image onto the canvas


      function drawImg() {
        // Ratios previously worked out (resizeCanvas), so it should fill canvas
        ctx.drawImage(img, 0, 0, canvasWidth, canvasHeight);
      } // Used to draw focal point on canvas


      function drawFocus(x, y, w, h) {
        // always ensure the value is > 0
        var x = Math.abs(x);
        var y = Math.abs(y);
        var w = Math.abs(w);
        var h = Math.abs(h);
        ctx.strokeStyle = "rgba(255, 255, 255, 0.8)";
        ctx.fillStyle = "rgba(164, 56, 56, 0.8";
        ctx.beginPath();
        ctx.arc(x, y, w, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke(); // ctx.strokeRect(x, y, w, h);
        // ctx.fillRect(x, y, w, h);
      } // Used to draw focal point on load


      function redrawFocus() {
        // if existing values set...
        if (values.top !== null) {
          // Get our positions
          var x = values.left * canvasWidth,
              y = values.top * canvasHeight,
              w = values.width * canvasWidth - x,
              h = values.height * canvasHeight - y; // draw focual point

          drawFocus(x, y, w, h);
        }
      } // Shortcut to calling canvas draw functions


      function drawCanvas() {
        // resize, redraw, refocus
        resizeCanvas();
        drawImg();
        redrawFocus();
      } // Used to clear canvas


      function clearCanvas() {
        // Faster than clearRect
        ctx.fillStyle = "#ffffff";
        ctx.fillRect(0, 0, canvasWidth, canvasHeight);
      } // Used to set up canvas sizing


      function resizeCanvas() {
        // Get natural imge sizes
        var natural_width = img.naturalWidth,
            natural_height = img.naturalHeight,
            // Get image width/height ratio
        ratio = natural_width / natural_height,
            // Get parent width (annoyingly, we have to account for delete button)
        parent_width = $el.parent().width() - $del.width() / 2,
            // tweak hidden element - if you using tab
        parent_width = parent_width < 0 ? natural_width : parent_width,
            // To hold new canvas widths
        new_width,
            new_height; // If image is naturally bigger than parent...

        if (natural_width > parent_width) {
          // Set to full width (same as parent)
          new_width = parent_width; // And use ratio to work out new proportional height

          new_height = parent_width / ratio; // Otherwise...
        } else {
          // Set to same width/height as image
          new_width = natural_width;
          new_height = natural_height;
        } // Set canvas DOM width


        $canvas.width(new_width);
        $canvas.height(new_height); // And canvas attribute widths 
        // (otherwise it gets a weird coord system)

        canvas.width = new_width;
        canvas.height = new_height; // Remember our new sizes

        canvasWidth = new_width;
        canvasHeight = new_height;
      }
    }

    if (typeof acf.add_action !== 'undefined') {
      acf.add_action('load', function ($el) {
        // search $el for fields of type 'focal_point'
        acf.get_fields({
          type: 'focal_point'
        }, $el).each(function () {
          initialize_field($(this));
        });
      });
      acf.add_action('append', function ($el) {
        // search $el for fields of type 'focal_point'
        acf.get_fields({
          type: 'focal_point'
        }, $el).each(function () {
          initialize_field($(this));
        });
      });
    } else {
      /*
      *  acf/setup_fields (ACF4)
      *
      *  This event is triggered when ACF adds any new elements to the DOM. 
      *
      *  @type	function
      *  @since	1.0.0
      *  @date	01/01/12
      *
      *  @param	event		e: an event object. This can be ignored
      *  @param	Element		postbox: An element which contains the new HTML
      *
      *  @return	n/a
      */
      $(document).on('acf/setup_fields', function (e, postbox) {
        $(postbox).find('.field[data-field_type="focal_point"]').each(function () {
          initialize_field($(this));
        });
      });
    }
  } // resend pronto xml


  $(document).on('click', 'button[name="resend-to-pronto"]', function (e) {
    e.preventDefault();
    var paren = $(this).parents('.inside');
    paren.addClass('loading');
    var order_id = $(this).attr('data-order-id');
    $.ajax({
      url: ajaxurl,
      type: 'POST',
      data: {
        action: 'weber_re_push_xml_pronto',
        order_id: order_id
      },
      cache: false,
      dataType: 'json',
      beforeSend: function beforeSend(jqXHR, settings) {
        $('#resend-to-pronto-results').html('');
      },
      success: function success(data, textStatus, jqXHR) {
        if (data.result == 'error') {
          paren.removeClass('loading');
          $('#resend-to-pronto-results').html('<div class="inner">' + data.message + '</div>');
        } else {
          window.location.reload();
        }
      },
      complete: function complete(jqXHR, textStatus) {//console.log( 'AJAX is complete.' ); 
      }
    }); // End AJAX.
  }); // ACF Weber Create Code

  if ($(document).find('input[name="_w_start_date"]').length > 0) {
    $(document).find('input[name="_w_start_date"]').datepicker({
      dateFormat: "yy-mm-dd",
      minDate: '-1W',
      onSelect: function onSelect() {
        var dt2 = $(document).find('input[name="_w_end_date"]');
        var startDate = $(this).datepicker('getDate'); //add 30 days to selected date

        startDate.setDate(startDate.getDate() + 30);
        var minDate = $(this).datepicker('getDate');
        var dt2Date = dt2.datepicker('getDate'); //difference in days. 86400 seconds in day, 1000 ms in second

        var dateDiff = (dt2Date - minDate) / (86400 * 1000); //dt2 not set or dt1 date is greater than dt2 date

        if (dt2Date == null || dateDiff < 0) {
          dt2.datepicker('setDate', minDate);
        } //dt1 date is 30 days under dt2 date
        else if (dateDiff > 30) {
            dt2.datepicker('setDate', startDate);
          } //sets dt2 maxDate to the last day of 30 days window
        //dt2.datepicker('option', 'maxDate', startDate);
        //first day which can be selected in dt2 is selected date in dt1


        dt2.datepicker('option', 'minDate', minDate);
      }
    });
    $(document).find('input[name="_w_end_date"]').datepicker({
      dateFormat: "yy-mm-dd",
      minDate: 0
    });
  } // on click submit

// Edit Start  //
  $(document).on('click', '#weber-business-codes-add #generate-code', function (e) {
    e.preventDefault();
    var $this = $(this);
    $this.closest('tr').find('input.error').removeClass('error');
    $this.closest('tr').find('input').filter(function () {
      return !this.value;
    }).addClass('error');

    if ($this.closest('tr').find('input.error').length > 0) {
      return;
    }

    var $parent = $(document).find('#weber-business-codes'); // do ajax

    $.ajax({
      url: ajaxurl,
      type: 'POST',
      data: {
        action: 'weber_do_generate_business_code',
        name: $(document).find('input[name="_w_campaign_name"]').val(),
        start_date: $(document).find('input[name="_w_start_date"]').val(),
        end_date: $(document).find('input[name="_w_end_date"]').val(),
        amount: $(document).find('input[name="_w_total_code"]').val(),
        post_id: $this.attr('data-id')
      },
      cache: false,
      dataType: 'json',
      beforeSend: function beforeSend(jqXHR, settings) {
        $parent.addClass('loading');
      },
      success: function success(data, textStatus, jqXHR) {
        $parent.removeClass('loading');
        $(document).find('#replace--results').replaceWith(data.html);
        $this.closest('tr').find('input').val('');
        $('#add-campain-wrap').remove();
      },
      complete: function complete(jqXHR, textStatus) {
        console.log('AJAX is complete.');
      }
    }); // End AJAX.
  }); // pause or delete campaign

  $(document).on('click', '.campaign-action-btn', function (e) {
    e.preventDefault();
    var action = $(this).attr('data-action');
    var theID = $(this).attr('data-id');
    var partner_id = $(this).attr('data-partner');
    var is_sure = action == 'delete' ? confirm('This action cannot be reversible, all generated access code will be deleted from the database. Are you sure want to do this? ') : confirm('Are you sure want to do this?');
    var $parent = $(document).find('#weber-business-codes');

    if (is_sure) {
      // do ajax
      $.ajax({
        url: ajaxurl,
        type: 'POST',
        data: {
          action: 'weber_business_code_action',
          run_action: action,
          campaign_id: theID,
          partner_id: partner_id
        },
        cache: false,
        dataType: 'json',
        beforeSend: function beforeSend(jqXHR, settings) {
          $parent.addClass('loading');
        },
        success: function success(data, textStatus, jqXHR) {
          $parent.removeClass('loading');
          $(document).find('#replace--results').replaceWith(data.html); //$this.closest('tr').find('input').val('')
          if(data.noaddnew==1){$('#add-campain-wrap').remove();}
          else{
              $parent.append(data.html2);
              if ($(document).find('input[name="_w_start_date"]').length > 0) {
                $(document).find('input[name="_w_start_date"]').datepicker({
                  dateFormat: "yy-mm-dd",
                  minDate: '-1W',
                  onSelect: function onSelect() {
                    var dt2 = $(document).find('input[name="_w_end_date"]');
                    var startDate = $(this).datepicker('getDate'); //add 30 days to selected date

                    startDate.setDate(startDate.getDate() + 30);
                    var minDate = $(this).datepicker('getDate');
                    var dt2Date = dt2.datepicker('getDate'); //difference in days. 86400 seconds in day, 1000 ms in second

                    var dateDiff = (dt2Date - minDate) / (86400 * 1000); //dt2 not set or dt1 date is greater than dt2 date

                    if (dt2Date == null || dateDiff < 0) {
                      dt2.datepicker('setDate', minDate);
                    } //dt1 date is 30 days under dt2 date
                    else if (dateDiff > 30) {
                        dt2.datepicker('setDate', startDate);
                      } //sets dt2 maxDate to the last day of 30 days window
                    //dt2.datepicker('option', 'maxDate', startDate);
                    //first day which can be selected in dt2 is selected date in dt1


                    dt2.datepicker('option', 'minDate', minDate);
                  }
                });
                $(document).find('input[name="_w_end_date"]').datepicker({
                  dateFormat: "yy-mm-dd",
                  minDate: 0
                });
              }
          }
        },
        complete: function complete(jqXHR, textStatus) {
          console.log('AJAX is complete.');
        }
      }); // End AJAX.
    }
  });
});

/***/ }),
// Edit End  //

/***/ "./wp-content/themes/weber-landing/assets/src/sass/admin.scss":
/*!********************************************************************!*\
  !*** ./wp-content/themes/weber-landing/assets/src/sass/admin.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./wp-content/themes/weber-landing/assets/src/sass/style.scss":
/*!********************************************************************!*\
  !*** ./wp-content/themes/weber-landing/assets/src/sass/style.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!************************************************************************************************************************************************************************************************!*\
  !*** multi ./wp-content/themes/weber-landing/assets/src/js/admin.js ./wp-content/themes/weber-landing/assets/src/sass/admin.scss ./wp-content/themes/weber-landing/assets/src/sass/style.scss ***!
  \************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Applications/MAMP/htdocs/weber/wp-content/themes/weber-landing/assets/src/js/admin.js */"./wp-content/themes/weber-landing/assets/src/js/admin.js");
__webpack_require__(/*! /Applications/MAMP/htdocs/weber/wp-content/themes/weber-landing/assets/src/sass/admin.scss */"./wp-content/themes/weber-landing/assets/src/sass/admin.scss");
module.exports = __webpack_require__(/*! /Applications/MAMP/htdocs/weber/wp-content/themes/weber-landing/assets/src/sass/style.scss */"./wp-content/themes/weber-landing/assets/src/sass/style.scss");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vd3AtY29udGVudC90aGVtZXMvd2ViZXItbGFuZGluZy9hc3NldHMvc3JjL2pzL2FkbWluLmpzIiwid2VicGFjazovLy8uL3dwLWNvbnRlbnQvdGhlbWVzL3dlYmVyLWxhbmRpbmcvYXNzZXRzL3NyYy9zYXNzL2FkbWluLnNjc3M/MjRlYSIsIndlYnBhY2s6Ly8vLi93cC1jb250ZW50L3RoZW1lcy93ZWJlci1sYW5kaW5nL2Fzc2V0cy9zcmMvc2Fzcy9zdHlsZS5zY3NzPzNkNzkiXSwibmFtZXMiOlsialF1ZXJ5IiwiJCIsImRvY3VtZW50IiwicmVhZHkiLCJGb2NhbFBvaW50Q29udHJvbHMiLCJvbiIsIiR0aGlzIiwicGFyZW50IiwicGFyZW50cyIsImZpbGVuYW1lIiwiYXR0ciIsImZpbmQiLCJsZW5ndGgiLCJodG1sIiwidGhlbWVfdmFyIiwidXBsb2FkIiwiYXBwZW5kIiwiZW1wdHkiLCJpbml0aWFsaXplX2ZpZWxkIiwiJGVsIiwiJGlkIiwiJHRvcCIsIiRsZWZ0IiwiJHJpZ2h0IiwiJGJvdHRvbSIsIiRmcCIsIiRpbWciLCIkY2FudmFzIiwiJGFkZCIsIiRkZWwiLCJ2YWx1ZXMiLCJpZCIsInZhbCIsInRvcCIsImxlZnQiLCJ3aWR0aCIsImhlaWdodCIsInNpemUiLCJkYXRhIiwiaW1nIiwiZ2V0IiwiY2FudmFzIiwiZmlsZV9mcmFtZSIsImN0eCIsImdldENvbnRleHQiLCJyZWN0IiwibW91c2VEb3duIiwiY2FudmFzV2lkdGgiLCJjYW52YXNIZWlnaHQiLCJkcmF3Q2FudmFzIiwiZWFjaCIsImNvbXBsZXRlIiwibG9hZCIsIndpbmRvdyIsIm9wZW4iLCJ3cCIsIm1lZGlhIiwiZnJhbWVzIiwidGl0bGUiLCJidXR0b24iLCJ0ZXh0IiwiYXR0YWNobWVudCIsInN0YXRlIiwiZmlyc3QiLCJ0b0pTT04iLCJzcmMiLCJzaXplcyIsImNvbnNvbGUiLCJsb2ciLCJhZGRDbGFzcyIsInVuZGVmaW5lZCIsInVybCIsInJlbW92ZUF0dHIiLCJyZW1vdmVDbGFzcyIsImFkZEV2ZW50TGlzdGVuZXIiLCJlIiwiQXBwbHlQb2ludE92Q2FudmFzIiwiZHJhd0ltZyIsInN0YXJ0WCIsImxheWVyWCIsInN0YXJ0WSIsImxheWVyWSIsInciLCJoIiwidG9GaXhlZCIsImRyYXdGb2N1cyIsImRyYXdJbWFnZSIsIngiLCJ5IiwiTWF0aCIsImFicyIsInN0cm9rZVN0eWxlIiwiZmlsbFN0eWxlIiwiYmVnaW5QYXRoIiwiYXJjIiwiUEkiLCJmaWxsIiwic3Ryb2tlIiwicmVkcmF3Rm9jdXMiLCJyZXNpemVDYW52YXMiLCJjbGVhckNhbnZhcyIsImZpbGxSZWN0IiwibmF0dXJhbF93aWR0aCIsIm5hdHVyYWxXaWR0aCIsIm5hdHVyYWxfaGVpZ2h0IiwibmF0dXJhbEhlaWdodCIsInJhdGlvIiwicGFyZW50X3dpZHRoIiwibmV3X3dpZHRoIiwibmV3X2hlaWdodCIsImFjZiIsImFkZF9hY3Rpb24iLCJnZXRfZmllbGRzIiwidHlwZSIsInBvc3Rib3giLCJwcmV2ZW50RGVmYXVsdCIsInBhcmVuIiwib3JkZXJfaWQiLCJhamF4IiwiYWpheHVybCIsImFjdGlvbiIsImNhY2hlIiwiZGF0YVR5cGUiLCJiZWZvcmVTZW5kIiwianFYSFIiLCJzZXR0aW5ncyIsInN1Y2Nlc3MiLCJ0ZXh0U3RhdHVzIiwicmVzdWx0IiwibWVzc2FnZSIsImxvY2F0aW9uIiwicmVsb2FkIiwiZGF0ZXBpY2tlciIsImRhdGVGb3JtYXQiLCJtaW5EYXRlIiwib25TZWxlY3QiLCJkdDIiLCJzdGFydERhdGUiLCJzZXREYXRlIiwiZ2V0RGF0ZSIsImR0MkRhdGUiLCJkYXRlRGlmZiIsImNsb3Nlc3QiLCJmaWx0ZXIiLCJ2YWx1ZSIsIiRwYXJlbnQiLCJuYW1lIiwic3RhcnRfZGF0ZSIsImVuZF9kYXRlIiwiYW1vdW50IiwicG9zdF9pZCIsInJlcGxhY2VXaXRoIiwidGhlSUQiLCJwYXJ0bmVyX2lkIiwiaXNfc3VyZSIsImNvbmZpcm0iLCJydW5fYWN0aW9uIiwiY2FtcGFpZ25faWQiXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUVBQSxNQUFNLENBQUMsVUFBU0MsQ0FBVCxFQUFZO0FBRVhBLEdBQUMsQ0FBRUMsUUFBRixDQUFELENBQWNDLEtBQWQsQ0FBb0IsWUFBVztBQUMzQkMsc0JBQWtCO0FBQ3JCLEdBRkQ7QUFJQUgsR0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWUcsRUFBWixDQUFlLFlBQWYsRUFBNkIsb0JBQTdCLEVBQW1ELFlBQVc7QUFFMUQsUUFBSUMsS0FBSyxHQUFHTCxDQUFDLENBQUMsSUFBRCxDQUFiO0FBQ0EsUUFBSU0sTUFBTSxHQUFHRCxLQUFLLENBQUNFLE9BQU4sQ0FBYyxlQUFkLENBQWI7QUFDQSxRQUFJQyxRQUFRLEdBQUdILEtBQUssQ0FBQ0ksSUFBTixDQUFXLGFBQVgsQ0FBZjs7QUFHQSxRQUFJSCxNQUFNLENBQUNJLElBQVAsQ0FBWSxVQUFaLEVBQXdCQyxNQUF4QixHQUFpQyxDQUFyQyxFQUF3QztBQUNwQ0wsWUFBTSxDQUFDSSxJQUFQLENBQVksVUFBWixFQUF3QkUsSUFBeEIsQ0FBNkIsMENBQTBDQyxTQUFTLENBQUNDLE1BQXBELEdBQTZETixRQUE3RCxHQUF3RSxlQUFyRztBQUNILEtBRkQsTUFFTztBQUNIRixZQUFNLENBQUNTLE1BQVAsQ0FBYywrREFBK0RGLFNBQVMsQ0FBQ0MsTUFBekUsR0FBa0ZOLFFBQWxGLEdBQTZGLHFCQUEzRztBQUNIO0FBQ0osR0FaRDtBQWNBUixHQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZRyxFQUFaLENBQWUsWUFBZixFQUE2QixlQUE3QixFQUE4QyxZQUFXO0FBRXJESixLQUFDLENBQUMsSUFBRCxDQUFELENBQVFVLElBQVIsQ0FBYSxVQUFiLEVBQXlCTSxLQUF6QjtBQUVILEdBSkQ7O0FBTUEsV0FBU2Isa0JBQVQsR0FBK0I7QUFFM0I7QUFDQSxhQUFTYyxnQkFBVCxDQUEyQkMsR0FBM0IsRUFBaUM7QUFFN0I7QUFDQTtBQUNBLFVBQUlDLEdBQUcsR0FBSUQsR0FBRyxDQUFDUixJQUFKLENBQVMscUJBQVQsQ0FBWDtBQUFBLFVBQ0lVLElBQUksR0FBSUYsR0FBRyxDQUFDUixJQUFKLENBQVMsc0JBQVQsQ0FEWjtBQUFBLFVBRUlXLEtBQUssR0FBSUgsR0FBRyxDQUFDUixJQUFKLENBQVMsdUJBQVQsQ0FGYjtBQUFBLFVBR0lZLE1BQU0sR0FBSUosR0FBRyxDQUFDUixJQUFKLENBQVMsd0JBQVQsQ0FIZDtBQUFBLFVBSUlhLE9BQU8sR0FBR0wsR0FBRyxDQUFDUixJQUFKLENBQVMseUJBQVQsQ0FKZDtBQUFBLFVBTUk7QUFDQWMsU0FBRyxHQUFJTixHQUFHLENBQUNSLElBQUosQ0FBUyxrQkFBVCxDQVBYO0FBQUEsVUFRSWUsSUFBSSxHQUFJUCxHQUFHLENBQUNSLElBQUosQ0FBUyx3QkFBVCxDQVJaO0FBQUEsVUFTSWdCLE9BQU8sR0FBR1IsR0FBRyxDQUFDUixJQUFKLENBQVMseUJBQVQsQ0FUZDtBQUFBLFVBV0k7QUFDQWlCLFVBQUksR0FBSVQsR0FBRyxDQUFDUixJQUFKLENBQVMsWUFBVCxDQVpaO0FBQUEsVUFhSWtCLElBQUksR0FBSVYsR0FBRyxDQUFDUixJQUFKLENBQVMsb0JBQVQsQ0FiWixDQUo2QixDQW9CN0I7O0FBQ0EsVUFBSW1CLE1BQU0sR0FBRztBQUNUQyxVQUFFLEVBQUdYLEdBQUcsQ0FBQ1ksR0FBSixFQURJO0FBRVRDLFdBQUcsRUFBR1osSUFBSSxDQUFDVyxHQUFMLEVBRkc7QUFHVEUsWUFBSSxFQUFHWixLQUFLLENBQUNVLEdBQU4sRUFIRTtBQUlURyxhQUFLLEVBQUdaLE1BQU0sQ0FBQ1MsR0FBUCxFQUpDO0FBS1RJLGNBQU0sRUFBRVosT0FBTyxDQUFDUSxHQUFSLEVBTEM7QUFNVEssWUFBSSxFQUFHWixHQUFHLENBQUNhLElBQUosQ0FBUyxjQUFUO0FBTkUsT0FBYixDQXJCNkIsQ0ErQjdCOztBQUNBLFVBQUlDLEdBQUcsR0FBTWIsSUFBSSxDQUFDYyxHQUFMLENBQVMsQ0FBVCxDQUFiO0FBQUEsVUFDSUMsTUFBTSxHQUFLZCxPQUFPLENBQUNhLEdBQVIsQ0FBWSxDQUFaLENBRGYsQ0FoQzZCLENBb0M3Qjs7QUFDQSxVQUFJRSxVQUFKLENBckM2QixDQXdDN0I7O0FBQ0EsVUFBSUMsR0FBRyxHQUFLRixNQUFNLENBQUNHLFVBQVAsQ0FBa0IsSUFBbEIsQ0FBWjtBQUFBLFVBQ0lDLElBQUksR0FBSyxFQURiO0FBQUEsVUFFSUMsU0FBUyxHQUFJLEtBRmpCO0FBQUEsVUFJSUMsV0FKSjtBQUFBLFVBS0lDLFlBTEosQ0F6QzZCLENBbUQ3QjtBQUNBOztBQUNBdEIsVUFBSSxDQUFDckIsRUFBTCxDQUFRLE1BQVIsRUFBZ0I0QyxVQUFoQixFQUE0QkMsSUFBNUIsQ0FBaUMsWUFBVztBQUV4QztBQUNBO0FBQ0EsWUFBSSxLQUFLQyxRQUFULEVBQW1CO0FBQ2ZsRCxXQUFDLENBQUMsSUFBRCxDQUFELENBQVFtRCxJQUFSO0FBQ0g7QUFDSixPQVBELEVBckQ2QixDQStEN0I7O0FBQ0FuRCxPQUFDLENBQUNvRCxNQUFELENBQUQsQ0FBVWhELEVBQVYsQ0FBYSxRQUFiLEVBQXVCNEMsVUFBdkIsRUFoRTZCLENBbUU3Qjs7QUFDQXJCLFVBQUksQ0FBQ3ZCLEVBQUwsQ0FBUSxPQUFSLEVBQWlCLFlBQVU7QUFFdkI7QUFDQSxZQUFLcUMsVUFBTCxFQUFrQjtBQUNkQSxvQkFBVSxDQUFDWSxJQUFYO0FBQ0E7QUFDSCxTQU5zQixDQVF2Qjs7O0FBQ0FaLGtCQUFVLEdBQUdhLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTQyxNQUFULENBQWdCZixVQUFoQixHQUE2QmEsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDL0NFLGVBQUssRUFBRSxjQUR3QztBQUUvQ0MsZ0JBQU0sRUFBRTtBQUFFQyxnQkFBSSxFQUFFO0FBQVI7QUFGdUMsU0FBVCxDQUExQyxDQVR1QixDQWN2Qjs7QUFDQWxCLGtCQUFVLENBQUNyQyxFQUFYLENBQWMsUUFBZCxFQUF3QixZQUFXO0FBRS9CO0FBQ0EsY0FBSXdELFVBQVUsR0FBSW5CLFVBQVUsQ0FBQ29CLEtBQVgsR0FBbUJ0QixHQUFuQixDQUF1QixXQUF2QixFQUFvQ3VCLEtBQXBDLEdBQTRDQyxNQUE1QyxFQUFsQjtBQUFBLGNBQ0lDLEdBQUcsR0FBS0osVUFBVSxDQUFDSyxLQUFYLENBQWlCcEMsTUFBTSxDQUFDTyxJQUF4QixDQURaO0FBRUk4QixpQkFBTyxDQUFDQyxHQUFSLENBQVlQLFVBQVosRUFMMkIsQ0FPL0I7O0FBQ0FwQyxhQUFHLENBQUM0QyxRQUFKLENBQWEsUUFBYjs7QUFFQSxjQUFJSixHQUFHLEtBQUtLLFNBQVosRUFBdUI7QUFDbkJMLGVBQUcsR0FBR0osVUFBTjtBQUNILFdBWjhCLENBYy9COzs7QUFDQW5DLGNBQUksQ0FBQ2hCLElBQUwsQ0FBVSxLQUFWLEVBQWlCdUQsR0FBRyxDQUFDTSxHQUFyQixFQWYrQixDQWlCL0I7O0FBQ0FuRCxhQUFHLENBQUNZLEdBQUosQ0FBUTZCLFVBQVUsQ0FBQzlCLEVBQW5CO0FBQ0FELGdCQUFNLENBQUNDLEVBQVAsR0FBWThCLFVBQVUsQ0FBQzlCLEVBQXZCO0FBRUgsU0FyQkQsRUFmdUIsQ0FzQ3ZCOztBQUNBVyxrQkFBVSxDQUFDWSxJQUFYO0FBQ0gsT0F4Q0QsRUFwRTZCLENBK0c3Qjs7QUFDQXpCLFVBQUksQ0FBQ3hCLEVBQUwsQ0FBUSxPQUFSLEVBQWlCLFlBQVU7QUFFdkI7QUFDQXFCLFlBQUksQ0FBQzhDLFVBQUwsQ0FBZ0Isa0JBQWhCLEVBSHVCLENBS3ZCOztBQUNBL0MsV0FBRyxDQUFDZ0QsV0FBSixDQUFnQixRQUFoQixFQU51QixDQVF2Qjs7QUFDQXJELFdBQUcsQ0FBQ1ksR0FBSixDQUFRLEVBQVI7QUFDQVgsWUFBSSxDQUFDVyxHQUFMLENBQVMsRUFBVDtBQUNBVixhQUFLLENBQUNVLEdBQU4sQ0FBVSxFQUFWO0FBQ0FULGNBQU0sQ0FBQ1MsR0FBUCxDQUFXLEVBQVg7QUFDQVIsZUFBTyxDQUFDUSxHQUFSLENBQVksRUFBWixFQWJ1QixDQWV2Qjs7QUFDQUYsY0FBTSxDQUFDRyxHQUFQLEdBQWEsSUFBYjtBQUNILE9BakJELEVBaEg2QixDQW1JN0I7O0FBQ0FRLFlBQU0sQ0FBQ2lDLGdCQUFQLENBQXdCLFdBQXhCLEVBQXFDLFVBQVNDLENBQVQsRUFBWTtBQUM3QyxZQUFJN0IsU0FBSixFQUFlO0FBQ1g4Qiw0QkFBa0IsQ0FBQ0QsQ0FBRCxDQUFsQjtBQUNIO0FBQ0osT0FKRCxFQUlHLEtBSkg7QUFNQWxDLFlBQU0sQ0FBQ2lDLGdCQUFQLENBQXdCLFdBQXhCLEVBQXFDLFVBQVNDLENBQVQsRUFBWTtBQUM3QzdCLGlCQUFTLEdBQUcsSUFBWjtBQUNBOEIsMEJBQWtCLENBQUNELENBQUQsQ0FBbEI7QUFDSCxPQUhELEVBR0csS0FISDtBQUtBbEMsWUFBTSxDQUFDaUMsZ0JBQVAsQ0FBd0IsU0FBeEIsRUFBbUMsWUFBVztBQUMxQzVCLGlCQUFTLEdBQUcsS0FBWjtBQUNILE9BRkQsRUFFRyxLQUZIOztBQUlBLGVBQVM4QixrQkFBVCxDQUE2QkQsQ0FBN0IsRUFBZ0M7QUFFNUI7QUFDQTtBQUNBRSxlQUFPLEdBSnFCLENBTTVCOztBQUNBaEMsWUFBSSxDQUFDaUMsTUFBTCxHQUFjSCxDQUFDLENBQUNJLE1BQWhCO0FBQ0FsQyxZQUFJLENBQUNtQyxNQUFMLEdBQWNMLENBQUMsQ0FBQ00sTUFBaEIsQ0FSNEIsQ0FVNUI7O0FBQ0FwQyxZQUFJLENBQUNxQyxDQUFMLEdBQVMsQ0FBVDtBQUNBckMsWUFBSSxDQUFDc0MsQ0FBTCxHQUFTLENBQVQsQ0FaNEIsQ0FjNUI7QUFDQTtBQUVBOztBQUNBckQsY0FBTSxDQUFDRyxHQUFQLEdBQWVZLElBQUksQ0FBQ21DLE1BQUwsR0FBY2hDLFlBQTdCO0FBQ0FsQixjQUFNLENBQUNJLElBQVAsR0FBZVcsSUFBSSxDQUFDaUMsTUFBTCxHQUFjL0IsV0FBN0I7QUFDQWpCLGNBQU0sQ0FBQ0ssS0FBUCxHQUFnQixDQUFDVSxJQUFJLENBQUNxQyxDQUFMLEdBQVNyQyxJQUFJLENBQUNpQyxNQUFmLElBQXlCL0IsV0FBekM7QUFDQWpCLGNBQU0sQ0FBQ00sTUFBUCxHQUFpQixDQUFDUyxJQUFJLENBQUNzQyxDQUFMLEdBQVN0QyxJQUFJLENBQUNtQyxNQUFmLElBQXlCaEMsWUFBMUMsQ0FyQjRCLENBdUI1Qjs7QUFDQTNCLFlBQUksQ0FBQ1csR0FBTCxDQUFTRixNQUFNLENBQUNHLEdBQVAsQ0FBV21ELE9BQVgsQ0FBbUIsQ0FBbkIsQ0FBVDtBQUNBOUQsYUFBSyxDQUFDVSxHQUFOLENBQVVGLE1BQU0sQ0FBQ0ksSUFBUCxDQUFZa0QsT0FBWixDQUFvQixDQUFwQixDQUFWO0FBQ0E3RCxjQUFNLENBQUNTLEdBQVAsQ0FBV0YsTUFBTSxDQUFDSyxLQUFQLENBQWFpRCxPQUFiLENBQXFCLENBQXJCLENBQVg7QUFDQTVELGVBQU8sQ0FBQ1EsR0FBUixDQUFZRixNQUFNLENBQUNNLE1BQVAsQ0FBY2dELE9BQWQsQ0FBc0IsQ0FBdEIsQ0FBWixFQTNCNEIsQ0E2QjVCOztBQUNBQyxpQkFBUyxDQUFDeEMsSUFBSSxDQUFDaUMsTUFBTixFQUFjakMsSUFBSSxDQUFDbUMsTUFBbkIsRUFBMkJuQyxJQUFJLENBQUNxQyxDQUFoQyxFQUFtQ3JDLElBQUksQ0FBQ3NDLENBQXhDLENBQVQ7QUFDSCxPQWxMNEIsQ0FvTDdCO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUlBOzs7QUFDQSxlQUFTTixPQUFULEdBQW1CO0FBQ2Y7QUFDQWxDLFdBQUcsQ0FBQzJDLFNBQUosQ0FBYy9DLEdBQWQsRUFBbUIsQ0FBbkIsRUFBc0IsQ0FBdEIsRUFBeUJRLFdBQXpCLEVBQXNDQyxZQUF0QztBQUNILE9BM080QixDQTZPN0I7OztBQUNBLGVBQVNxQyxTQUFULENBQW1CRSxDQUFuQixFQUFzQkMsQ0FBdEIsRUFBeUJOLENBQXpCLEVBQTRCQyxDQUE1QixFQUErQjtBQUUzQjtBQUNBLFlBQUlJLENBQUMsR0FBR0UsSUFBSSxDQUFDQyxHQUFMLENBQVNILENBQVQsQ0FBUjtBQUNBLFlBQUlDLENBQUMsR0FBR0MsSUFBSSxDQUFDQyxHQUFMLENBQVNGLENBQVQsQ0FBUjtBQUNBLFlBQUlOLENBQUMsR0FBR08sSUFBSSxDQUFDQyxHQUFMLENBQVNSLENBQVQsQ0FBUjtBQUNBLFlBQUlDLENBQUMsR0FBR00sSUFBSSxDQUFDQyxHQUFMLENBQVNQLENBQVQsQ0FBUjtBQUVBeEMsV0FBRyxDQUFDZ0QsV0FBSixHQUFrQiwwQkFBbEI7QUFDQWhELFdBQUcsQ0FBQ2lELFNBQUosR0FBZ0IsdUJBQWhCO0FBRUFqRCxXQUFHLENBQUNrRCxTQUFKO0FBQ0FsRCxXQUFHLENBQUNtRCxHQUFKLENBQVFQLENBQVIsRUFBVUMsQ0FBVixFQUFZTixDQUFaLEVBQWMsQ0FBZCxFQUFnQixJQUFFTyxJQUFJLENBQUNNLEVBQXZCO0FBQ0FwRCxXQUFHLENBQUNxRCxJQUFKO0FBQ0FyRCxXQUFHLENBQUNzRCxNQUFKLEdBZDJCLENBZ0IzQjtBQUNBO0FBQ0gsT0FoUTRCLENBa1E3Qjs7O0FBQ0EsZUFBU0MsV0FBVCxHQUF1QjtBQUVuQjtBQUNBLFlBQUlwRSxNQUFNLENBQUNHLEdBQVAsS0FBZSxJQUFuQixFQUF5QjtBQUVyQjtBQUNBLGNBQUlzRCxDQUFDLEdBQUd6RCxNQUFNLENBQUNJLElBQVAsR0FBY2EsV0FBdEI7QUFBQSxjQUNJeUMsQ0FBQyxHQUFHMUQsTUFBTSxDQUFDRyxHQUFQLEdBQWFlLFlBRHJCO0FBQUEsY0FFSWtDLENBQUMsR0FBSXBELE1BQU0sQ0FBQ0ssS0FBUCxHQUFlWSxXQUFoQixHQUErQndDLENBRnZDO0FBQUEsY0FHSUosQ0FBQyxHQUFJckQsTUFBTSxDQUFDTSxNQUFQLEdBQWlCWSxZQUFsQixHQUFrQ3dDLENBSDFDLENBSHFCLENBUXJCOztBQUNBSCxtQkFBUyxDQUFDRSxDQUFELEVBQUlDLENBQUosRUFBT04sQ0FBUCxFQUFVQyxDQUFWLENBQVQ7QUFDSDtBQUNKLE9BalI0QixDQW1SN0I7OztBQUNBLGVBQVNsQyxVQUFULEdBQXNCO0FBRWxCO0FBQ0FrRCxvQkFBWTtBQUNadEIsZUFBTztBQUNQcUIsbUJBQVc7QUFDZCxPQTFSNEIsQ0E0UjdCOzs7QUFDQSxlQUFTRSxXQUFULEdBQXVCO0FBRW5CO0FBQ0F6RCxXQUFHLENBQUNpRCxTQUFKLEdBQWdCLFNBQWhCO0FBQ0FqRCxXQUFHLENBQUMwRCxRQUFKLENBQWEsQ0FBYixFQUFnQixDQUFoQixFQUFtQnRELFdBQW5CLEVBQWdDQyxZQUFoQztBQUNILE9BbFM0QixDQW9TN0I7OztBQUNBLGVBQVNtRCxZQUFULEdBQXdCO0FBRXBCO0FBQ0EsWUFBSUcsYUFBYSxHQUFJL0QsR0FBRyxDQUFDZ0UsWUFBekI7QUFBQSxZQUNJQyxjQUFjLEdBQUlqRSxHQUFHLENBQUNrRSxhQUQxQjtBQUFBLFlBR0k7QUFDQUMsYUFBSyxHQUFNSixhQUFhLEdBQUdFLGNBSi9CO0FBQUEsWUFNSTtBQUNBRyxvQkFBWSxHQUFLeEYsR0FBRyxDQUFDWixNQUFKLEdBQWE0QixLQUFiLEtBQXdCTixJQUFJLENBQUNNLEtBQUwsS0FBYSxDQVAxRDtBQUFBLFlBU0k7QUFDQXdFLG9CQUFZLEdBQUdBLFlBQVksR0FBRyxDQUFmLEdBQW1CTCxhQUFuQixHQUFtQ0ssWUFWdEQ7QUFBQSxZQVlJO0FBQ0FDLGlCQWJKO0FBQUEsWUFhZUMsVUFiZixDQUhvQixDQW1CcEI7O0FBQ0EsWUFBSVAsYUFBYSxHQUFHSyxZQUFwQixFQUFrQztBQUU5QjtBQUNBQyxtQkFBUyxHQUFJRCxZQUFiLENBSDhCLENBSzlCOztBQUNBRSxvQkFBVSxHQUFJRixZQUFZLEdBQUdELEtBQTdCLENBTjhCLENBUWxDO0FBQ0MsU0FURCxNQVNPO0FBRUg7QUFDQUUsbUJBQVMsR0FBSU4sYUFBYjtBQUNBTyxvQkFBVSxHQUFJTCxjQUFkO0FBQ0gsU0FsQ21CLENBcUNwQjs7O0FBQ0E3RSxlQUFPLENBQUNRLEtBQVIsQ0FBY3lFLFNBQWQ7QUFDQWpGLGVBQU8sQ0FBQ1MsTUFBUixDQUFleUUsVUFBZixFQXZDb0IsQ0F5Q3BCO0FBQ0E7O0FBQ0FwRSxjQUFNLENBQUNOLEtBQVAsR0FBZXlFLFNBQWY7QUFDQW5FLGNBQU0sQ0FBQ0wsTUFBUCxHQUFnQnlFLFVBQWhCLENBNUNvQixDQStDcEI7O0FBQ0E5RCxtQkFBVyxHQUFHNkQsU0FBZDtBQUNBNUQsb0JBQVksR0FBRzZELFVBQWY7QUFDSDtBQUVKOztBQUdELFFBQUksT0FBT0MsR0FBRyxDQUFDQyxVQUFYLEtBQTBCLFdBQTlCLEVBQTRDO0FBRXhDRCxTQUFHLENBQUNDLFVBQUosQ0FBZSxNQUFmLEVBQXVCLFVBQVU1RixHQUFWLEVBQWU7QUFFbEM7QUFDQTJGLFdBQUcsQ0FBQ0UsVUFBSixDQUFlO0FBQUVDLGNBQUksRUFBRztBQUFULFNBQWYsRUFBd0M5RixHQUF4QyxFQUE2QytCLElBQTdDLENBQWtELFlBQVU7QUFFeERoQywwQkFBZ0IsQ0FBRWpCLENBQUMsQ0FBQyxJQUFELENBQUgsQ0FBaEI7QUFFSCxTQUpEO0FBTUgsT0FURDtBQVdBNkcsU0FBRyxDQUFDQyxVQUFKLENBQWUsUUFBZixFQUF5QixVQUFVNUYsR0FBVixFQUFlO0FBRXBDO0FBQ0EyRixXQUFHLENBQUNFLFVBQUosQ0FBZTtBQUFFQyxjQUFJLEVBQUc7QUFBVCxTQUFmLEVBQXdDOUYsR0FBeEMsRUFBNkMrQixJQUE3QyxDQUFrRCxZQUFVO0FBRXhEaEMsMEJBQWdCLENBQUVqQixDQUFDLENBQUMsSUFBRCxDQUFILENBQWhCO0FBRUgsU0FKRDtBQUtILE9BUkQ7QUFXSCxLQXhCRCxNQXdCTztBQUdIOzs7Ozs7Ozs7Ozs7OztBQWVBQSxPQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZRyxFQUFaLENBQWUsa0JBQWYsRUFBbUMsVUFBU3NFLENBQVQsRUFBWXVDLE9BQVosRUFBb0I7QUFFbkRqSCxTQUFDLENBQUNpSCxPQUFELENBQUQsQ0FBV3ZHLElBQVgsQ0FBZ0IsdUNBQWhCLEVBQXlEdUMsSUFBekQsQ0FBOEQsWUFBVTtBQUVwRWhDLDBCQUFnQixDQUFFakIsQ0FBQyxDQUFDLElBQUQsQ0FBSCxDQUFoQjtBQUVILFNBSkQ7QUFNSCxPQVJEO0FBV0g7QUFDSixHQS9hVSxDQWliWDs7O0FBQ0FBLEdBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlHLEVBQVosQ0FBZSxPQUFmLEVBQXdCLGlDQUF4QixFQUEyRCxVQUFTc0UsQ0FBVCxFQUFXO0FBRWxFQSxLQUFDLENBQUN3QyxjQUFGO0FBQ0EsUUFBSUMsS0FBSyxHQUFHbkgsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRTyxPQUFSLENBQWdCLFNBQWhCLENBQVo7QUFDQTRHLFNBQUssQ0FBQy9DLFFBQU4sQ0FBZSxTQUFmO0FBRUEsUUFBSWdELFFBQVEsR0FBR3BILENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVMsSUFBUixDQUFhLGVBQWIsQ0FBZjtBQUVBVCxLQUFDLENBQUNxSCxJQUFGLENBQU87QUFDSC9DLFNBQUcsRUFBRWdELE9BREY7QUFFSE4sVUFBSSxFQUFFLE1BRkg7QUFHSDNFLFVBQUksRUFBRTtBQUNGa0YsY0FBTSxFQUFFLDBCQUROO0FBRUZILGdCQUFRLEVBQUVBO0FBRlIsT0FISDtBQU9ISSxXQUFLLEVBQUUsS0FQSjtBQVFIQyxjQUFRLEVBQUUsTUFSUDtBQVVIQyxnQkFBVSxFQUFFLG9CQUFVQyxLQUFWLEVBQWlCQyxRQUFqQixFQUEyQjtBQUNuQzVILFNBQUMsQ0FBQywyQkFBRCxDQUFELENBQStCWSxJQUEvQixDQUFvQyxFQUFwQztBQUNILE9BWkU7QUFhSGlILGFBQU8sRUFBRSxpQkFBVXhGLElBQVYsRUFBZ0J5RixVQUFoQixFQUE0QkgsS0FBNUIsRUFBb0M7QUFDMUMsWUFBSXRGLElBQUksQ0FBQzBGLE1BQUwsSUFBZSxPQUFuQixFQUEyQjtBQUN0QlosZUFBSyxDQUFDM0MsV0FBTixDQUFrQixTQUFsQjtBQUNBeEUsV0FBQyxDQUFDLDJCQUFELENBQUQsQ0FBK0JZLElBQS9CLENBQW9DLHdCQUF1QnlCLElBQUksQ0FBQzJGLE9BQTVCLEdBQXFDLFFBQXpFO0FBQ0osU0FIRCxNQUdLO0FBQ0Q1RSxnQkFBTSxDQUFDNkUsUUFBUCxDQUFnQkMsTUFBaEI7QUFDSDtBQUNILE9BcEJFO0FBcUJIaEYsY0FBUSxFQUFFLGtCQUFTeUUsS0FBVCxFQUFnQkcsVUFBaEIsRUFBMkIsQ0FDakM7QUFDSDtBQXZCRSxLQUFQLEVBUmtFLENBZ0MvRDtBQUVOLEdBbENELEVBbGJXLENBc2RmOztBQUNBLE1BQUc5SCxDQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZUyxJQUFaLENBQWlCLDZCQUFqQixFQUFnREMsTUFBaEQsR0FBeUQsQ0FBNUQsRUFBOEQ7QUFDMURYLEtBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlTLElBQVosQ0FBaUIsNkJBQWpCLEVBQWdEeUgsVUFBaEQsQ0FBMkQ7QUFDdkRDLGdCQUFVLEVBQUUsVUFEMkM7QUFFdkRDLGFBQU8sRUFBRSxLQUY4QztBQUd2REMsY0FBUSxFQUFFLG9CQUFZO0FBQ2xCLFlBQUlDLEdBQUcsR0FBR3ZJLENBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlTLElBQVosQ0FBaUIsMkJBQWpCLENBQVY7QUFDQSxZQUFJOEgsU0FBUyxHQUFHeEksQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRbUksVUFBUixDQUFtQixTQUFuQixDQUFoQixDQUZrQixDQUdsQjs7QUFDQUssaUJBQVMsQ0FBQ0MsT0FBVixDQUFrQkQsU0FBUyxDQUFDRSxPQUFWLEtBQXNCLEVBQXhDO0FBQ0EsWUFBSUwsT0FBTyxHQUFHckksQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRbUksVUFBUixDQUFtQixTQUFuQixDQUFkO0FBQ0EsWUFBSVEsT0FBTyxHQUFHSixHQUFHLENBQUNKLFVBQUosQ0FBZSxTQUFmLENBQWQsQ0FOa0IsQ0FPbEI7O0FBQ0EsWUFBSVMsUUFBUSxHQUFHLENBQUNELE9BQU8sR0FBR04sT0FBWCxLQUFxQixRQUFRLElBQTdCLENBQWYsQ0FSa0IsQ0FVbEI7O0FBQ0EsWUFBSU0sT0FBTyxJQUFJLElBQVgsSUFBbUJDLFFBQVEsR0FBRyxDQUFsQyxFQUFxQztBQUM3QkwsYUFBRyxDQUFDSixVQUFKLENBQWUsU0FBZixFQUEwQkUsT0FBMUI7QUFDUCxTQUZELENBR0E7QUFIQSxhQUlLLElBQUlPLFFBQVEsR0FBRyxFQUFmLEVBQWtCO0FBQ2ZMLGVBQUcsQ0FBQ0osVUFBSixDQUFlLFNBQWYsRUFBMEJLLFNBQTFCO0FBQ1AsV0FqQmlCLENBa0JsQjtBQUNBO0FBQ0E7OztBQUNBRCxXQUFHLENBQUNKLFVBQUosQ0FBZSxRQUFmLEVBQXlCLFNBQXpCLEVBQW9DRSxPQUFwQztBQUNIO0FBekJzRCxLQUEzRDtBQTJCQXJJLEtBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlTLElBQVosQ0FBaUIsMkJBQWpCLEVBQThDeUgsVUFBOUMsQ0FBeUQ7QUFDckRDLGdCQUFVLEVBQUUsVUFEeUM7QUFFckRDLGFBQU8sRUFBRTtBQUY0QyxLQUF6RDtBQUlILEdBdmZjLENBeWZmOzs7QUFDQXJJLEdBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlHLEVBQVosQ0FBZSxPQUFmLEVBQXVCLDBDQUF2QixFQUFrRSxVQUFTc0UsQ0FBVCxFQUFXO0FBRXpFQSxLQUFDLENBQUN3QyxjQUFGO0FBQ0EsUUFBSTdHLEtBQUssR0FBR0wsQ0FBQyxDQUFDLElBQUQsQ0FBYjtBQUNBSyxTQUFLLENBQUN3SSxPQUFOLENBQWMsSUFBZCxFQUFvQm5JLElBQXBCLENBQXlCLGFBQXpCLEVBQXdDOEQsV0FBeEMsQ0FBb0QsT0FBcEQ7QUFDQW5FLFNBQUssQ0FBQ3dJLE9BQU4sQ0FBYyxJQUFkLEVBQW9CbkksSUFBcEIsQ0FBeUIsT0FBekIsRUFBa0NvSSxNQUFsQyxDQUF5QyxZQUFVO0FBQy9DLGFBQU8sQ0FBQyxLQUFLQyxLQUFiO0FBQ0gsS0FGRCxFQUVHM0UsUUFGSCxDQUVZLE9BRlo7O0FBSUEsUUFBRy9ELEtBQUssQ0FBQ3dJLE9BQU4sQ0FBYyxJQUFkLEVBQW9CbkksSUFBcEIsQ0FBeUIsYUFBekIsRUFBd0NDLE1BQXhDLEdBQWlELENBQXBELEVBQXNEO0FBQ2xEO0FBQ0g7O0FBRUQsUUFBSXFJLE9BQU8sR0FBR2hKLENBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlTLElBQVosQ0FBaUIsdUJBQWpCLENBQWQsQ0FieUUsQ0FlekU7O0FBQ0FWLEtBQUMsQ0FBQ3FILElBQUYsQ0FBTztBQUNIL0MsU0FBRyxFQUFFZ0QsT0FERjtBQUVITixVQUFJLEVBQUUsTUFGSDtBQUdIM0UsVUFBSSxFQUFFO0FBQ0ZrRixjQUFNLEVBQUUsaUNBRE47QUFFRjBCLFlBQUksRUFBRWpKLENBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlTLElBQVosQ0FBaUIsZ0NBQWpCLEVBQW1EcUIsR0FBbkQsRUFGSjtBQUdGbUgsa0JBQVUsRUFBRWxKLENBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlTLElBQVosQ0FBaUIsNkJBQWpCLEVBQWdEcUIsR0FBaEQsRUFIVjtBQUlGb0gsZ0JBQVEsRUFBRW5KLENBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlTLElBQVosQ0FBaUIsMkJBQWpCLEVBQThDcUIsR0FBOUMsRUFKUjtBQUtGcUgsY0FBTSxFQUFFcEosQ0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWVMsSUFBWixDQUFpQiw2QkFBakIsRUFBZ0RxQixHQUFoRCxFQUxOO0FBTUZzSCxlQUFPLEVBQUVoSixLQUFLLENBQUNJLElBQU4sQ0FBVyxTQUFYO0FBTlAsT0FISDtBQVdIK0csV0FBSyxFQUFFLEtBWEo7QUFZSEMsY0FBUSxFQUFFLE1BWlA7QUFjSEMsZ0JBQVUsRUFBRSxvQkFBVUMsS0FBVixFQUFpQkMsUUFBakIsRUFBMkI7QUFDbkNvQixlQUFPLENBQUM1RSxRQUFSLENBQWlCLFNBQWpCO0FBQ0gsT0FoQkU7QUFpQkh5RCxhQUFPLEVBQUUsaUJBQVV4RixJQUFWLEVBQWdCeUYsVUFBaEIsRUFBNEJILEtBQTVCLEVBQW9DO0FBQ3pDcUIsZUFBTyxDQUFDeEUsV0FBUixDQUFvQixTQUFwQjtBQUNBeEUsU0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWVMsSUFBWixDQUFpQixtQkFBakIsRUFBc0M0SSxXQUF0QyxDQUFrRGpILElBQUksQ0FBQ3pCLElBQXZEO0FBQ0FQLGFBQUssQ0FBQ3dJLE9BQU4sQ0FBYyxJQUFkLEVBQW9CbkksSUFBcEIsQ0FBeUIsT0FBekIsRUFBa0NxQixHQUFsQyxDQUFzQyxFQUF0QztBQUNILE9BckJFO0FBc0JIbUIsY0FBUSxFQUFFLGtCQUFTeUUsS0FBVCxFQUFnQkcsVUFBaEIsRUFBMkI7QUFDakM1RCxlQUFPLENBQUNDLEdBQVIsQ0FBYSxtQkFBYjtBQUNIO0FBeEJFLEtBQVAsRUFoQnlFLENBeUN0RTtBQUVOLEdBM0NELEVBMWZlLENBdWlCZjs7QUFDQW5FLEdBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlHLEVBQVosQ0FBZSxPQUFmLEVBQXVCLHNCQUF2QixFQUE4QyxVQUFTc0UsQ0FBVCxFQUFXO0FBRXJEQSxLQUFDLENBQUN3QyxjQUFGO0FBQ0EsUUFBSUssTUFBTSxHQUFHdkgsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRUyxJQUFSLENBQWEsYUFBYixDQUFiO0FBQ0EsUUFBSThJLEtBQUssR0FBR3ZKLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVMsSUFBUixDQUFhLFNBQWIsQ0FBWjtBQUNBLFFBQUkrSSxVQUFVLEdBQUd4SixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFTLElBQVIsQ0FBYSxjQUFiLENBQWpCO0FBRUEsUUFBSWdKLE9BQU8sR0FBR2xDLE1BQU0sSUFBSSxRQUFWLEdBQXNCbUMsT0FBTyxDQUFDLCtIQUFELENBQTdCLEdBQWlLQSxPQUFPLENBQUMsK0JBQUQsQ0FBdEw7QUFFQSxRQUFJVixPQUFPLEdBQUdoSixDQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZUyxJQUFaLENBQWlCLHVCQUFqQixDQUFkOztBQUVBLFFBQUcrSSxPQUFILEVBQVc7QUFDUDtBQUNBekosT0FBQyxDQUFDcUgsSUFBRixDQUFPO0FBQ0gvQyxXQUFHLEVBQUVnRCxPQURGO0FBRUhOLFlBQUksRUFBRSxNQUZIO0FBR0gzRSxZQUFJLEVBQUU7QUFDRmtGLGdCQUFNLEVBQUUsNEJBRE47QUFFRm9DLG9CQUFVLEVBQUVwQyxNQUZWO0FBR0ZxQyxxQkFBVyxFQUFFTCxLQUhYO0FBSUZDLG9CQUFVLEVBQUVBO0FBSlYsU0FISDtBQVNIaEMsYUFBSyxFQUFFLEtBVEo7QUFVSEMsZ0JBQVEsRUFBRSxNQVZQO0FBWUhDLGtCQUFVLEVBQUUsb0JBQVVDLEtBQVYsRUFBaUJDLFFBQWpCLEVBQTJCO0FBQ25Db0IsaUJBQU8sQ0FBQzVFLFFBQVIsQ0FBaUIsU0FBakI7QUFDSCxTQWRFO0FBZUh5RCxlQUFPLEVBQUUsaUJBQVV4RixJQUFWLEVBQWdCeUYsVUFBaEIsRUFBNEJILEtBQTVCLEVBQW9DO0FBQ3pDcUIsaUJBQU8sQ0FBQ3hFLFdBQVIsQ0FBb0IsU0FBcEI7QUFDQXhFLFdBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlTLElBQVosQ0FBaUIsbUJBQWpCLEVBQXNDNEksV0FBdEMsQ0FBa0RqSCxJQUFJLENBQUN6QixJQUF2RCxFQUZ5QyxDQUd6QztBQUNILFNBbkJFO0FBb0JIc0MsZ0JBQVEsRUFBRSxrQkFBU3lFLEtBQVQsRUFBZ0JHLFVBQWhCLEVBQTJCO0FBQ2pDNUQsaUJBQU8sQ0FBQ0MsR0FBUixDQUFhLG1CQUFiO0FBQ0g7QUF0QkUsT0FBUCxFQUZPLENBeUJKO0FBQ047QUFFSixHQXZDRDtBQXlDSCxDQWpsQkssQ0FBTixDOzs7Ozs7Ozs7OztBQ0ZBLHlDOzs7Ozs7Ozs7OztBQ0FBLHlDIiwiZmlsZSI6Ii9qcy9hZG1pbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiL1wiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMCk7XG4iLCIvL2ltcG9ydCAnLi9wbHVnaW5zL3RpbnltY2UuanMnXHJcblxyXG5qUXVlcnkoZnVuY3Rpb24oJCkge1xyXG4gICAgXHJcbiAgICAgICAgJCggZG9jdW1lbnQgKS5yZWFkeShmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgRm9jYWxQb2ludENvbnRyb2xzKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICBcclxuICAgICAgICAkKGRvY3VtZW50KS5vbignbW91c2VlbnRlcicsICcuYWNmLWZjLXBvcHVwIGxpIGEnLCBmdW5jdGlvbigpIHtcclxuICAgIFxyXG4gICAgICAgICAgICB2YXIgJHRoaXMgPSAkKHRoaXMpXHJcbiAgICAgICAgICAgIHZhciBwYXJlbnQgPSAkdGhpcy5wYXJlbnRzKCcuYWNmLWZjLXBvcHVwJyk7XHJcbiAgICAgICAgICAgIHZhciBmaWxlbmFtZSA9ICR0aGlzLmF0dHIoJ2RhdGEtbGF5b3V0Jyk7ICBcclxuICAgIFxyXG4gICAgXHJcbiAgICAgICAgICAgIGlmIChwYXJlbnQuZmluZCgnLnByZXZpZXcnKS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBwYXJlbnQuZmluZCgnLnByZXZpZXcnKS5odG1sKCc8ZGl2IGNsYXNzPVwiaW5uZXItcHJldmlld1wiPjxpbWcgc3JjPVwiJyArIHRoZW1lX3Zhci51cGxvYWQgKyBmaWxlbmFtZSArICcuanBnXCIvPjwvZGl2PicpXHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBwYXJlbnQuYXBwZW5kKCc8ZGl2IGNsYXNzPVwicHJldmlld1wiPjxkaXYgY2xhc3M9XCJpbm5lci1wcmV2aWV3XCI+PGltZyBzcmM9XCInICsgdGhlbWVfdmFyLnVwbG9hZCArIGZpbGVuYW1lICsgJy5qcGdcIi8+PC9kaXY+PC9kaXY+JykgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KVxyXG4gICAgXHJcbiAgICAgICAgJChkb2N1bWVudCkub24oJ21vdXNlbGVhdmUnLCAnLmFjZi1mYy1wb3B1cCcsIGZ1bmN0aW9uKCkge1xyXG4gICAgXHJcbiAgICAgICAgICAgICQodGhpcykuZmluZCgnLnByZXZpZXcnKS5lbXB0eSgpO1xyXG4gICAgXHJcbiAgICAgICAgfSlcclxuICAgIFxyXG4gICAgICAgIGZ1bmN0aW9uIEZvY2FsUG9pbnRDb250cm9scyAoKSB7IFxyXG4gICAgXHJcbiAgICAgICAgICAgIC8vIGluaXRpYWxpemVkIG9uIEFDRiBldmVudHNcclxuICAgICAgICAgICAgZnVuY3Rpb24gaW5pdGlhbGl6ZV9maWVsZCggJGVsICkge1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAvLyBDYWNoZSBqcXVlcnkgc2VsZWN0b3JzXHJcbiAgICAgICAgICAgICAgICAvLyBWYWx1ZXMgdG8gZ2V0L3NldFxyXG4gICAgICAgICAgICAgICAgdmFyICRpZCBcdD0gJGVsLmZpbmQoJy5hY2YtZm9jYWxfcG9pbnQtaWQnKSxcclxuICAgICAgICAgICAgICAgICAgICAkdG9wIFx0PSAkZWwuZmluZCgnLmFjZi1mb2NhbF9wb2ludC10b3AnKSxcclxuICAgICAgICAgICAgICAgICAgICAkbGVmdCBcdD0gJGVsLmZpbmQoJy5hY2YtZm9jYWxfcG9pbnQtbGVmdCcpLFxyXG4gICAgICAgICAgICAgICAgICAgICRyaWdodCBcdD0gJGVsLmZpbmQoJy5hY2YtZm9jYWxfcG9pbnQtcmlnaHQnKSxcclxuICAgICAgICAgICAgICAgICAgICAkYm90dG9tID0gJGVsLmZpbmQoJy5hY2YtZm9jYWxfcG9pbnQtYm90dG9tJyksXHJcbiAgICBcclxuICAgICAgICAgICAgICAgICAgICAvLyBFbGVtZW50cyB0byBnZXQvc2V0IFxyXG4gICAgICAgICAgICAgICAgICAgICRmcCBcdD0gJGVsLmZpbmQoJy5hY2YtZm9jYWxfcG9pbnQnKSxcclxuICAgICAgICAgICAgICAgICAgICAkaW1nIFx0PSAkZWwuZmluZCgnLmFjZi1mb2NhbF9wb2ludC1pbWFnZScpLFxyXG4gICAgICAgICAgICAgICAgICAgICRjYW52YXMgPSAkZWwuZmluZCgnLmFjZi1mb2NhbF9wb2ludC1jYW52YXMnKSxcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIEJ1dHRvbnMgdG8gdHJpZ2dlciBldmVudHNcclxuICAgICAgICAgICAgICAgICAgICAkYWRkIFx0PSAkZWwuZmluZCgnLmFkZC1pbWFnZScpLFxyXG4gICAgICAgICAgICAgICAgICAgICRkZWwgXHQ9ICRlbC5maW5kKCcuYWNmLWJ1dHRvbi1kZWxldGUnKTtcclxuICAgIFxyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAvLyBIb2xkL2dldCBvdXIgdmFsdWVzXHJcbiAgICAgICAgICAgICAgICB2YXIgdmFsdWVzID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiBcdCRpZC52YWwoKSxcclxuICAgICAgICAgICAgICAgICAgICB0b3A6IFx0JHRvcC52YWwoKSxcclxuICAgICAgICAgICAgICAgICAgICBsZWZ0OiBcdCRsZWZ0LnZhbCgpLFxyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiBcdCRyaWdodC52YWwoKSxcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6ICRib3R0b20udmFsKCksXHJcbiAgICAgICAgICAgICAgICAgICAgc2l6ZTogXHQkZnAuZGF0YSgncHJldmlld19zaXplJylcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICBcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgLy8gRE9NIGVsZW1lbnRzXHJcbiAgICAgICAgICAgICAgICB2YXIgaW1nICBcdCA9ICRpbWcuZ2V0KDApLFxyXG4gICAgICAgICAgICAgICAgICAgIGNhbnZhcyBcdCA9ICRjYW52YXMuZ2V0KDApO1xyXG4gICAgXHJcbiAgICBcclxuICAgICAgICAgICAgICAgIC8vIFRvIGhvbGQgV1AgbWVkaWEgZnJhbWVcclxuICAgICAgICAgICAgICAgIHZhciBmaWxlX2ZyYW1lO1xyXG4gICAgXHJcbiAgICBcclxuICAgICAgICAgICAgICAgIC8vIFZhcnMgZm9yIENhbnZhcyB3b3JrXHJcbiAgICAgICAgICAgICAgICB2YXIgY3R4IFx0XHQ9IGNhbnZhcy5nZXRDb250ZXh0KFwiMmRcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgcmVjdCBcdFx0PSB7fSxcclxuICAgICAgICAgICAgICAgICAgICBtb3VzZURvd24gXHQ9IGZhbHNlLFxyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAgICAgY2FudmFzV2lkdGgsXHJcbiAgICAgICAgICAgICAgICAgICAgY2FudmFzSGVpZ2h0O1xyXG4gICAgXHJcbiAgICBcclxuICAgIFxyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAvLyBXaGVuIHdlJ3ZlIGxvYWRlZCBhbiBpbWFnZSwgZHJhdyB0aGUgY2FudmFzLlxyXG4gICAgICAgICAgICAgICAgLy8gKGVpdGhlciBvbiBkb20gbG9hZCBvciBhZGRpbmcgbmV3IGltYWdlIGZyb20gV1AgbWVkaWEgbWFuYWdlcilcclxuICAgICAgICAgICAgICAgICRpbWcub24oXCJsb2FkXCIsIGRyYXdDYW52YXMpLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gTWFrZSBzdXJlIHRvIHRyaWdnZXIgbG9hZCBldmVudCBieSB0cmlnZ2VyaW5nIGxvYWRcclxuICAgICAgICAgICAgICAgICAgICAvLyBhZnRlciBqcXVlcnkgaGFzIGRvbmUgaXQncyBpdGVyYXRpb25cclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5jb21wbGV0ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLmxvYWQoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgIFxyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAvLyBXaGVuIHJlc2l6aW5nIHRoZSBwYWdlLCByZWRyYXcgdGhlIGNhbnZhcy5cclxuICAgICAgICAgICAgICAgICQod2luZG93KS5vbigncmVzaXplJywgZHJhd0NhbnZhcyk7XHJcbiAgICBcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgLy8gV2hlbiB3ZSBjbGljayB0aGUgYWRkIGltYWdlIGJ1dHRvbi4uLlxyXG4gICAgICAgICAgICAgICAgJGFkZC5vbignY2xpY2snLCBmdW5jdGlvbigpe1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gSWYgdGhlIG1lZGlhIGZyYW1lIGFscmVhZHkgZXhpc3RzLCByZW9wZW4gaXQuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCBmaWxlX2ZyYW1lICkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWxlX2ZyYW1lLm9wZW4oKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIENyZWF0ZSB0aGUgbWVkaWEgZnJhbWUuXHJcbiAgICAgICAgICAgICAgICAgICAgZmlsZV9mcmFtZSA9IHdwLm1lZGlhLmZyYW1lcy5maWxlX2ZyYW1lID0gd3AubWVkaWEoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ1NlbGVjdCBJbWFnZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJ1dHRvbjogeyB0ZXh0OiAnU2VsZWN0JyB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgICAgICAvLyBXaGVuIGFuIGltYWdlIGlzIHNlbGVjdGVkLi5cclxuICAgICAgICAgICAgICAgICAgICBmaWxlX2ZyYW1lLm9uKCdzZWxlY3QnLCBmdW5jdGlvbigpIHtcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBHZXQgc2VsZWN0ZWQgaW1hZ2Ugb2JqZWN0c1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgYXR0YWNobWVudCBcdD0gZmlsZV9mcmFtZS5zdGF0ZSgpLmdldCgnc2VsZWN0aW9uJykuZmlyc3QoKS50b0pTT04oKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNyYyBcdFx0PSBhdHRhY2htZW50LnNpemVzW3ZhbHVlcy5zaXplXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGF0dGFjaG1lbnQpXHJcbiAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gTWFrZSBVSSBhY3RpdmUgKGhpZGUgYWRkIGltYWdlIGJ1dHRvbiwgc2hvdyBjYW52YXMpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICRmcC5hZGRDbGFzcygnYWN0aXZlJyk7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNyYyA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzcmMgPSBhdHRhY2htZW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gU2V0IGltYWdlIHRvIG5ldyBzcmMsIHRyaWdnZXJpbmcgb24gbG9hZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAkaW1nLmF0dHIoJ3NyYycsIHNyYy51cmwpO1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIFVwZGF0ZSBvdXIgcG9zdCB2YWx1ZXMgYW5kIHZhbHVlcyBvYmpcclxuICAgICAgICAgICAgICAgICAgICAgICAgJGlkLnZhbChhdHRhY2htZW50LmlkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWVzLmlkID0gYXR0YWNobWVudC5pZDtcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gRmluYWxseSwgb3BlbiB0aGUgbW9kYWxcclxuICAgICAgICAgICAgICAgICAgICBmaWxlX2ZyYW1lLm9wZW4oKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgXHJcbiAgICBcclxuICAgICAgICAgICAgICAgIC8vIFdoZW4gd2UgY2xpY2sgdGhlIGRlbGV0ZSBpbWFnZSBidXR0b24uLi5cclxuICAgICAgICAgICAgICAgICRkZWwub24oJ2NsaWNrJywgZnVuY3Rpb24oKXtcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIFJlc2V0IERPTSBpbWFnZSBhdHRyaWJ1dGVzXHJcbiAgICAgICAgICAgICAgICAgICAgJGltZy5yZW1vdmVBdHRyKCdzcmMgd2lkdGggaGVpZ2h0Jyk7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgICAgICAvLyBIaWRlIGNhbnZhcyBhbmQgc2hvdyBhZGQgaW1hZ2UgYnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgICAgJGZwLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIFJlc2V0IG91ciBwb3N0IHZhbHVlc1xyXG4gICAgICAgICAgICAgICAgICAgICRpZC52YWwoJycpO1xyXG4gICAgICAgICAgICAgICAgICAgICR0b3AudmFsKCcnKTtcclxuICAgICAgICAgICAgICAgICAgICAkbGVmdC52YWwoJycpO1xyXG4gICAgICAgICAgICAgICAgICAgICRyaWdodC52YWwoJycpO1xyXG4gICAgICAgICAgICAgICAgICAgICRib3R0b20udmFsKCcnKTtcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIEFuZCBvdXIgdmFsdWVzIG9iaiwgYnV0IGp1c3Qgb25lIHZhbHVlICh0byBjaGVjayBsYXRlcikgd2lsbCBkby5cclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZXMudG9wID0gbnVsbDtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAvLyBXaGVuIHdlIGNsaWNrIGFuZCBtb3ZlIG9uIHRoZSBjYW52YXMuLi5cclxuICAgICAgICAgICAgICAgIGNhbnZhcy5hZGRFdmVudExpc3RlbmVyKFwibW91c2Vtb3ZlXCIsIGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAobW91c2VEb3duKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEFwcGx5UG9pbnRPdkNhbnZhcyhlKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCBmYWxzZSk7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgIGNhbnZhcy5hZGRFdmVudExpc3RlbmVyKFwibW91c2Vkb3duXCIsIGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICAgICAgICAgICAgICBtb3VzZURvd24gPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIEFwcGx5UG9pbnRPdkNhbnZhcyhlKTtcclxuICAgICAgICAgICAgICAgIH0sIGZhbHNlKTtcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgY2FudmFzLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZXVwXCIsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIG1vdXNlRG93biA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfSwgZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICBmdW5jdGlvbiBBcHBseVBvaW50T3ZDYW52YXMgKGUpIHtcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIEtlZXAgZHJhd2luZyBpbWFnZSBhcyBib3R0b20gbGF5ZXIgXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gKG90aGVyd2lzZSB3ZSBnZXQgbXVsdGlwbGUgbGF5ZXJzIG9mIHRoZSBmb2N1cywgbWFraW5nIGl0IG9wYXF1ZSlcclxuICAgICAgICAgICAgICAgICAgICBkcmF3SW1nKCk7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgICAgICAvLyBTZXQgdGhlIHBvaW50IG9mIHRoZSBjbGlja1xyXG4gICAgICAgICAgICAgICAgICAgIHJlY3Quc3RhcnRYID0gZS5sYXllclg7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVjdC5zdGFydFkgPSBlLmxheWVyWTtcclxuICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAvLyBDcmVhdGUgdGhlIGRpc3RhbmNlIHdlIHdhbnQgZnJvbSB0aGUgY2xpY2sgcG9pbnRcclxuICAgICAgICAgICAgICAgICAgICByZWN0LncgPSA4O1xyXG4gICAgICAgICAgICAgICAgICAgIHJlY3QuaCA9IDg7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyggdmFsdWVzLmxlZnQgKTtcclxuICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyggdmFsdWVzLnRvcCApO1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gUHV0IHBvc2l0aW9ucyBpbiBvdXIgdmFsdWVzIG9iamVjdFxyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlcy50b3AgXHRcdD0gcmVjdC5zdGFydFkgLyBjYW52YXNIZWlnaHQ7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWVzLmxlZnQgXHQ9IHJlY3Quc3RhcnRYIC8gY2FudmFzV2lkdGg7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWVzLndpZHRoIFx0PSAocmVjdC53ICsgcmVjdC5zdGFydFgpIC8gY2FudmFzV2lkdGg7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWVzLmhlaWdodCBcdD0gKHJlY3QuaCArIHJlY3Quc3RhcnRZKSAvIGNhbnZhc0hlaWdodDtcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIFNldCBwb3N0IHZhbHVlc1xyXG4gICAgICAgICAgICAgICAgICAgICR0b3AudmFsKHZhbHVlcy50b3AudG9GaXhlZCgyKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgJGxlZnQudmFsKHZhbHVlcy5sZWZ0LnRvRml4ZWQoMikpO1xyXG4gICAgICAgICAgICAgICAgICAgICRyaWdodC52YWwodmFsdWVzLndpZHRoLnRvRml4ZWQoMikpO1xyXG4gICAgICAgICAgICAgICAgICAgICRib3R0b20udmFsKHZhbHVlcy5oZWlnaHQudG9GaXhlZCgyKSk7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgICAgICAvLyBkcmF3IGZvY2FsIHBvaW50XHJcbiAgICAgICAgICAgICAgICAgICAgZHJhd0ZvY3VzKHJlY3Quc3RhcnRYLCByZWN0LnN0YXJ0WSwgcmVjdC53LCByZWN0LmgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xyXG4gICAgICAgICAgICAgICAgLy8gT2xkIGZ1bmN0aW9ucyBhbGxvdyBmb3IgZHJhZ2dpbmcgLy9cclxuICAgICAgICAgICAgICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXHJcbiAgICBcclxuICAgICAgICAgICAgICAgIC8vIC8vIFdoZW4gd2UgY2xpY2sgb24gY2FudmFzLi4uXHJcbiAgICAgICAgICAgICAgICAvLyBjYW52YXMuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlZG93blwiLCBmdW5jdGlvbihlKSB7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgIC8vIFx0Ly8gVHJhY2sgb3VyIHBvc2l0aW9uXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgcmVjdC5zdGFydFggPSBlLmxheWVyWDtcclxuICAgICAgICAgICAgICAgIC8vICAgICByZWN0LnN0YXJ0WSA9IGUubGF5ZXJZO1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgLy8gQW5kIGFsbG93IGRyYXdpbmdcclxuICAgICAgICAgICAgICAgIC8vICAgICBtb3VzZURvd24gXHQ9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAvLyB9LCBmYWxzZSk7XHJcbiAgICBcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgLy8gV2hlbiB3ZSBzdG9wcGVkIGhvbGRpbmcgZG93biBtb3VzZSBidXR0b24sIHByZXZlbnQgZnVydGhlciBkcmF3aW5nLlxyXG4gICAgICAgICAgICAgICAgLy8gY2FudmFzLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZXVwXCIsIGZ1bmN0aW9uKCkgeyBtb3VzZURvd24gPSBmYWxzZTsgfSwgZmFsc2UpO1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAvLyAvLyBXaGVuIG1vdXNlIGJ1dHRvbiBpcyBkb3duIGFuZCB3ZSdyZSBtb3ZpbmcgdGhlIG1vdXNlXHJcbiAgICAgICAgICAgICAgICAvLyBjYW52YXMuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlbW92ZVwiLCBmdW5jdGlvbihlKSB7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgIC8vICAgICBpZiAobW91c2VEb3duKSB7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgIC8vICAgICBcdC8vIEtlZXAgZHJhd2luZyBpbWFnZSBhcyBib3R0b20gbGF5ZXIgXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgXHQvLyAob3RoZXJ3aXNlIHdlIGdldCBtdWx0aXBsZSBsYXllcnMgb2YgdGhlIGZvY3VzLCBtYWtpbmcgaXQgb3BhcXVlKVxyXG4gICAgICAgICAgICAgICAgLy8gICAgICAgICBkcmF3SW1nKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgLy8gICAgICAgICAvLyBHZXQgZGlzdGFuY2UgZnJvbSB3aGVuIHdlIGZpcnN0IGNsaWNrZWQgb24gY2FudmFzXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgICAgIHJlY3QudyBcdFx0XHQ9IChlLmxheWVyWCkgLSByZWN0LnN0YXJ0WDtcclxuICAgICAgICAgICAgICAgIC8vICAgICAgICAgcmVjdC5oIFx0XHRcdD0gKGUubGF5ZXJZKSAtIHJlY3Quc3RhcnRZO1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgICAgIC8vIFB1dCBwb3NpdGlvbnMgaW4gb3VyIHZhbHVlcyBvYmplY3RcclxuICAgICAgICAgICAgICAgIC8vICAgICAgICAgdmFsdWVzLnRvcCBcdFx0PSByZWN0LnN0YXJ0WSAvIGNhbnZhc0hlaWdodDtcclxuICAgICAgICAgICAgICAgIC8vICAgICAgICAgdmFsdWVzLmxlZnQgXHQ9IHJlY3Quc3RhcnRYIC8gY2FudmFzV2lkdGg7XHJcbiAgICAgICAgICAgICAgICAvLyAgICAgICAgIHZhbHVlcy53aWR0aCBcdD0gKHJlY3QudyArIHJlY3Quc3RhcnRYKSAvIGNhbnZhc1dpZHRoO1xyXG4gICAgICAgICAgICAgICAgLy8gICAgICAgICB2YWx1ZXMuaGVpZ2h0IFx0PSAocmVjdC5oICsgcmVjdC5zdGFydFkpIC8gY2FudmFzSGVpZ2h0O1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgICAgIC8vIFNldCBwb3N0IHZhbHVlc1xyXG4gICAgICAgICAgICAgICAgLy8gICAgIFx0JHRvcC52YWwodmFsdWVzLnRvcC50b0ZpeGVkKDIpKTtcclxuICAgICAgICAgICAgICAgIC8vICAgICBcdCRsZWZ0LnZhbCh2YWx1ZXMubGVmdC50b0ZpeGVkKDIpKTtcclxuICAgICAgICAgICAgICAgIC8vICAgICBcdCRyaWdodC52YWwodmFsdWVzLndpZHRoLnRvRml4ZWQoMikpO1xyXG4gICAgICAgICAgICAgICAgLy8gICAgIFx0JGJvdHRvbS52YWwodmFsdWVzLmhlaWdodC50b0ZpeGVkKDIpKTtcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgLy8gICAgIFx0Ly8gZHJhdyBmb2NhbCBwb2ludFxyXG4gICAgICAgICAgICAgICAgLy8gICAgICAgICBkcmF3Rm9jdXMocmVjdC5zdGFydFgsIHJlY3Quc3RhcnRZLCByZWN0LncsIHJlY3QuaCk7XHJcbiAgICAgICAgICAgICAgICAvLyAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy8gfSwgZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICBcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgLy8gVXNlZCB0byBkcmF3IHRoZSBpbWFnZSBvbnRvIHRoZSBjYW52YXNcclxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uIGRyYXdJbWcoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gUmF0aW9zIHByZXZpb3VzbHkgd29ya2VkIG91dCAocmVzaXplQ2FudmFzKSwgc28gaXQgc2hvdWxkIGZpbGwgY2FudmFzXHJcbiAgICAgICAgICAgICAgICAgICAgY3R4LmRyYXdJbWFnZShpbWcsIDAsIDAsIGNhbnZhc1dpZHRoLCBjYW52YXNIZWlnaHQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAvLyBVc2VkIHRvIGRyYXcgZm9jYWwgcG9pbnQgb24gY2FudmFzXHJcbiAgICAgICAgICAgICAgICBmdW5jdGlvbiBkcmF3Rm9jdXMoeCwgeSwgdywgaCkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBhbHdheXMgZW5zdXJlIHRoZSB2YWx1ZSBpcyA+IDBcclxuICAgICAgICAgICAgICAgICAgICB2YXIgeCA9IE1hdGguYWJzKHgpXHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIHkgPSBNYXRoLmFicyh5KVxyXG4gICAgICAgICAgICAgICAgICAgIHZhciB3ID0gTWF0aC5hYnModylcclxuICAgICAgICAgICAgICAgICAgICB2YXIgaCA9IE1hdGguYWJzKGgpXHJcbiAgICBcclxuICAgICAgICAgICAgICAgICAgICBjdHguc3Ryb2tlU3R5bGUgPSBcInJnYmEoMjU1LCAyNTUsIDI1NSwgMC44KVwiO1xyXG4gICAgICAgICAgICAgICAgICAgIGN0eC5maWxsU3R5bGUgPSBcInJnYmEoMTY0LCA1NiwgNTYsIDAuOFwiO1xyXG4gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIGN0eC5iZWdpblBhdGgoKTtcclxuICAgICAgICAgICAgICAgICAgICBjdHguYXJjKHgseSx3LDAsMipNYXRoLlBJKTtcclxuICAgICAgICAgICAgICAgICAgICBjdHguZmlsbCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGN0eC5zdHJva2UoKTtcclxuICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAvLyBjdHguc3Ryb2tlUmVjdCh4LCB5LCB3LCBoKTtcclxuICAgICAgICAgICAgICAgICAgICAvLyBjdHguZmlsbFJlY3QoeCwgeSwgdywgaCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICBcclxuICAgICAgICAgICAgICAgIC8vIFVzZWQgdG8gZHJhdyBmb2NhbCBwb2ludCBvbiBsb2FkXHJcbiAgICAgICAgICAgICAgICBmdW5jdGlvbiByZWRyYXdGb2N1cygpIHtcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGlmIGV4aXN0aW5nIHZhbHVlcyBzZXQuLi5cclxuICAgICAgICAgICAgICAgICAgICBpZiAodmFsdWVzLnRvcCAhPT0gbnVsbCkge1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIEdldCBvdXIgcG9zaXRpb25zXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciB4ID0gdmFsdWVzLmxlZnQgKiBjYW52YXNXaWR0aCwgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB5ID0gdmFsdWVzLnRvcCAqIGNhbnZhc0hlaWdodCwgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3ID0gKHZhbHVlcy53aWR0aCAqIGNhbnZhc1dpZHRoKSAtIHgsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaCA9ICh2YWx1ZXMuaGVpZ2h0ICAqIGNhbnZhc0hlaWdodCkgLSB5O1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGRyYXcgZm9jdWFsIHBvaW50XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRyYXdGb2N1cyh4LCB5LCB3LCBoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICBcclxuICAgICAgICAgICAgICAgIC8vIFNob3J0Y3V0IHRvIGNhbGxpbmcgY2FudmFzIGRyYXcgZnVuY3Rpb25zXHJcbiAgICAgICAgICAgICAgICBmdW5jdGlvbiBkcmF3Q2FudmFzKCkge1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gcmVzaXplLCByZWRyYXcsIHJlZm9jdXNcclxuICAgICAgICAgICAgICAgICAgICByZXNpemVDYW52YXMoKTtcclxuICAgICAgICAgICAgICAgICAgICBkcmF3SW1nKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVkcmF3Rm9jdXMoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgIFxyXG4gICAgICAgICAgICAgICAgLy8gVXNlZCB0byBjbGVhciBjYW52YXNcclxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uIGNsZWFyQ2FudmFzKCkge1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gRmFzdGVyIHRoYW4gY2xlYXJSZWN0XHJcbiAgICAgICAgICAgICAgICAgICAgY3R4LmZpbGxTdHlsZSA9IFwiI2ZmZmZmZlwiO1xyXG4gICAgICAgICAgICAgICAgICAgIGN0eC5maWxsUmVjdCgwLCAwLCBjYW52YXNXaWR0aCwgY2FudmFzSGVpZ2h0KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgIFxyXG4gICAgICAgICAgICAgICAgLy8gVXNlZCB0byBzZXQgdXAgY2FudmFzIHNpemluZ1xyXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gcmVzaXplQ2FudmFzKCkge1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gR2V0IG5hdHVyYWwgaW1nZSBzaXplc1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBuYXR1cmFsX3dpZHRoIFx0PSBpbWcubmF0dXJhbFdpZHRoLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYXR1cmFsX2hlaWdodCBcdD0gaW1nLm5hdHVyYWxIZWlnaHQsXHJcbiAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gR2V0IGltYWdlIHdpZHRoL2hlaWdodCByYXRpb1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByYXRpbyBcdFx0XHQ9IG5hdHVyYWxfd2lkdGggLyBuYXR1cmFsX2hlaWdodCxcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBHZXQgcGFyZW50IHdpZHRoIChhbm5veWluZ2x5LCB3ZSBoYXZlIHRvIGFjY291bnQgZm9yIGRlbGV0ZSBidXR0b24pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmVudF93aWR0aCBcdFx0PSAkZWwucGFyZW50KCkud2lkdGgoKSAtICgkZGVsLndpZHRoKCkvMiksXHJcbiAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gdHdlYWsgaGlkZGVuIGVsZW1lbnQgLSBpZiB5b3UgdXNpbmcgdGFiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmVudF93aWR0aCA9IHBhcmVudF93aWR0aCA8IDAgPyBuYXR1cmFsX3dpZHRoIDogcGFyZW50X3dpZHRoLFxyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIFRvIGhvbGQgbmV3IGNhbnZhcyB3aWR0aHNcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmV3X3dpZHRoLCBuZXdfaGVpZ2h0O1xyXG4gICAgXHJcbiAgICBcclxuICAgICAgICAgICAgICAgICAgICAvLyBJZiBpbWFnZSBpcyBuYXR1cmFsbHkgYmlnZ2VyIHRoYW4gcGFyZW50Li4uXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKG5hdHVyYWxfd2lkdGggPiBwYXJlbnRfd2lkdGgpIHtcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBTZXQgdG8gZnVsbCB3aWR0aCAoc2FtZSBhcyBwYXJlbnQpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5ld193aWR0aCBcdD0gcGFyZW50X3dpZHRoO1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIEFuZCB1c2UgcmF0aW8gdG8gd29yayBvdXQgbmV3IHByb3BvcnRpb25hbCBoZWlnaHRcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmV3X2hlaWdodCBcdD0gcGFyZW50X3dpZHRoIC8gcmF0aW87XHJcbiAgICBcclxuICAgICAgICAgICAgICAgICAgICAvLyBPdGhlcndpc2UuLi5cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIFNldCB0byBzYW1lIHdpZHRoL2hlaWdodCBhcyBpbWFnZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuZXdfd2lkdGggXHQ9IG5hdHVyYWxfd2lkdGg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5ld19oZWlnaHQgXHQ9IG5hdHVyYWxfaGVpZ2h0O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgIFxyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gU2V0IGNhbnZhcyBET00gd2lkdGhcclxuICAgICAgICAgICAgICAgICAgICAkY2FudmFzLndpZHRoKG5ld193aWR0aCk7XHJcbiAgICAgICAgICAgICAgICAgICAgJGNhbnZhcy5oZWlnaHQobmV3X2hlaWdodCk7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgICAgICAvLyBBbmQgY2FudmFzIGF0dHJpYnV0ZSB3aWR0aHMgXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gKG90aGVyd2lzZSBpdCBnZXRzIGEgd2VpcmQgY29vcmQgc3lzdGVtKVxyXG4gICAgICAgICAgICAgICAgICAgIGNhbnZhcy53aWR0aCA9IG5ld193aWR0aDtcclxuICAgICAgICAgICAgICAgICAgICBjYW52YXMuaGVpZ2h0ID0gbmV3X2hlaWdodDtcclxuICAgIFxyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gUmVtZW1iZXIgb3VyIG5ldyBzaXplc1xyXG4gICAgICAgICAgICAgICAgICAgIGNhbnZhc1dpZHRoID0gbmV3X3dpZHRoO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhbnZhc0hlaWdodCA9IG5ld19oZWlnaHQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGlmKCB0eXBlb2YgYWNmLmFkZF9hY3Rpb24gIT09ICd1bmRlZmluZWQnICkge1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICBhY2YuYWRkX2FjdGlvbignbG9hZCcsIGZ1bmN0aW9uKCAkZWwgKXtcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHNlYXJjaCAkZWwgZm9yIGZpZWxkcyBvZiB0eXBlICdmb2NhbF9wb2ludCdcclxuICAgICAgICAgICAgICAgICAgICBhY2YuZ2V0X2ZpZWxkcyh7IHR5cGUgOiAnZm9jYWxfcG9pbnQnfSwgJGVsKS5lYWNoKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpbml0aWFsaXplX2ZpZWxkKCAkKHRoaXMpICk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgIGFjZi5hZGRfYWN0aW9uKCdhcHBlbmQnLCBmdW5jdGlvbiggJGVsICl7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgICAgICAvLyBzZWFyY2ggJGVsIGZvciBmaWVsZHMgb2YgdHlwZSAnZm9jYWxfcG9pbnQnXHJcbiAgICAgICAgICAgICAgICAgICAgYWNmLmdldF9maWVsZHMoeyB0eXBlIDogJ2ZvY2FsX3BvaW50J30sICRlbCkuZWFjaChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgaW5pdGlhbGl6ZV9maWVsZCggJCh0aGlzKSApO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIC8qXHJcbiAgICAgICAgICAgICAgICAqICBhY2Yvc2V0dXBfZmllbGRzIChBQ0Y0KVxyXG4gICAgICAgICAgICAgICAgKlxyXG4gICAgICAgICAgICAgICAgKiAgVGhpcyBldmVudCBpcyB0cmlnZ2VyZWQgd2hlbiBBQ0YgYWRkcyBhbnkgbmV3IGVsZW1lbnRzIHRvIHRoZSBET00uIFxyXG4gICAgICAgICAgICAgICAgKlxyXG4gICAgICAgICAgICAgICAgKiAgQHR5cGVcdGZ1bmN0aW9uXHJcbiAgICAgICAgICAgICAgICAqICBAc2luY2VcdDEuMC4wXHJcbiAgICAgICAgICAgICAgICAqICBAZGF0ZVx0MDEvMDEvMTJcclxuICAgICAgICAgICAgICAgICpcclxuICAgICAgICAgICAgICAgICogIEBwYXJhbVx0ZXZlbnRcdFx0ZTogYW4gZXZlbnQgb2JqZWN0LiBUaGlzIGNhbiBiZSBpZ25vcmVkXHJcbiAgICAgICAgICAgICAgICAqICBAcGFyYW1cdEVsZW1lbnRcdFx0cG9zdGJveDogQW4gZWxlbWVudCB3aGljaCBjb250YWlucyB0aGUgbmV3IEhUTUxcclxuICAgICAgICAgICAgICAgICpcclxuICAgICAgICAgICAgICAgICogIEByZXR1cm5cdG4vYVxyXG4gICAgICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgJChkb2N1bWVudCkub24oJ2FjZi9zZXR1cF9maWVsZHMnLCBmdW5jdGlvbihlLCBwb3N0Ym94KXtcclxuICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAkKHBvc3Rib3gpLmZpbmQoJy5maWVsZFtkYXRhLWZpZWxkX3R5cGU9XCJmb2NhbF9wb2ludFwiXScpLmVhY2goZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGluaXRpYWxpemVfZmllbGQoICQodGhpcykgKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIHJlc2VuZCBwcm9udG8geG1sXHJcbiAgICAgICAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgJ2J1dHRvbltuYW1lPVwicmVzZW5kLXRvLXByb250b1wiXScsIGZ1bmN0aW9uKGUpe1xyXG5cclxuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpXHJcbiAgICAgICAgICAgIHZhciBwYXJlbiA9ICQodGhpcykucGFyZW50cygnLmluc2lkZScpXHJcbiAgICAgICAgICAgIHBhcmVuLmFkZENsYXNzKCdsb2FkaW5nJylcclxuXHJcbiAgICAgICAgICAgIHZhciBvcmRlcl9pZCA9ICQodGhpcykuYXR0cignZGF0YS1vcmRlci1pZCcpXHJcblxyXG4gICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgdXJsOiBhamF4dXJsLFxyXG4gICAgICAgICAgICAgICAgdHlwZTogJ1BPU1QnLFxyXG4gICAgICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ3dlYmVyX3JlX3B1c2hfeG1sX3Byb250bycsXHJcbiAgICAgICAgICAgICAgICAgICAgb3JkZXJfaWQ6IG9yZGVyX2lkXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgY2FjaGU6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgZGF0YVR5cGU6ICdqc29uJyxcclxuICAgICAgXHJcbiAgICAgICAgICAgICAgICBiZWZvcmVTZW5kOiBmdW5jdGlvbigganFYSFIsIHNldHRpbmdzICl7XHJcbiAgICAgICAgICAgICAgICAgICAgJCgnI3Jlc2VuZC10by1wcm9udG8tcmVzdWx0cycpLmh0bWwoJycpXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24oIGRhdGEsIHRleHRTdGF0dXMsIGpxWEhSICkge1xyXG4gICAgICAgICAgICAgICAgICAgaWYoIGRhdGEucmVzdWx0ID09ICdlcnJvcicpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJlbi5yZW1vdmVDbGFzcygnbG9hZGluZycpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJyNyZXNlbmQtdG8tcHJvbnRvLXJlc3VsdHMnKS5odG1sKCc8ZGl2IGNsYXNzPVwiaW5uZXJcIj4nKyBkYXRhLm1lc3NhZ2UgKyc8L2Rpdj4nKVxyXG4gICAgICAgICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLnJlbG9hZCgpXHJcbiAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgY29tcGxldGU6IGZ1bmN0aW9uKGpxWEhSLCB0ZXh0U3RhdHVzKXtcclxuICAgICAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKCAnQUpBWCBpcyBjb21wbGV0ZS4nICk7IFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTsvLyBFbmQgQUpBWC5cclxuXHJcbiAgICAgICAgfSlcclxuXHJcbiAgICAvLyBBQ0YgV2ViZXIgQ3JlYXRlIENvZGVcclxuICAgIGlmKCQoZG9jdW1lbnQpLmZpbmQoJ2lucHV0W25hbWU9XCJfd19zdGFydF9kYXRlXCJdJykubGVuZ3RoID4gMCl7XHJcbiAgICAgICAgJChkb2N1bWVudCkuZmluZCgnaW5wdXRbbmFtZT1cIl93X3N0YXJ0X2RhdGVcIl0nKS5kYXRlcGlja2VyKHtcclxuICAgICAgICAgICAgZGF0ZUZvcm1hdDogXCJ5eS1tbS1kZFwiLFxyXG4gICAgICAgICAgICBtaW5EYXRlOiAnLTFXJyxcclxuICAgICAgICAgICAgb25TZWxlY3Q6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHZhciBkdDIgPSAkKGRvY3VtZW50KS5maW5kKCdpbnB1dFtuYW1lPVwiX3dfZW5kX2RhdGVcIl0nKTtcclxuICAgICAgICAgICAgICAgIHZhciBzdGFydERhdGUgPSAkKHRoaXMpLmRhdGVwaWNrZXIoJ2dldERhdGUnKTtcclxuICAgICAgICAgICAgICAgIC8vYWRkIDMwIGRheXMgdG8gc2VsZWN0ZWQgZGF0ZVxyXG4gICAgICAgICAgICAgICAgc3RhcnREYXRlLnNldERhdGUoc3RhcnREYXRlLmdldERhdGUoKSArIDMwKTtcclxuICAgICAgICAgICAgICAgIHZhciBtaW5EYXRlID0gJCh0aGlzKS5kYXRlcGlja2VyKCdnZXREYXRlJyk7XHJcbiAgICAgICAgICAgICAgICB2YXIgZHQyRGF0ZSA9IGR0Mi5kYXRlcGlja2VyKCdnZXREYXRlJyk7XHJcbiAgICAgICAgICAgICAgICAvL2RpZmZlcmVuY2UgaW4gZGF5cy4gODY0MDAgc2Vjb25kcyBpbiBkYXksIDEwMDAgbXMgaW4gc2Vjb25kXHJcbiAgICAgICAgICAgICAgICB2YXIgZGF0ZURpZmYgPSAoZHQyRGF0ZSAtIG1pbkRhdGUpLyg4NjQwMCAqIDEwMDApO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vZHQyIG5vdCBzZXQgb3IgZHQxIGRhdGUgaXMgZ3JlYXRlciB0aGFuIGR0MiBkYXRlXHJcbiAgICAgICAgICAgICAgICBpZiAoZHQyRGF0ZSA9PSBudWxsIHx8IGRhdGVEaWZmIDwgMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkdDIuZGF0ZXBpY2tlcignc2V0RGF0ZScsIG1pbkRhdGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy9kdDEgZGF0ZSBpcyAzMCBkYXlzIHVuZGVyIGR0MiBkYXRlXHJcbiAgICAgICAgICAgICAgICBlbHNlIGlmIChkYXRlRGlmZiA+IDMwKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZHQyLmRhdGVwaWNrZXIoJ3NldERhdGUnLCBzdGFydERhdGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy9zZXRzIGR0MiBtYXhEYXRlIHRvIHRoZSBsYXN0IGRheSBvZiAzMCBkYXlzIHdpbmRvd1xyXG4gICAgICAgICAgICAgICAgLy9kdDIuZGF0ZXBpY2tlcignb3B0aW9uJywgJ21heERhdGUnLCBzdGFydERhdGUpO1xyXG4gICAgICAgICAgICAgICAgLy9maXJzdCBkYXkgd2hpY2ggY2FuIGJlIHNlbGVjdGVkIGluIGR0MiBpcyBzZWxlY3RlZCBkYXRlIGluIGR0MVxyXG4gICAgICAgICAgICAgICAgZHQyLmRhdGVwaWNrZXIoJ29wdGlvbicsICdtaW5EYXRlJywgbWluRGF0ZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICAkKGRvY3VtZW50KS5maW5kKCdpbnB1dFtuYW1lPVwiX3dfZW5kX2RhdGVcIl0nKS5kYXRlcGlja2VyKHtcclxuICAgICAgICAgICAgZGF0ZUZvcm1hdDogXCJ5eS1tbS1kZFwiLFxyXG4gICAgICAgICAgICBtaW5EYXRlOiAwXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gb24gY2xpY2sgc3VibWl0XHJcbiAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCcjd2ViZXItYnVzaW5lc3MtY29kZXMtYWRkICNnZW5lcmF0ZS1jb2RlJyxmdW5jdGlvbihlKXtcclxuICAgICAgICBcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KClcclxuICAgICAgICB2YXIgJHRoaXMgPSAkKHRoaXMpXHJcbiAgICAgICAgJHRoaXMuY2xvc2VzdCgndHInKS5maW5kKCdpbnB1dC5lcnJvcicpLnJlbW92ZUNsYXNzKCdlcnJvcicpXHJcbiAgICAgICAgJHRoaXMuY2xvc2VzdCgndHInKS5maW5kKCdpbnB1dCcpLmZpbHRlcihmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICByZXR1cm4gIXRoaXMudmFsdWU7XHJcbiAgICAgICAgfSkuYWRkQ2xhc3MoJ2Vycm9yJylcclxuXHJcbiAgICAgICAgaWYoJHRoaXMuY2xvc2VzdCgndHInKS5maW5kKCdpbnB1dC5lcnJvcicpLmxlbmd0aCA+IDApe1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB2YXIgJHBhcmVudCA9ICQoZG9jdW1lbnQpLmZpbmQoJyN3ZWJlci1idXNpbmVzcy1jb2RlcycpXHJcblxyXG4gICAgICAgIC8vIGRvIGFqYXhcclxuICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICB1cmw6IGFqYXh1cmwsXHJcbiAgICAgICAgICAgIHR5cGU6ICdQT1NUJyxcclxuICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAnd2ViZXJfZG9fZ2VuZXJhdGVfYnVzaW5lc3NfY29kZScsXHJcbiAgICAgICAgICAgICAgICBuYW1lOiAkKGRvY3VtZW50KS5maW5kKCdpbnB1dFtuYW1lPVwiX3dfY2FtcGFpZ25fbmFtZVwiXScpLnZhbCgpLFxyXG4gICAgICAgICAgICAgICAgc3RhcnRfZGF0ZTogJChkb2N1bWVudCkuZmluZCgnaW5wdXRbbmFtZT1cIl93X3N0YXJ0X2RhdGVcIl0nKS52YWwoKSxcclxuICAgICAgICAgICAgICAgIGVuZF9kYXRlOiAkKGRvY3VtZW50KS5maW5kKCdpbnB1dFtuYW1lPVwiX3dfZW5kX2RhdGVcIl0nKS52YWwoKSxcclxuICAgICAgICAgICAgICAgIGFtb3VudDogJChkb2N1bWVudCkuZmluZCgnaW5wdXRbbmFtZT1cIl93X3RvdGFsX2NvZGVcIl0nKS52YWwoKSxcclxuICAgICAgICAgICAgICAgIHBvc3RfaWQ6ICR0aGlzLmF0dHIoJ2RhdGEtaWQnKSxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgY2FjaGU6IGZhbHNlLFxyXG4gICAgICAgICAgICBkYXRhVHlwZTogJ2pzb24nLFxyXG4gICAgXHJcbiAgICAgICAgICAgIGJlZm9yZVNlbmQ6IGZ1bmN0aW9uKCBqcVhIUiwgc2V0dGluZ3MgKXtcclxuICAgICAgICAgICAgICAgICRwYXJlbnQuYWRkQ2xhc3MoJ2xvYWRpbmcnKVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiggZGF0YSwgdGV4dFN0YXR1cywganFYSFIgKSB7XHJcbiAgICAgICAgICAgICAgICAkcGFyZW50LnJlbW92ZUNsYXNzKCdsb2FkaW5nJylcclxuICAgICAgICAgICAgICAgICQoZG9jdW1lbnQpLmZpbmQoJyNyZXBsYWNlLS1yZXN1bHRzJykucmVwbGFjZVdpdGgoZGF0YS5odG1sKVxyXG4gICAgICAgICAgICAgICAgJHRoaXMuY2xvc2VzdCgndHInKS5maW5kKCdpbnB1dCcpLnZhbCgnJylcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgY29tcGxldGU6IGZ1bmN0aW9uKGpxWEhSLCB0ZXh0U3RhdHVzKXtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCAnQUpBWCBpcyBjb21wbGV0ZS4nICk7IFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7Ly8gRW5kIEFKQVguXHJcblxyXG4gICAgfSlcclxuXHJcbiAgICAvLyBwYXVzZSBvciBkZWxldGUgY2FtcGFpZ25cclxuICAgICQoZG9jdW1lbnQpLm9uKCdjbGljaycsJy5jYW1wYWlnbi1hY3Rpb24tYnRuJyxmdW5jdGlvbihlKXtcclxuXHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpXHJcbiAgICAgICAgdmFyIGFjdGlvbiA9ICQodGhpcykuYXR0cignZGF0YS1hY3Rpb24nKVxyXG4gICAgICAgIHZhciB0aGVJRCA9ICQodGhpcykuYXR0cignZGF0YS1pZCcpXHJcbiAgICAgICAgdmFyIHBhcnRuZXJfaWQgPSAkKHRoaXMpLmF0dHIoJ2RhdGEtcGFydG5lcicpXHJcblxyXG4gICAgICAgIHZhciBpc19zdXJlID0gYWN0aW9uID09ICdkZWxldGUnID8gIGNvbmZpcm0oJ1RoaXMgYWN0aW9uIGNhbm5vdCBiZSByZXZlcnNpYmxlLCBhbGwgZ2VuZXJhdGVkIGFjY2VzcyBjb2RlIHdpbGwgYmUgZGVsZXRlZCBmcm9tIHRoZSBkYXRhYmFzZS4gQXJlIHlvdSBzdXJlIHdhbnQgdG8gZG8gdGhpcz8gJykgOiBjb25maXJtKCdBcmUgeW91IHN1cmUgd2FudCB0byBkbyB0aGlzPycpXHJcblxyXG4gICAgICAgIHZhciAkcGFyZW50ID0gJChkb2N1bWVudCkuZmluZCgnI3dlYmVyLWJ1c2luZXNzLWNvZGVzJylcclxuXHJcbiAgICAgICAgaWYoaXNfc3VyZSl7XHJcbiAgICAgICAgICAgIC8vIGRvIGFqYXhcclxuICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgIHVybDogYWpheHVybCxcclxuICAgICAgICAgICAgICAgIHR5cGU6ICdQT1NUJyxcclxuICAgICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICd3ZWJlcl9idXNpbmVzc19jb2RlX2FjdGlvbicsXHJcbiAgICAgICAgICAgICAgICAgICAgcnVuX2FjdGlvbjogYWN0aW9uLFxyXG4gICAgICAgICAgICAgICAgICAgIGNhbXBhaWduX2lkOiB0aGVJRCxcclxuICAgICAgICAgICAgICAgICAgICBwYXJ0bmVyX2lkOiBwYXJ0bmVyX2lkXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgY2FjaGU6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgZGF0YVR5cGU6ICdqc29uJyxcclxuICAgICAgICBcclxuICAgICAgICAgICAgICAgIGJlZm9yZVNlbmQ6IGZ1bmN0aW9uKCBqcVhIUiwgc2V0dGluZ3MgKXtcclxuICAgICAgICAgICAgICAgICAgICAkcGFyZW50LmFkZENsYXNzKCdsb2FkaW5nJylcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiggZGF0YSwgdGV4dFN0YXR1cywganFYSFIgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJHBhcmVudC5yZW1vdmVDbGFzcygnbG9hZGluZycpXHJcbiAgICAgICAgICAgICAgICAgICAgJChkb2N1bWVudCkuZmluZCgnI3JlcGxhY2UtLXJlc3VsdHMnKS5yZXBsYWNlV2l0aChkYXRhLmh0bWwpXHJcbiAgICAgICAgICAgICAgICAgICAgLy8kdGhpcy5jbG9zZXN0KCd0cicpLmZpbmQoJ2lucHV0JykudmFsKCcnKVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGNvbXBsZXRlOiBmdW5jdGlvbihqcVhIUiwgdGV4dFN0YXR1cyl7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coICdBSkFYIGlzIGNvbXBsZXRlLicgKTsgXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pOy8vIEVuZCBBSkFYLlxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9KVxyXG4gICAgICAgIFxyXG59KSIsIi8vIHJlbW92ZWQgYnkgZXh0cmFjdC10ZXh0LXdlYnBhY2stcGx1Z2luIiwiLy8gcmVtb3ZlZCBieSBleHRyYWN0LXRleHQtd2VicGFjay1wbHVnaW4iXSwic291cmNlUm9vdCI6IiJ9