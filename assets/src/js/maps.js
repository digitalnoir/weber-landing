// GMap Style
// Please refer to this site: https://snazzymaps.com/
var GMapStyle = [];

jQuery(function($){
    

	// Add Google Map
	$('.gmap').each(function () {
		initMap($(this).data("lat"), $(this).data("lng"), $(this).data("icon"), $(this) );
	});

	

	function initMap(lat, lng, icon_url, $container) {

		var viewCentreLat = lat;
		var viewCentreLng = lng;
		if ($(window).width() > 991) {
			// viewCentreLat -= 0.0035;
			// viewCentreLng += 0.015;
		}
		var mapClass = $container
		var map = new google.maps.Map( mapClass[0], {
			scrollwheel: false,
			zoom: 15,
			styles: GMapStyle,
			center: new google.maps.LatLng(viewCentreLat, viewCentreLng),
			mapTypeId: 'roadmap',
			disableDefaultUI: true
		});

		// Disable map drag on mobile
		// if ( $(window).width() < 992 ) {
		// map.setOptions({draggable: false});
		// }

		var IconSize = new google.maps.Size(55, 72);
		if ($(window).width() < 992) {
			IconSize = new google.maps.Size(39, 52);
		}

		var feature = {
			position: new google.maps.LatLng(lat, lng),
		};
		var icon = {
			url: icon_url,
			scaledSize: IconSize,
		};
		var marker = new google.maps.Marker({
			position: feature.position,
			icon: icon,
			scaledSize: new google.maps.Size(10, 10),
			map: map
		});

		// create info window
		var infowindow = new google.maps.InfoWindow({
			content: $container.siblings(".bubble-info").html(),
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function () {

			infowindow.open(map, marker);

		});
	}
})