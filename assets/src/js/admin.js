//import './plugins/tinymce.js'

jQuery(function($) {
    
        $( document ).ready(function() {
            FocalPointControls();
        });
    
        $(document).on('mouseenter', '.acf-fc-popup li a', function() {
    
            var $this = $(this)
            var parent = $this.parents('.acf-fc-popup');
            var filename = $this.attr('data-layout');  
    
    
            if (parent.find('.preview').length > 0) {
                parent.find('.preview').html('<div class="inner-preview"><img src="' + theme_var.upload + filename + '.jpg"/></div>')
            } else {
                parent.append('<div class="preview"><div class="inner-preview"><img src="' + theme_var.upload + filename + '.jpg"/></div></div>') 
            }
        })
    
        $(document).on('mouseleave', '.acf-fc-popup', function() {
    
            $(this).find('.preview').empty();
    
        })
    
        function FocalPointControls () { 
    
            // initialized on ACF events
            function initialize_field( $el ) {
    
                // Cache jquery selectors
                // Values to get/set
                var $id 	= $el.find('.acf-focal_point-id'),
                    $top 	= $el.find('.acf-focal_point-top'),
                    $left 	= $el.find('.acf-focal_point-left'),
                    $right 	= $el.find('.acf-focal_point-right'),
                    $bottom = $el.find('.acf-focal_point-bottom'),
    
                    // Elements to get/set 
                    $fp 	= $el.find('.acf-focal_point'),
                    $img 	= $el.find('.acf-focal_point-image'),
                    $canvas = $el.find('.acf-focal_point-canvas'),
    
                    // Buttons to trigger events
                    $add 	= $el.find('.add-image'),
                    $del 	= $el.find('.acf-button-delete');
    
    
                // Hold/get our values
                var values = {
                    id: 	$id.val(),
                    top: 	$top.val(),
                    left: 	$left.val(),
                    width: 	$right.val(),
                    height: $bottom.val(),
                    size: 	$fp.data('preview_size')
                };
                
    
                // DOM elements
                var img  	 = $img.get(0),
                    canvas 	 = $canvas.get(0);
    
    
                // To hold WP media frame
                var file_frame;
    
    
                // Vars for Canvas work
                var ctx 		= canvas.getContext("2d"),
                    rect 		= {},
                    mouseDown 	= false,
    
                    canvasWidth,
                    canvasHeight;
    
    
    
    
                // When we've loaded an image, draw the canvas.
                // (either on dom load or adding new image from WP media manager)
                $img.on("load", drawCanvas).each(function() {
                    
                    // Make sure to trigger load event by triggering load
                    // after jquery has done it's iteration
                    if (this.complete) {
                        $(this).load();
                    }
                });
    
    
                // When resizing the page, redraw the canvas.
                $(window).on('resize', drawCanvas);
    
    
                // When we click the add image button...
                $add.on('click', function(){
    
                    // If the media frame already exists, reopen it.
                    if ( file_frame ) {
                        file_frame.open();
                        return;
                    }
    
                    // Create the media frame.
                    file_frame = wp.media.frames.file_frame = wp.media({
                        title: 'Select Image',
                        button: { text: 'Select' }
                    });
    
                    // When an image is selected..
                    file_frame.on('select', function() {
    
                        // Get selected image objects
                        var attachment 	= file_frame.state().get('selection').first().toJSON(),
                            src 		= attachment.sizes[values.size];
                            console.log(attachment)
    
                        // Make UI active (hide add image button, show canvas)
                        $fp.addClass('active');
    
                        if (src === undefined) {
                            src = attachment;
                        }
    
                        // Set image to new src, triggering on load
                        $img.attr('src', src.url);
    
                        // Update our post values and values obj
                        $id.val(attachment.id);
                        values.id = attachment.id;
    
                    });
    
                    // Finally, open the modal
                    file_frame.open();
                });
    
    
                // When we click the delete image button...
                $del.on('click', function(){
    
                    // Reset DOM image attributes
                    $img.removeAttr('src width height');
    
                    // Hide canvas and show add image button
                    $fp.removeClass('active');
    
                    // Reset our post values
                    $id.val('');
                    $top.val('');
                    $left.val('');
                    $right.val('');
                    $bottom.val('');
    
                    // And our values obj, but just one value (to check later) will do.
                    values.top = null;
                });
                
                // When we click and move on the canvas...
                canvas.addEventListener("mousemove", function(e) {
                    if (mouseDown) {
                        ApplyPointOvCanvas(e);
                    }
                }, false);
    
                canvas.addEventListener("mousedown", function(e) {
                    mouseDown = true;
                    ApplyPointOvCanvas(e);
                }, false);
    
                canvas.addEventListener("mouseup", function() {
                    mouseDown = false;
                }, false);
                
                function ApplyPointOvCanvas (e) {
    
                    // Keep drawing image as bottom layer 
                    // (otherwise we get multiple layers of the focus, making it opaque)
                    drawImg();
    
                    // Set the point of the click
                    rect.startX = e.layerX;
                    rect.startY = e.layerY;
                    
                    // Create the distance we want from the click point
                    rect.w = 8;
                    rect.h = 8;
    
                    // console.log( values.left );
                    // console.log( values.top );
    
                    // Put positions in our values object
                    values.top 		= rect.startY / canvasHeight;
                    values.left 	= rect.startX / canvasWidth;
                    values.width 	= (rect.w + rect.startX) / canvasWidth;
                    values.height 	= (rect.h + rect.startY) / canvasHeight;
    
                    // Set post values
                    $top.val(values.top.toFixed(2));
                    $left.val(values.left.toFixed(2));
                    $right.val(values.width.toFixed(2));
                    $bottom.val(values.height.toFixed(2));
    
                    // draw focal point
                    drawFocus(rect.startX, rect.startY, rect.w, rect.h);
                }
    
                //////////////////////////////////////
                // Old functions allow for dragging //
                //////////////////////////////////////
    
                // // When we click on canvas...
                // canvas.addEventListener("mousedown", function(e) {
    
                // 	// Track our position
                //     rect.startX = e.layerX;
                //     rect.startY = e.layerY;
    
                //     // And allow drawing
                //     mouseDown 	= true;
                // }, false);
    
    
                // When we stopped holding down mouse button, prevent further drawing.
                // canvas.addEventListener("mouseup", function() { mouseDown = false; }, false);
    
                // // When mouse button is down and we're moving the mouse
                // canvas.addEventListener("mousemove", function(e) {
    
                //     if (mouseDown) {
    
                //     	// Keep drawing image as bottom layer 
                //     	// (otherwise we get multiple layers of the focus, making it opaque)
                //         drawImg();
                        
                //         // Get distance from when we first clicked on canvas
                //         rect.w 			= (e.layerX) - rect.startX;
                //         rect.h 			= (e.layerY) - rect.startY;
    
                //         // Put positions in our values object
                //         values.top 		= rect.startY / canvasHeight;
                //         values.left 	= rect.startX / canvasWidth;
                //         values.width 	= (rect.w + rect.startX) / canvasWidth;
                //         values.height 	= (rect.h + rect.startY) / canvasHeight;
    
                //         // Set post values
                //     	$top.val(values.top.toFixed(2));
                //     	$left.val(values.left.toFixed(2));
                //     	$right.val(values.width.toFixed(2));
                //     	$bottom.val(values.height.toFixed(2));
    
                //     	// draw focal point
                //         drawFocus(rect.startX, rect.startY, rect.w, rect.h);
                //     }
                // }, false);
                
                
    
                // Used to draw the image onto the canvas
                function drawImg() {
                    // Ratios previously worked out (resizeCanvas), so it should fill canvas
                    ctx.drawImage(img, 0, 0, canvasWidth, canvasHeight);
                }
    
                // Used to draw focal point on canvas
                function drawFocus(x, y, w, h) {

                    // always ensure the value is > 0
                    var x = Math.abs(x)
                    var y = Math.abs(y)
                    var w = Math.abs(w)
                    var h = Math.abs(h)
    
                    ctx.strokeStyle = "rgba(255, 255, 255, 0.8)";
                    ctx.fillStyle = "rgba(164, 56, 56, 0.8";
                    
                    ctx.beginPath();
                    ctx.arc(x,y,w,0,2*Math.PI);
                    ctx.fill();
                    ctx.stroke();
                    
                    // ctx.strokeRect(x, y, w, h);
                    // ctx.fillRect(x, y, w, h);
                }
    
                // Used to draw focal point on load
                function redrawFocus() {
    
                    // if existing values set...
                    if (values.top !== null) {
    
                        // Get our positions
                        var x = values.left * canvasWidth, 
                            y = values.top * canvasHeight, 
                            w = (values.width * canvasWidth) - x, 
                            h = (values.height  * canvasHeight) - y;
    
                        // draw focual point
                        drawFocus(x, y, w, h);
                    }
                }
    
                // Shortcut to calling canvas draw functions
                function drawCanvas() {
    
                    // resize, redraw, refocus
                    resizeCanvas();
                    drawImg();
                    redrawFocus();
                }
    
                // Used to clear canvas
                function clearCanvas() {
    
                    // Faster than clearRect
                    ctx.fillStyle = "#ffffff";
                    ctx.fillRect(0, 0, canvasWidth, canvasHeight);
                }
    
                // Used to set up canvas sizing
                function resizeCanvas() {
    
                    // Get natural imge sizes
                    var natural_width 	= img.naturalWidth,
                        natural_height 	= img.naturalHeight,
    
                        // Get image width/height ratio
                        ratio 			= natural_width / natural_height,
    
                        // Get parent width (annoyingly, we have to account for delete button)
                        parent_width 		= $el.parent().width() - ($del.width()/2),
    
                        // tweak hidden element - if you using tab
                        parent_width = parent_width < 0 ? natural_width : parent_width,
    
                        // To hold new canvas widths
                        new_width, new_height;
    
    
                    // If image is naturally bigger than parent...
                    if (natural_width > parent_width) {
    
                        // Set to full width (same as parent)
                        new_width 	= parent_width;
    
                        // And use ratio to work out new proportional height
                        new_height 	= parent_width / ratio;
    
                    // Otherwise...
                    } else {
    
                        // Set to same width/height as image
                        new_width 	= natural_width;
                        new_height 	= natural_height;
                    }
    
    
                    // Set canvas DOM width
                    $canvas.width(new_width);
                    $canvas.height(new_height);
    
                    // And canvas attribute widths 
                    // (otherwise it gets a weird coord system)
                    canvas.width = new_width;
                    canvas.height = new_height;
    
    
                    // Remember our new sizes
                    canvasWidth = new_width;
                    canvasHeight = new_height;
                }
                
            }
            
            
            if( typeof acf.add_action !== 'undefined' ) {
    
                acf.add_action('load', function( $el ){
    
                    // search $el for fields of type 'focal_point'
                    acf.get_fields({ type : 'focal_point'}, $el).each(function(){
                        
                        initialize_field( $(this) );
                        
                    });
                    
                });
    
                acf.add_action('append', function( $el ){
    
                    // search $el for fields of type 'focal_point'
                    acf.get_fields({ type : 'focal_point'}, $el).each(function(){
                        
                        initialize_field( $(this) );
                        
                    });
                })
                
                
            } else {
                
                
                /*
                *  acf/setup_fields (ACF4)
                *
                *  This event is triggered when ACF adds any new elements to the DOM. 
                *
                *  @type	function
                *  @since	1.0.0
                *  @date	01/01/12
                *
                *  @param	event		e: an event object. This can be ignored
                *  @param	Element		postbox: An element which contains the new HTML
                *
                *  @return	n/a
                */
                
                $(document).on('acf/setup_fields', function(e, postbox){
                    
                    $(postbox).find('.field[data-field_type="focal_point"]').each(function(){
                        
                        initialize_field( $(this) );
                        
                    });
                
                });
            
            
            }
        }

        // resend pronto xml
        $(document).on('click', 'button[name="resend-to-pronto"]', function(e){

            e.preventDefault()
            var paren = $(this).parents('.inside')
            paren.addClass('loading')

            var order_id = $(this).attr('data-order-id')

            $.ajax({
                url: ajaxurl,
                type: 'POST',
                data: {
                    action: 'weber_re_push_xml_pronto',
                    order_id: order_id
                },
                cache: false,
                dataType: 'json',
      
                beforeSend: function( jqXHR, settings ){
                    $('#resend-to-pronto-results').html('')
                },
                success: function( data, textStatus, jqXHR ) {
                   if( data.result == 'error'){
                        paren.removeClass('loading')
                        $('#resend-to-pronto-results').html('<div class="inner">'+ data.message +'</div>')
                   }else{
                       window.location.reload()
                   }
                },
                complete: function(jqXHR, textStatus){
                    //console.log( 'AJAX is complete.' ); 
                }
            });// End AJAX.

        })

    // ACF Weber Create Code
    if($(document).find('input[name="_w_start_date"]').length > 0){
        $(document).find('input[name="_w_start_date"]').datepicker({
            dateFormat: "yy-mm-dd",
            minDate: '-1W',
            onSelect: function () {
                var dt2 = $(document).find('input[name="_w_end_date"]');
                var startDate = $(this).datepicker('getDate');
                //add 30 days to selected date
                startDate.setDate(startDate.getDate() + 30);
                var minDate = $(this).datepicker('getDate');
                var dt2Date = dt2.datepicker('getDate');
                //difference in days. 86400 seconds in day, 1000 ms in second
                var dateDiff = (dt2Date - minDate)/(86400 * 1000);

                //dt2 not set or dt1 date is greater than dt2 date
                if (dt2Date == null || dateDiff < 0) {
                        dt2.datepicker('setDate', minDate);
                }
                //dt1 date is 30 days under dt2 date
                else if (dateDiff > 30){
                        dt2.datepicker('setDate', startDate);
                }
                //sets dt2 maxDate to the last day of 30 days window
                //dt2.datepicker('option', 'maxDate', startDate);
                //first day which can be selected in dt2 is selected date in dt1
                dt2.datepicker('option', 'minDate', minDate);
            }
        });
        $(document).find('input[name="_w_end_date"]').datepicker({
            dateFormat: "yy-mm-dd",
            minDate: 0
        });
    }

    // on click submit
    $(document).on('click','#weber-business-codes-add #generate-code',function(e){
        
        e.preventDefault()
        var $this = $(this)
        $this.closest('tr').find('input.error').removeClass('error')
        $this.closest('tr').find('input').filter(function(){
            return !this.value;
        }).addClass('error')

        if($this.closest('tr').find('input.error').length > 0){
            return;
        }

        var $parent = $(document).find('#weber-business-codes')

        // do ajax
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: {
                action: 'weber_do_generate_business_code',
                name: $(document).find('input[name="_w_campaign_name"]').val(),
                start_date: $(document).find('input[name="_w_start_date"]').val(),
                end_date: $(document).find('input[name="_w_end_date"]').val(),
                amount: $(document).find('input[name="_w_total_code"]').val(),
                post_id: $this.attr('data-id'),
            },
            cache: false,
            dataType: 'json',
    
            beforeSend: function( jqXHR, settings ){
                $parent.addClass('loading')
            },
            success: function( data, textStatus, jqXHR ) {
                $parent.removeClass('loading')
                $(document).find('#replace--results').replaceWith(data.html)
                $this.closest('tr').find('input').val('')
            },
            complete: function(jqXHR, textStatus){
                console.log( 'AJAX is complete.' ); 
            }
        });// End AJAX.

    })

    // pause or delete campaign
    $(document).on('click','.campaign-action-btn',function(e){

        e.preventDefault()
        var action = $(this).attr('data-action')
        var theID = $(this).attr('data-id')
        var partner_id = $(this).attr('data-partner')

        var is_sure = action == 'delete' ?  confirm('This action cannot be reversible, all generated access code will be deleted from the database. Are you sure want to do this? ') : confirm('Are you sure want to do this?')

        var $parent = $(document).find('#weber-business-codes')

        if(is_sure){
            // do ajax
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                data: {
                    action: 'weber_business_code_action',
                    run_action: action,
                    campaign_id: theID,
                    partner_id: partner_id
                },
                cache: false,
                dataType: 'json',
        
                beforeSend: function( jqXHR, settings ){
                    $parent.addClass('loading')
                },
                success: function( data, textStatus, jqXHR ) {
                    $parent.removeClass('loading')
                    $(document).find('#replace--results').replaceWith(data.html)
                    //$this.closest('tr').find('input').val('')
                },
                complete: function(jqXHR, textStatus){
                    console.log( 'AJAX is complete.' ); 
                }
            });// End AJAX.
        }

    })
        
})