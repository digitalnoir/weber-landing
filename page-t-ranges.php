<?php
/*
Template Name: Page Ranges
 */
get_header(); ?>

<div class="site-content ux2-wrapper">
	<div id="content" class="content-area">
		<main id="main" class="site-main" >
			<?php while ( have_posts() ) : the_post(); ?>
			<article>



			<section id="ranges-filter">
				<?php
				# Parameter
				$the_query_args = array (
					'post_type' => array( 'product', ),
					'posts_per_page'  => get_option('posts_per_page'),  # -1 for all
					'order'   => 'DESC',  # Newest
					'orderby' => 'date',  # 'rand' 'post__in'
				);
				
				# Connect Loop to Parameter
				$the_query_query = new WP_Query( $the_query_args );
				?>
				
				<?php
				# Loop
				if ( $the_query_query->have_posts() ) : ?>
					<div class="filter-galleries">
						<div class="container">
							<div class="row">
								 	<?php while ( $the_query_query->have_posts() ) : $the_query_query->the_post(); ?>
								 		<div class="col-xs-12 col-md-6 ">
								 		    <a href="<?php echo get_the_permalink(); ?>">
								 		    	  <img src="http://via.placeholder.com/800x800/BBBBBB/FFFFFF.jpg?text=Sample+Image" title="800x800" alt="800x800">
								 		    </a>
								 		</div>
								 	<?php endwhile; ?>
							    
							</div>
						</div>
					</div>
				
					<?php wp_reset_query(); ?>
				<?php else : ?>
				   <?php # Template Part | Blog
				   get_template_part('template-parts/general/content-no-post'); ?>
				<?php endif; ?>
			</section>	
				
			</article>
			<?php dn_post_edit_link(); ?>
			<?php endwhile; // end of the loop. ?>
		</main>
	</div>
</div>

<div class="section-how how-weber-q">
	<h2 class="acc-title">How It Works</h2>
	<h3 class="subtitle">Weber Q Cooking System</h3>
	<div class="separator"><img src="https://test.weberbbq.com.au/wp-content/themes/weber/img/how-line.png" alt="" width="240" height="9" /></div>
	<div class="desc">
		<p>The Weber Q cooking system has evolved from the legendary Weber kettle.  Just about all Australians know that our Weber kettle produces those famous roasts; roasts with that fabulous flavour you just can’t get any other way. But not many people know how this flavour is created. It comes from the smoke that’s circulated around the roast while it is being cooked. This is a unique Weber kettle trait and it’s what made the Weber kettle a world famous icon. So it comes as no surprise that Weber’s research and development team set about trying to reproduce this flavour when developing the Weber Q cooking system. That’s why the Weber Q (even when grilling a steak or a snag) is operated with the lid down. Just like our Weber kettle, all the barbecue smoke is circulated around the food inside, imparting that fabulous flavour to your favourite meals. So the ‘super barbecue’ flavour is created by both branding the meat on hot iron and the natural convection that circulates barbecue smoke all around the meat. It’s this flavour that is the hallmark of the fabulous Weber Q cooking system</p>
	</div>
	<div class="snapshot-range">
		<div class="snapshot snap1" style="width: 225.981px; left: 431.148px; top: 307.312px;"><img src="https://test.weberbbq.com.au/wp-content/themes/weber/img/assets/range-q/snap1.png" width="228" height="446" data-top="310" data-left="435" alt="Easy to use - Total control at your fingertips" /></div>
		<div class="snapshot snap2" style="width: 343.928px; left: 768.138px; top: 302.356px;" ><img src="https://test.weberbbq.com.au/wp-content/themes/weber/img/assets/range-q/snap2.png" width="347" height="176" data-top="305" data-left="775" alt="The Q is versatile - It bakes, It barbecues, It roasts" /></div>
		<div class="snapshot snap3" style="width: 404.388px; left: 1115.04px; top: 242.876px;"><img src="https://test.weberbbq.com.au/wp-content/themes/weber/img/assets/range-q/snap3.png" width="408" height="416" data-top="245" data-left="1125" alt="Flavour Magic - Circulates the smoke to create brilliant flavour" /></div>
		<div class="snapshot snap4" style="width: 732.457px; left: 555.042px; top: 584.884px;"><img src="https://test.weberbbq.com.au/wp-content/themes/weber/img/assets/range-q/snap4.png" width="739" height="186" data-top="590" data-left="560" alt="arrows" /></div>
		<div class="snapshot snap5" style="width: 490.617px; left: 688.846px; top: 827.76px;"><img src="https://test.weberbbq.com.au/wp-content/themes/weber/img/assets/range-q/snap5.png" width="495" height="111" data-top="835" data-left="695" alt="Even Cooking - Natural Convection" /></div>
		<div class="main-image"><img src="https://test.weberbbq.com.au/wp-content/themes/weber/img/assets/range-q/how-work-weber-q.png" alt="Weber Q barbecue" width="1920" height="1038" data-margin="-250" /></div>
	</div>
</div>
<?php get_footer();