<?php get_header('simple'); ?>

<?php
    $business_code = $email_address = $postcode = '';
    if(isset($_SESSION['partner_posted_data']) && is_array( $_SESSION['partner_posted_data'] )){
        $business_code = $_SESSION['partner_posted_data']['business_code'];
        $email_address = $_SESSION['partner_posted_data']['email_address'];
        $postcode = $_SESSION['partner_posted_data']['postcode'];
    }
?>

<div class="partner-login-container">
    <div class="container">
        <div class="inner-flex" style="justify-content: center;">      
                <div class="logo"><img src="<?php echo THEME_URL ?>/img/header-logo.png" alt="Weber Logo" /></div>
  
        </div>
    </div>
</div>
 

<?php
    // Clear posted session
    $_SESSION['partner_posted_data'] = '';
?>

<?php get_footer('simple'); ?>