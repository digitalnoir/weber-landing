<?php

get_header();

$sess = $_SESSION['weber_partner'];
$landing_welcome = $sess['campaign_rules'] == 'one_product_only' ? get_field('partner_welcome_message', $sess['partner_id']) : get_field('landing_description');
$frontpage_id = get_option( 'page_on_front' );

?>

<div class="site-content">
    
    <main id="main" class="site-main" >
        <article>

            <div id="front-partner-logo">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">

                        <?php
                            // if has logo
                            $site_logo = get_field('partner_welcome_logo', $sess['partner_id']);
                            if(!empty($site_logo)){
                                echo '<div class="partner-logo">'. wp_get_attachment_image( $site_logo, 'full' ) .'</div>';
                            }
                        ?>

                        <?php if( $sess['campaign_rules'] != 'one_product_only' ) : ?>
                            <h1><?php echo get_field('landing_title', $frontpage_id) ?></h1>
                            <div class="desc"><?php echo $landing_welcome ?></div>
                        <?php else: ?>
                            <div class="desc"><?php echo $landing_welcome ?></div>
                        <?php endif ?>

                        </div>
                    </div>
                </div>
            </div>

            <?php 

            // if the rule only for 1 product
            if( $sess['campaign_rules'] != 'one_product_only' ) : ?>

                <div id="home-product-category-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <h2><?php the_field('bbq_range_title') ?></h2>
                            </div>
                        </div>
                        <div class="row">
                            <?php
                                $cats = get_terms(array(
                                    'taxonomy' => 'product_cat',
                                    'parent' => 0
                                ));
                                foreach($cats as $cat):
                                    if($cat->slug === 'weber-accessories') continue;
                                    $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
                                
                            ?>
                            <div class="col-xs-12 col-sm-6 col-lg-3 home-product-category">
                                <a href="<?php echo get_term_link($cat, 'product_cat')?>">
                                
                                    <?php echo dn_get_background_image( $thumbnail_id ) ?>

                                    <div class="desc">
                                        <h3><?php echo $cat->name?></h3>
                                        <div class="button-holder"><div class="button-svg-outline red" data-href="<?php echo get_term_link($cat, 'product_cat')?>">
                                            <span class="anchor">Explore</span>
                                        </div></div>
                                    </div>
                                </a>
                            </div>
                            <?php
                                endforeach;
                            ?>
                        </div>
                    </div>
                </div>

                <div id="weber-accessories-cta">
                    <div class="container">
                        <div class="cta-blocks">
                            <div class="background-container"><?php the_field('accessories_cta_bg') ?></div>
                            <div class="content">
                                <h2><?php the_field('accessories_cta') ?></h2>
                                <div class="link"><a class="border-link special-link" href="<?php the_field('accessories_cta_link') ?>">Explore</a></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="why-weber" class="container">
                    <div class="weber-description">
                        <h2 class="section-title"><?php the_field('why_heading') ?></h2>
                        <h2 class="weber-title"><?php the_field('why_subheading') ?></h2>
                        <?php the_field('why_description') ?>
                    </div>

                    <div class="weber-feature visible-xs">
                        <ul class="feature">
                            <?php
                                $features = get_field('featured_item_mobile');
                                foreach($features as $feat){
                                    $is_swap = $feat['swap_size'] ? 'swap' : '';
                                    $color = $feat['color'];
                                    echo '<li class="feat-item '. $is_swap .' '. $color .'">';
                                    echo '<div class="line_1">'. $feat['line_1'] .'</div>';
                                    echo '<div class="line_2">'. $feat['line_2'] .'</div>';
                                    echo '<div class="desc">'. wpautop( $feat['description'] ) .'</div>';
                                    echo '</li>';
                                }
                            ?>
                        </ul>
                        <?php echo dn_get_attachment_image_lazy( get_field('mobile_image') ) ?>          
                    </div>
                    
                    <div class="weber-snapshot hidden-xs">
                        <?php echo dn_get_attachment_image_lazy( get_field('desktop_image') ) ?>          
                    </div>

                    

                </div>
                <!-- why-weber -->

            <?php else: ?>

                <?php
                    include locate_template('template-home-one-product.php');
                ?>
                
            <?php endif; ?>

            
        </article>

    </main>
 
</div>
<?php get_footer();