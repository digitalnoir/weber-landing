
<ul>
    <li class="title">Sort By</li>
    <li><a href="#" class="sort-action special-link active" data-sort="alpha" data-order="asc">A-Z</a></li>
    <li><a href="#" class="sort-action special-link" data-sort="alpha" data-order="desc">Z-A</a></li>
    <li><a href="#" class="sort-action special-link" data-sort="price" data-order="asc">Price: Low - High</a></li>
    <li><a href="#" class="sort-action special-link" data-sort="price" data-order="desc">Price: High - Low</a></li>
</ul>
<div class="visible-xs visible-sm triger-filter">
    <a href="#">Filter Accessories </a>
</div>