
<div class="section-how">
	<div class="container">
		<h2 class="acc-title">How It Works</h2>
		<h3 class="subtitle h2"><?php echo get_field('how_title', 'product_cat_'. $_term_id) ?></h3>
		<div class="desc">
			<?php echo get_field('how_descriptions', 'product_cat_'. $_term_id) ?>
		</div>
	</div>
	<div class="feature-image visible-md visible-lg">
		<?php echo dn_get_attachment_image_lazy( get_field('how_desktop_image', 'product_cat_'. $_term_id) ) ?>   
	</div>
	<div class="feature-image feature-mobile visible-xs visible-sm container">
		<?php
			$feat = get_field('mobile_feature_items', 'product_cat_'. $_term_id);
			if(is_array($feat)){
				echo '<ul>';
				foreach($feat as $f){
					$src = ( $f['image']['url'] );
					echo '<li><img src="'. $src .'" alt="Featured Item" /></li>';
				}
				echo '</ul>';
			}
		?>
		<div class="mobile-feat">
			<?php echo dn_get_attachment_image_lazy( get_field('how_mobile_image', 'product_cat_'. $_term_id) ) ?>   
		</div>
	</div>
</div>