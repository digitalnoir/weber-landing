<?php

$barbie_on_cart = $meta_query = array();

// get product id in cart
$weber_cart = WC()->cart->get_cart();
foreach ( $weber_cart as $cart_item_key => $cart_item ) {
    $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
    $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
    if( get_post_meta( $product_id, 'product_type', true) == 'product' ){
        $barbie_on_cart[] = $product_id;
    }
}

// show related accessories for its barbie
if( !empty($barbie_on_cart) ){
    $meta_query['relation'] = 'OR';
    foreach ($barbie_on_cart as $bbq){
        $meta_query[] = array(
            'key' => 'accessories_for',
            'value' => $bbq,
            'compare' => 'LIKE'
        );
    }
}


$args = array(
    'post_type' => 'product',
    'post_status' => 'publish',
    'meta_key' => 'accessories_for',
    'meta_query' => $meta_query,
    'posts_per_page' => 3,
    'orderby' => 'rand'
);

$the_query = new WP_Query( $args );
$total_post = $the_query->found_posts;

if($total_post > 0) :
    ?>
    
            <div class="cart-related-accessories">
                <div class="container-cart-related">
                    <h2 class="cart-head">You may also like</h2>
                </div>
                <div class="gallery-accessories">
                    <ul class="products">
                        <?php
                        while ( $the_query->have_posts() ) {
                            $the_query->the_post();
                            get_template_part('woocommerce/content-product');
                        }
                        ?>
                    </ul>
                </div>
            </div>
            

    <?php wp_reset_postdata(); ?>

<?php else: ?>

    <?php
        // not found, show categories for this product
        $all_cats[] = 15; // weber accessories
        if( !empty($barbie_on_cart) ){
            foreach( $barbie_on_cart as $bbq){
                $terms = wp_get_post_terms( $bbq, 'product_cat', array('hide_empty'=> false, 'parent' => 0) );
                foreach($terms as $term){
                    $all_cats[] = $term->term_id;
                }
            }

            $args = array(
                'post_type' => 'product',
                'post_status' => 'publish',
                'meta_key' => 'product_type',
                'meta_value' => 'accessories',
                'posts_per_page' => 3,
                'orderby' => 'rand',
                'tax_query' => array(
					array(
						'taxonomy' => 'product_cat',
						'field' => 'term_id',
                        'terms' => $all_cats,
                        'operator' => 'IN',
					)
			    )
            );

            
            $the_query = new WP_Query( $args );
            $total_post = $the_query->found_posts;

            if( $total_post > 0 ){
                ?>
                
                <div class="cart-related-accessories">
                    <div class="container-cart-related">
                        <h2 class="cart-head">You may also like</h2>
                    </div>
                    <div class="gallery-accessories">
                        <ul class="products">
                            <?php
                            while ( $the_query->have_posts() ) {
                                $the_query->the_post();
                                get_template_part('woocommerce/content-product');
                            }
                            ?>
                        </ul>
                    </div>
                </div>

                <?php
            }

        }
    ?>
    <?php wp_reset_postdata(); ?>
<?php endif; ?>

<?php
return;


	global $dn_option, $section_icon_imag;
	$icon_url = wp_get_attachment_image_src( @$section_icon_image,'full' ); 
	$is_image = get_field('accessories_background_image'); 
	$is_accessories = get_field('product_type');

	$product_name = get_the_title();
	
	
	if($is_accessories != 'accessories') {
		$args = array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'meta_key' => 'accessories_for',
			'meta_query' => array(
				'key' => 'accessories_for',
				'value' => get_the_ID(),
				'compare' => 'LIKE'
			),
				
		);
	}else{
		$terms = wp_get_post_terms( get_the_ID(), 'range', array('hide_empty'=> false) );
		$args = array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'post__not_in' => array(get_the_ID()),
			'meta_key' => 'product_type',
			'meta_query' => array(
				'key' => 'product_type',
				'value' => 'accessories',
				'compare' => '='
			),
			'tax_query' => array(
					array(
						'taxonomy' => 'range',
						'field' => 'id',
						'terms' => $terms[0]->term_id,
					)
			)
		);
	
	}
	
	
	$the_query = new WP_Query( $args );
	$total_post = $the_query->found_posts;
	
	// ONLY SHOW IF THERE A POST TYPE HERE
	if($total_post > 0) :
?>

<div id="related-accessories">

		<div class="inner-related-accessories">
            <div class="container">
                <h2><span class="red">Sets & Accessories</span> for the <?php echo $product_name ?></h2>
                <p>You can add up to 1 of each accessory into your cart</p>
            </div>
            <div class="gallery-accessories">
                <ul class="products">
                    <?php
                    while ( $the_query->have_posts() ) {
                        $the_query->the_post();
                        get_template_part('woocommerce/content-product');
                    }
                    ?>
                </ul>
            </div>
		</div>
		
</div>

<?php endif; ?>