<?php
/**
 * The Template for displaying products in a product category. Simply includes the archive template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/taxonomy-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$term = get_queried_object();

get_header( 'shop' );

wp_dequeue_script( 'js-main' );
wp_enqueue_script( 'js-tinysort' );
wp_enqueue_script( 'js-main' );


/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
<header class="woocommerce-products-header">


	<div class="filter-section">
	   	<div class="container">
	   		<div class="row">
	   		    <div class="col-xs-12 col-md-12">
	   		        <h2>SELECT YOUR FUEL TYPE AND UNLOCK THE POSSIBILITIES </h2>
	   		    </div>
	   		    <div class="col-xs-12 col-md-12">
	   		    	 <ul class="weber-product-range">
	   		    	 	<li>
							<a href="<?php echo get_term_link(35, 'product_cat')?>" class="filter-item-content special-link <?php echo (( $term->term_id === 35 ) ? 'active-circle' : ''); ?>" aria-label="Weber Q">
								 <?php get_template_part('blocks/weber-icon/icon-weber-q') ?>
							</a>
	   		    	 	</li>
	   		    	 	<li>
							<a href="<?php echo get_term_link(45, 'product_cat')?>" class="filter-item-content special-link <?php echo (( $term->term_id === 45 ) ? 'active-circle' : ''); ?>" aria-label="Gas">
								<?php get_template_part('blocks/weber-icon/icon-premium-gas') ?>
							</a>
	   		    	 	</li>
	   		    	 	<li>
							<a href="<?php echo get_term_link(64, 'product_cat')?>" class="filter-item-content special-link <?php echo (( $term->term_id === 64 ) ? 'active-circle' : ''); ?> " aria-label="Charcoal">
								<?php get_template_part('blocks/weber-icon/icon-charcoal') ?>
							</a>
	   		    	 	</li>
	   		    	 	<li>
							<a href="<?php echo get_term_link(71, 'product_cat')?>" class="filter-item-content special-link <?php echo (( $term->term_id === 71 ) ? 'active-circle' : ''); ?>" aria-label="Electric">
								<?php get_template_part('blocks/weber-icon/icon-electric') ?>
							</a>
	   		    	 	</li>
	   		    	 </ul>
	   		    </div>
	   		</div>
	   	</div>
	   </div>
	
	
	<div class="container-header-accessories">
		<div class="pre-content">
			<h1 style="text-align: center;">Weber&reg; barbecue Accessories</h1>
		</div>
	</div>
	</header>

	<div class="container-accessories">
		<div class="inner-accessories">
			<div class="sort-filter">
				<?php get_template_part('woocommerce/ajax-accessories-sort') ?>
			</div>
			<div class="sidebar-filter">
				<?php get_template_part('woocommerce/ajax-accessories-filter') ?>
			</div>
			<div class="container-loop">
				<?php get_template_part('woocommerce/ajax-accessories-loop') ?>
			</div>
		</div>
	</div>

<?php
/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

?>

<?php
get_footer( 'shop' );
