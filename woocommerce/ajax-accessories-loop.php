<?php
    $posts_per_page = -1;
?>
<ul class="products" id="ajax-append-target" data-posts-per-page="<?php echo $posts_per_page ?>">
    <?php

        $args = array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'posts_per_page' => $posts_per_page,
            'orderby' => 'title',
            'order' => 'ASC',
            'meta_query' => array(
                array(
                    'key'     => 'product_type',
                    'value'   => 'accessories',
                    'compare' => '=',
                ),
            ),
        );

        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) {
       
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                global $product;
                wc_get_template_part( 'content', 'product' );
            }
            // not found
            echo '<li class="not-found product" style="display:none">No accessories found.</li>';
        } 
        /* Restore original Post Data */
        wp_reset_postdata();
    ?>
</ul>