<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// check if business code has exclude this product
$is_partner_can_access = weber_partner_is_partner_can_access( $product );
if( ! $is_partner_can_access ){
	return;
}

// check product stock
// to do need to confirm functionality
$is_instock_warehouse = weber_warehouse_instock( $product->get_id() );
if( !$is_instock_warehouse ){
	//return;
}

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
$__term = wp_get_post_terms( get_the_ID(), 'product_cat' );
$another_cat = array();
foreach( (array) $__term as $term){
	if( $term->parent != 0 ){

		$the_parent = get_term($term->parent, 'product_cat');
		$another_cat[] = 'product_cat-'. $the_parent->slug;

	}
}
$cat_class = implode(' ', $another_cat);
?>
<li <?php wc_product_class( $cat_class , $product ); ?> data-alpha="<?php echo sanitize_title( get_the_title(  ) ) ?>" data-price="<?php echo $product->get_price() ?>">
<div class="inner-wrapper">
	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item' );

	woocommerce_template_loop_product_link_open();

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	
	 $bg_image = dn_get_background_image( get_post_thumbnail_id() );
	 echo str_replace('bg-image ','product-image-wrapper ', $bg_image);

	//echo '<div class="product-image-wrapper" style="background-image: url(' . get_the_post_thumbnail_url($post, 'full') . ');"></div>';

	woocommerce_template_loop_product_link_close();

	echo '<div class="product-text-wrapper">';
	
	woocommerce_template_loop_product_link_open();
	echo '<h3>' . get_the_title() . '</h3>';
	woocommerce_template_loop_price();
	woocommerce_template_loop_product_link_close();


	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	//do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	//do_action( 'woocommerce_after_shop_loop_item' );
	if( ! weber_is_user_has_purchased() && weber_warehouse_instock( $product->get_id() ) ){
		woocommerce_template_loop_add_to_cart();
	}
	echo '</div>';
	?>
</div>
</li>
