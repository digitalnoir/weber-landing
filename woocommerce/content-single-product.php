<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product, $dn_option;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$ts = [];
$campaign_type = $_SESSION['weber_partner']['campaign_rules'];
if(  $campaign_type != 'one_product_only' ) :

	$terms = get_the_terms($post, 'product_cat');
	
	foreach($terms as $t){
		$ts[] = $t->term_id;
	}
	?>

	<div class="filter-section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<h2>SELECT YOUR FUEL TYPE AND UNLOCK THE POSSIBILITIES </h2>
				</div>
				<div class="col-xs-12 col-md-12">
					<ul class="weber-product-range">
						<li>
							<a href="<?php echo get_term_link(35, 'product_cat')?>" class="filter-item-content special-link <?php echo (( in_array(35, $ts) ) ? 'active-circle' : ''); ?>">
								<?php get_template_part('blocks/weber-icon/icon-weber-q') ?>
							</a>
						</li>
						<li>
							<a href="<?php echo get_term_link(45, 'product_cat')?>" class="filter-item-content special-link <?php echo (( in_array(45, $ts) ) ? 'active-circle' : ''); ?>">
								<?php get_template_part('blocks/weber-icon/icon-premium-gas') ?>
							</a>
						</li>
						<li>
							<a href="<?php echo get_term_link(64, 'product_cat')?>" class="filter-item-content special-link <?php echo (( in_array(64, $ts) ) ? 'active-circle' : ''); ?>">
								<?php get_template_part('blocks/weber-icon/icon-charcoal') ?>
							</a>
						</li>
						<li>
							<a href="<?php echo get_term_link(71, 'product_cat')?>" class="filter-item-content special-link <?php echo (( in_array(71, $ts) ) ? 'active-circle' : ''); ?>">
								<?php get_template_part('blocks/weber-icon/icon-electric') ?>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
		
	<div class="browse-all-accessories text-center">
		<div class="container">
			<a href="<?php echo get_category_link(15) ?>" class="special-link h3">View Our Range of Weber BBQ Acessories</a>
		</div>
	</div>

<?php endif; ?> 


<div class="single-product-content">
	<div id="product-<?php the_ID(); ?>" class="product row">
		<div class="product-main-detail container">
			<div class="row">
				
				<div class="single-product-content col-md-6 col-sm-12  col-xs-12 pull-right">
					<div class="summary entry-summary">
						<div class="badge-guarantee">
						<?php /*
						$product_image_type = get_field('product_image_type');
				 
				$color_options = get_field('color_choice');
				$product_badge = get_field('product_badge');
						
							if($product_badge != '' && $product_badge != 'no' && $product_badge != 'sold_out'):
							
							$is_new_size = $product_badge == 'award_choice' ? '1' : '';
							$is_print_size = '';
							if($product_badge == 'award_choice'){
								$is_print_size = 'width="85" height="86"';
							}
							if($product_badge == 'guarantee_new'){
								$is_print_size = 'width="98" height="85"';
							}
							?>
							<div class="the-badges"><img src="<?php bloginfo('template_url')?>/img/badge-<?php echo $product_badge.$is_new_size?>.png?v=2018" alt="" <?php echo $is_print_size?>  /></div>
							<?php endif; ?>

<?php
$show_specialist_guarantee = get_field('show_specialist_dealer_badge');
if($show_specialist_guarantee){
?>
<div class="the-badges"><a href="https://www.theweberspecialist.com.au/" target="_blank"><img src="<?php bloginfo('template_url')?>/img/badge-specialist-new.png" class="specialist-image" alt="" width="85" height="86" /></a></div>
<?php } ?>

<?php
$show_badge_guarantee = get_field('show_guarantee_badge');
if($show_badge_guarantee){
?>
<div class="the-badges"><img src="<?php bloginfo('template_url')?>/img/badge-guarantee.png" class="guarantee-image" alt="" width="85" height="85" /></div>
<?php } */?>
							
						</div>
						<div class="product-title">
							<h1 class="product_title entry-title">
							<?php if(get_field('hide_weber_title',get_the_ID()) != true) : ?> 
		<span class="weber-brand">Weber&reg;</span>
		<?php endif; ?>
								<?php the_title() ?>
							</h1>
							<div id="weber-price-wrap">
								<?php echo woocommerce_template_single_price()?>
							</div>
						</div>
						<div class="clearfix"></div>

					</div>
				</div>
				<div class="single-product-images col-md-6 col-sm-12  col-xs-12">
					<div id="product-slider-container">
						<?php
				
				

				if($product_badge != '' && $product_badge == 'sold_out'){
					echo '<div class="sold-out-badge"><img src="'.get_bloginfo('template_url').'/img/badge-sold-out.png" alt="" /></div>';
				}
				
				//print_r($color_options_gallery);
				
				
				
				woocommerce_show_product_images();
				
				
			
				?>
				<div class="loading-infinite"><div class="circle"><div class="inner"></div></div><div class="circle">	<div class="inner"></div></div><div class="circle">	<div class="inner"></div></div><div class="circle">	<div class="inner"></div></div><div class="circle">	<div class="inner"></div></div>
</div>

					</div>
				</div>
				
				<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="product-description">
							<?php the_content() ?>
							<?php dn_post_edit_link() ?>
						</div>
						<?php
							$video_text = get_field('video_text');
							$video_url = get_field('video_url');
							$is_table = $video_text != '' && $video_url != '' ? 'table-view' : '';
						?>
						<div class="product-summary <?php echo $is_table ?>">
					<?php echo woocommerce_template_single_add_to_cart()?>
							
						</div>
						<!-- product-summary -->
						
				</div>
				

			</div>
		</div>
		<!-- product section1  -->

<?php
	if(!in_array(15, $ts)):
?>

		<div class="product-features clearfix">
			<?php
				$featured_detail = get_field('featured_detail');
				if($featured_detail)	{
					$z = 0;
					$total_featured = sizeof($featured_detail);
					$col_width = 100 / $total_featured;
					
					$style = 'width:'.$col_width.'%;';
					
					foreach($featured_detail as $detail){
						$z++;
						$feature_bg = $detail['featured_bg_image'];
						if($feature_bg != ''){
							$style .= 'background-image:url('.$feature_bg.');';
						}
						?>
			<div class="product-feature-col" style="<?php echo $style ?>">
				<div class="feature"><span><?php echo $detail['featured_text'] ?></span></div>
			</div>
			<?php			
					}
				}
			?>
		</div>
		<!-- product-features -->
		
				
		
		
		
		
		
		
		
		

		
		<div class="product-specification">
			<?php
			$specification_image = get_field('specification_image');
			$specification_handbook = get_field('specification_handbook');
			$specification_full = get_field('specification_full');
		?>
			<div class="container">
				<div class="row">
					<div class="titles col-xs-12">
						<h3>Full Specs</h3>
					</div>
					
					
					<div class="spec-detail col-xs-12 col-md-6">
					<?php
					$print_features = '';
					$consruction_content = get_field('compare_construction');
					$features_content = get_field('compare_features');
					
					$print_features .= $consruction_content != '' ? "<div class='spec-item'><h4>Construction</h4>\r\n".$consruction_content.'</div>' : '' ;
					$print_features .= $features_content != '' ? "<div class='spec-item'><h4>Features</h4>\r\n".$features_content.'</div>' : '' ;
					
					echo $print_features;
					
					?>
					</div>
					<div class="spec-detail col-xs-12 col-md-6">
					<?php
					$print_features = '';
					$color_content = get_field('compare_color');
					$compare_dimensions = get_field('compare_dimensions');
					
					$print_features .= $color_content != '' ? "<div class='spec-item'><h4>Colour</h4>\r\n".$color_content.'</div>' : '' ;
					$print_features .= $compare_dimensions != '' ? "<div class='spec-item'><h4>Dimensions</h4>\r\n".$compare_dimensions.'</div>' : '' ;
					
					echo $print_features;
					
					?>
					</div>
					
				</div>
			</div>
		</div>
		<!-- product-specificaion --> 
		<?php /*
		<div class="video-section">
			<div class="container">
				<?php wc_get_template_part( 'content', 'type-slider-video'); ?>
				<div class="clearfix"></div>
			</div>
		</div> */ ?>

		<?php if( $campaign_type != 'one_product_only') : ?>
			<div class="accessories-section">
				<?php wc_get_template_part( 'content', 'accessories'); ?>
				<div class="clearfix"></div>
			</div>
		<?php endif; ?>
		

<?php endif; ?>

</div>
	<!-- #product-<?php the_ID(); ?> --> 
</div>