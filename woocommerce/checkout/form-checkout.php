<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


?>

<div class="weber-cart-checkout-page row">

	<div class="col-xs-12 text-center">
		<div class="checkout-head">
			<h1>Checkout</h1>
			<div class="checkout-steps">
				<ul>
					<li class="shipping active" data-page="1"><span class="number">1</span><span>Shipping</span></li>
					<li class="payment" data-page="2"><span class="number">2</span><span>Payment</span></li>
				</ul>
			</div>
		</div>
	</div>
	
	<div class="col-xs-12">
		<div class="one-time-purchase-note text-center">
			<?php
				$extra_text = '';
				if($_SESSION['weber_partner']['campaign_rules'] != 'one_product_only'){
					$extra_text = 'Please ensure you have included all items before you checkout.';
				}
			?>
			You are entitled to one single transaction during this Partner of Weber period. <?php echo $extra_text ?>
		</div>
	</div>

	<div class="col-xs-12">
		<?php do_action( 'woocommerce_before_checkout_form', $checkout ); ?>
	</div>
	
	<form name="checkout" method="post" class="checkout woocommerce-checkout col-xs-12" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

		<?php if ( $checkout->get_checkout_fields() ) : ?>

			

			<div id="customer_details" data-page="1" class="row">
				<div class="col-sm-12 col-md-7">
					<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>
						<?php do_action( 'woocommerce_checkout_billing' ); ?>
					<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>
				</div>
				<div class="col-sm-12 col-md-5">
					<div class="inner-review-holder">
						<h3>Summary</h3>
						<?php woocommerce_order_review() ?>
					</div>
					<?php wc_get_template('checkout/mini-checkout-cart.php') ?>
				</div>
				<div class="next-button col-xs-12">
					<a class="dn-button" data-next="2" href="#">Next: Payment</a>
				</div>
			</div>
		

			<div id="order-details" data-page="2" class="row" style="display:none">

				<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

				<div id="order_review" class="woocommerce-checkout-review-order">
					<?php // use directly //do_action( 'woocommerce_checkout_order_review' ); ?>
					<div class="col-sm-12 col-md-7">
						<h3>Payment method</h3>
						<?php woocommerce_checkout_payment(); ?>
					</div>
					<div class="col-sm-12 col-md-5">
						<div class="inner-review-holder">
							<h3>Summary</h3>
							<?php woocommerce_order_review() ?>
							<div class="secure" style="text-align:center">
								<img src="<?php echo THEME_URL ?>/img/ssl.svg" alt="secure checkout badge" />
							</div>
						</div>
						<?php // wc_get_template('checkout/mini-checkout-cart.php') ?>
					</div>
				</div>

				<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
			</div>



		<?php endif; ?>

	</form>
</div>
<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
