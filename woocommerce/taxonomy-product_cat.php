<?php
/**
 * The Template for displaying products in a product category. Simply includes the archive template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/taxonomy-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$term = get_queried_object();

get_header( 'shop' );


/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
<header class="woocommerce-products-header">


	<div class="filter-section">
	   	<div class="container">
	   		<div class="row">
	   		    <div class="col-xs-12 col-md-12">
	   		        <h2>SELECT YOUR FUEL TYPE AND UNLOCK THE POSSIBILITIES </h2>
	   		    </div>
	   		    <div class="col-xs-12 col-md-12">
	   		    	 <ul class="weber-product-range">
	   		    	 	<li>
							<a href="<?php echo get_term_link(35, 'product_cat')?>" class="filter-item-content special-link <?php echo (( $term->term_id === 35 ) ? 'active-circle' : ''); ?>" aria-label="Weber Q">
								 <?php get_template_part('blocks/weber-icon/icon-weber-q') ?>
							</a>
	   		    	 	</li>
	   		    	 	<li>
							<a href="<?php echo get_term_link(45, 'product_cat')?>" class="filter-item-content special-link <?php echo (( $term->term_id === 45 ) ? 'active-circle' : ''); ?>" aria-label="Gas">
								<?php get_template_part('blocks/weber-icon/icon-premium-gas') ?>
							</a>
	   		    	 	</li>
	   		    	 	<li>
							<a href="<?php echo get_term_link(64, 'product_cat')?>" class="filter-item-content special-link <?php echo (( $term->term_id === 64 ) ? 'active-circle' : ''); ?> " aria-label="Charcoal">
								<?php get_template_part('blocks/weber-icon/icon-charcoal') ?>
							</a>
	   		    	 	</li>
	   		    	 	<li>
							<a href="<?php echo get_term_link(71, 'product_cat')?>" class="filter-item-content special-link <?php echo (( $term->term_id === 71 ) ? 'active-circle' : ''); ?>" aria-label="Electric">
								<?php get_template_part('blocks/weber-icon/icon-electric') ?>
							</a>
	   		    	 	</li>
	   		    	 </ul>
	   		    </div>
	   		</div>
	   	</div>
	   </div>
	
	   <?php if( !is_product_category(15) ){ ?>
			<div class="browse-all-accessories text-center">
				<div class="container">
					<a href="<?php echo get_category_link(15) ?>" class="special-link h3">View Our Range of Weber BBQ Acessories</a>
				</div>
			</div>
	   <?php } ?>
	
<div class="container">
	<div class="pre-content">
		<?php echo apply_filters('the_content', get_field('archive_text', $term)); ?>
	</div>
</div>

	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>


</header>

<?php
if ( woocommerce_product_loop() ) {

	echo '<div class="container">';
	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );

	echo '</div><!-- .container -->';

} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );
?>
