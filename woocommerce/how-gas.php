<?php
    wp_dequeue_script( 'js-main' );
    wp_enqueue_script( 'js-magnific' );
    wp_enqueue_script( 'js-main' );
?>
<div class="section-how">
	<div class="container">
		<h2 class="acc-title">How It Works</h2>
		<h3 class="subtitle h2"><?php echo get_field('how_title', 'product_cat_'. $_term_id) ?></h3>
		<div class="desc">
			<?php echo get_field('how_descriptions', 'product_cat_'. $_term_id) ?>
		</div>
	</div>
	<div class="feature-image">
		<a class="open-video-popup" href="<?php echo THEME_URL ?>/img/Weber-Original-Gas-Cooking-System.mp4"><img src="<?php echo THEME_URL ?>/img/press-to-find-out.png" alt="press"/></a>
	</div>
</div>