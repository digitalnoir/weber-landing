<div class="filter-mobile-header visible-sm visible-xs">
    <div class="close-filter"><a href="#"><svg viewBox="0 0 49 49">
		<circle cx="24.5" cy="24.5" r="24" style="fill: none;stroke: currentColor; stroke-miterlimit: 10;" vector-effect="non-scaling-stroke" stroke-dasharray="3"></circle>
	</svg></a></div>
    <h3>Filter Accessories By:</h3>
</div>
<div class="parent-cat">
    <?php
        // parent category
        $parents = get_terms('product_cat', array('hide_empty' => false, 'parent' => 0, 'exclude'=> 15));
        $parents_cat = array();
        echo '<h3 class="h2">Range</h3>';
        echo '<ul>';
        foreach($parents as $parent){
            echo '<li><a class="special-link filter-item" data-cat-id="product_cat-'. $parent->slug .'" href="#">'. str_replace('Range','',$parent->name) .'</a></li>';
            $parents_cat[$parent->term_id] = $parent->slug;
        }
        echo '</ul>';
    ?>
</div>


<div class="child-cat">
    <?php
        // child category
        echo '<h3 class="h2">Series</h3>';
        echo '<ul>';
        foreach($parents_cat as $parent => $val){
            $parents = get_terms('product_cat', array('hide_empty' => false, 'parent' => $parent ));
            foreach( (array) $parents as $child ){
                echo '<li><a class="special-link filter-item" data-parent-id="product_cat-'. $val .'" data-cat-id="product_cat-'. $child->slug .'" href="#">'. str_replace('Series','',$child->name) .'</a></li>';
            }
        }
        echo '</ul>';
    ?>
</div>