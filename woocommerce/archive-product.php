<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );


/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
<header class="woocommerce-products-header">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
	<?php endif; ?>

	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>
</header>
<?php
if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );
?>


<div class="section-how how-charcoal">
	<h2 class="acc-title">How It Works</h2>
	<h3 class="subtitle">The Weber Kettle Cooking System</h3>
	<div class="separator"><img src="<?php echo get_template_directory_uri(); ?>/img/how-line.png" alt="" width="240" height="9" /></div>
	<div class="desc">
		<p>We use two fires instead of one. The fat from the turkey will drop into the disposable drip tray below. This means that you get much healthier, fat free roasts, without any flare ups. We call this ‘indirect cooking’ because the food is cooked using natural convection without any fire directly underneath it. Apart from the unforgettable flavour, the great advantage of using this method of cooking is that you don’t even have to turn the meat. Your barbecue cooks the most extraordinary roast, every time, all by itself.</p>
	</div>
	<div class="snapshot-range">
		<div class="snapshot snap1" style="width: 350.866px; left: 436.104px; top: 336.979px;"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-charcoal/snap1.png" width="354" height="473" data-top="340" data-left="440" alt="Original Weber Flavour" /></div>
		<div class="snapshot snap1-mob mobile"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-charcoal/snap1-mob.png" width="161" height="270" data-top="280" data-left="540" alt="Original Weber Flavour" /></div>
		<div class="snapshot snap2" style="width: 333.025px; left: 1115.04px; top: 336.979px;"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-charcoal/snap2.png" width="336" height="323" data-top="340" data-left="1125" alt="World Famous Outdoor Roast" /></div>
		<div class="snapshot snap2-mob mobile"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-charcoal/snap2-mob.png" width="134" height="297" data-top="280" data-left="1225" alt="World Famous Outdoor Roast" /></div>
		<div class="snapshot snap3" style="width: 157.592px; left: 594.688px; top: 614.491px;"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-charcoal/snap3.png" width="159" height="416" data-top="620" data-left="600" alt="Smoke" /></div>
		<div class="snapshot snap4" style="width: 126.867px; left: 1174.51px; top: 614.491px;"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-charcoal/snap4.png" width="128" height="416" data-top="620" data-left="1185" alt="Smoke" /></div>
		<div class="snapshot snap5" style="width: 490.617px; left: 708.669px; top: 986.159px;"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-charcoal/snap5.png" width="495" height="174" data-top="995" data-left="715" alt="Cooke over charcoal for the authentic barbecue experience" /></div>
		<div class="main-image"><img src="<?php echo get_template_directory_uri(); ?>/img/assets/range-charcoal/how-work-charcoal.png" alt="Weber Kettle Barbecue" width="1920" height="1238" data-margin="-310" /></div>
	</div>
</div>

<?php
get_footer( 'shop' );
