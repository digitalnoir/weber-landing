<?php
	global $dn_option, $section_icon_imag;
	$icon_url = wp_get_attachment_image_src( @$section_icon_image,'full' ); 
	$is_image = get_field('accessories_background_image'); 
	$is_accessories = get_field('product_type');

	$product_name = get_the_title();
	
	
	if($is_accessories != 'accessories') {
		$args = array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'meta_key' => 'accessories_for',
			'meta_query' => array(
				'key' => 'accessories_for',
				'value' => get_the_ID(),
				'compare' => 'LIKE'
			),
				
		);
	}else{
		$terms = wp_get_post_terms( get_the_ID(), 'range', array('hide_empty'=> false) );
		$args = array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'post__not_in' => array(get_the_ID()),
			'meta_key' => 'product_type',
			'meta_query' => array(
				'key' => 'product_type',
				'value' => 'accessories',
				'compare' => '='
			),
			'tax_query' => array(
					array(
						'taxonomy' => 'range',
						'field' => 'id',
						'terms' => $terms[0]->term_id,
					)
			)
		);
	
	}
	
	
	$the_query = new WP_Query( $args );
	$total_post = $the_query->found_posts;
	
	// ONLY SHOW IF THERE A POST TYPE HERE
	if($total_post > 0) :
?>

<div id="related-accessories">

		<div class="inner-related-accessories">
            <div class="container text-center">
                <h2><span class="red">Sets & Accessories</span> for the <?php echo $product_name ?></h2>
                <p>You can add up to 1 of each accessory into your cart</p>
            </div>
            <div class="gallery-accessories">
			<div class="gal-inner">
                <ul class="products">
                    <?php
                    while ( $the_query->have_posts() ) {
                        $the_query->the_post();
                        get_template_part('woocommerce/content-product');
                    }
                    ?>
                </ul>
			</div>
            </div>
		</div>
		
</div>

<?php endif; ?>