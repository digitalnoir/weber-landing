<?php

    $term_id = $terms->term_id;
    $_args = array(
        'post_type' => 'product',
        'post_status' => 'publish',
        'meta_key' => 'product_type',
        'meta_value' => 'accessories',
        'posts_per_page' => 20,
        'orderby' => 'rand',
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field'    => 'ID',
                'terms'    => $term_id,
            ),
        ),
    );
    $the_query = new WP_Query( $_args );

    // The Loop
    if ( $the_query->have_posts() ) {
        ?>
        <div class="inner-related-accessories">
            <div class="container text-center">
                <h2><span class="red">Sets & Accessories</span> for the <?php echo $terms->name ?></h2>
                <p>You can add up to 1 of each accessory into your cart</p>
            </div>
            <div class="gallery-accessories">
            <div class="gal-inner">
                <ul class="products">
                    <?php
                    while ( $the_query->have_posts() ) {
                        $the_query->the_post();
                        get_template_part('woocommerce/content-product');
                    }
                    ?>
                </ul>
            </div>
            </div>
        </div>
        <?php
    } 
    /* Restore original Post Data */
    wp_reset_postdata();

?>