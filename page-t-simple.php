<?php
/*
Template Name: Simple Page
 */
get_header(); ?>

<div class="site-content">
	<div id="content" class="content-area">
		<main id="main" class="site-main" >
			<?php while ( have_posts() ) : the_post(); ?>
			<article>

				<header class="entry-header">
					<div class="container">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</div>
				</header><!-- .entry-header -->

				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
				
			</article>
			<?php dn_post_edit_link(); ?>
			<?php endwhile; // end of the loop. ?>
		</main>
	</div>
</div>


<div class="login-terms">
    <?php
        $terms = get_field('footer_page_link', 'options');
        if( !empty( $terms ) ){
            echo '<ul class="footer-login-link">';
            echo '<li>Copyright &copy; '.date('Y').' Weber</li>';
            foreach( $terms as $page_id){
                echo '<li><a class="special-link" href="'. get_permalink( $page_id ) .'" target="_blank">'. get_the_title( $page_id ) .'</a></li>';
            }
            echo '</ul>';
        }
    ?>
</div>

<?php get_footer('simple');