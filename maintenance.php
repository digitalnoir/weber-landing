<?php
$protocol = $_SERVER["SERVER_PROTOCOL"];
if ( 'HTTP/1.1' != $protocol && 'HTTP/1.0' != $protocol )
    $protocol = 'HTTP/1.0';
header( "$protocol 503 Service Unavailable", true, 503 );
header( 'Content-Type: text/html; charset=utf-8' );
?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
	<title><?php bloginfo() ?> - Maintenance</title>
	<link rel="icon" type="image/png" href="<?php echo THEME_URL ?>/img/favicon.png" />
	<style>
		*{ margin:0; padding:0; box-sizing:border-box;-moz-box-sizing:border-box; -webkit-box-sizing:border-box; -o-box-sizing:border-box; -ms-box-sizing:border-box; }
		body{
            display: flex;
            align-content: center;
            flex-direction: column;
            flex-basis: auto;
            width: 100%;
            height: 100vh;
            justify-content: center;
            text-align: center;
            font-family:sans-serif;
            padding:30px
        }
        .inner{
            position: relative;
            display: block;
            width: auto;
            margin: 0 auto;
        }
        .inner *{
            z-index:5;
            position:relative
        }
        .inner svg{
            position:absolute;
            top:50%;
            left:50%;
            transform:translateX(-50%) translateY(-50%);
            z-index:1;
            width: 90%;
        }
        .site-title{
            font-size:80%;
            margin-top:2em;
            text-transform:uppercase
        }
        h2{
            font-weight:normal
        }
        h1{
            margin-bottom:.2em
        }
        @media (max-width: 767px){
            .inner svg{
                width:70%
            }
            h2{
                font-size:1.25em
            }
        }
	</style>
</head>
<body>
    <div class="inner">
        <h1>We'll be back soon!</h1>
        <h2>We're undergoing maintanance.</h2>
        <div class="site-title">- <?php bloginfo() ?> -</div>
        
            <svg xmlns="http://www.w3.org/2000/svg" width="301.719" height="301.729" viewBox="0 0 301.719 301.729">
            <g id="support_2_" data-name="support (2)" transform="translate(-0.008 0)">
                <g id="Group_153" data-name="Group 153" transform="translate(0.14 0)">
                <g id="Group_152" data-name="Group 152">
                    <path id="Path_217" data-name="Path 217" d="M283.808,196.178c-15.521-15.533-37.057-22.113-64.716-14.7L120.466,82.751l1.3-4.929a62.147,62.147,0,0,0-16.013-59.778A60.931,60.931,0,0,0,46.107,2.227,8.833,8.833,0,0,0,42.206,17L65.953,40.748A17.662,17.662,0,0,1,65.97,65.73a17.874,17.874,0,0,1-25.016.017L17.2,41.993A8.833,8.833,0,0,0,2.43,45.918,61.136,61.136,0,0,0,18.25,105.549c15.514,15.525,37.024,22.129,64.7,14.7l98.634,98.727c-7.361,27.65-1.011,48.978,14.718,64.707A60.941,60.941,0,0,0,255.935,299.5a8.834,8.834,0,0,0,3.9-14.775L236.1,260.98A17.662,17.662,0,0,1,236.079,236a17.9,17.9,0,0,1,25.016-.009l23.754,23.748a8.833,8.833,0,0,0,14.771-3.925A61.155,61.155,0,0,0,283.808,196.178Z" transform="translate(-0.231 0)" fill="#eaebeb"/>
                </g>
                </g>
                <g id="Group_155" data-name="Group 155" transform="translate(0.008 157.102)">
                <g id="Group_154" data-name="Group 154">
                    <path id="Path_218" data-name="Path 218" d="M94.618,266.584,10.351,350.862a35.36,35.36,0,0,0,50.006,50.006l84.265-84.275ZM52.053,373.435a8.838,8.838,0,0,1-12.5-12.5L93.169,307.31a8.838,8.838,0,1,1,12.5,12.5Z" transform="translate(-0.008 -266.584)" fill="#eaebeb"/>
                </g>
                </g>
                <g id="Group_157" data-name="Group 157" transform="translate(168.741 0)">
                <g id="Group_156" data-name="Group 156">
                    <path id="Path_219" data-name="Path 219" d="M416.725,17.828,401.486,2.589a8.84,8.84,0,0,0-10.8-1.331L343.81,29.383a8.839,8.839,0,0,0-1.7,13.831l3.732,3.73-59.509,59.266,25.008,25.008,59.509-59.266,5.256,5.257a8.84,8.84,0,0,0,13.831-1.7l28.124-46.878A8.842,8.842,0,0,0,416.725,17.828Z" transform="translate(-286.329 0)" fill="#eaebeb"/>
                </g>
                </g>
            </g>
            </svg>

    </div>
</body>
</html>
<?php die(); ?>