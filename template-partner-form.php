<?php // Template by Sherica // ?>
<?php get_header('simple'); ?>

<?php
    $business_code = $email_address = $postcode = '';
    if(isset($_SESSION['partner_posted_data']) && is_array( $_SESSION['partner_posted_data'] )){
        $business_code = $_SESSION['partner_posted_data']['business_code'];
        $email_address = $_SESSION['partner_posted_data']['email_address'];
        $postcode = $_SESSION['partner_posted_data']['postcode'];
    }
    $opts = get_option("partnerform_options");
    $opts2 = get_post_meta(get_the_ID(), "partner_departments", true);
    $toclink = get_permalink($opts["toc"]);
    $departments = explode("\r\n", $opts2);
?>

<div class="partner-login-container">
    <div class="container">
        <div class="inner-flex">        
            <form method="POST">
                <div class="logo"><img src="<?php echo THEME_URL ?>/img/header-logo.png" alt="Weber Logo" /></div>

                <?php weber_check_partner_form_error() ?>
                <?php weber_check_business_code_error() ?>

                <div class="form-group">
                    <label for="fullname">
                        <strong>Enter your full name*</strong>
                    </label>
                    <input id="fullname" type="text" name="weber_partner[fullname]" placeholder="John Doe" required value="<?php echo $fullname ?>" />
                </div>
                
                <div class="form-group">
                    <label for="mobile">
                        <strong>Enter your mobile number</strong>
                    </label>
                    <input id="mobile" type="tel" name="weber_partner[mobile]" placeholder="" value="<?php echo $mobile ?>" />
                </div>

                <div class="form-group">
                    <label for="email">
                        <strong>Enter your email address*</strong>
                    </label>
                    <input id="email" type="email" name="weber_partner[email]" placeholder="john@doe.com" required value="<?php echo $email ?>" />
                </div>
                
                <div class="form-group">
                    <label for="address">
                        <strong>Enter your postal address</strong>
                    </label>
                    <input id="address" type="text" name="weber_partner[address]" placeholder="" value="<?php echo $address ?>" />
                </div>
                
                <div class="form-group">
                    <label for="postcode">
                        <strong>Enter your postcode*</strong>
                        <small>This will determine stock and delivery and assembly availability in your area.</small>
                    </label>
                    <input id="postcode"
                    oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                    maxlength = "4"
                    type="tel" name="weber_partner[postcode]" placeholder="Eg. 5000" required value="<?php echo $postcode ?>" />
                </div>
                
                <div class="form-group">
                    <label for="store-name">
                        <strong>Enter your store name</strong>
                    </label>
                    <input id="store-name" type="text" name="weber_partner[storename]" placeholder="" value="<?php echo $storename ?>" />
                </div>
                
                <div class="form-group">
                    <label for="department">
                        <strong>Select your department</strong>
                    </label>
                    <select id="department" name="weber_partner[department]" style="width:100%;padding:0 10px;"><option value="">Select department</option>
                    <?php if(is_array($departments) && !empty($departments)){foreach($departments as $k=>$v){echo '<option>'.$v.'</option>';}}?>
                    </select>
                </div>
                
                <div class="form-group">
                    <input id="toc" type="checkbox" name="weber_partner[toc]" style="position:initial;left:0;" value="true" required /> By clicking this, I accept the <a href="<?php echo $toclink;?>">Terms & Conditions</a>
                </div>
                
                <input type="hidden" name="weber_partner[partner_id]" value="<?php echo get_the_ID( ) ?>" />

                <input type="hidden" name="login_visitor_nonce" value="<?php echo wp_create_nonce( 'login_visitor' ) ?>" />

                <div class="form-group form-footer">
                    <input type="submit" value="Submit" />
                </div>

            </form>
        </div>
    </div>
</div>

<div class="login-terms">
    <?php
        $terms = get_field('footer_page_link', 'options');
        if( !empty( $terms ) ){
            echo '<ul class="footer-login-link">';
            echo '<li>Copyright &copy; '.date('Y').' Weber</li>';
            foreach( $terms as $page_id){
                echo '<li><a class="special-link" href="'. get_permalink( $page_id ) .'" target="_blank">'. get_the_title( $page_id ) .'</a></li>';
            }
            echo '</ul>';
        }
    ?>
</div>

<?php
    // Clear posted session
    $_SESSION['partner_posted_data'] = '';
?>

<?php get_footer('simple'); ?>