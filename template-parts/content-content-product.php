<?php
# Parameter
$the_query_args = array (
	'post_type' => array( 'product', ),
	'posts_per_page'  => get_option('posts_per_page'),  # -1 for all
	'order'   => 'DESC',  # Newest
	'orderby' => 'date',  # 'rand' 'post__in'
);

# Connect Loop to Parameter
$the_query_query = new WP_Query( $the_query_args );
?>

<?php
# Loop
if ( $the_query_query->have_posts() ) : ?>
	<section id="ranges-filter">
		<div class="filter-galleries">
			<div class="container">
				<div class="row">
					 	<?php while ( $the_query_query->have_posts() ) : $the_query_query->the_post(); ?>
					 		<div class="col-xs-12 col-md-4 " style="text-align: center;margin-bottom: 20px;">
					 		    <a href="<?php echo get_the_permalink(); ?>">
					 		    	  <?php  $post_featured_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));; ?>
					 		    	  <?php if($post_featured_image): ?>
					 		    	     <img src="<?php echo $post_featured_image; ?>" title="<?php get_the_title(); ?>" alt="<?php get_the_title(); ?>">
					 		    	  <?php endif; ?>
					 		    	  <div>
					 		    	  	<strong><?php the_title(); ?></strong>
					 		    	  </div>
					 		    </a>
					 		</div>
					 	<?php endwhile; ?>
				    
				</div>
			</div>
		</div>
	
	</section>	
	<?php wp_reset_query(); ?>
<?php else : ?>
   <?php # Template Part | Blog
   get_template_part('template-parts/general/content-no-post'); ?>
<?php endif; ?>