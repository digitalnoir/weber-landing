<!doctype html>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Weber Partners | Coming Soon</title>
        <meta name='robots' content='noindex,nofollow' />
        <link rel='stylesheet' id='style-main-css'  href='<?php echo THEME_URL ?>/assets/dist/css/style.css?ver=1.0<?php echo time()?>' type='text/css' media='all' />
        <style>
        .coming--soon{
            display:flex;
            height:100vh;
            align-items: center;
        }
        .center{
            width:100%;
            text-align:center;
            padding-top:0!important
        }
        .subheading{
            font-size:25px
        }
        .logo{
            margin-bottom:30px;
        }
        .logo img{
            max-width:150px
        }
       
        </style>
    </head>
    <body class="coming--soon">
        <div id="page" class="center">
            <div class="logo"><img src="<?php echo THEME_URL ?>/img/header-logo.png" alt=""/></div>
            <h1 class="heading">Weber Partners</h1>
            <div class="subheading">Coming Soon</div>
           
       </div>
    </body>
    
</html>