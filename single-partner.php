<?php
/**
 * The template for displaying the partner
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Digital_Noir_Starter_Pack
 */

 // Edit Start  //
// Redirect to login if session not found
if ( isset( $_SESSION['weber_partner'] ) && isset( $_SESSION['weber_partner']['end_date'] ) ) :

    // check campaign end
    $campaign_end = strtotime( $_SESSION['weber_partner']['end_date'] );
    $now = strtotime( current_time('Y-m-d H:i:s') );

    if( $now < $campaign_end ){
        include locate_template('template-home.php');
    }else{
        include locate_template('template-partner-form.php');
    }

else:

    include locate_template('template-partner-form.php');

endif;
// Edit End   //