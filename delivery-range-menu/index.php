<?php 
$result = json_decode(json_encode($result),true);
?>
<div class="container-flute" >
<div class="row">
	<div class="col-sm-6">
		<h3>Manage Delivery & Assembly Ranges</h3>
	</div>

</div>
<?php  if(isset($_GET['postcode']) && $_GET['postcode'] !=''){ ?>
	<div class="row">
      <div class="col-sm-6">
        <h2>Edit details</h2>
          <form method="post" action="admin.php?page=delivery-range&postcode=<?php echo $result[0]['postcode'];?>">
            <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label for="postcode">Postcode:</label>
                     <input type="text" class="form-control" id="postcode" placeholder="Enter postcode" name="postcode" value="<?php echo $result[0]['postcode'];?>" readonly="readonly" required>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label for="tech">Teach:</label>
                     <input type="text" class="form-control" id="tech" placeholder="Enter tech" name="tech" value="<?php echo $result[0]['tech'];?>" required>
                  </div>
               </div>
            </div>
             <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label for="area">Area:</label>
                     <input type="text" class="form-control" id="area" placeholder="Enter area" name="area" value="<?php echo $result[0]['area'];?>" required>
                  </div>
               </div>
            </div>
            <button type="submit" class="btn btn-primary" name="update">Submit</button>
          </form>
        </div>
    </div>

<?php }else{ ?>
	<div class="row">
		<div class="col-sm-4">
			<div class="row">
			<div class="col-sm-12">
				<form method="post" action="admin.php?page=delivery-range">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="postcode">Postcode:</label>
								<input type="text" class="form-control" id="postcode" placeholder="Enter postcode" name="postcode" value="" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="tech">Teach:</label>
								<input type="text" class="form-control" id="tech" placeholder="Enter tech" name="tech" value="" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="area">Area:</label>
								<input type="text" class="form-control" id="area" placeholder="Enter area" name="area" value="" required>
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-primary" name="addnew">Submit</button>
				</form>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12">
					<h2>Import CSV File</h2>
					<form method="post" action="admin.php?page=delivery-range-menu3"  enctype="multipart/form-data">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="postcode">Upload csv file:</label>
									<input type="file" class="form-control" id="csv_file" name="csv_file" required>
									<input type="hidden" class="form-control" id="menu1" name="menu1">
								</div>
							</div>
						</div>
						<button   type="submit" class="btn btn-primary" name="csvupdate" >Upload</button>
					</form>
				</div>
			</div>
		</div>
		<div class="col-sm-8">
			<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
					<thead>
						<tr>
							<th>Postcode</th>
							<th>Tech</th>
							<th>Area</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						foreach ($result as $key => $value) {
							if(!empty( $value['postcode'])){
								?>
								<tr>
									<td><?php echo $value['postcode'];?></td>
									<td><?php echo $value['tech'];?></td>
									<td><?php echo $value['area'];?></td>
									<td>
										<a href="admin.php?page=delivery-range&postcode=<?php echo $value['postcode'];?>"><button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button></a>

										<button class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onclick="deleteItem('<?php echo $value['postcode'];?>')"><i class="fa fa-trash"></i></button>
									</td>
								</tr>
								<?php
							}
						} ?>
					</tbody>
				</table>
			</div>
		</div>
	<?php } ?>
</div>
<style>
div#wpwrap {
    overflow: hidden;
}
</style>
    <script type="text/javascript">
    	jQuery(document).ready(function() {
		    jQuery('#example').DataTable({
		    	"pageLength": 50
		    });

		} );
		function deleteItem(postcode) {
		    if (confirm("Are you sure? You want to delete!")) {
		       window.location.href = "admin.php?page=delivery-range-menu2&postcode="+postcode+'&return=menu1';
		    }
		    return false;
		}
    </script>